package com.epam.news.service.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;

import com.epam.news.dto.NewsDTO;
import com.epam.news.exception.CustomOptimisticLockException;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * REST service for work with news resource.
 */
public class NewsServiceRest extends AbstractRestService implements NewsService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO findById(long id) {
		return restTemplate.getForObject(url + "/news/{newsId}", NewsDTO.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO save(NewsDTO newsValueObject) {
		HttpEntity<NewsDTO> httpEntity = new HttpEntity<>(newsValueObject, getAuthorizationHeader());
		ResponseEntity<String> response = restTemplate.exchange(url + "/news", HttpMethod.POST, httpEntity,
				String.class);
		return restTemplate.getForObject(response.getHeaders().getLocation(), NewsDTO.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		HttpEntity<Long> httpEntity = new HttpEntity<>(id, getAuthorizationHeader());
		restTemplate.exchange(url + "/news/{newsId}", HttpMethod.DELETE, httpEntity, Void.class, id);
		// restTemplate.delete(url + "/news/{newsId}", id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteList(List<Long> newsidList) {
		HttpEntity<List<Long>> httpEntity = new HttpEntity<>(newsidList, getAuthorizationHeader());
		restTemplate.exchange(url + "/news/deleteList", HttpMethod.POST, httpEntity, Void.class);
		// restTemplate.postForObject(url + "/news/deleteList", newsidList,
		// Void.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(NewsDTO newsValueObject) {
		HttpEntity<NewsDTO> httpEntity = new HttpEntity<>(newsValueObject, getAuthorizationHeader());
		try {
			restTemplate.exchange(url + "/news/{newsId}", HttpMethod.PUT, httpEntity, Void.class,
					newsValueObject.getNewsId());
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.PRECONDITION_FAILED)) {
				throw new CustomOptimisticLockException();
			}
		}
		// restTemplate.put(url + "/news/{newsId}", newsValueObject,
		// newsValueObject.getNewsId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NewsDTO> findBySearchCriteria(SearchCriteria searchCriteria) {
		NewsDTO[] newsArray = restTemplate.postForObject(url + "/news/search", searchCriteria, NewsDTO[].class);
		List<NewsDTO> newsList = Arrays.asList(newsArray);
		return new ArrayList<>(newsList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NewsDTO> findAll() {
		NewsDTO[] newsArray = restTemplate.getForObject(url + "/news", NewsDTO[].class);
		List<NewsDTO> newsList = Arrays.asList(newsArray);
		return new ArrayList<>(newsList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countNews() {
		return restTemplate.getForObject(url + "news/count", Long.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) {
		return restTemplate.postForObject(url + "/news/count", searchCriteria, Integer.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) {
		Long[] idArray = restTemplate.postForObject(url + "/news/idList", searchCriteria, Long[].class);
		List<Long> idList = Arrays.asList(idArray);
		return new ArrayList<>(idList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO findPrevNews(Long newsId, SearchCriteria searchCriteria) {
		return restTemplate.postForObject(url + "/news/prev/{newsId}", searchCriteria, NewsDTO.class, newsId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO findNextNews(Long newsId, SearchCriteria searchCriteria) {
		return restTemplate.postForObject(url + "/news/next/{newsId}", searchCriteria, NewsDTO.class, newsId);
	}

}
