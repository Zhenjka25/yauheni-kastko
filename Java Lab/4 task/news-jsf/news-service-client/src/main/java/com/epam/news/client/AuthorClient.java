package com.epam.news.client;

import static com.epam.news.constant.ConstantsNewsService.CONTEXT_PATH_AUTHOR;
import static com.epam.news.constant.ConstantsNewsService.URI_AUTHOR;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.epam.news.manager.PropertyManager;
import com.epam.news.transport.WebServiceMessageSenderWithAuth;
import com.epam.news.ws.author.*;

/**
 * Convenient class for processing soap requests to authors resource.
 */
public class AuthorClient extends WebServiceGatewaySupport {

	/**
	 * Instantiates a new author client.
	 */
	public AuthorClient() {
		setDefaultUri(PropertyManager.getInstance().getProperty(URI_AUTHOR));
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(CONTEXT_PATH_AUTHOR);
		setMarshaller(marshaller);
		setUnmarshaller(marshaller);
		setMessageSender(new WebServiceMessageSenderWithAuth());
	}

	/**
	 * Gets the all authors.
	 *
	 * @return the all authors
	 */
	public GetAllAuthorsResponse getAllAuthors() {
		GetAllAuthorsRequest request = new GetAllAuthorsRequest();
		GetAllAuthorsResponse response = (GetAllAuthorsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the actual authors.
	 *
	 * @return the actual authors
	 */
	public GetActualAuthorsResponse getActualAuthors() {
		GetActualAuthorsRequest request = new GetActualAuthorsRequest();
		GetActualAuthorsResponse response = (GetActualAuthorsResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the author by id.
	 *
	 * @param authorId
	 *            the author id
	 * @return the author by id
	 */
	public GetAuthorByIdResponse getAuthorById(long authorId) {
		GetAuthorByIdRequest request = new GetAuthorByIdRequest();
		request.setAuthorId(authorId);
		GetAuthorByIdResponse response = (GetAuthorByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Creates the author.
	 *
	 * @param author
	 *            the author
	 * @return the creates the author response
	 */
	public CreateAuthorResponse createAuthor(Author author) {
		CreateAuthorRequest request = new CreateAuthorRequest();
		request.setAuthor(author);
		CreateAuthorResponse response = (CreateAuthorResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Updates author.
	 *
	 * @param author
	 *            the author
	 * @return the update author response
	 */
	public UpdateAuthorResponse updateAuthor(Author author) {
		UpdateAuthorRequest request = new UpdateAuthorRequest();
		request.setAuthor(author);
		UpdateAuthorResponse response = (UpdateAuthorResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Deletes author.
	 *
	 * @param authorId
	 *            the author id
	 * @return the delete author response
	 */
	public DeleteAuthorResponse deleteAuthor(long authorId) {
		DeleteAuthorRequest request = new DeleteAuthorRequest();
		request.setAuthorId(authorId);
		DeleteAuthorResponse response = (DeleteAuthorResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the author by name.
	 *
	 * @param authorName
	 *            the author name
	 * @return the author by name
	 */
	public GetAuthorByNameResponse getAuthorByName(String authorName) {
		GetAuthorByNameRequest request = new GetAuthorByNameRequest();
		request.setAuthorName(authorName);
		GetAuthorByNameResponse response = (GetAuthorByNameResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

}
