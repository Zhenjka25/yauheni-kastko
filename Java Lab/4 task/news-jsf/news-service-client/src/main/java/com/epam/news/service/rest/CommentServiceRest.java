package com.epam.news.service.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.epam.news.dto.CommentDTO;
import com.epam.news.service.CommentService;

/**
 * REST service for work with comments resource.
 */
public class CommentServiceRest extends AbstractRestService implements CommentService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDTO save(CommentDTO entity) {
		HttpEntity<CommentDTO> httpEntity = new HttpEntity<>(entity);
		ResponseEntity<String> response = restTemplate.exchange(url + "/comment", HttpMethod.POST, httpEntity,
				String.class);
		return restTemplate.getForObject(response.getHeaders().getLocation(), CommentDTO.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDTO findById(long id) {
		return restTemplate.getForObject(url + "/comment/{commentId}", CommentDTO.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(CommentDTO entity) {
		HttpEntity<CommentDTO> httpEntity = new HttpEntity<>(entity, getAuthorizationHeader());
		restTemplate.exchange(url + "/comment/{commentId}", HttpMethod.PUT, httpEntity, Void.class,
				entity.getCommentId());
		// restTemplate.put(url + "/comment/{commentId}", entity,
		// entity.getCommentId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		HttpEntity<Long> httpEntity = new HttpEntity<>(id, getAuthorizationHeader());
		restTemplate.exchange(url + "/comment/{commentId}", HttpMethod.DELETE, httpEntity, Void.class, id);
		// restTemplate.delete(url + "/comment/{commentId}", id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentDTO> findAll() {
		CommentDTO[] commentArray = restTemplate.getForObject(url + "/comment", CommentDTO[].class);
		List<CommentDTO> commentList = Arrays.asList(commentArray);
		return new ArrayList<>(commentList);
	}

}
