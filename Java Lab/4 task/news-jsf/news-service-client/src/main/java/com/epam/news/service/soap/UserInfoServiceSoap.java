package com.epam.news.service.soap;

import com.epam.news.client.UserInfoClient;
import com.epam.news.dto.UserInfoDTO;
import com.epam.news.service.UserInfoServiceClient;
import com.epam.news.ws.userinfo.GetUserInfoResponse;
import com.epam.news.ws.userinfo.UserInfo;

/**
 * SOAP service for work with user info resource.
 */
public class UserInfoServiceSoap extends AbstractServiceSoap implements UserInfoServiceClient {

	private UserInfoClient client = new UserInfoClient();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInfoDTO getUserInfo(String username, String password) {
		GetUserInfoResponse response = client.getUserInfo(username, password);
		UserInfo userInfo = response.getUserInfo();
		return convert(userInfo);
	}

	/**
	 * Converts XML entity to DTO.
	 *
	 * @param userInfo
	 *            the user info
	 * @return the user info dto
	 */
	private UserInfoDTO convert(UserInfo userInfo) {
		if (userInfo == null) {
			return null;
		}
		return mapper.map(userInfo, UserInfoDTO.class);
	}

}
