package com.epam.news.client;

import static com.epam.news.constant.ConstantsNewsService.CONTEXT_PATH_TAG;
import static com.epam.news.constant.ConstantsNewsService.URI_TAG;

import java.util.List;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.epam.news.manager.PropertyManager;
import com.epam.news.transport.WebServiceMessageSenderWithAuth;
import com.epam.news.ws.tag.*;

/**
 * Convenient class for processing soap requests to tags resource.
 */
public class TagClient extends WebServiceGatewaySupport {

	/**
	 * Instantiates a new tag client.
	 */
	public TagClient() {
		setDefaultUri(PropertyManager.getInstance().getProperty(URI_TAG));
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(CONTEXT_PATH_TAG);
		setMarshaller(marshaller);
		setUnmarshaller(marshaller);
		setMessageSender(new WebServiceMessageSenderWithAuth());
	}

	/**
	 * Gets the all tags.
	 *
	 * @return the all tags
	 */
	public GetAllTagsResponse getAllTags() {
		GetAllTagsRequest request = new GetAllTagsRequest();
		GetAllTagsResponse response = (GetAllTagsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the tag by id.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the tag by id
	 */
	public GetTagByIdResponse getTagById(long tagId) {
		GetTagByIdRequest request = new GetTagByIdRequest();
		request.setTagId(tagId);
		GetTagByIdResponse response = (GetTagByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Creates the tag.
	 *
	 * @param tag
	 *            the tag
	 * @return the creates the tag response
	 */
	public CreateTagResponse createTag(Tag tag) {
		CreateTagRequest request = new CreateTagRequest();
		request.setTag(tag);
		CreateTagResponse response = (CreateTagResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Updates tag.
	 *
	 * @param tag
	 *            the tag
	 * @return the update tag response
	 */
	public UpdateTagResponse updateTag(Tag tag) {
		UpdateTagRequest request = new UpdateTagRequest();
		request.setTag(tag);
		UpdateTagResponse response = (UpdateTagResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Deletes tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the delete tag response
	 */
	public DeleteTagResponse deleteTag(long tagId) {
		DeleteTagRequest request = new DeleteTagRequest();
		request.setTagId(tagId);
		DeleteTagResponse response = (DeleteTagResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the tag by name.
	 *
	 * @param tagName
	 *            the tag name
	 * @return the tag by name
	 */
	public GetTagByNameResponse getTagByName(String tagName) {
		GetTagByNameRequest request = new GetTagByNameRequest();
		request.setTagName(tagName);
		GetTagByNameResponse response = (GetTagByNameResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the tags by id list.
	 *
	 * @param tagList
	 *            the tag list
	 * @return the tags by id list
	 */
	public GetTagsByIdListResponse getTagsByIdList(List<Tag> tagList) {
		GetTagsByIdListRequest request = new GetTagsByIdListRequest();
		request.getTags().addAll(tagList);
		GetTagsByIdListResponse response = (GetTagsByIdListResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

}
