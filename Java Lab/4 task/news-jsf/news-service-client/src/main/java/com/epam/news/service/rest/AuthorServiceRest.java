package com.epam.news.service.rest;

import java.util.*;

import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;

import com.epam.news.dto.AuthorDTO;
import com.epam.news.exception.CustomOptimisticLockException;
import com.epam.news.service.AuthorService;

/**
 * REST service for work with authors resource.
 */
public class AuthorServiceRest extends AbstractRestService implements AuthorService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO save(AuthorDTO entity) {
		HttpEntity<AuthorDTO> httpEntity = new HttpEntity<>(entity, getAuthorizationHeader());
		ResponseEntity<String> response = restTemplate.exchange(url + "/author", HttpMethod.POST, httpEntity,
				String.class);
		return restTemplate.getForObject(response.getHeaders().getLocation(), AuthorDTO.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO findById(long id) {
		return restTemplate.getForObject(url + "/author/{authorId}", AuthorDTO.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(AuthorDTO entity) {
		HttpEntity<AuthorDTO> httpEntity = new HttpEntity<>(entity, getAuthorizationHeader());
		try {
			restTemplate.exchange(url + "/author/{authorId}", HttpMethod.PUT, httpEntity, Void.class,
					entity.getAuthorId());
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.PRECONDITION_FAILED)) {
				throw new CustomOptimisticLockException();
			}
		}
		// restTemplate.put(url + "/author/{authorId}", entity,
		// entity.getAuthorId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		HttpEntity<Long> httpEntity = new HttpEntity<>(id, getAuthorizationHeader());
		restTemplate.exchange(url + "/author/{authorId}", HttpMethod.DELETE, httpEntity, Void.class, id);
		// restTemplate.delete(url + "/author/{authorId}", id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AuthorDTO> findAll() {
		AuthorDTO[] authorArray = restTemplate.getForObject(url + "/author", AuthorDTO[].class);
		List<AuthorDTO> authorList = Arrays.asList(authorArray);
		return new ArrayList<>(authorList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO findByName(String name) {
		return restTemplate.getForObject(url + "/author/{authorName}", AuthorDTO.class, name);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AuthorDTO> findActualAuthors() {
		AuthorDTO[] authorArray = restTemplate.getForObject(url + "/author/actual", AuthorDTO[].class);
		List<AuthorDTO> authorList = Arrays.asList(authorArray);
		return new ArrayList<>(authorList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void expireAuthor(AuthorDTO entity) {
		entity.setExpired(new Date());
		HttpEntity<AuthorDTO> httpEntity = new HttpEntity<>(entity, getAuthorizationHeader());
		restTemplate.exchange(url + "/author/{authorId}", HttpMethod.PUT, httpEntity, AuthorDTO.class,
				entity.getAuthorId());
	}

}
