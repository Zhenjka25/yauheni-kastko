package com.epam.news.service.soap;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

/**
 * Basic abstract class for all soap services.
 */
public abstract class AbstractServiceSoap {

	protected DozerBeanMapper mapper;

	/**
	 * Instantiates a new abstract service soap.
	 */
	public AbstractServiceSoap() {
		mapper = new DozerBeanMapper();
		List<String> mappingList = new ArrayList<>();
		mappingList.add("dozerMappingServiceClient.xml");
		mapper.setMappingFiles(mappingList);
	}

}
