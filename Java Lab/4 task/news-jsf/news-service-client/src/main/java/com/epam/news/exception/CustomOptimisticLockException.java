package com.epam.news.exception;

/**
 * The Class CustomOptimisticLockException.
 */
public class CustomOptimisticLockException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new custom optimistic lock exception.
	 */
	public CustomOptimisticLockException() {
	}

	/**
	 * Instantiates a new custom optimistic lock exception.
	 *
	 * @param message
	 *            the message
	 * @param cause
	 *            the cause
	 */
	public CustomOptimisticLockException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new custom optimistic lock exception.
	 *
	 * @param message
	 *            the message
	 */
	public CustomOptimisticLockException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new custom optimistic lock exception.
	 *
	 * @param cause
	 *            the cause
	 */
	public CustomOptimisticLockException(Throwable cause) {
		super(cause);
	}

}
