package com.epam.news.manager;

import java.util.ResourceBundle;

/**
 * Convenient class for work with property files.
 */
public class PropertyManager {

	private static final String PROPERTY_FILE = "config";

	private ResourceBundle resourceBundle;

	private static PropertyManager instance = new PropertyManager();

	/**
	 * Instantiates a new property manager.
	 */
	private PropertyManager() {
		resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE);
	}

	/**
	 * Gets the single instance of PropertyManager.
	 *
	 * @return single instance of PropertyManager
	 */
	public static PropertyManager getInstance() {
		return instance;
	}

	/**
	 * Gets the property.
	 *
	 * @param key
	 *            the key
	 * @return the property
	 */
	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}

}
