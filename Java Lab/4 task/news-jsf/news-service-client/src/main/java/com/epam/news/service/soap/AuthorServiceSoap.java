package com.epam.news.service.soap;

import static com.epam.news.constant.ConstantsNewsService.OPT_LOCK_EXCEPTION;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.ws.soap.client.SoapFaultClientException;

import com.epam.news.client.AuthorClient;
import com.epam.news.dto.AuthorDTO;
import com.epam.news.exception.CustomOptimisticLockException;
import com.epam.news.service.AuthorService;
import com.epam.news.ws.author.Author;
import com.epam.news.ws.author.CreateAuthorResponse;

/**
 * SOAP service for work with authors resource.
 */
public class AuthorServiceSoap extends AbstractServiceSoap implements AuthorService {

	private AuthorClient client = new AuthorClient();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO save(AuthorDTO entity) {
		CreateAuthorResponse response = client.createAuthor(convert(entity));
		Author author = client.getAuthorById(response.getAuthorId()).getAuthor();
		return convert(author);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO findById(long id) {
		Author author = client.getAuthorById(id).getAuthor();
		return convert(author);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(AuthorDTO entity) {
		Author author = convert(entity);
		try {
			client.updateAuthor(author);
		} catch (SoapFaultClientException e) {
			if (OPT_LOCK_EXCEPTION.equals(e.getMessage())) {
				throw new CustomOptimisticLockException();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		client.deleteAuthor(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AuthorDTO> findAll() {
		List<Author> authorList = client.getAllAuthors().getAuthors();
		List<AuthorDTO> authorDTOList = new ArrayList<>();
		for (Author author : authorList) {
			authorDTOList.add(convert(author));
		}
		return authorDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO findByName(String name) {
		Author author = client.getAuthorByName(name).getAuthor();
		return convert(author);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AuthorDTO> findActualAuthors() {
		List<Author> authorList = client.getActualAuthors().getAuthors();
		List<AuthorDTO> authorDTOList = new ArrayList<>();
		for (Author author : authorList) {
			authorDTOList.add(convert(author));
		}
		return authorDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void expireAuthor(AuthorDTO entity) {
		entity.setExpired(new Date());
		Author author = convert(entity);
		client.updateAuthor(author);
	}

	/**
	 * Converts DTO to XML entity.
	 *
	 * @param authorDTO
	 *            the author dto
	 * @return the author
	 */
	private Author convert(AuthorDTO authorDTO) {
		if (authorDTO == null) {
			return null;
		}
		return mapper.map(authorDTO, Author.class);
	}

	/**
	 * Converts XML entity to DTO.
	 *
	 * @param author
	 *            the author
	 * @return the author dto
	 */
	private AuthorDTO convert(Author author) {
		if (author == null) {
			return null;
		}
		return mapper.map(author, AuthorDTO.class);
	}

}
