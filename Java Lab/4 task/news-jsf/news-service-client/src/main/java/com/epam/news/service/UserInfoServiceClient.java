package com.epam.news.service;

import com.epam.news.dto.UserInfoDTO;

/**
 * Interface for work with user info resource.
 */
public interface UserInfoServiceClient {

	/**
	 * Gets the user info.
	 *
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @return the user info
	 */
	UserInfoDTO getUserInfo(String username, String password);

}
