package com.epam.news.constant;

/**
 * Contains all constants for client service module.
 */
public class ConstantsNewsService {

	// REST URL
	public static final String REST_SERVICE_URL = "rest.service.url";

	// SOAP URI
	public static final String URI_AUTHOR = "soap.uri.author";
	public static final String URI_TAG = "soap.uri.tag";
	public static final String URI_COMMENT = "soap.uri.comment";
	public static final String URI_NEWS = "soap.uri.news";
	public static final String URI_USER = "soap.uri.user";

	// Context path for SOAP requests
	public static final String CONTEXT_PATH_AUTHOR = "com.epam.news.ws.author";
	public static final String CONTEXT_PATH_TAG = "com.epam.news.ws.tag";
	public static final String CONTEXT_PATH_COMMENT = "com.epam.news.ws.comment";
	public static final String CONTEXT_PATH_NEWS = "com.epam.news.ws.news";
	public static final String CONTEXT_PATH_USER = "com.epam.news.ws.userinfo";

	// Exception messages
	public static final String OPT_LOCK_EXCEPTION = "javax.persistence.OptimisticLockException";

}
