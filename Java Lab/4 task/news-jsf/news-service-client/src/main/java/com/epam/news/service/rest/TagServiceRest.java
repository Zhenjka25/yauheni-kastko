package com.epam.news.service.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;

import com.epam.news.dto.TagDTO;
import com.epam.news.exception.CustomOptimisticLockException;
import com.epam.news.service.TagService;

/**
 * REST service for work with tags resource.
 */
public class TagServiceRest extends AbstractRestService implements TagService {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO save(TagDTO entity) {
		HttpEntity<TagDTO> httpEntity = new HttpEntity<>(entity, getAuthorizationHeader());
		ResponseEntity<String> response = restTemplate.exchange(url + "/tag", HttpMethod.POST, httpEntity,
				String.class);
		return restTemplate.getForObject(response.getHeaders().getLocation(), TagDTO.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO findById(long id) {
		return restTemplate.getForObject(url + "/tag/{tagId}", TagDTO.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(TagDTO entity) {
		HttpEntity<TagDTO> httpEntity = new HttpEntity<>(entity, getAuthorizationHeader());
		try {
			restTemplate.exchange(url + "/tag/{tagId}", HttpMethod.PUT, httpEntity, Void.class, entity.getTagId());
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode().equals(HttpStatus.PRECONDITION_FAILED)) {
				throw new CustomOptimisticLockException();
			}
		}
		// restTemplate.put(url + "/tag/{tagId}", entity, entity.getTagId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		HttpEntity<Long> httpEntity = new HttpEntity<>(id, getAuthorizationHeader());
		restTemplate.exchange(url + "/tag/{tagId}", HttpMethod.DELETE, httpEntity, Void.class, id);
		// restTemplate.delete(url + "/tag/{tagId}", id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TagDTO> findAll() {
		TagDTO[] tagArray = restTemplate.getForObject(url + "/tag", TagDTO[].class);
		List<TagDTO> tagList = Arrays.asList(tagArray);
		return new ArrayList<>(tagList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO findByName(String name) {
		return restTemplate.getForObject(url + "/tag/{tagName}", TagDTO.class, name);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TagDTO> findByTagIdList(List<TagDTO> tagList) {
		TagDTO[] tagArray = restTemplate.postForObject(url + "/tag/tagIdList", tagList, TagDTO[].class);
		List<TagDTO> tags = Arrays.asList(tagArray);
		return new ArrayList<>(tags);
	}

}
