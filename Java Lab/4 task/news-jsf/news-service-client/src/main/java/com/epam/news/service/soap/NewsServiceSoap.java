package com.epam.news.service.soap;

import static com.epam.news.constant.ConstantsNewsService.OPT_LOCK_EXCEPTION;

import java.util.ArrayList;
import java.util.List;

import org.springframework.ws.soap.client.SoapFaultClientException;

import com.epam.news.client.NewsClient;
import com.epam.news.dto.NewsDTO;
import com.epam.news.exception.CustomOptimisticLockException;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;
import com.epam.news.ws.news.CreateNewsResponse;
import com.epam.news.ws.news.News;
import com.epam.news.ws.news.SearchCriteriaXML;

/**
 * SOAP service for work with news resource.
 */
public class NewsServiceSoap extends AbstractServiceSoap implements NewsService {

	private NewsClient client = new NewsClient();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO save(NewsDTO entity) {
		CreateNewsResponse response = client.createNews(convert(entity));
		News news = client.getNewsById(response.getNewsId()).getNews();
		return convert(news);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO findById(long id) {
		News news = client.getNewsById(id).getNews();
		return convert(news);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(NewsDTO entity) {
		News news = convert(entity);
		try {
			client.updateNews(news);
		} catch (SoapFaultClientException e) {
			if (OPT_LOCK_EXCEPTION.equals(e.getMessage())) {
				throw new CustomOptimisticLockException();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		client.deleteNews(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NewsDTO> findAll() {
		List<News> newsList = client.getAllNews().getNewsList();
		List<NewsDTO> newsDTOList = new ArrayList<>();
		for (News news : newsList) {
			newsDTOList.add(convert(news));
		}
		return newsDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<NewsDTO> findBySearchCriteria(SearchCriteria searchObject) {
		SearchCriteriaXML searchCriteriaXML = convert(searchObject);
		List<News> newsList = client.getNewsListBySearchCriteria(searchCriteriaXML).getNewsList();
		List<NewsDTO> newsDTOList = new ArrayList<>();
		for (News news : newsList) {
			newsDTOList.add(convert(news));
		}
		return newsDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countNews() {
		return client.countNews().getNewsAmount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) {
		SearchCriteriaXML searchCriteriaXML = convert(searchCriteria);
		return (int) client.countNewsBySearchCriteria(searchCriteriaXML).getNewsAmount();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) {
		SearchCriteriaXML searchCriteriaXML = convert(searchCriteria);
		return client.findIdListBySearchCriteria(searchCriteriaXML).getNewsIdList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO findPrevNews(Long newsId, SearchCriteria searchCriteria) {
		SearchCriteriaXML searchCriteriaXML = convert(searchCriteria);
		News news = client.getPrevNews(newsId, searchCriteriaXML).getNews();
		return convert(news);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO findNextNews(Long newsId, SearchCriteria searchCriteria) {
		SearchCriteriaXML searchCriteriaXML = convert(searchCriteria);
		News news = client.getNextNews(newsId, searchCriteriaXML).getNews();
		return convert(news);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteList(List<Long> newsIdList) {
		client.deleteNewsList(newsIdList);
	}

	/**
	 * Converts DTO to XML entity.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return the news
	 */
	private News convert(NewsDTO newsDTO) {
		if (newsDTO == null) {
			return null;
		}
		return mapper.map(newsDTO, News.class);
	}

	/**
	 * Converts XML entity to DTO.
	 *
	 * @param news
	 *            the news
	 * @return the news dto
	 */
	private NewsDTO convert(News news) {
		if (news == null) {
			return null;
		}
		return mapper.map(news, NewsDTO.class);
	}

	/**
	 * Converts entity to XML entity.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the search criteria xml
	 */
	private SearchCriteriaXML convert(SearchCriteria searchCriteria) {
		if (searchCriteria == null) {
			return null;
		}
		return mapper.map(searchCriteria, SearchCriteriaXML.class);
	}

}
