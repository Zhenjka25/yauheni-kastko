package com.epam.news.transport;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.util.Base64Utils;
import org.springframework.ws.transport.http.HttpUrlConnectionMessageSender;

/**
 * Message sender for SOAP requests. Used for adding authorization header to
 * requests.
 */
public class WebServiceMessageSenderWithAuth extends HttpUrlConnectionMessageSender {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void prepareConnection(HttpURLConnection connection) throws IOException {

		Subject currentUser = SecurityUtils.getSubject();
		String principal = (String) currentUser.getPrincipal();

		if (principal != null) {
			String encodedAuthorization = Base64Utils.encodeToString(principal.getBytes());
			connection.setRequestProperty("Authorization", "Basic " + encodedAuthorization);
		}

		super.prepareConnection(connection);
	}

}
