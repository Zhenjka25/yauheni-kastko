package com.epam.news.service.rest;

import org.springframework.http.HttpEntity;

import com.epam.news.dto.UserInfoDTO;
import com.epam.news.service.UserInfoServiceClient;

/**
 * REST service for work with user info resource.
 */
public class UserInfoServiceRest extends AbstractRestService implements UserInfoServiceClient {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInfoDTO getUserInfo(String username, String password) {
		UserInfoDTO user = new UserInfoDTO();
		user.setLogin(username);
		user.setPassword(password);
		HttpEntity<UserInfoDTO> httpEntity = new HttpEntity<>(user);
		return restTemplate.postForObject(url + "/user", httpEntity, UserInfoDTO.class);
	}

}
