package com.epam.news.client;

import static com.epam.news.constant.ConstantsNewsService.CONTEXT_PATH_USER;
import static com.epam.news.constant.ConstantsNewsService.URI_USER;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.epam.news.manager.PropertyManager;
import com.epam.news.transport.WebServiceMessageSenderWithAuth;
import com.epam.news.ws.userinfo.GetUserInfoRequest;
import com.epam.news.ws.userinfo.GetUserInfoResponse;
import com.epam.news.ws.userinfo.UserInfo;

/**
 * Convenient class for processing soap requests to user info resource.
 */
public class UserInfoClient extends WebServiceGatewaySupport {

	/**
	 * Instantiates a new user info client.
	 */
	public UserInfoClient() {
		setDefaultUri(PropertyManager.getInstance().getProperty(URI_USER));
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(CONTEXT_PATH_USER);
		setMarshaller(marshaller);
		setUnmarshaller(marshaller);
		setMessageSender(new WebServiceMessageSenderWithAuth());
	}

	/**
	 * Gets the user info.
	 *
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 * @return the user info
	 */
	public GetUserInfoResponse getUserInfo(String login, String password) {
		GetUserInfoRequest request = new GetUserInfoRequest();

		UserInfo userInfo = new UserInfo();
		userInfo.setLogin(login);
		userInfo.setPassword(password);

		request.setUserInfo(userInfo);

		GetUserInfoResponse response = (GetUserInfoResponse) getWebServiceTemplate().marshalSendAndReceive(request);

		return response;
	}

}
