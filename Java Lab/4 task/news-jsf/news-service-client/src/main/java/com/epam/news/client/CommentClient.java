package com.epam.news.client;

import static com.epam.news.constant.ConstantsNewsService.CONTEXT_PATH_COMMENT;
import static com.epam.news.constant.ConstantsNewsService.URI_COMMENT;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.epam.news.manager.PropertyManager;
import com.epam.news.transport.WebServiceMessageSenderWithAuth;
import com.epam.news.ws.comment.*;

/**
 * Convenient class for processing soap requests to comments resource.
 */
public class CommentClient extends WebServiceGatewaySupport {

	/**
	 * Instantiates a new comment client.
	 */
	public CommentClient() {
		setDefaultUri(PropertyManager.getInstance().getProperty(URI_COMMENT));
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(CONTEXT_PATH_COMMENT);
		setMarshaller(marshaller);
		setUnmarshaller(marshaller);
		setMessageSender(new WebServiceMessageSenderWithAuth());
	}

	/**
	 * Gets the all comments.
	 *
	 * @return the all comments
	 */
	public GetAllCommentsResponse getAllComments() {
		GetAllCommentsRequest request = new GetAllCommentsRequest();
		GetAllCommentsResponse response = (GetAllCommentsResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the comment by id.
	 *
	 * @param commentId
	 *            the comment id
	 * @return the comment by id
	 */
	public GetCommentByIdResponse getCommentById(long commentId) {
		GetCommentByIdRequest request = new GetCommentByIdRequest();
		request.setCommentId(commentId);
		GetCommentByIdResponse response = (GetCommentByIdResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Creates the comment.
	 *
	 * @param comment
	 *            the comment
	 * @return the creates the comment response
	 */
	public CreateCommentResponse createComment(Comment comment) {
		CreateCommentRequest request = new CreateCommentRequest();
		request.setComment(comment);
		CreateCommentResponse response = (CreateCommentResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Updates comment.
	 *
	 * @param comment
	 *            the comment
	 * @return the update comment response
	 */
	public UpdateCommentResponse updateComment(Comment comment) {
		UpdateCommentRequest request = new UpdateCommentRequest();
		request.setComment(comment);
		UpdateCommentResponse response = (UpdateCommentResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Deletes comment.
	 *
	 * @param commentId
	 *            the comment id
	 * @return the delete comment response
	 */
	public DeleteCommentResponse deleteComment(long commentId) {
		DeleteCommentRequest request = new DeleteCommentRequest();
		request.setCommentId(commentId);
		DeleteCommentResponse response = (DeleteCommentResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

}
