package com.epam.news.service.rest;

import static com.epam.news.constant.ConstantsNewsService.REST_SERVICE_URL;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.http.HttpHeaders;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import com.epam.news.manager.PropertyManager;

/**
 * Basic abstract class for all rest services.
 */
public abstract class AbstractRestService {

	protected RestTemplate restTemplate;

	protected String url;

	/**
	 * Instantiates a new abstract rest service.
	 */
	public AbstractRestService() {
		restTemplate = new RestTemplate();
		PropertyManager propertyManager = PropertyManager.getInstance();
		url = propertyManager.getProperty(REST_SERVICE_URL);
	}

	/**
	 * Gets the authorization header. This header is used for basic
	 * authorization.
	 *
	 * @return the authorization header
	 */
	public HttpHeaders getAuthorizationHeader() {

		Subject currentUser = SecurityUtils.getSubject();
		String principal = (String) currentUser.getPrincipal();

		HttpHeaders headers = new HttpHeaders();

		if (principal != null) {
			String encodedAuthorization = Base64Utils.encodeToString(principal.getBytes());
			headers.add("Authorization", "Basic " + encodedAuthorization);
		}

		return headers;

	}

}
