package com.epam.news.service.soap;

import static com.epam.news.constant.ConstantsNewsService.OPT_LOCK_EXCEPTION;

import java.util.ArrayList;
import java.util.List;

import org.springframework.ws.soap.client.SoapFaultClientException;

import com.epam.news.client.TagClient;
import com.epam.news.dto.TagDTO;
import com.epam.news.exception.CustomOptimisticLockException;
import com.epam.news.service.TagService;
import com.epam.news.ws.tag.CreateTagResponse;
import com.epam.news.ws.tag.Tag;

/**
 * SOAP service for work with tags resource.
 */
public class TagServiceSoap extends AbstractServiceSoap implements TagService {

	private TagClient client = new TagClient();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO save(TagDTO entity) {
		CreateTagResponse response = client.createTag(convert(entity));
		Tag tag = client.getTagById(response.getTagId()).getTag();
		return convert(tag);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO findById(long id) {
		Tag tag = client.getTagById(id).getTag();
		return convert(tag);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(TagDTO entity) {
		Tag tag = convert(entity);
		try {
			client.updateTag(tag);
		} catch (SoapFaultClientException e) {
			if (OPT_LOCK_EXCEPTION.equals(e.getMessage())) {
				throw new CustomOptimisticLockException();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		client.deleteTag(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TagDTO> findAll() {
		List<Tag> tagList = client.getAllTags().getTags();
		List<TagDTO> tagDTOList = new ArrayList<>();
		for (Tag tag : tagList) {
			tagDTOList.add(convert(tag));
		}
		return tagDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO findByName(String name) {
		Tag tag = client.getTagByName(name).getTag();
		return convert(tag);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TagDTO> findByTagIdList(List<TagDTO> tagList) {
		List<Tag> tags = new ArrayList<>();

		for (TagDTO tagDTO : tagList) {
			tags.add(convert(tagDTO));
		}

		tags = client.getTagsByIdList(tags).getTags();
		tagList.clear();

		for (Tag tag : tags) {
			tagList.add(convert(tag));
		}

		return tagList;
	}

	/**
	 * Converts DTO to XML entity.
	 *
	 * @param tagDTO
	 *            the tag dto
	 * @return the tag
	 */
	private Tag convert(TagDTO tagDTO) {
		if (tagDTO == null) {
			return null;
		}
		return mapper.map(tagDTO, Tag.class);
	}

	/**
	 * Converts XML entity to DTO.
	 *
	 * @param tag
	 *            the tag
	 * @return the tag dto
	 */
	private TagDTO convert(Tag tag) {
		if (tag == null) {
			return null;
		}
		return mapper.map(tag, TagDTO.class);
	}

}
