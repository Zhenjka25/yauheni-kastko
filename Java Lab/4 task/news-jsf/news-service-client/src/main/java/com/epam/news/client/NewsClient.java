package com.epam.news.client;

import static com.epam.news.constant.ConstantsNewsService.CONTEXT_PATH_NEWS;
import static com.epam.news.constant.ConstantsNewsService.URI_NEWS;

import java.util.List;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.epam.news.manager.PropertyManager;
import com.epam.news.transport.WebServiceMessageSenderWithAuth;
import com.epam.news.ws.news.*;

/**
 * Convenient class for processing soap requests to news resource.
 */
public class NewsClient extends WebServiceGatewaySupport {

	/**
	 * Instantiates a new news client.
	 */
	public NewsClient() {
		setDefaultUri(PropertyManager.getInstance().getProperty(URI_NEWS));
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(CONTEXT_PATH_NEWS);
		setMarshaller(marshaller);
		setUnmarshaller(marshaller);
		setMessageSender(new WebServiceMessageSenderWithAuth());
	}

	/**
	 * Gets the all news.
	 *
	 * @return the all news
	 */
	public GetAllNewsResponse getAllNews() {
		GetAllNewsRequest request = new GetAllNewsRequest();
		GetAllNewsResponse response = (GetAllNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Counts news.
	 *
	 * @return the count news response
	 */
	public CountNewsResponse countNews() {
		CountNewsRequest request = new CountNewsRequest();
		CountNewsResponse response = (CountNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Counts news by search criteria.
	 *
	 * @param searchCriteriaXML
	 *            the search criteria xml
	 * @return the count news by search criteria response
	 */
	public CountNewsBySearchCriteriaResponse countNewsBySearchCriteria(SearchCriteriaXML searchCriteriaXML) {
		CountNewsBySearchCriteriaRequest request = new CountNewsBySearchCriteriaRequest();
		request.setSearchCriteria(searchCriteriaXML);
		CountNewsBySearchCriteriaResponse response = (CountNewsBySearchCriteriaResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the news by id.
	 *
	 * @param newsId
	 *            the news id
	 * @return the news by id
	 */
	public GetNewsByIdResponse getNewsById(long newsId) {
		GetNewsByIdRequest request = new GetNewsByIdRequest();
		request.setNewsId(newsId);
		GetNewsByIdResponse response = (GetNewsByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Deletes news.
	 *
	 * @param newsId
	 *            the news id
	 * @return the delete news response
	 */
	public DeleteNewsResponse deleteNews(long newsId) {
		DeleteNewsRequest request = new DeleteNewsRequest();
		request.setNewsId(newsId);
		DeleteNewsResponse response = (DeleteNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the next news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteriaXML
	 *            the search criteria xml
	 * @return the next news
	 */
	public GetNextNewsResponse getNextNews(long newsId, SearchCriteriaXML searchCriteriaXML) {
		GetNextNewsRequest request = new GetNextNewsRequest();
		request.setNewsId(newsId);
		request.setSearchCriteria(searchCriteriaXML);
		GetNextNewsResponse response = (GetNextNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the prev news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteriaXML
	 *            the search criteria xml
	 * @return the prev news
	 */
	public GetPrevNewsResponse getPrevNews(long newsId, SearchCriteriaXML searchCriteriaXML) {
		GetPrevNewsRequest request = new GetPrevNewsRequest();
		request.setNewsId(newsId);
		request.setSearchCriteria(searchCriteriaXML);
		GetPrevNewsResponse response = (GetPrevNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Creates the news.
	 *
	 * @param news
	 *            the news
	 * @return the creates the news response
	 */
	public CreateNewsResponse createNews(News news) {
		CreateNewsRequest request = new CreateNewsRequest();
		request.setNews(news);
		CreateNewsResponse response = (CreateNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Updates news.
	 *
	 * @param news
	 *            the news
	 * @return the update news response
	 */
	public UpdateNewsResponse updateNews(News news) {
		UpdateNewsRequest request = new UpdateNewsRequest();
		request.setNews(news);
		UpdateNewsResponse response = (UpdateNewsResponse) getWebServiceTemplate().marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Deletes news list.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @return the delete news list response
	 */
	public DeleteNewsListResponse deleteNewsList(List<Long> newsIdList) {
		DeleteNewsListRequest request = new DeleteNewsListRequest();
		request.getNewsIdList().addAll(newsIdList);
		DeleteNewsListResponse response = (DeleteNewsListResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Gets the news list by search criteria.
	 *
	 * @param searchCriteriaXML
	 *            the search criteria xml
	 * @return the news list by search criteria
	 */
	public GetNewsListBySearchCriteriaResponse getNewsListBySearchCriteria(SearchCriteriaXML searchCriteriaXML) {
		GetNewsListBySearchCriteriaRequest request = new GetNewsListBySearchCriteriaRequest();
		request.setSearchCriteria(searchCriteriaXML);
		GetNewsListBySearchCriteriaResponse response = (GetNewsListBySearchCriteriaResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

	/**
	 * Finds id list by search criteria.
	 *
	 * @param searchCriteriaXML
	 *            the search criteria xml
	 * @return the find id list by search criteria response
	 */
	public FindIdListBySearchCriteriaResponse findIdListBySearchCriteria(SearchCriteriaXML searchCriteriaXML) {
		FindIdListBySearchCriteriaRequest request = new FindIdListBySearchCriteriaRequest();
		request.setSearchCriteria(searchCriteriaXML);
		FindIdListBySearchCriteriaResponse response = (FindIdListBySearchCriteriaResponse) getWebServiceTemplate()
				.marshalSendAndReceive(request);
		return response;
	}

}
