package com.epam.news.service.soap;

import java.util.ArrayList;
import java.util.List;

import com.epam.news.client.CommentClient;
import com.epam.news.dto.CommentDTO;
import com.epam.news.service.CommentService;
import com.epam.news.ws.comment.Comment;
import com.epam.news.ws.comment.CreateCommentResponse;

/**
 * SOAP service for work with comments resource.
 */
public class CommentServiceSoap extends AbstractServiceSoap implements CommentService {

	private CommentClient client = new CommentClient();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDTO save(CommentDTO entity) {
		CreateCommentResponse response = client.createComment(convert(entity));
		Comment comment = client.getCommentById(response.getCommentId()).getComment();
		return conevrt(comment);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDTO findById(long id) {
		Comment comment = client.getCommentById(id).getComment();
		return conevrt(comment);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(CommentDTO entity) {
		Comment comment = convert(entity);
		client.updateComment(comment);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		client.deleteComment(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommentDTO> findAll() {
		List<Comment> commentList = client.getAllComments().getComments();
		List<CommentDTO> commentDTOList = new ArrayList<>();
		for (Comment comment : commentList) {
			commentDTOList.add(conevrt(comment));
		}
		return commentDTOList;
	}

	/**
	 * Converts DTO to XML entity.
	 *
	 * @param commentDTO
	 *            the comment dto
	 * @return the comment
	 */
	private Comment convert(CommentDTO commentDTO) {
		if (commentDTO == null) {
			return null;
		}
		return mapper.map(commentDTO, Comment.class);
	}

	/**
	 * Converts XML entity to DTO.
	 *
	 * @param comment
	 *            the comment
	 * @return the comment dto
	 */
	private CommentDTO conevrt(Comment comment) {
		if (comment == null) {
			return null;
		}
		return mapper.map(comment, CommentDTO.class);
	}

}
