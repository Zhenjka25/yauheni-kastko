package com.epam.news.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Contains all beans needed for rest service.
 */
@EnableWebMvc
@Configuration
@Import({ ApplicationConfigJPA.class })
@ComponentScan("com.epam.news.controller")
public class RestAppConfig extends WebMvcConfigurerAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

}
