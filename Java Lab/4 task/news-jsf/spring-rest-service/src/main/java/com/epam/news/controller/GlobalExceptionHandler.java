package com.epam.news.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.news.exception.DTOIntegrityException;

/**
 * Exception handler for all rest controllers.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	/**
	 * Handles method argument not valid exception.
	 *
	 * @param error
	 *            the error
	 * @return the response entity
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<BindingResult> handleMethodArgumentNotValidException(MethodArgumentNotValidException error) {
		log.error(error);
		return new ResponseEntity<BindingResult>(error.getBindingResult(), HttpStatus.CONFLICT);
	}

	/**
	 * Handles dto integrity exception.
	 *
	 * @param e
	 *            the e
	 * @return the response entity
	 */
	@ExceptionHandler(DTOIntegrityException.class)
	public ResponseEntity<BindingResult> handleDTOIntegrityException(DTOIntegrityException e) {
		log.error(e);
		return new ResponseEntity<BindingResult>(e.getResults(), HttpStatus.CONFLICT);
	}

}
