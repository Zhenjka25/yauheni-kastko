package com.epam.news.controller;

import static com.epam.news.constant.ConstantsRest.ROLE_ADMIN;

import java.util.List;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import com.epam.news.dto.TagDTO;
import com.epam.news.service.TagService;

/**
 * Controller for tags resource.
 */
@RestController
public class TagRestController {

	@Autowired
	private TagService service;

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	@RequestMapping(value = "/tag", method = RequestMethod.GET)
	public ResponseEntity<List<TagDTO>> getTags() {
		return new ResponseEntity<List<TagDTO>>(service.findAll(), HttpStatus.OK);
	}

	/**
	 * Gets the tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the tag
	 */
	@RequestMapping(value = "/tag/{tagId}", method = RequestMethod.GET)
	public ResponseEntity<TagDTO> getTag(@PathVariable(value = "tagId") Long tagId) {
		TagDTO tag = service.findById(tagId);
		if (tag == null) {
			return new ResponseEntity<TagDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<TagDTO>(tag, HttpStatus.OK);
	}

	/**
	 * Creates the tag.
	 *
	 * @param tag
	 *            the tag
	 * @param ucBuilder
	 *            the uc builder
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/tag", method = RequestMethod.POST)
	public ResponseEntity<Void> createTag(@Valid @RequestBody TagDTO tag, UriComponentsBuilder ucBuilder) {
		tag = service.save(tag);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/tag/{tagId}").buildAndExpand(tag.getTagId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/**
	 * Updates tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @param tag
	 *            the tag
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/tag/{tagId}", method = RequestMethod.PUT)
	public ResponseEntity<TagDTO> updateTag(@PathVariable(value = "tagId") Long tagId, @Valid @RequestBody TagDTO tag) {
		try {
			service.update(tag);
		} catch (OptimisticLockException e) {
			return new ResponseEntity<TagDTO>(HttpStatus.PRECONDITION_FAILED);
		}
		return new ResponseEntity<TagDTO>(tag, HttpStatus.OK);
	}

	/**
	 * Deletes tag.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/tag/{tagId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteTag(@PathVariable(value = "tagId") Long tagId) {
		service.deleteById(tagId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * Gets the tags by id list.
	 *
	 * @param tagList
	 *            the tag list
	 * @return the tags by id list
	 */
	@RequestMapping(value = "/tag/tagIdList", method = RequestMethod.POST)
	public ResponseEntity<List<TagDTO>> getTagsByIdList(@RequestBody List<TagDTO> tagList) {
		return new ResponseEntity<List<TagDTO>>(service.findByTagIdList(tagList), HttpStatus.OK);
	}

}
