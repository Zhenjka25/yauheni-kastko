package com.epam.news.controller;

import static com.epam.news.constant.ConstantsRest.ROLE_ADMIN;

import java.util.List;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import com.epam.news.dto.AuthorDTO;
import com.epam.news.service.AuthorService;

/**
 * Controller for authors resource.
 */
@RestController
public class AuthorRestController {

	@Autowired
	private AuthorService service;

	/**
	 * Gets the all authors.
	 *
	 * @return the all authors
	 */
	@RequestMapping(value = "/author", method = RequestMethod.GET)
	public ResponseEntity<List<AuthorDTO>> getAllAuthors() {
		return new ResponseEntity<List<AuthorDTO>>(service.findAll(), HttpStatus.OK);
	}

	/**
	 * Gets the actual authors.
	 *
	 * @return the actual authors
	 */
	@RequestMapping(value = "/author/actual", method = RequestMethod.GET)
	public ResponseEntity<List<AuthorDTO>> getActualAuthors() {
		return new ResponseEntity<List<AuthorDTO>>(service.findActualAuthors(), HttpStatus.OK);
	}

	/**
	 * Gets the author.
	 *
	 * @param authorId
	 *            the author id
	 * @return the author
	 */
	@RequestMapping(value = "/author/{authorId}", method = RequestMethod.GET)
	public ResponseEntity<AuthorDTO> getAuthor(@PathVariable(value = "authorId") Long authorId) {
		AuthorDTO author = service.findById(authorId);
		if (author == null) {
			return new ResponseEntity<AuthorDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<AuthorDTO>(author, HttpStatus.OK);
	}

	/**
	 * Creates the author.
	 *
	 * @param author
	 *            the author
	 * @param ucBuilder
	 *            the uc builder
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/author", method = RequestMethod.POST)
	public ResponseEntity<Void> createAuthor(@Valid @RequestBody AuthorDTO author, UriComponentsBuilder ucBuilder) {
		author = service.save(author);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/author/{authorId}").buildAndExpand(author.getAuthorId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/**
	 * Updates author.
	 *
	 * @param authorId
	 *            the author id
	 * @param author
	 *            the author
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/author/{authorId}", method = RequestMethod.PUT)
	public ResponseEntity<AuthorDTO> updateAuthor(@PathVariable(value = "authorId") Long authorId,
			@Valid @RequestBody AuthorDTO author) {
		try {
			service.update(author);
		} catch (OptimisticLockException e) {
			return new ResponseEntity<AuthorDTO>(HttpStatus.PRECONDITION_FAILED);
		}
		return new ResponseEntity<AuthorDTO>(author, HttpStatus.OK);
	}

	/**
	 * Deletes author.
	 *
	 * @param authorId
	 *            the author id
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/author/{authorId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteAuthor(@PathVariable(value = "authorId") Long authorId) {
		service.deleteById(authorId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
