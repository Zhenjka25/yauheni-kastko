package com.epam.news.service;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.Service;

import com.epam.news.dto.UserInfoDTO;

/**
 * Service is used for Spring Security authorization.
 */
@Service
public class UserService implements UserDetailsService {

	private static Logger log = Logger.getLogger(UserService.class);

	@Autowired
	private UserInfoService userInfoService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserInfoDTO userInfo = null;

		try {
			userInfo = userInfoService.getUserInfo(username);
		} catch (Exception e) {
			log.error(e);
		}

		GrantedAuthority authority = new SimpleGrantedAuthority(userInfo.getRole());
		UserDetails userDetails = new User(userInfo.getUsername(), userInfo.getPassword(), Arrays.asList(authority));

		return userDetails;
	}

}
