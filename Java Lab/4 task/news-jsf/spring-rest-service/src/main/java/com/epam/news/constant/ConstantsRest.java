package com.epam.news.constant;

/**
 * Contains all constants for rest service.
 */
public final class ConstantsRest {

	// Parameters
	public static final String PARAM_AUTHOR_ID = "authorId";
	public static final String PARAM_TAGS = "tags";
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_NEWS_ID_LIST = "newsIdList";
	public static final String PARAM_SEARCH_CRITERIA = "searchCriteria";

	// Roles
	public static final String ROLE_ADMIN = "ROLE_ADMIN";

}
