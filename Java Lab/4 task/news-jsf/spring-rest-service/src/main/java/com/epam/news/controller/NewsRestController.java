package com.epam.news.controller;

import static com.epam.news.constant.ConstantsRest.ROLE_ADMIN;

import java.util.List;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import com.epam.news.dto.NewsDTO;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * Controller for news resource.
 */
@RestController
public class NewsRestController {

	@Autowired
	private NewsService service;

	/**
	 * Gets the news list.
	 *
	 * @return the news list
	 */
	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public ResponseEntity<List<NewsDTO>> getNewsList() {
		return new ResponseEntity<List<NewsDTO>>(service.findBySearchCriteria(new SearchCriteria()), HttpStatus.OK);
	}

	/**
	 * Counts news.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = "/news/count", method = RequestMethod.GET)
	public ResponseEntity<Integer> countNews() {
		return new ResponseEntity<Integer>(service.countNews().intValue(), HttpStatus.OK);
	}

	/**
	 * Counts news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the response entity
	 */
	@RequestMapping(value = "/news/count", method = RequestMethod.POST)
	public ResponseEntity<Integer> countNewsBySearchCriteria(@RequestBody SearchCriteria searchCriteria) {
		return new ResponseEntity<Integer>(service.countNewsBySearchCriteria(searchCriteria), HttpStatus.OK);
	}

	/**
	 * Gets the news.
	 *
	 * @param newsId
	 *            the news id
	 * @return the news
	 */
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.GET)
	public ResponseEntity<NewsDTO> getNews(@PathVariable(value = "newsId") Long newsId) {
		NewsDTO news = service.findById(newsId);
		if (news == null) {
			return new ResponseEntity<NewsDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<NewsDTO>(news, HttpStatus.OK);
	}

	/**
	 * Deletes news.
	 *
	 * @param newsId
	 *            the news id
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteNews(@PathVariable(value = "newsId") Long newsId) {
		service.deleteById(newsId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * Gets the next news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the next news
	 */
	@RequestMapping(value = "/news/next/{newsId}", method = RequestMethod.POST)
	public ResponseEntity<NewsDTO> getNextNews(@PathVariable(value = "newsId") Long newsId,
			@RequestBody SearchCriteria searchCriteria) {
		NewsDTO news = service.findNextNews(newsId, searchCriteria);
		return new ResponseEntity<NewsDTO>(news, HttpStatus.OK);
	}

	/**
	 * Gets the prev news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the prev news
	 */
	@RequestMapping(value = "/news/prev/{newsId}", method = RequestMethod.POST)
	public ResponseEntity<NewsDTO> getPrevNews(@PathVariable(value = "newsId") Long newsId,
			@RequestBody SearchCriteria searchCriteria) {
		NewsDTO news = service.findPrevNews(newsId, searchCriteria);
		return new ResponseEntity<NewsDTO>(news, HttpStatus.OK);
	}

	/**
	 * Creates the news.
	 *
	 * @param newsValue
	 *            the news value
	 * @param ucBuilder
	 *            the uc builder
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news", method = RequestMethod.POST)
	public ResponseEntity<Void> createNews(@Valid @RequestBody NewsDTO newsValue, UriComponentsBuilder ucBuilder) {
		newsValue = service.save(newsValue);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/news/{newsId}").buildAndExpand(newsValue.getNewsId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/**
	 * Updates news.
	 *
	 * @param newsId
	 *            the news id
	 * @param newsValue
	 *            the news value
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.PUT)
	public ResponseEntity<NewsDTO> updateNews(@PathVariable(value = "newsId") Long newsId,
			@Valid @RequestBody NewsDTO newsValue) {
		try {
			service.update(newsValue);
		} catch (OptimisticLockException e) {
			return new ResponseEntity<NewsDTO>(HttpStatus.PRECONDITION_FAILED);
		}
		return new ResponseEntity<NewsDTO>(newsValue, HttpStatus.OK);
	}

	/**
	 * Deletes news list.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/deleteList", method = RequestMethod.POST)
	public ResponseEntity<Void> deleteNewsList(@RequestBody List<Long> newsIdList) {
		service.deleteList(newsIdList);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	/**
	 * Gets the news list by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news list by search criteria
	 */
	@RequestMapping(value = "/news/search", method = RequestMethod.POST)
	public ResponseEntity<List<NewsDTO>> getNewsListBySearchCriteria(@RequestBody SearchCriteria searchCriteria) {
		return new ResponseEntity<List<NewsDTO>>(service.findBySearchCriteria(searchCriteria), HttpStatus.OK);
	}

	/**
	 * Finds id list by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the response entity
	 */
	@RequestMapping(value = "/news/idList", method = RequestMethod.POST)
	public ResponseEntity<List<Long>> findIdListBySearchCriteria(@RequestBody SearchCriteria searchCriteria) {
		return new ResponseEntity<List<Long>>(service.findIdListBySearchCriteria(searchCriteria), HttpStatus.OK);
	}

}
