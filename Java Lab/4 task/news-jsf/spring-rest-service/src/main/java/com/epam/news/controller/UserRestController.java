package com.epam.news.controller;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.epam.news.dto.UserInfoDTO;
import com.epam.news.service.UserInfoService;

/**
 * Controller for user info resource.
 */
@RestController
public class UserRestController {

	@Autowired
	private UserInfoService userInfoService;

	/**
	 * Gets the user.
	 *
	 * @param user
	 *            the user
	 * @return the user
	 */
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<UserInfoDTO> getUser(@RequestBody UserInfoDTO user) {
		UserInfoDTO userInfoDTO = userInfoService.getUserInfo(user.getLogin());

		String password = DigestUtils.md5Hex(user.getPassword());

		if (userInfoDTO != null && userInfoDTO.getPassword().equals(password)) {
			return new ResponseEntity<UserInfoDTO>(userInfoDTO, HttpStatus.OK);
		}

		return new ResponseEntity<UserInfoDTO>(HttpStatus.UNAUTHORIZED);
	}

}
