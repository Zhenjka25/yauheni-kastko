package com.epam.news.controller;

import static com.epam.news.constant.ConstantsRest.ROLE_ADMIN;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import com.epam.news.dto.CommentDTO;
import com.epam.news.service.CommentService;

/**
 * Controller for comments resource.
 */
@RestController
public class CommentRestController {

	@Autowired
	private CommentService service;

	/**
	 * Gets the all comments.
	 *
	 * @return the all comments
	 */
	@RequestMapping(value = "/comment", method = RequestMethod.GET)
	public ResponseEntity<List<CommentDTO>> getAllComments() {
		return new ResponseEntity<List<CommentDTO>>(service.findAll(), HttpStatus.OK);
	}

	/**
	 * Gets the comment.
	 *
	 * @param commentId
	 *            the comment id
	 * @return the comment
	 */
	@RequestMapping(value = "/comment/{commentId}", method = RequestMethod.GET)
	public ResponseEntity<CommentDTO> getComment(@PathVariable(value = "commentId") Long commentId) {
		CommentDTO comment = service.findById(commentId);
		if (comment == null) {
			return new ResponseEntity<CommentDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<CommentDTO>(comment, HttpStatus.OK);
	}

	/**
	 * Creates the comment.
	 *
	 * @param comment
	 *            the comment
	 * @param ucBuilder
	 *            the uc builder
	 * @return the response entity
	 */
	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	public ResponseEntity<Void> createComment(@Valid @RequestBody CommentDTO comment, UriComponentsBuilder ucBuilder) {
		comment = service.save(comment);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/comment/{commentId}").buildAndExpand(comment.getCommentId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	/**
	 * Updates comment.
	 *
	 * @param commentId
	 *            the comment id
	 * @param comment
	 *            the comment
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/comment/{commentId}", method = RequestMethod.PUT)
	public ResponseEntity<CommentDTO> updateComment(@PathVariable(value = "commentId") Long commentId,
			@Valid @RequestBody CommentDTO comment) {
		service.update(comment);
		return new ResponseEntity<CommentDTO>(comment, HttpStatus.OK);
	}

	/**
	 * Deletes comment.
	 *
	 * @param commentId
	 *            the comment id
	 * @return the response entity
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/comment/{commentId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteComment(@PathVariable(value = "commentId") Long commentId) {
		service.deleteById(commentId);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
