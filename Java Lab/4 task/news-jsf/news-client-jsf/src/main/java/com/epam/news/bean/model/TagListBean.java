package com.epam.news.bean.model;

import static com.epam.news.constant.ClientJSFConstants.BEAN_SERVICE;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.TagDTO;

/**
 * JSF bean which represents tag list.
 */
@ManagedBean
@RequestScoped
public class TagListBean {

	private List<TagDTO> tagList;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		tagList = serviceBean.getTagService().findAll();
	}

	/**
	 * Gets the tag list.
	 *
	 * @return the tag list
	 */
	public List<TagDTO> getTagList() {
		return tagList;
	}

	/**
	 * Sets the tag list.
	 *
	 * @param tagList
	 *            the new tag list
	 */
	public void setTagList(List<TagDTO> tagList) {
		this.tagList = tagList;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

}
