package com.epam.news.bean.model;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.epam.news.dto.CommentDTO;

/**
 * JSF bean which represents comment entity.
 */
@ManagedBean
@RequestScoped
public class CommentBean {

	private CommentDTO commentValue;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		commentValue = new CommentDTO();
	}

	/**
	 * Gets the comment value.
	 *
	 * @return the comment value
	 */
	public CommentDTO getCommentValue() {
		return commentValue;
	}

	/**
	 * Sets the comment value.
	 *
	 * @param commentValue
	 *            the new comment value
	 */
	public void setCommentValue(CommentDTO commentValue) {
		this.commentValue = commentValue;
	}

}
