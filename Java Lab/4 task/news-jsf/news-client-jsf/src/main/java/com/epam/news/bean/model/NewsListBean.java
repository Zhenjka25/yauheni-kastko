package com.epam.news.bean.model;

import static com.epam.news.constant.ClientJSFConstants.BEAN_SEARCH_CRITERIA;
import static com.epam.news.constant.ClientJSFConstants.BEAN_SERVICE;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.epam.news.bean.support.SearchCriteriaBean;
import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.NewsDTO;
import com.epam.news.util.Paginator;
import com.epam.news.util.SearchCriteria;

/**
 * JSF bean which represents news list.
 */
@ManagedBean
@RequestScoped
public class NewsListBean {

	private List<NewsDTO> newsValueList;

	@ManagedProperty(value = BEAN_SEARCH_CRITERIA)
	private SearchCriteriaBean searchCriteriaBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	private Integer pagesCount;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		SearchCriteria searchCriteria = searchCriteriaBean.createSearchCriteria();
		newsValueList = serviceBean.getNewsService().findBySearchCriteria(searchCriteria);
		pagesCount = Paginator.getPagesCount(searchCriteriaBean, serviceBean.getNewsService());
	}

	/**
	 * Gets the news value list.
	 *
	 * @return the news value list
	 */
	public List<NewsDTO> getNewsValueList() {
		return newsValueList;
	}

	/**
	 * Sets the news value list.
	 *
	 * @param newsValueList
	 *            the new news value list
	 */
	public void setNewsValueList(List<NewsDTO> newsValueList) {
		this.newsValueList = newsValueList;
	}

	/**
	 * Gets the search criteria bean.
	 *
	 * @return the search criteria bean
	 */
	public SearchCriteriaBean getSearchCriteriaBean() {
		return searchCriteriaBean;
	}

	/**
	 * Sets the search criteria bean.
	 *
	 * @param searchCriteriaBean
	 *            the new search criteria bean
	 */
	public void setSearchCriteriaBean(SearchCriteriaBean searchCriteriaBean) {
		this.searchCriteriaBean = searchCriteriaBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	/**
	 * Gets the pages count.
	 *
	 * @return the pages count
	 */
	public Integer getPagesCount() {
		return pagesCount;
	}

	/**
	 * Sets the pages count.
	 *
	 * @param pagesCount
	 *            the new pages count
	 */
	public void setPagesCount(Integer pagesCount) {
		this.pagesCount = pagesCount;
	}

}
