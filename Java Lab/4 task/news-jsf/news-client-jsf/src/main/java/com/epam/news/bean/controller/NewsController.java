package com.epam.news.bean.controller;

import static com.epam.news.constant.ClientJSFConstants.*;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.epam.news.bean.model.CommentBean;
import com.epam.news.bean.support.ServiceBean;

/**
 * JSF controller class related to the news details page.
 */
@ManagedBean
@RequestScoped
public class NewsController {

	@ManagedProperty(value = BEAN_COMMENT)
	private CommentBean commentBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	@ManagedProperty(value = PARAM_NEWS_ID)
	private Long newsId;

	/**
	 * Shows news.
	 *
	 * @return the string
	 */
	public String showNews() {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ATTRIBUTE_NEWS_ID, newsId);
		return PAGE_NEWS + FACES_REDIRECT;
	}

	/**
	 * Adds the comment.
	 *
	 * @return the string
	 */
	public String addComment() {

		commentBean.getCommentValue().setCreationDate(new Date());
		commentBean.getCommentValue().setNewsId(newsId);

		serviceBean.getCommentService().save(commentBean.getCommentValue());

		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ATTRIBUTE_NEWS_ID, newsId);

		return PAGE_NEWS + FACES_REDIRECT;
	}

	/**
	 * Gets the comment bean.
	 *
	 * @return the comment bean
	 */
	public CommentBean getCommentBean() {
		return commentBean;
	}

	/**
	 * Sets the comment bean.
	 *
	 * @param commentBean
	 *            the new comment bean
	 */
	public void setCommentBean(CommentBean commentBean) {
		this.commentBean = commentBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

}
