package com.epam.news.service.container;

import com.epam.news.service.*;
import com.epam.news.service.rest.*;
import com.epam.news.service.soap.*;

/**
 * Enumeration which contains all service implementations needed for
 * application.
 */
enum ServiceEnum {

	SOAP {
		{
			authorService = new AuthorServiceSoap();
			commentService = new CommentServiceSoap();
			tagService = new TagServiceSoap();
			newsService = new NewsServiceSoap();
		}
	},

	REST {
		{
			authorService = new AuthorServiceRest();
			commentService = new CommentServiceRest();
			tagService = new TagServiceRest();
			newsService = new NewsServiceRest();
		}
	};

	AuthorService authorService;
	CommentService commentService;
	TagService tagService;
	NewsService newsService;

	/**
	 * Gets the author service.
	 *
	 * @return the author service
	 */
	public AuthorService getAuthorService() {
		return authorService;
	}

	/**
	 * Gets the comment service.
	 *
	 * @return the comment service
	 */
	public CommentService getCommentService() {
		return commentService;
	}

	/**
	 * Gets the tag service.
	 *
	 * @return the tag service
	 */
	public TagService getTagService() {
		return tagService;
	}

	/**
	 * Gets the news service.
	 *
	 * @return the news service
	 */
	public NewsService getNewsService() {
		return newsService;
	}

}
