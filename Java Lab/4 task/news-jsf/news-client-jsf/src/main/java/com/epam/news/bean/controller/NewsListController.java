package com.epam.news.bean.controller;

import static com.epam.news.constant.ClientJSFConstants.*;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.epam.news.bean.model.NewsListBean;
import com.epam.news.bean.support.SearchCriteriaBean;
import com.epam.news.bean.support.ServiceBean;
import com.epam.news.util.Paginator;

/**
 * JSF controller related to news list page.
 */
@ManagedBean
@RequestScoped
public class NewsListController {

	private int pagesCount;

	@ManagedProperty(value = BEAN_SEARCH_CRITERIA)
	private SearchCriteriaBean searchCriteriaBean;

	@ManagedProperty(value = BEAN_NEWS_LIST)
	private NewsListBean newsListBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	@ManagedProperty(value = PARAM_SWITCH_TO)
	private String switchTo;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		pagesCount = Paginator.getPagesCount(searchCriteriaBean, serviceBean.getNewsService());
	}

	/**
	 * Shows news list.
	 *
	 * @return the string
	 */
	public String showNewsList() {
		return PAGE_NEWS_LIST + FACES_REDIRECT;
	}

	/**
	 * Clears search criteria.
	 *
	 * @return the string
	 */
	public String clearSearchCriteria() {
		searchCriteriaBean.clearSearchCriteria();
		return PAGE_NEWS_LIST + FACES_REDIRECT;
	}

	/**
	 * Switches page.
	 *
	 * @return the string
	 */
	public String switchPage() {
		Paginator.switchPage(switchTo, searchCriteriaBean, serviceBean.getNewsService());
		return PAGE_NEWS_LIST + FACES_REDIRECT;
	}

	/**
	 * Gets the pages count.
	 *
	 * @return the pages count
	 */
	public int getPagesCount() {
		return pagesCount;
	}

	/**
	 * Sets the pages count.
	 *
	 * @param pagesCount
	 *            the new pages count
	 */
	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	/**
	 * Gets the search criteria bean.
	 *
	 * @return the search criteria bean
	 */
	public SearchCriteriaBean getSearchCriteriaBean() {
		return searchCriteriaBean;
	}

	/**
	 * Sets the search criteria bean.
	 *
	 * @param searchCriteriaBean
	 *            the new search criteria bean
	 */
	public void setSearchCriteriaBean(SearchCriteriaBean searchCriteriaBean) {
		this.searchCriteriaBean = searchCriteriaBean;
	}

	/**
	 * Gets the news list bean.
	 *
	 * @return the news list bean
	 */
	public NewsListBean getNewsListBean() {
		return newsListBean;
	}

	/**
	 * Sets the news list bean.
	 *
	 * @param newsListBean
	 *            the new news list bean
	 */
	public void setNewsListBean(NewsListBean newsListBean) {
		this.newsListBean = newsListBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	/**
	 * Gets the switch to.
	 *
	 * @return the switch to
	 */
	public String getSwitchTo() {
		return switchTo;
	}

	/**
	 * Sets the switch to.
	 *
	 * @param switchTo
	 *            the new switch to
	 */
	public void setSwitchTo(String switchTo) {
		this.switchTo = switchTo;
	}

}
