package com.epam.news.service.container;

import static com.epam.news.constant.ClientJSFConstants.SERVICE_IMPLEMENTATION;

import com.epam.news.manager.AppPropertyManager;
import com.epam.news.service.*;

/**
 * Locator for retrieving services.
 */
public class ServiceLocator {

	private static ServiceLocator instance = new ServiceLocator();

	private ServiceEnum serviceEnum;

	/**
	 * Instantiates a new service locator.
	 */
	private ServiceLocator() {

		AppPropertyManager manager = AppPropertyManager.getInstance();
		String service = manager.getProperty(SERVICE_IMPLEMENTATION).toUpperCase();

		try {
			serviceEnum = ServiceEnum.valueOf(service);
		} catch (IllegalArgumentException e) {
			// Default service
			serviceEnum = ServiceEnum.REST;
		}

	}

	/**
	 * Gets the single instance of ServiceLocator.
	 *
	 * @return single instance of ServiceLocator
	 */
	public static ServiceLocator getInstance() {
		return instance;
	}

	/**
	 * Gets the author service.
	 *
	 * @return the author service
	 */
	public AuthorService getAuthorService() {
		return serviceEnum.getAuthorService();
	}

	/**
	 * Gets the comment service.
	 *
	 * @return the comment service
	 */
	public CommentService getCommentService() {
		return serviceEnum.getCommentService();
	}

	/**
	 * Gets the tag service.
	 *
	 * @return the tag service
	 */
	public TagService getTagService() {
		return serviceEnum.getTagService();
	}

	/**
	 * Gets the news service.
	 *
	 * @return the news service
	 */
	public NewsService getNewsService() {
		return serviceEnum.getNewsService();
	}

}
