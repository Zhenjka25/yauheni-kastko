package com.epam.news.constant;

/**
 * Contains all constants for application.
 */
public final class ClientJSFConstants {

	public static final String REST_SERVICE_URI = "http://localhost:8181/spring-rest-service";

	// Pages
	public static final String PAGE_NEWS_LIST = "news-list";
	public static final String PAGE_NEWS = "news";
	public static final String PAGE_ERROR = "error";

	// Adds to result page, if redirect is needed
	public static final String FACES_REDIRECT = "?faces-redirect=true";

	// Beans
	public static final String BEAN_NEWS_LIST_CONTROLLER = "#{newsListController}";
	public static final String BEAN_AUTHOR_LIST = "#{authorListBean}";
	public static final String BEAN_NEWS = "#{newsBean}";
	public static final String BEAN_NEWS_LIST = "#{newsListBean}";
	public static final String BEAN_TAG_LIST = "#{tagListBean}";
	public static final String BEAN_LANGUAGE = "#{languageBean}";
	public static final String BEAN_SEARCH_CRITERIA = "#{searchCriteriaBean}";
	public static final String BEAN_SERVICE = "#{serviceBean}";
	public static final String BEAN_COMMENT = "#{commentBean}";

	// Beans string
	public static final String BEAN_NEWS_LIST_STRING = "newsListBean";
	public static final String BEAN_NEWS_STRING = "newsBean";

	// Parameters
	public static final String PARAM_SWITCH_TO = "#{param.switchTo}";
	public static final String PARAM_NEWS_ID = "#{param.newsId}";

	public static final String PARAM_ERROR = "error";

	// Attributes
	public static final String ATTRIBUTE_NEWS_ID = "newsId";
	public static final String ATTRIBUTE_TIME_ZONE_OFFSET = "timezoneOffset";

	// Cookies
	public static final String COOKIE_OFFSET = "timezoneOffset";

	// Service implementation
	public static final String SERVICE_IMPLEMENTATION = "service.implementation";

	// Error messages
	public static final String ERROR_MESSAGE_DEFAULT = "error.message.default";

}
