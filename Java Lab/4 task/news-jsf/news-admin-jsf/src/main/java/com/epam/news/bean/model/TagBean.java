package com.epam.news.bean.model;

import static com.epam.news.constant.AdminJSFConstants.BEAN_SERVICE;
import static com.epam.news.constant.AdminJSFConstants.PARAM_TAG_ID;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.TagDTO;

/**
 * JSF bean which represents tag entity.
 */
@ManagedBean
@RequestScoped
public class TagBean {

	private TagDTO tagDTO;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {

		String tagIdString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get(PARAM_TAG_ID);

		if (tagIdString != null) {
			Long tagId = Long.parseLong(tagIdString);
			tagDTO = serviceBean.getTagService().findById(tagId);
		} else {
			tagDTO = new TagDTO();
		}

	}

	/**
	 * Gets the tag dto.
	 *
	 * @return the tag dto
	 */
	public TagDTO getTagDTO() {
		return tagDTO;
	}

	/**
	 * Sets the tag dto.
	 *
	 * @param tagDTO
	 *            the new tag dto
	 */
	public void setTagDTO(TagDTO tagDTO) {
		this.tagDTO = tagDTO;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

}
