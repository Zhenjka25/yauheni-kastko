package com.epam.news.bean.model;

import static com.epam.news.constant.AdminJSFConstants.*;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.epam.news.bean.support.SearchCriteriaBean;
import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.NewsDTO;
import com.epam.news.util.SearchCriteria;

/**
 * JSF bean which represents news entity. Contains news entity, previous news id
 * and next news id.
 */
@ManagedBean
@RequestScoped
public class NewsBean {

	@ManagedProperty(value = BEAN_SEARCH_CRITERIA)
	private SearchCriteriaBean searchCriteriaBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	private NewsDTO newsValue;

	private Long nextNewsId;

	private Long prevNewsId;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {

		newsValue = new NewsDTO();
		newsValue.setModificationDate(new Date());

		if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get(PARAM_CLEAR_NEWS_ID) != null) {
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(ATTRIBUTE_NEWS_ID);
			return;
		}

		String newsIdString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get(PARAM_NEWS_ID_STRING);
		Long newsId = null;
		newsId = getNewsId(newsIdString);

		initializeNews(newsId);

	}

	/**
	 * Gets the news id.
	 *
	 * @param newsIdString
	 *            the news id string
	 * @return the news id
	 */
	private Long getNewsId(String newsIdString) {
		Long newsId = null;

		if (newsIdString != null) {
			newsId = Long.parseLong(newsIdString);
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ATTRIBUTE_NEWS_ID, newsId);
		} else {
			newsId = (Long) FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
					.get(ATTRIBUTE_NEWS_ID);
		}

		return newsId;
	}

	/**
	 * Initializes news.
	 *
	 * @param newsId
	 *            the news id
	 */
	private void initializeNews(Long newsId) {
		if (newsId != null) {
			newsValue = serviceBean.getNewsService().findById(newsId);

			SearchCriteria searchCriteria = searchCriteriaBean.createSearchCriteria();

			NewsDTO nextNews = serviceBean.getNewsService().findNextNews(newsId, searchCriteria);
			if (nextNews != null) {
				nextNewsId = nextNews.getNewsId();
			}

			NewsDTO prevNews = serviceBean.getNewsService().findPrevNews(newsId, searchCriteria);
			if (prevNews != null) {
				prevNewsId = prevNews.getNewsId();
			}
		}
	}

	/**
	 * Gets the search criteria bean.
	 *
	 * @return the search criteria bean
	 */
	public SearchCriteriaBean getSearchCriteriaBean() {
		return searchCriteriaBean;
	}

	/**
	 * Sets the search criteria bean.
	 *
	 * @param searchCriteriaBean
	 *            the new search criteria bean
	 */
	public void setSearchCriteriaBean(SearchCriteriaBean searchCriteriaBean) {
		this.searchCriteriaBean = searchCriteriaBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	/**
	 * Gets the news value.
	 *
	 * @return the news value
	 */
	public NewsDTO getNewsValue() {
		return newsValue;
	}

	/**
	 * Sets the news value.
	 *
	 * @param newsValue
	 *            the new news value
	 */
	public void setNewsValue(NewsDTO newsValue) {
		this.newsValue = newsValue;
	}

	/**
	 * Gets the next news id.
	 *
	 * @return the next news id
	 */
	public Long getNextNewsId() {
		return nextNewsId;
	}

	/**
	 * Sets the next news id.
	 *
	 * @param nextNewsId
	 *            the new next news id
	 */
	public void setNextNewsId(Long nextNewsId) {
		this.nextNewsId = nextNewsId;
	}

	/**
	 * Gets the prev news id.
	 *
	 * @return the prev news id
	 */
	public Long getPrevNewsId() {
		return prevNewsId;
	}

	/**
	 * Sets the prev news id.
	 *
	 * @param prevNewsId
	 *            the new prev news id
	 */
	public void setPrevNewsId(Long prevNewsId) {
		this.prevNewsId = prevNewsId;
	}

}
