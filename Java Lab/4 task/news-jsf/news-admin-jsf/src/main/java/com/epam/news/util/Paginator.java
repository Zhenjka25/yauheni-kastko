package com.epam.news.util;

import static com.epam.news.constant.ConstantsCommon.NEWS_PER_PAGE_KEY;

import java.util.ResourceBundle;

import com.epam.news.bean.support.SearchCriteriaBean;
import com.epam.news.service.NewsService;

/**
 * Util class for pagination processing.
 */
public class Paginator {

	private static final String SWITCH_FIRST = "first";
	private static final String SWITCH_DECR = "decr";
	private static final String SWITCH_INCR = "incr";
	private static final String SWITCH_LAST = "last";

	private static final String PROPERTY_FILE = "app";

	private static final ResourceBundle resourceBundle;

	private static final int newsPerPage;

	static {
		resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE);
		newsPerPage = Integer.parseInt(resourceBundle.getString(NEWS_PER_PAGE_KEY));
	}

	/**
	 * Switches page.
	 *
	 * @param switchTo
	 *            the switch to
	 * @param searchCriteriaBean
	 *            the search criteria bean
	 * @param newsService
	 *            the news service
	 */
	public static void switchPage(String switchTo, SearchCriteriaBean searchCriteriaBean, NewsService newsService) {
		switch (switchTo) {
		case SWITCH_FIRST:
			searchCriteriaBean.setPage(1);
			break;
		case SWITCH_DECR:
			searchCriteriaBean.prevPage();
			break;
		case SWITCH_INCR:
			int pagesCount = getPagesCount(searchCriteriaBean, newsService);
			if (searchCriteriaBean.getPage() < pagesCount) {
				searchCriteriaBean.nextPage();
			}
			break;
		case SWITCH_LAST:
			searchCriteriaBean.setPage(getPagesCount(searchCriteriaBean, newsService));
			break;
		}
	}

	/**
	 * Gets the pages count.
	 *
	 * @param searchCriteriaBean
	 *            the search criteria bean
	 * @param newsService
	 *            the news service
	 * @return the pages count
	 */
	public static int getPagesCount(SearchCriteriaBean searchCriteriaBean, NewsService newsService) {
		int newsNumber = newsService.countNewsBySearchCriteria(searchCriteriaBean.createSearchCriteria());
		int pagesCount = newsNumber / newsPerPage;
		if ((newsNumber % newsPerPage) != 0) {
			pagesCount++;
		}
		return pagesCount;
	}

}
