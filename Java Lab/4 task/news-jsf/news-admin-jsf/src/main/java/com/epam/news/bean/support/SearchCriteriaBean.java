package com.epam.news.bean.support;

import static com.epam.news.constant.AdminJSFConstants.BEAN_NEWS_LIST_STRING;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import com.epam.news.bean.model.NewsListBean;
import com.epam.news.util.SearchCriteria;

/**
 * JSF bean which contains search criteria entity.
 */
@ManagedBean
@SessionScoped
public class SearchCriteriaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long[] tagIdArray;

	private Long authorId;

	private int page;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		page = 1;
	}

	/**
	 * Creates the search criteria.
	 *
	 * @return the search criteria
	 */
	public SearchCriteria createSearchCriteria() {

		SearchCriteria searchCriteria = new SearchCriteria();

		searchCriteria.setAuthorId(authorId);
		searchCriteria.setPage(page);

		if (tagIdArray != null && tagIdArray.length != 0) {
			searchCriteria.getTagsId().addAll(Arrays.asList(tagIdArray));
		}

		return searchCriteria;
	}

	/**
	 * Clears search criteria.
	 */
	public void clearSearchCriteria() {
		tagIdArray = null;
		authorId = null;
		page = 1;
	}

	/**
	 * Next page.
	 */
	public void nextPage() {
		page++;
	}

	/**
	 * Prev page.
	 */
	public void prevPage() {
		page = page != 1 ? --page : 1;
	}

	/**
	 * Processes author id value change.
	 *
	 * @param event
	 *            the event
	 */
	public void processAuthorIdValueChange(ValueChangeEvent event) {
		page = 1;
	}

	/**
	 * Processes tag id array value change.
	 *
	 * @param event
	 *            the event
	 */
	public void processTagIdArrayValueChange(ValueChangeEvent event) {
		page = 1;
	}

	/**
	 * Processes page value change.
	 *
	 * @param event
	 *            the event
	 */
	public void processPageValueChange(ValueChangeEvent event) {
		page = (int) event.getNewValue();
		Map<String, Object> requestMap = FacesContext.getCurrentInstance().getExternalContext().getRequestMap();
		NewsListBean newsListBean = (NewsListBean) requestMap.get(BEAN_NEWS_LIST_STRING);
		newsListBean.init();
	}

	/**
	 * Gets the tag id array.
	 *
	 * @return the tag id array
	 */
	public Long[] getTagIdArray() {
		return tagIdArray;
	}

	/**
	 * Sets the tag id array.
	 *
	 * @param tagIdArray
	 *            the new tag id array
	 */
	public void setTagIdArray(Long[] tagIdArray) {
		this.tagIdArray = tagIdArray;
	}

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the page.
	 *
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * Sets the page.
	 *
	 * @param page
	 *            the new page
	 */
	public void setPage(int page) {
		this.page = page;
	}

}
