package com.epam.news.bean.support;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.epam.news.service.*;
import com.epam.news.service.container.ServiceLocator;

/**
 * JSF bean which contains all services needed for application.
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class ServiceBean {

	private AuthorService authorService;
	private CommentService commentService;
	private NewsService newsService;
	private TagService tagService;
	private UserInfoServiceClient userService;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {

		ServiceLocator factory = ServiceLocator.getInstance();

		authorService = factory.getAuthorService();
		commentService = factory.getCommentService();
		newsService = factory.getNewsService();
		tagService = factory.getTagService();
		userService = factory.getUserInfoService();

	}

	/**
	 * Gets the author service.
	 *
	 * @return the author service
	 */
	public AuthorService getAuthorService() {
		return authorService;
	}

	/**
	 * Sets the author service.
	 *
	 * @param authorService
	 *            the new author service
	 */
	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	/**
	 * Gets the comment service.
	 *
	 * @return the comment service
	 */
	public CommentService getCommentService() {
		return commentService;
	}

	/**
	 * Sets the comment service.
	 *
	 * @param commentService
	 *            the new comment service
	 */
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	/**
	 * Gets the news service.
	 *
	 * @return the news service
	 */
	public NewsService getNewsService() {
		return newsService;
	}

	/**
	 * Sets the news service.
	 *
	 * @param newsService
	 *            the new news service
	 */
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	/**
	 * Gets the tag service.
	 *
	 * @return the tag service
	 */
	public TagService getTagService() {
		return tagService;
	}

	/**
	 * Sets the tag service.
	 *
	 * @param tagService
	 *            the new tag service
	 */
	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	/**
	 * Gets the user service.
	 *
	 * @return the user service
	 */
	public UserInfoServiceClient getUserService() {
		return userService;
	}

	/**
	 * Sets the user service.
	 *
	 * @param userService
	 *            the new user service
	 */
	public void setUserService(UserInfoServiceClient userService) {
		this.userService = userService;
	}

}
