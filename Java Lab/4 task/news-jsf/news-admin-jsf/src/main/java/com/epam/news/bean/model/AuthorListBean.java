package com.epam.news.bean.model;

import static com.epam.news.constant.AdminJSFConstants.BEAN_SERVICE;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.AuthorDTO;

/**
 * JSF bean which represents author list.
 */
@ManagedBean
@RequestScoped
public class AuthorListBean {

	private List<AuthorDTO> authorList;

	@ManagedProperty(BEAN_SERVICE)
	private ServiceBean serviceBean;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		authorList = serviceBean.getAuthorService().findActualAuthors();
	}

	/**
	 * Gets the author list.
	 *
	 * @return the author list
	 */
	public List<AuthorDTO> getAuthorList() {
		return authorList;
	}

	/**
	 * Sets the author list.
	 *
	 * @param authorList
	 *            the new author list
	 */
	public void setAuthorList(List<AuthorDTO> authorList) {
		this.authorList = authorList;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

}
