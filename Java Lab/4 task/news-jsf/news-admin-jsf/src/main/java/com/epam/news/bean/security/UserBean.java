package com.epam.news.bean.security;

import static com.epam.news.constant.AdminJSFConstants.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

/**
 * JSF bean which is used for securing of application. Contains user info
 * entity.
 */
@ManagedBean
@SessionScoped
public class UserBean {

	@ManagedProperty(value = PARAM_LOGIN)
	private String userName;

	@ManagedProperty(value = PARAM_PASSWORD)
	private String password;

	/**
	 * Processes login action.
	 *
	 * @return the string
	 */
	public String login() {

		Subject currentUser = SecurityUtils.getSubject();
		UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
		token.setRememberMe(true);
		try {
			currentUser.login(token);
		} catch (AuthenticationException ex) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put(ATTRIBUTE_ERROR, ERROR_MESSAGE_LOGIN);
			return PAGE_LOGIN + FACES_REDIRECT;
		}

		return PAGE_NEWS_LIST + FACES_REDIRECT;
	}

	/**
	 * Processes logout action.
	 *
	 * @return the string
	 */
	public String logout() {
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.logout();

		return PAGE_LOGIN + FACES_REDIRECT;
	}

	/**
	 * Checks if user is authenticated.
	 *
	 * @return true, if is authenticated
	 */
	public boolean isAuthenticated() {
		return SecurityUtils.getSubject().isAuthenticated();
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName
	 *            the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
