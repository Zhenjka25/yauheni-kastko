package com.epam.news.bean.model;

import static com.epam.news.constant.AdminJSFConstants.BEAN_SERVICE;
import static com.epam.news.constant.AdminJSFConstants.PARAM_AUTHOR_ID;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.AuthorDTO;

/**
 * JSF bean which represents author entity.
 */
@ManagedBean
@RequestScoped
public class AuthorBean {

	private AuthorDTO authorDTO;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {

		String authorIdString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
				.get(PARAM_AUTHOR_ID);

		if (authorIdString != null) {
			Long authorId = Long.parseLong(authorIdString);
			authorDTO = serviceBean.getAuthorService().findById(authorId);
		} else {
			authorDTO = new AuthorDTO();
		}

	}

	/**
	 * Gets the author dto.
	 *
	 * @return the author dto
	 */
	public AuthorDTO getAuthorDTO() {
		return authorDTO;
	}

	/**
	 * Sets the author dto.
	 *
	 * @param authorDTO
	 *            the new author dto
	 */
	public void setAuthorDTO(AuthorDTO authorDTO) {
		this.authorDTO = authorDTO;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

}
