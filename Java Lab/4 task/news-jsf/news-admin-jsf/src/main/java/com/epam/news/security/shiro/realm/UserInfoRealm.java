package com.epam.news.security.shiro.realm;

import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import com.epam.news.dto.UserInfoDTO;
import com.epam.news.service.UserInfoServiceClient;
import com.epam.news.service.container.ServiceLocator;

/**
 * Class for connecting between user info service and SHIRO framework.
 */
public class UserInfoRealm extends AuthorizingRealm {

	private UserInfoServiceClient service;

	/**
	 * Instantiates a new user info realm.
	 */
	public UserInfoRealm() {
		setName("UserInfoRealm");
		setCredentialsMatcher(new HashedCredentialsMatcher(Md5Hash.ALGORITHM_NAME));

		ServiceLocator factory = ServiceLocator.getInstance();
		service = factory.getUserInfoService();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String[] principalsArray = ((String) principals.fromRealm(getName()).iterator().next()).split(":");
		String login = principalsArray[0];
		String password = principalsArray[1];
		UserInfoDTO user = service.getUserInfo(login, password);
		if (user != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			info.addRole(user.getRole());
			return info;
		} else {
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken)
			throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
		String login = token.getUsername();
		String password = new String(token.getPassword());
		UserInfoDTO user = service.getUserInfo(login, password);
		if (user != null) {
			return new SimpleAuthenticationInfo(login + ":" + password, user.getPassword(), getName());
		} else {
			return null;
		}
	}

}
