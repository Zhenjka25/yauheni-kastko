package com.epam.news.bean.controller;

import static com.epam.news.constant.AdminJSFConstants.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.authz.annotation.RequiresRoles;

import com.epam.news.bean.model.TagBean;
import com.epam.news.bean.support.ServiceBean;
import com.epam.news.exception.CustomOptimisticLockException;

/**
 * JSF controller for tags.
 */
@ManagedBean
@RequestScoped
public class TagController {

	@ManagedProperty(value = BEAN_TAG)
	private TagBean tagBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	/**
	 * Updates tag.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String updateTag() {
		try {
			serviceBean.getTagService().update(tagBean.getTagDTO());
		} catch (CustomOptimisticLockException e) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put(ATTRIBUTE_ERROR,
					ERROR_MESSAGE_OPTIM_LOCK_TAG);
		}
		return PAGE_TAGS + FACES_REDIRECT;
	}

	/**
	 * Creates the tag.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String createTag() {
		serviceBean.getTagService().save(tagBean.getTagDTO());
		return PAGE_TAGS + FACES_REDIRECT;
	}

	/**
	 * Deletes tag.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String deleteTag() {
		serviceBean.getTagService().deleteById(tagBean.getTagDTO().getTagId());
		return PAGE_TAGS + FACES_REDIRECT;
	}

	/**
	 * Gets the tag bean.
	 *
	 * @return the tag bean
	 */
	public TagBean getTagBean() {
		return tagBean;
	}

	/**
	 * Sets the tag bean.
	 *
	 * @param tagBean
	 *            the new tag bean
	 */
	public void setTagBean(TagBean tagBean) {
		this.tagBean = tagBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

}
