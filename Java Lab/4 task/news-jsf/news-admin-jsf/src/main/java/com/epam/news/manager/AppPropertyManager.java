package com.epam.news.manager;

import java.util.ResourceBundle;

/**
 * Convenient class for work with property files.
 */
public class AppPropertyManager {

	private static final String PROPERTY_FILE = "app-admin-jsf";

	private ResourceBundle resourceBundle;

	private static AppPropertyManager instance = new AppPropertyManager();

	/**
	 * Instantiates a new app property manager.
	 */
	private AppPropertyManager() {
		resourceBundle = ResourceBundle.getBundle(PROPERTY_FILE);
	}

	/**
	 * Gets the single instance of AppPropertyManager.
	 *
	 * @return single instance of AppPropertyManager
	 */
	public static AppPropertyManager getInstance() {
		return instance;
	}

	/**
	 * Gets the property.
	 *
	 * @param key
	 *            the key
	 * @return the property
	 */
	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}

}
