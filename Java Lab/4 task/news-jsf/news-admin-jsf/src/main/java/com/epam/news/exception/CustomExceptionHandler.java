package com.epam.news.exception;

import static com.epam.news.constant.AdminJSFConstants.ERROR_MESSAGE_DEFAULT;
import static com.epam.news.constant.AdminJSFConstants.PAGE_ERROR;
import static com.epam.news.constant.AdminJSFConstants.PARAM_ERROR;

import java.util.Iterator;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.apache.log4j.Logger;

/**
 * Global exception handler for application.
 */
public class CustomExceptionHandler extends ExceptionHandlerWrapper {

	private static Logger log = Logger.getLogger(CustomExceptionHandler.class);

	private ExceptionHandler wrapped;

	/**
	 * Instantiates a new custom exception handler.
	 *
	 * @param exception
	 *            the exception
	 */
	public CustomExceptionHandler(ExceptionHandler exception) {
		this.wrapped = exception;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExceptionHandler getWrapped() {
		return wrapped;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void handle() throws FacesException {

		final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
		while (i.hasNext()) {
			ExceptionQueuedEvent event = i.next();
			ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

			Throwable e = context.getException();

			final FacesContext fc = FacesContext.getCurrentInstance();
			final Map<String, Object> requestMap = fc.getExternalContext().getRequestMap();
			final NavigationHandler nav = fc.getApplication().getNavigationHandler();

			try {

				log.error(e);

				requestMap.put(PARAM_ERROR, ERROR_MESSAGE_DEFAULT);
				nav.handleNavigation(fc, null, PAGE_ERROR);
				fc.renderResponse();

			} finally {
				i.remove();
			}
		}
		getWrapped().handle();
	}

}
