package com.epam.news.bean.controller;

import static com.epam.news.constant.AdminJSFConstants.*;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.authz.annotation.RequiresRoles;

import com.epam.news.bean.model.NewsBean;
import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.AuthorDTO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.dto.TagDTO;
import com.epam.news.exception.CustomOptimisticLockException;

/**
 * JSF controller for news.
 */
@ManagedBean
@RequestScoped
public class NewsController {

	@ManagedProperty(value = BEAN_NEWS)
	private NewsBean newsBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	@ManagedProperty(value = PARAM_NEWS_ID)
	private Long newsId;

	private Long[] tagIdArray;

	private Long authorId;

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {

		List<Long> idList = new ArrayList<>();
		for (TagDTO tagDTO : newsBean.getNewsValue().getTags()) {
			idList.add(tagDTO.getTagId());
		}
		tagIdArray = idList.toArray(new Long[] {});

		if (newsBean.getNewsValue().getAuthor() != null) {
			authorId = newsBean.getNewsValue().getAuthor().getAuthorId();
		}

	}

	/**
	 * Creates the or updates news.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String createOrUpdateNews() {

		NewsDTO newsDTO = newsBean.getNewsValue();

		fillNewsByTags(newsDTO);

		AuthorDTO author = serviceBean.getAuthorService().findById(authorId);
		newsDTO.setAuthor(author);

		if (newsDTO.getCreationDate() == null) {
			newsDTO.setCreationDate(newsDTO.getModificationDate());
		}

		return processSaveOrUpdate(newsDTO);
	}

	/**
	 * Fills news by tags.
	 *
	 * @param newsDTO
	 *            the news dto
	 */
	private void fillNewsByTags(NewsDTO newsDTO) {
		if (tagIdArray != null && tagIdArray.length != 0) {
			List<TagDTO> tagList = new ArrayList<>();
			for (int i = 0; i < tagIdArray.length; i++) {
				tagList.add(serviceBean.getTagService().findById(tagIdArray[i]));
			}
			newsDTO.getTags().clear();
			newsDTO.getTags().addAll(tagList);
		}
	}

	/**
	 * Processes saving or updating.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return the string
	 */
	private String processSaveOrUpdate(NewsDTO newsDTO) {
		if (newsDTO.getNewsId() == null) {
			Long newsId = serviceBean.getNewsService().save(newsDTO).getNewsId();
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(ATTRIBUTE_NEWS_ID, newsId);
		} else {
			try {
				serviceBean.getNewsService().update(newsDTO);
			} catch (CustomOptimisticLockException e) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put(ATTRIBUTE_ERROR,
						ERROR_MESSAGE_OPTIM_LOCK_NEWS);
				return PAGE_EDIT_NEWS + FACES_REDIRECT;
			}
		}
		return PAGE_NEWS + FACES_REDIRECT;
	}

	/**
	 * Gets the news bean.
	 *
	 * @return the news bean
	 */
	public NewsBean getNewsBean() {
		return newsBean;
	}

	/**
	 * Sets the news bean.
	 *
	 * @param newsBean
	 *            the new news bean
	 */
	public void setNewsBean(NewsBean newsBean) {
		this.newsBean = newsBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the tag id array.
	 *
	 * @return the tag id array
	 */
	public Long[] getTagIdArray() {
		return tagIdArray;
	}

	/**
	 * Sets the tag id array.
	 *
	 * @param tagIdArray
	 *            the new tag id array
	 */
	public void setTagIdArray(Long[] tagIdArray) {
		this.tagIdArray = tagIdArray;
	}

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

}
