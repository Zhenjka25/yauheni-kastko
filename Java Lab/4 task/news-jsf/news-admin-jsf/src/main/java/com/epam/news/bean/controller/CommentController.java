package com.epam.news.bean.controller;

import static com.epam.news.constant.AdminJSFConstants.*;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.apache.shiro.authz.annotation.RequiresRoles;

import com.epam.news.bean.model.CommentBean;
import com.epam.news.bean.support.ServiceBean;

/**
 * JSF controller for comments.
 */
@ManagedBean
@RequestScoped
public class CommentController {

	@ManagedProperty(value = BEAN_COMMENT)
	private CommentBean commentBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	@ManagedProperty(value = PARAM_NEWS_ID)
	private Long newsId;

	@ManagedProperty(value = PARAM_COMMENT_ID)
	private Long commentId;

	/**
	 * Adds the comment.
	 *
	 * @return the string
	 */
	public String addComment() {

		commentBean.getCommentValue().setCreationDate(new Date());
		commentBean.getCommentValue().setNewsId(newsId);

		serviceBean.getCommentService().save(commentBean.getCommentValue());

		return PAGE_NEWS + FACES_REDIRECT;
	}

	/**
	 * Deletes comment.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String deleteComment() {
		serviceBean.getCommentService().deleteById(commentId);

		return PAGE_NEWS + FACES_REDIRECT;
	}

	/**
	 * Gets the comment bean.
	 *
	 * @return the comment bean
	 */
	public CommentBean getCommentBean() {
		return commentBean;
	}

	/**
	 * Sets the comment bean.
	 *
	 * @param commentBean
	 *            the new comment bean
	 */
	public void setCommentBean(CommentBean commentBean) {
		this.commentBean = commentBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the comment id.
	 *
	 * @return the comment id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Sets the comment id.
	 *
	 * @param commentId
	 *            the new comment id
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

}
