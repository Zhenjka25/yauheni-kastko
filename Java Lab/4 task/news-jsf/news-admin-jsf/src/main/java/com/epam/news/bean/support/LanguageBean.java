package com.epam.news.bean.support;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * JSF bean which is used for localization support.
 */
@ManagedBean
@SessionScoped
public class LanguageBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String EN_US = "en_US";
	private static final String RU_RU = "ru_RU";

	private static Map<String, Locale> countries;

	private Locale locale;

	static {
		countries = new HashMap<String, Locale>();
		countries.put(EN_US, new Locale("en", "US"));
		countries.put(RU_RU, new Locale("ru", "RU"));
	}

	/**
	 * Inits the bean.
	 */
	@PostConstruct
	public void init() {
		locale = countries.get(EN_US);
	}

	/**
	 * Sets the EN locale.
	 */
	public void setLocaleEn() {
		locale = countries.get(EN_US);
	}

	/**
	 * Sets the RU locale.
	 */
	public void setLocaleRu() {
		locale = countries.get(RU_RU);
	}

	/**
	 * Gets the locale.
	 *
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Sets the locale.
	 *
	 * @param locale
	 *            the new locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

}
