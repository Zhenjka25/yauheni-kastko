package com.epam.news.bean.controller;

import static com.epam.news.constant.AdminJSFConstants.*;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.apache.shiro.authz.annotation.RequiresRoles;

import com.epam.news.bean.model.AuthorBean;
import com.epam.news.bean.support.ServiceBean;
import com.epam.news.dto.AuthorDTO;
import com.epam.news.exception.CustomOptimisticLockException;

/**
 * JSF controller for authors page.
 */
@ManagedBean
@RequestScoped
public class AuthorController {

	@ManagedProperty(value = BEAN_AUTHOR)
	private AuthorBean authorBean;

	@ManagedProperty(value = BEAN_SERVICE)
	private ServiceBean serviceBean;

	/**
	 * Updates author.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String updateAuthor() {
		try {
			serviceBean.getAuthorService().update(authorBean.getAuthorDTO());
		} catch (CustomOptimisticLockException e) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put(ATTRIBUTE_ERROR,
					ERROR_MESSAGE_OPTIM_LOCK_AUTHOR);
		}
		return PAGE_AUTHORS + FACES_REDIRECT;
	}

	/**
	 * Creates the author.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String createAuthor() {
		serviceBean.getAuthorService().save(authorBean.getAuthorDTO());
		return PAGE_AUTHORS + FACES_REDIRECT;
	}

	/**
	 * Expires author.
	 *
	 * @return the string
	 */
	@RequiresRoles({ ROLE_ADMIN })
	public String expireAuthor() {
		AuthorDTO authorDTO = authorBean.getAuthorDTO();
		authorDTO.setExpired(new Date());
		serviceBean.getAuthorService().update(authorDTO);
		return PAGE_AUTHORS + FACES_REDIRECT;
	}

	/**
	 * Gets the author bean.
	 *
	 * @return the author bean
	 */
	public AuthorBean getAuthorBean() {
		return authorBean;
	}

	/**
	 * Sets the author bean.
	 *
	 * @param authorBean
	 *            the new author bean
	 */
	public void setAuthorBean(AuthorBean authorBean) {
		this.authorBean = authorBean;
	}

	/**
	 * Gets the service bean.
	 *
	 * @return the service bean
	 */
	public ServiceBean getServiceBean() {
		return serviceBean;
	}

	/**
	 * Sets the service bean.
	 *
	 * @param serviceBean
	 *            the new service bean
	 */
	public void setServiceBean(ServiceBean serviceBean) {
		this.serviceBean = serviceBean;
	}

}
