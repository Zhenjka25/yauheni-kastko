package com.epam.news.constant;

/**
 * Contains all constants for application.
 */
public final class AdminJSFConstants {

	// Beans
	public static final String BEAN_NEWS_LIST_CONTROLLER = "#{newsListController}";
	public static final String BEAN_AUTHOR_LIST = "#{authorListBean}";
	public static final String BEAN_NEWS = "#{newsBean}";
	public static final String BEAN_NEWS_LIST = "#{newsListBean}";
	public static final String BEAN_TAG_LIST = "#{tagListBean}";
	public static final String BEAN_LANGUAGE = "#{languageBean}";
	public static final String BEAN_SEARCH_CRITERIA = "#{searchCriteriaBean}";
	public static final String BEAN_SERVICE = "#{serviceBean}";
	public static final String BEAN_COMMENT = "#{commentBean}";
	public static final String BEAN_AUTHOR = "#{authorBean}";
	public static final String BEAN_TAG = "#{tagBean}";

	// Attributes
	public static final String ATTRIBUTE_NEWS_ID = "newsId";
	public static final String ATTRIBUTE_TIME_ZONE_OFFSET = "timezoneOffset";
	public static final String ATTRIBUTE_USER_INFO = "userInfo";
	public static final String ATTRIBUTE_AUTHOR_ID = "authorId";
	public static final String ATTRIBUTE_ERROR = "error";
	public static final String ATTRIBUTE_SIDE_BAR_PAGE = "sideBarPage";

	// Params
	public static final String PARAM_SWITCH_TO = "#{param.switchTo}";
	public static final String PARAM_NEWS_ID = "#{param.newsId}";
	public static final String PARAM_COMMENT_ID = "#{param.commentId}";

	// Params String
	public static final String PARAM_LOGIN = "login";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_NEWS_ID_STRING = "newsId";
	public static final String PARAM_CLEAR_NEWS_ID = "clearNewsId";
	public static final String PARAM_AUTHOR_ID = "authorId";
	public static final String PARAM_TAG_ID = "tagId";
	public static final String PARAM_ERROR = "error";

	// Roles
	public static final String ROLE_ADMIN = "ROLE_ADMIN";

	// Beans string
	public static final String BEAN_NEWS_LIST_STRING = "newsListBean";
	public static final String BEAN_NEWS_STRING = "newsBean";

	public static final String SERVICE_IMPLEMENTATION = "service.implementation";

	// Pages
	public static final String PAGE_LOGIN = "login.xhtml";
	public static final String PAGE_NEWS_LIST = "news-list";
	public static final String PAGE_NEWS = "news";
	public static final String PAGE_AUTHORS = "edit-authors";
	public static final String PAGE_TAGS = "edit-tags";
	public static final String PAGE_ERROR = "error";
	public static final String PAGE_EDIT_NEWS = "edit-news";

	// Adds to result page, if redirect is needed
	public static final String FACES_REDIRECT = "?faces-redirect=true";

	// Cookies
	public static final String COOKIE_OFFSET = "timezoneOffset";

	// Error messages
	public static final String ERROR_MESSAGE_DEFAULT = "error.message.default";
	public static final String ERROR_MESSAGE_NOT_FOUND = "error.message.notfound";
	public static final String ERROR_MESSAGE_OPTIM_LOCK_NEWS = "error.message.optlock.news";
	public static final String ERROR_MESSAGE_OPTIM_LOCK_AUTHOR = "error.message.optlock.author";
	public static final String ERROR_MESSAGE_OPTIM_LOCK_TAG = "error.message.optlock.tag";
	public static final String ERROR_MESSAGE_LOGIN = "login.error.message";

}
