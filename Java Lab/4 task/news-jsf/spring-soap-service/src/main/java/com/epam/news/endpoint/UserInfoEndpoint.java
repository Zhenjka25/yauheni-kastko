package com.epam.news.endpoint;

import static com.epam.news.constant.SoapServiceConstants.NAMESPACE_URI_USER;

import org.apache.commons.codec.digest.DigestUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.*;

import com.epam.news.dto.UserInfoDTO;
import com.epam.news.service.UserInfoService;
import com.epam.news.ws.userinfo.GetUserInfoRequest;
import com.epam.news.ws.userinfo.GetUserInfoResponse;
import com.epam.news.ws.userinfo.UserInfo;

/**
 * Endpoint for user info resource.
 */
@Endpoint
public class UserInfoEndpoint {

	@Autowired
	private UserInfoService service;

	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * Gets the user info.
	 *
	 * @param request
	 *            the request
	 * @return the user info
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_USER, localPart = "getUserInfoRequest")
	@ResponsePayload
	public GetUserInfoResponse getUserInfo(@RequestPayload GetUserInfoRequest request) {
		GetUserInfoResponse response = new GetUserInfoResponse();

		UserInfo userInfoRequest = request.getUserInfo();

		String password = userInfoRequest.getPassword();
		password = DigestUtils.md5Hex(password);

		UserInfoDTO userInfoDTOResponse = service.getUserInfo(userInfoRequest.getLogin());

		if (userInfoDTOResponse != null && userInfoDTOResponse.getPassword().equals(password)) {
			response.setUserInfo(convert(userInfoDTOResponse));
		}

		return response;
	}

	/**
	 * Converts dto to xml entity.
	 *
	 * @param userInfoDTO
	 *            the user info dto
	 * @return the user info
	 */
	private UserInfo convert(UserInfoDTO userInfoDTO) {
		if (userInfoDTO == null) {
			return null;
		}
		return mapper.map(userInfoDTO, UserInfo.class);
	}

}
