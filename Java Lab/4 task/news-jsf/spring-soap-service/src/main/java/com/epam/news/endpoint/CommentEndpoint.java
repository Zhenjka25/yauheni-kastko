package com.epam.news.endpoint;

import static com.epam.news.constant.SoapServiceConstants.NAMESPACE_URI_COMMENT;
import static com.epam.news.constant.SoapServiceConstants.ROLE_ADMIN;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ws.server.endpoint.annotation.*;

import com.epam.news.dto.CommentDTO;
import com.epam.news.service.CommentService;
import com.epam.news.ws.comment.*;

/**
 * Endpoint for comments resource.
 */
@Endpoint
public class CommentEndpoint {

	@Autowired
	private CommentService commentService;

	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * Gets the all comments.
	 *
	 * @param request
	 *            the request
	 * @return the all comments
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_COMMENT, localPart = "getAllCommentsRequest")
	@ResponsePayload
	public GetAllCommentsResponse getAllComments(@RequestPayload GetAllCommentsRequest request) {
		GetAllCommentsResponse response = new GetAllCommentsResponse();
		List<CommentDTO> commentDTOList = commentService.findAll();
		for (CommentDTO commentDTO : commentDTOList) {
			response.getComments().add(convert(commentDTO));
		}
		return response;
	}

	/**
	 * Gets the comment by id.
	 *
	 * @param request
	 *            the request
	 * @return the comment by id
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_COMMENT, localPart = "getCommentByIdRequest")
	@ResponsePayload
	public GetCommentByIdResponse getCommentById(@RequestPayload GetCommentByIdRequest request) {
		GetCommentByIdResponse response = new GetCommentByIdResponse();
		response.setComment(convert(commentService.findById(request.getCommentId())));
		return response;
	}

	/**
	 * Creates the comment.
	 *
	 * @param request
	 *            the request
	 * @return the creates the comment response
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_COMMENT, localPart = "createCommentRequest")
	@ResponsePayload
	public CreateCommentResponse createComment(@RequestPayload CreateCommentRequest request) {
		CreateCommentResponse response = new CreateCommentResponse();
		CommentDTO commentDTO = convert(request.getComment());
		commentDTO = commentService.save(commentDTO);
		response.setCommentId(commentDTO.getCommentId());
		return response;
	}

	/**
	 * Updates comment.
	 *
	 * @param request
	 *            the request
	 * @return the update comment response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_COMMENT, localPart = "updateCommentRequest")
	@ResponsePayload
	public UpdateCommentResponse updateComment(@RequestPayload UpdateCommentRequest request) {
		UpdateCommentResponse response = new UpdateCommentResponse();
		CommentDTO commentDTO = convert(request.getComment());
		commentService.update(commentDTO);
		response.setComment(convert(commentDTO));
		return response;
	}

	/**
	 * Deletes comment.
	 *
	 * @param request
	 *            the request
	 * @return the delete comment response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_COMMENT, localPart = "deleteCommentRequest")
	@ResponsePayload
	public DeleteCommentResponse deleteComment(@RequestPayload DeleteCommentRequest request) {
		DeleteCommentResponse response = new DeleteCommentResponse();
		commentService.deleteById(request.getCommentId());
		return response;
	}

	/**
	 * Converts dto to xml entity.
	 *
	 * @param commentDTO
	 *            the comment dto
	 * @return the comment
	 */
	private Comment convert(CommentDTO commentDTO) {
		if (commentDTO == null) {
			return null;
		}
		return mapper.map(commentDTO, Comment.class);
	}

	/**
	 * Converts xml entity to dto.
	 *
	 * @param comment
	 *            the comment
	 * @return the comment dto
	 */
	private CommentDTO convert(Comment comment) {
		if (comment == null) {
			return null;
		}
		return mapper.map(comment, CommentDTO.class);
	}

}
