package com.epam.news.constant;

/**
 * Contains all constants needed for soap service.
 */
public final class SoapServiceConstants {

	// Name spaces
	public static final String NAMESPACE_URI_AUTHOR = "http://www.news.epam.com/ws/author";
	public static final String NAMESPACE_URI_TAG = "http://www.news.epam.com/ws/tag";
	public static final String NAMESPACE_URI_COMMENT = "http://www.news.epam.com/ws/comment";
	public static final String NAMESPACE_URI_NEWS = "http://www.news.epam.com/ws/news";
	public static final String NAMESPACE_URI_USER = "http://www.news.epam.com/ws/userInfo";

	// Ports
	public static final String PORT_AUTHOR = "AuthorPort";
	public static final String PORT_TAG = "TagPort";
	public static final String PORT_COMMENT = "CommentPort";
	public static final String PORT_NEWS = "NewsPort";
	public static final String PORT_USER = "UserPort";

	public static final String LOCATION_URI = "/ws";

	// Schemas
	public static final String SCHEMA_AUTHOR = "author.xsd";
	public static final String SCHEMA_TAG = "tag.xsd";
	public static final String SCHEMA_COMMENT = "comment.xsd";
	public static final String SCHEMA_NEWS = "news.xsd";
	public static final String SCHEMA_USER = "userInfo.xsd";

	// WSDL names
	public static final String WSDL_AUTHOR = "author";
	public static final String WSDL_TAG = "tag";
	public static final String WSDL_COMMENT = "comment";
	public static final String WSDL_NEWS = "news";
	public static final String WSDL_USER = "user";

	// Roles
	public static final String ROLE_ADMIN = "ROLE_ADMIN";

}
