package com.epam.news.endpoint;

import static com.epam.news.constant.SoapServiceConstants.NAMESPACE_URI_AUTHOR;
import static com.epam.news.constant.SoapServiceConstants.ROLE_ADMIN;

import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ws.server.endpoint.annotation.*;

import com.epam.news.dto.AuthorDTO;
import com.epam.news.service.AuthorService;
import com.epam.news.ws.author.*;

/**
 * Endpoint for authors resource.
 */
@Endpoint
public class AuthorEndpoint {

	@Autowired
	private AuthorService authorService;

	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * Gets the all authors.
	 *
	 * @param request
	 *            the request
	 * @return the all authors
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "getAllAuthorsRequest")
	@ResponsePayload
	public GetAllAuthorsResponse getAllAuthors(@RequestPayload GetAllAuthorsRequest request) {
		GetAllAuthorsResponse response = new GetAllAuthorsResponse();
		List<AuthorDTO> authorDTOList = authorService.findAll();
		for (AuthorDTO authorDTO : authorDTOList) {
			response.getAuthors().add(convert(authorDTO));
		}
		return response;
	}

	/**
	 * Gets the actual authors.
	 *
	 * @param request
	 *            the request
	 * @return the actual authors
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "getActualAuthorsRequest")
	@ResponsePayload
	public GetActualAuthorsResponse getActualAuthors(@RequestPayload GetActualAuthorsRequest request) {
		GetActualAuthorsResponse response = new GetActualAuthorsResponse();
		List<AuthorDTO> authorDTOList = authorService.findActualAuthors();
		for (AuthorDTO authorDTO : authorDTOList) {
			response.getAuthors().add(convert(authorDTO));
		}
		return response;
	}

	/**
	 * Gets the author by id.
	 *
	 * @param request
	 *            the request
	 * @return the author by id
	 * @throws DatatypeConfigurationException
	 *             the datatype configuration exception
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "getAuthorByIdRequest")
	@ResponsePayload
	public GetAuthorByIdResponse getAuthorById(@RequestPayload GetAuthorByIdRequest request)
			throws DatatypeConfigurationException {
		GetAuthorByIdResponse response = new GetAuthorByIdResponse();
		response.setAuthor(convert(authorService.findById(request.getAuthorId())));
		return response;
	}

	/**
	 * Creates the author.
	 *
	 * @param request
	 *            the request
	 * @return the creates the author response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "createAuthorRequest")
	@ResponsePayload
	public CreateAuthorResponse createAuthor(@RequestPayload CreateAuthorRequest request) {
		CreateAuthorResponse response = new CreateAuthorResponse();
		AuthorDTO authorDTO = convert(request.getAuthor());
		authorDTO = authorService.save(authorDTO);
		response.setAuthorId(authorDTO.getAuthorId());
		return response;
	}

	/**
	 * Updates author request.
	 *
	 * @param request
	 *            the request
	 * @return the update author response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "updateAuthorRequest")
	@ResponsePayload
	public UpdateAuthorResponse updateAuthorRequest(@RequestPayload UpdateAuthorRequest request) {
		UpdateAuthorResponse response = new UpdateAuthorResponse();
		AuthorDTO authorDTO = convert(request.getAuthor());
		authorService.update(authorDTO);
		response.setAuthor(convert(authorDTO));
		return response;
	}

	/**
	 * Deletes author.
	 *
	 * @param request
	 *            the request
	 * @return the delete author response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "deleteAuthorRequest")
	@ResponsePayload
	public DeleteAuthorResponse deleteAuthor(@RequestPayload DeleteAuthorRequest request) {
		DeleteAuthorResponse response = new DeleteAuthorResponse();
		authorService.deleteById(request.getAuthorId());
		return response;
	}

	/**
	 * Gets the author by name.
	 *
	 * @param request
	 *            the request
	 * @return the author by name
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_AUTHOR, localPart = "getAuthorByNameRequest")
	@ResponsePayload
	public GetAuthorByNameResponse getAuthorByName(@RequestPayload GetAuthorByNameRequest request) {
		GetAuthorByNameResponse response = new GetAuthorByNameResponse();
		response.setAuthor(convert(authorService.findByName(request.getAuthorName())));
		return response;
	}

	/**
	 * Converts dto to xml entity.
	 *
	 * @param authorDTO
	 *            the author dto
	 * @return the author
	 */
	private Author convert(AuthorDTO authorDTO) {
		if (authorDTO == null) {
			return null;
		}
		return mapper.map(authorDTO, Author.class);
	}

	/**
	 * Converts xml entity to dto.
	 *
	 * @param author
	 *            the author
	 * @return the author dto
	 */
	private AuthorDTO convert(Author author) {
		if (author == null) {
			return null;
		}
		return mapper.map(author, AuthorDTO.class);
	}

}