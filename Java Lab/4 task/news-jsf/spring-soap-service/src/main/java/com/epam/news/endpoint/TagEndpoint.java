package com.epam.news.endpoint;

import static com.epam.news.constant.SoapServiceConstants.NAMESPACE_URI_TAG;
import static com.epam.news.constant.SoapServiceConstants.ROLE_ADMIN;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ws.server.endpoint.annotation.*;

import com.epam.news.dto.TagDTO;
import com.epam.news.service.TagService;
import com.epam.news.ws.tag.*;

/**
 * Endpoint for tags resource.
 */
@Endpoint
public class TagEndpoint {

	@Autowired
	private TagService tagService;

	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * Gets the all tags.
	 *
	 * @param request
	 *            the request
	 * @return the all tags
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "getAllTagsRequest")
	@ResponsePayload
	public GetAllTagsResponse getAllTags(@RequestPayload GetAllTagsRequest request) {
		GetAllTagsResponse response = new GetAllTagsResponse();
		List<TagDTO> tagDTOList = tagService.findAll();
		for (TagDTO tagDTO : tagDTOList) {
			response.getTags().add(convert(tagDTO));
		}
		return response;
	}

	/**
	 * Gets the tag by id.
	 *
	 * @param request
	 *            the request
	 * @return the tag by id
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "getTagByIdRequest")
	@ResponsePayload
	public GetTagByIdResponse getTagById(@RequestPayload GetTagByIdRequest request) {
		GetTagByIdResponse response = new GetTagByIdResponse();
		response.setTag(convert(tagService.findById(request.getTagId())));
		return response;
	}

	/**
	 * Creates the tag.
	 *
	 * @param request
	 *            the request
	 * @return the creates the tag response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "createTagRequest")
	@ResponsePayload
	public CreateTagResponse createTag(@RequestPayload CreateTagRequest request) {
		CreateTagResponse response = new CreateTagResponse();
		TagDTO tagDTO = tagService.save(convert(request.getTag()));
		response.setTagId(tagDTO.getTagId());
		return response;
	}

	/**
	 * Updates tag.
	 *
	 * @param request
	 *            the request
	 * @return the update tag response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "updateTagRequest")
	@ResponsePayload
	public UpdateTagResponse updateTag(@RequestPayload UpdateTagRequest request) {
		UpdateTagResponse response = new UpdateTagResponse();
		TagDTO tagDTO = convert(request.getTag());
		tagService.update(tagDTO);
		response.setTag(convert(tagDTO));
		return response;
	}

	/**
	 * Deletes tag.
	 *
	 * @param request
	 *            the request
	 * @return the delete tag response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "deleteTagRequest")
	@ResponsePayload
	public DeleteTagResponse deleteTag(@RequestPayload DeleteTagRequest request) {
		DeleteTagResponse response = new DeleteTagResponse();
		tagService.deleteById(request.getTagId());
		return response;
	}

	/**
	 * Gets the tag by name.
	 *
	 * @param request
	 *            the request
	 * @return the tag by name
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "getTagByNameRequest")
	@ResponsePayload
	public GetTagByNameResponse getTagByName(@RequestPayload GetTagByNameRequest request) {
		GetTagByNameResponse response = new GetTagByNameResponse();
		response.setTag(convert(tagService.findByName(request.getTagName())));
		return response;
	}

	/**
	 * Gets the tags by id list.
	 *
	 * @param request
	 *            the request
	 * @return the tags by id list
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_TAG, localPart = "getTagsByIdListRequest")
	@ResponsePayload
	public GetTagsByIdListResponse getTagsByIdList(@RequestPayload GetTagsByIdListRequest request) {
		GetTagsByIdListResponse response = new GetTagsByIdListResponse();
		List<TagDTO> tagDTOList = new ArrayList<>();

		for (Tag tag : request.getTags()) {
			tagDTOList.add(convert(tag));
		}

		tagDTOList = tagService.findByTagIdList(tagDTOList);

		for (TagDTO tagDTO : tagDTOList) {
			response.getTags().add(convert(tagDTO));
		}

		return response;
	}

	/**
	 * Converts dto to xml entity.
	 *
	 * @param tagDTO
	 *            the tag dto
	 * @return the tag
	 */
	private Tag convert(TagDTO tagDTO) {
		if (tagDTO == null) {
			return null;
		}
		return mapper.map(tagDTO, Tag.class);
	}

	/**
	 * Converts xml entity to dto.
	 *
	 * @param tag
	 *            the tag
	 * @return the tag dto
	 */
	private TagDTO convert(Tag tag) {
		if (tag == null) {
			return null;
		}
		return mapper.map(tag, TagDTO.class);
	}

}
