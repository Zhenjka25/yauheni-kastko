package com.epam.news.config;

import org.springframework.ws.transport.http.support.AbstractAnnotationConfigMessageDispatcherServletInitializer;

/**
 * Initializes message servlet for soap service.
 */
public class WebInitializer extends AbstractAnnotationConfigMessageDispatcherServletInitializer {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { SoapAppConfig.class, SecurityConfig.class };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/ws/*" };
	}

}
