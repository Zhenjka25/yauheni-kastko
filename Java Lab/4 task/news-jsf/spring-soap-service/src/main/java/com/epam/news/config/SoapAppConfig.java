package com.epam.news.config;

import static com.epam.news.constant.SoapServiceConstants.*;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

/**
 * Registers all beans for soap service.
 */
@EnableWs
@Configuration
@Import({ ApplicationConfigJPA.class })
@ComponentScan("com.epam.news.endpoint")
public class SoapAppConfig extends WsConfigurerAdapter {

	/**
	 * Author wsdl11 definition.
	 *
	 * @return the default wsdl11 definition
	 */
	@Bean(name = WSDL_AUTHOR)
	public DefaultWsdl11Definition authorWsdl11Definition() {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName(PORT_AUTHOR);
		wsdl11Definition.setLocationUri(LOCATION_URI);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI_AUTHOR);
		wsdl11Definition.setSchema(authorSchema());
		return wsdl11Definition;
	}

	/**
	 * Tag wsdl11 definition.
	 *
	 * @return the default wsdl11 definition
	 */
	@Bean(name = WSDL_TAG)
	public DefaultWsdl11Definition tagWsdl11Definition() {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName(PORT_TAG);
		wsdl11Definition.setLocationUri(LOCATION_URI);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI_TAG);
		wsdl11Definition.setSchema(tagSchema());
		return wsdl11Definition;
	}

	/**
	 * Comment wsdl11 definition.
	 *
	 * @return the default wsdl11 definition
	 */
	@Bean(name = WSDL_COMMENT)
	public DefaultWsdl11Definition commentWsdl11Definition() {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName(PORT_COMMENT);
		wsdl11Definition.setLocationUri(LOCATION_URI);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI_COMMENT);
		wsdl11Definition.setSchema(commentSchema());
		return wsdl11Definition;
	}

	/**
	 * News wsdl11 definition.
	 *
	 * @return the default wsdl11 definition
	 */
	@Bean(name = WSDL_NEWS)
	public DefaultWsdl11Definition newsWsdl11Definition() {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName(PORT_NEWS);
		wsdl11Definition.setLocationUri(LOCATION_URI);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI_NEWS);
		wsdl11Definition.setSchema(newsSchema());
		return wsdl11Definition;
	}

	/**
	 * User wsdl11 definition.
	 *
	 * @return the default wsdl11 definition
	 */
	@Bean(name = WSDL_USER)
	public DefaultWsdl11Definition userWsdl11Definition() {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName(PORT_USER);
		wsdl11Definition.setLocationUri(LOCATION_URI);
		wsdl11Definition.setTargetNamespace(NAMESPACE_URI_USER);
		wsdl11Definition.setSchema(userSchema());
		return wsdl11Definition;
	}

	/**
	 * Author schema.
	 *
	 * @return the xsd schema
	 */
	@Bean(name = SCHEMA_AUTHOR)
	public XsdSchema authorSchema() {
		return new SimpleXsdSchema(new ClassPathResource(SCHEMA_AUTHOR));
	}

	/**
	 * Tag schema.
	 *
	 * @return the xsd schema
	 */
	@Bean(name = SCHEMA_TAG)
	public XsdSchema tagSchema() {
		return new SimpleXsdSchema(new ClassPathResource(SCHEMA_TAG));
	}

	/**
	 * Comment schema.
	 *
	 * @return the xsd schema
	 */
	@Bean(name = SCHEMA_COMMENT)
	public XsdSchema commentSchema() {
		return new SimpleXsdSchema(new ClassPathResource(SCHEMA_COMMENT));
	}

	/**
	 * News schema.
	 *
	 * @return the xsd schema
	 */
	@Bean(name = SCHEMA_NEWS)
	public XsdSchema newsSchema() {
		return new SimpleXsdSchema(new ClassPathResource(SCHEMA_NEWS));
	}

	/**
	 * User schema.
	 *
	 * @return the xsd schema
	 */
	@Bean(name = SCHEMA_USER)
	public XsdSchema userSchema() {
		return new SimpleXsdSchema(new ClassPathResource(SCHEMA_USER));
	}

	/**
	 * Dozer bean mapper.
	 *
	 * @return the dozer bean mapper
	 */
	@Bean
	public DozerBeanMapper dozerBeanMapper() {
		DozerBeanMapper mapper = new DozerBeanMapper();
		List<String> mappingList = new ArrayList<>();
		mappingList.add("dozerMappingSOAPServer.xml");
		mapper.setMappingFiles(mappingList);
		return mapper;
	}

}