package com.epam.news.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Registers Spring Security filters.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
