package com.epam.news.endpoint;

import static com.epam.news.constant.SoapServiceConstants.NAMESPACE_URI_NEWS;
import static com.epam.news.constant.SoapServiceConstants.ROLE_ADMIN;

import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.ws.server.endpoint.annotation.*;

import com.epam.news.dto.NewsDTO;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;
import com.epam.news.ws.news.*;

/**
 * Endpoint for news resource.
 */
@Endpoint
public class NewsEndpoint {

	@Autowired
	private NewsService newsService;

	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * Gets the all news.
	 *
	 * @param request
	 *            the request
	 * @return the all news
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "getAllNewsRequest")
	@ResponsePayload
	public GetAllNewsResponse getAllNews(@RequestPayload GetAllNewsRequest request) {
		GetAllNewsResponse response = new GetAllNewsResponse();
		List<NewsDTO> newsDTOList = newsService.findAll();
		for (NewsDTO newsDTO : newsDTOList) {
			response.getNewsList().add(convert(newsDTO));
		}
		return response;
	}

	/**
	 * Counts news.
	 *
	 * @param request
	 *            the request
	 * @return the count news response
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "countNewsRequest")
	@ResponsePayload
	public CountNewsResponse countNews(@RequestPayload CountNewsRequest request) {
		CountNewsResponse response = new CountNewsResponse();
		response.setNewsAmount(newsService.countNews());
		return response;
	}

	/**
	 * Counts news by search criteria.
	 *
	 * @param request
	 *            the request
	 * @return the count news by search criteria response
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "countNewsBySearchCriteriaRequest")
	@ResponsePayload
	public CountNewsBySearchCriteriaResponse countNewsBySearchCriteria(
			@RequestPayload CountNewsBySearchCriteriaRequest request) {
		CountNewsBySearchCriteriaResponse response = new CountNewsBySearchCriteriaResponse();
		SearchCriteria searchCriteria = convert(request.getSearchCriteria());
		response.setNewsAmount(newsService.countNewsBySearchCriteria(searchCriteria));
		return response;
	}

	/**
	 * Gets the news by id.
	 *
	 * @param request
	 *            the request
	 * @return the news by id
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "getNewsByIdRequest")
	@ResponsePayload
	public GetNewsByIdResponse getNewsById(@RequestPayload GetNewsByIdRequest request) {
		GetNewsByIdResponse response = new GetNewsByIdResponse();
		response.setNews(convert(newsService.findById(request.getNewsId())));
		return response;
	}

	/**
	 * Deletes news.
	 *
	 * @param request
	 *            the request
	 * @return the delete news response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "deleteNewsRequest")
	@ResponsePayload
	public DeleteNewsResponse deleteNews(@RequestPayload DeleteNewsRequest request) {
		DeleteNewsResponse response = new DeleteNewsResponse();
		newsService.deleteById(request.getNewsId());
		return response;
	}

	/**
	 * Gets the next news.
	 *
	 * @param request
	 *            the request
	 * @return the next news
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "getNextNewsRequest")
	@ResponsePayload
	public GetNextNewsResponse getNextNews(@RequestPayload GetNextNewsRequest request) {
		GetNextNewsResponse response = new GetNextNewsResponse();
		SearchCriteria searchCriteria = convert(request.getSearchCriteria());
		response.setNews(convert(newsService.findNextNews(request.getNewsId(), searchCriteria)));
		return response;
	}

	/**
	 * Gets the prev news.
	 *
	 * @param request
	 *            the request
	 * @return the prev news
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "getPrevNewsRequest")
	@ResponsePayload
	public GetPrevNewsResponse getPrevNews(@RequestPayload GetPrevNewsRequest request) {
		GetPrevNewsResponse response = new GetPrevNewsResponse();
		SearchCriteria searchCriteria = convert(request.getSearchCriteria());
		response.setNews(convert(newsService.findPrevNews(request.getNewsId(), searchCriteria)));
		return response;
	}

	/**
	 * Creates the news.
	 *
	 * @param request
	 *            the request
	 * @return the creates the news response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "createNewsRequest")
	@ResponsePayload
	public CreateNewsResponse createNews(@RequestPayload CreateNewsRequest request) {
		CreateNewsResponse response = new CreateNewsResponse();
		NewsDTO newsDTO = convert(request.getNews());
		newsDTO = newsService.save(newsDTO);
		response.setNewsId(newsDTO.getNewsId());
		return response;
	}

	/**
	 * Updates news.
	 *
	 * @param request
	 *            the request
	 * @return the update news response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "updateNewsRequest")
	@ResponsePayload
	public UpdateNewsResponse updateNews(@RequestPayload UpdateNewsRequest request) {
		UpdateNewsResponse response = new UpdateNewsResponse();
		NewsDTO newsDTO = convert(request.getNews());
		newsService.update(newsDTO);
		response.setNews(convert(newsDTO));
		return response;
	}

	/**
	 * Deletes news list.
	 *
	 * @param request
	 *            the request
	 * @return the delete news list response
	 */
	@Secured({ ROLE_ADMIN })
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "deleteNewsListRequest")
	@ResponsePayload
	public DeleteNewsListResponse deleteNewsList(@RequestPayload DeleteNewsListRequest request) {
		DeleteNewsListResponse response = new DeleteNewsListResponse();
		newsService.deleteList(request.getNewsIdList());
		return response;
	}

	/**
	 * Gets the news list by search criteria.
	 *
	 * @param request
	 *            the request
	 * @return the news list by search criteria
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "getNewsListBySearchCriteriaRequest")
	@ResponsePayload
	public GetNewsListBySearchCriteriaResponse getNewsListBySearchCriteria(
			@RequestPayload GetNewsListBySearchCriteriaRequest request) {
		GetNewsListBySearchCriteriaResponse response = new GetNewsListBySearchCriteriaResponse();
		SearchCriteria searchCriteria = convert(request.getSearchCriteria());
		List<NewsDTO> newsDTOList = newsService.findBySearchCriteria(searchCriteria);
		for (NewsDTO newsDTO : newsDTOList) {
			response.getNewsList().add(convert(newsDTO));
		}
		return response;
	}

	/**
	 * Finds id list by search criteria.
	 *
	 * @param request
	 *            the request
	 * @return the find id list by search criteria response
	 */
	@PayloadRoot(namespace = NAMESPACE_URI_NEWS, localPart = "findIdListBySearchCriteriaRequest")
	@ResponsePayload
	public FindIdListBySearchCriteriaResponse findIdListBySearchCriteria(
			@RequestPayload FindIdListBySearchCriteriaRequest request) {
		FindIdListBySearchCriteriaResponse response = new FindIdListBySearchCriteriaResponse();
		SearchCriteria searchCriteria = convert(request.getSearchCriteria());
		response.getNewsIdList().addAll(newsService.findIdListBySearchCriteria(searchCriteria));
		return response;
	}

	/**
	 * Converts dto to xml entity.
	 *
	 * @param newsDTO
	 *            the news dto
	 * @return the news
	 */
	private News convert(NewsDTO newsDTO) {
		if (newsDTO == null) {
			return null;
		}
		return mapper.map(newsDTO, News.class);
	}

	/**
	 * Converts xml entity to dto.
	 *
	 * @param news
	 *            the news
	 * @return the news dto
	 */
	private NewsDTO convert(News news) {
		if (news == null) {
			return null;
		}
		return mapper.map(news, NewsDTO.class);
	}

	/**
	 * Convert.
	 *
	 * @param searchCriteriaXML
	 *            the search criteria xml
	 * @return the search criteria
	 */
	private SearchCriteria convert(SearchCriteriaXML searchCriteriaXML) {
		if (searchCriteriaXML == null) {
			return null;
		}
		return mapper.map(searchCriteriaXML, SearchCriteria.class);
	}

}
