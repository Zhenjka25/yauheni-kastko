package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dto.CommentDTO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DTOIntegrityException;
import com.epam.news.service.CommentService;

/**
 * All methods work in transactional context.
 */
@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDAO commentDAO;

	@Autowired
	private NewsDAO newsDAO;

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	private Validator validator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CommentDTO save(CommentDTO entity) {
		validate(entity);
		Comment comment = reverse(entity);
		comment.setNews(newsDAO.getById(comment.getNewsId()));
		return reverse(commentDAO.saveOrUpdate(comment));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public CommentDTO findById(long id) {
		return reverse(commentDAO.getById(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(CommentDTO entity) {
		validate(entity);
		commentDAO.saveOrUpdate(reverse(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		commentDAO.deleteById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<CommentDTO> findAll() {
		List<CommentDTO> commentDTOList = new ArrayList<>();
		List<Comment> commentList = commentDAO.getAll();
		for (Comment comment : commentList) {
			commentDTOList.add(reverse(comment));
		}
		return commentDTOList;
	}

	private Comment reverse(CommentDTO commentDTO) {
		if (commentDTO == null) {
			return null;
		}
		return mapper.map(commentDTO, Comment.class);
	}

	private CommentDTO reverse(Comment comment) {
		if (comment == null) {
			return null;
		}
		return mapper.map(comment, CommentDTO.class);
	}

	private void validate(CommentDTO commentDTO) {
		DataBinder binder = new DataBinder(commentDTO);
		binder.setValidator(validator);
		binder.validate();
		BindingResult results = binder.getBindingResult();
		if (results.hasErrors()) {
			throw new DTOIntegrityException(results);
		}
	}

}
