package com.epam.news.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.entity.Tag_;

/**
 *JPA implementation of TagDAO interface.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class TagDAOJPA implements TagDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag saveOrUpdate(Tag entity) {
		entityManagerFactory.getCache().evict(Tag.class, entity.getTagId());
		try {
			entity = entityManager.merge(entity);
		} catch (OptimisticLockException e) {
			// For EclipseLink
			throw new OptimisticLockException(entity);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag getById(long id) {
		return entityManager.find(Tag.class, new Long(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		Tag tagToDelete = entityManager.getReference(Tag.class, id);
		entityManager.remove(tagToDelete);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tag> getAll() {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tag> criteria = builder.createQuery(Tag.class);
		Root<Tag> tagRoot = criteria.from(Tag.class);
		criteria.select(tagRoot);
		return entityManager.createQuery(criteria).getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag getByName(String name) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tag> criteria = builder.createQuery(Tag.class);
		Root<Tag> tagRoot = criteria.from(Tag.class);

		criteria.where(builder.equal(tagRoot.get(Tag_.tagName), name));

		return getSingleOrNoneResult(criteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tag> getByTagIdList(List<Tag> tagList) {

		// tagList contains tags with id only, other fields are null
		List<Long> idList = new ArrayList<>();
		for (Tag tag : tagList) {
			idList.add(tag.getTagId());
		}

		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tag> criteria = builder.createQuery(Tag.class);
		Root<Tag> tagRoot = criteria.from(Tag.class);

		criteria.where(tagRoot.get(Tag_.tagId).in(idList));

		return entityManager.createQuery(criteria).getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag getByFieldValues(Tag tag) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tag> criteria = builder.createQuery(Tag.class);
		Root<Tag> tagRoot = criteria.from(Tag.class);

		criteria.where(builder.equal(tagRoot.get(Tag_.tagName), tag.getTagName()));

		return getSingleOrNoneResult(criteria);
	}

	/**
	 * Gets the single or null result.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the single or none result
	 */
	private Tag getSingleOrNoneResult(CriteriaQuery<Tag> criteria) {
		List<Tag> tagList = entityManager.createQuery(criteria).getResultList();
		if (!tagList.isEmpty()) {
			return tagList.get(0);
		}
		return null;
	}

}
