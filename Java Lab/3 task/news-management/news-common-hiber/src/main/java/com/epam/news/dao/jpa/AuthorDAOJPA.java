package com.epam.news.dao.jpa;

import java.util.List;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.Author_;

/**
 * JPA implementation of AuthorDAO interface.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class AuthorDAOJPA implements AuthorDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author saveOrUpdate(Author entity) {
		entityManagerFactory.getCache().evict(Author.class, entity.getAuthorId());
		try {
			entity = entityManager.merge(entity);
		} catch (OptimisticLockException e) {
			// For EclipseLink
			throw new OptimisticLockException(entity);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author getById(long id) {
		return entityManager.find(Author.class, new Long(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		Author authorToDelete = entityManager.getReference(Author.class, id);
		entityManager.remove(authorToDelete);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Author> getAll() {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Author> criteria = builder.createQuery(Author.class);
		Root<Author> authorRoot = criteria.from(Author.class);
		criteria.select(authorRoot);

		return entityManager.createQuery(criteria).getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author getByFieldValues(Author author) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Author> criteria = builder.createQuery(Author.class);
		Root<Author> authorRoot = criteria.from(Author.class);

		criteria.where(builder.equal(authorRoot.get(Author_.authorName), author.getAuthorName()));

		return getSingleOrNoneResult(criteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author getByName(String name) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Author> criteria = builder.createQuery(Author.class);
		Root<Author> authorRoot = criteria.from(Author.class);

		criteria.where(builder.equal(authorRoot.get(Author_.authorName), name));

		return getSingleOrNoneResult(criteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Author> getActualAuthors() {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Author> criteria = builder.createQuery(Author.class);
		Root<Author> authorRoot = criteria.from(Author.class);

		criteria.where(builder.isNull(authorRoot.get(Author_.expired)));

		return entityManager.createQuery(criteria).getResultList();
	}

	/**
	 * Gets the single or null result from query.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the single or none result
	 */
	private Author getSingleOrNoneResult(CriteriaQuery<Author> criteria) {
		List<Author> authorList = entityManager.createQuery(criteria).getResultList();
		if (!authorList.isEmpty()) {
			return authorList.get(0);
		}
		return null;
	}

}
