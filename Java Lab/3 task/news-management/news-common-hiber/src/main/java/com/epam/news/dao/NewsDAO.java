package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.util.SearchCriteria;

/**
 * Classes that implements this interface, interacts with
 * the table "NEWS" in the database.
 */
public interface NewsDAO extends BasicDAO<News> {

	/**
	 * Finds entity by search criteria.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the list
	 */
	List<News> findBySearchCriteria(SearchCriteria searchObject);

	/**
	 * Counts all news.
	 *
	 * @return the long
	 */
	Long countNews();

	/**
	 * Counts news entities by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the int
	 */
	int countNewsBySearchCriteria(SearchCriteria searchCriteria);

	/**
	 * Finds list which contains id by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 */
	List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria);

	/**
	 * Finds previous news.
	 *
	 * @param newsId
	 *            the current news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 */
	News findPrevNews(Long newsId, SearchCriteria searchCriteria);

	/**
	 * Finds next news.
	 *
	 * @param newsId
	 *            the current news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 */
	News findNextNews(Long newsId, SearchCriteria searchCriteria);

	/**
	 * Deletes news list by its id.
	 *
	 * @param newsIdList
	 *            the news id list
	 */
	void deleteList(List<Long> newsIdList);
	
	/**
	 * Gets entity from database by field values or null.
	 *
	 * @param entity
	 *            the entity
	 * @return the by field values
	 */
	News getByFieldValues(News news);

}
