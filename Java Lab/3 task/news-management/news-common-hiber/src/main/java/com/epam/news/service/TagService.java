package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.TagDTO;

/**
 * Interacts with tagDAO.
 */
public interface TagService extends BasicService<TagDTO> {

	/**
	 * Finds entity by name.
	 *
	 * @param name
	 *            the name
	 * @return the tag
	 */
	TagDTO findByName(String name);

	/**
	 * Finds tag list by list of tag id.
	 *
	 * @param tagList
	 *            the tag list
	 * @return the list
	 */
	List<TagDTO> findByTagIdList(List<TagDTO> tagList);

}
