package com.epam.news.entity;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 * The Class Author. Represents news author business entity. Can be expired.
 */
@Entity
@Table(name = "AUTHOR")
public class Author implements Serializable {

	private static final long serialVersionUID = -5426506486010687516L;

	/** The author id. Primary key for author table in database. */
	@Id
	@SequenceGenerator(name = "AUTHOR_SEQ", sequenceName = "AUTHOR_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "AUTHOR_SEQ")
	@Column(name = "AUTHOR_ID", nullable = false)
	private Long authorId;

	/** The author name. Displayed with news, written by this author */
	@Column(name = "AUTHOR_NAME", nullable = false)
	private String authorName;

	/**
	 * The expired date. Any author can be expired. After that he cannot write
	 * news but he still does displayed with news, written by him earlier.
	 */
	@Column(name = "EXPIRED")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date expired;

	@Version
	@Column(name = "VERSION", nullable = false)
	private Long version;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "authors")
	private Set<News> news = new HashSet<>();

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the author name.
	 *
	 * @return the author name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the author name.
	 *
	 * @param authorName
	 *            the new author name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Gets the expired.
	 *
	 * @return the expired
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Sets the expired.
	 *
	 * @param expired
	 *            the new expired
	 */
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public Set<News> getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news
	 *            the new news
	 */
	public void setNews(Set<News> news) {
		this.news = news;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName + ", expired=" + expired + ", version="
				+ version + "]";
	}

}
