package com.epam.news.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tag.class)
public abstract class Tag_ {

	public static volatile SetAttribute<Tag, News> news;
	public static volatile SingularAttribute<Tag, Long> tagId;
	public static volatile SingularAttribute<Tag, String> tagName;
	public static volatile SingularAttribute<Tag, Long> version;

}

