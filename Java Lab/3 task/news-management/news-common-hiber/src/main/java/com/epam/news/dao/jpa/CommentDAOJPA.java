package com.epam.news.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.entity.Comment_;
import com.epam.news.entity.News;

/**
 * JPA implementation of CommentDAO interface.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class CommentDAOJPA implements CommentDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Comment saveOrUpdate(Comment entity) {
		entity = entityManager.merge(entity);
		entityManagerFactory.getCache().evict(News.class, entity.getNews().getNewsId());
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Comment getById(long id) {
		return entityManager.find(Comment.class, new Long(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		Comment commentToDelete = entityManager.getReference(Comment.class, id);
		entityManager.remove(commentToDelete);
		entityManagerFactory.getCache().evict(News.class, commentToDelete.getNewsId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Comment> getAll() {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Comment> criteria = builder.createQuery(Comment.class);
		Root<Comment> commentRoot = criteria.from(Comment.class);

		criteria.orderBy(builder.desc(commentRoot.get(Comment_.creationDate)));

		return entityManager.createQuery(criteria).getResultList();
	}

}
