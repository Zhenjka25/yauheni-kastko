package com.epam.news.config;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * The listener interface for receiving applicationContext events. The class
 * that is interested in processing a applicationContext event implements this
 * interface, and the object created with that class is registered with a
 * component using the component's <code>addApplicationContextListener
 * <code> method. When the applicationContext event occurs, that object's
 * appropriate method is invoked.
 *
 * @see ApplicationContextEvent
 */
@Component
public class ApplicationContextListener {

	public static final String PATH = "log4j.properties";

	/**
	 * Handle context refreshing. This event is invoked, when application
	 * context is created or refreshed.
	 *
	 * @param event
	 *            the event
	 */
	@EventListener
	public void handleContextRefresh(ContextRefreshedEvent event) {
		PropertyConfigurator.configure(PATH);
	}

}
