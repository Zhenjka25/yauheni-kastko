package com.epam.news.service.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.UserInfoDAO;
import com.epam.news.dto.UserInfoDTO;
import com.epam.news.entity.UserInfo;
import com.epam.news.service.UserInfoService;

/**
 * All methods work in transactional context.
 */
@Service
@Transactional(readOnly = true)
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	private UserInfoDAO userInfoDAO;

	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInfoDTO getUserInfo(String username) {
		return convert(userInfoDAO.getUserInfo(username));
	}

	private UserInfoDTO convert(UserInfo userInfo) {
		if (userInfo == null) {
			return null;
		}
		return mapper.map(userInfo, UserInfoDTO.class);
	}

}
