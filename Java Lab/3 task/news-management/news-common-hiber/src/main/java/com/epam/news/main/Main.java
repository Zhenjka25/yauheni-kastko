package com.epam.news.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.news.config.ApplicationConfigJPA;
import com.epam.news.dto.CommentDTO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.dto.TagDTO;
import com.epam.news.service.*;

public class Main {

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfigJPA.class);
		context.getEnvironment().setActiveProfiles("hibernate");
		NewsService newsService = context.getBean(NewsService.class);
		AuthorService authorService = context.getBean(AuthorService.class);
		TagService tagService = context.getBean(TagService.class);
		CommentService commentService = context.getBean(CommentService.class);
		NewsDTO newsDTO = newsService.findById(41L);
		System.out.println(newsDTO);
		System.out.println(newsDTO.getAuthor());
		System.out.println(newsDTO.getTags());
		System.out.println(newsDTO.getComments());
		testOptLock(authorService, tagService, newsService);
		checkValidation(commentService);
	}

	private static void testOptLock(AuthorService authorService, TagService tagService, NewsService newsService) {
		NewsDTO newsDTO = new NewsDTO();
		newsDTO.setTitle("Hahaha");
		newsDTO.setCreationDate(new Date());
		newsDTO.setModificationDate(new Date());
		newsDTO.setShortText("Hahahaha");
		newsDTO.setFullText("Hahahahha");
		newsDTO.setAuthor(authorService.findById(35L));
		List<TagDTO> tagDtoList = new ArrayList<>();
		tagDtoList.add(tagService.findById(72L));
		tagDtoList.add(tagService.findById(93L));
		newsDTO.setTags(tagDtoList);
		newsDTO = newsService.save(newsDTO);
		System.out.println(newsDTO);
	}
	
	private static void checkValidation(CommentService commentService) {
		CommentDTO commentDTO = new CommentDTO();
		commentDTO.setCommentText("aa");
		commentService.save(commentDTO);
	}

}
