package com.epam.news.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserInfo.class)
public abstract class UserInfo_ {

	public static volatile SingularAttribute<UserInfo, String> password;
	public static volatile SingularAttribute<UserInfo, String> role;
	public static volatile SingularAttribute<UserInfo, String> login;
	public static volatile SingularAttribute<UserInfo, Long> userId;
	public static volatile SingularAttribute<UserInfo, String> username;

}

