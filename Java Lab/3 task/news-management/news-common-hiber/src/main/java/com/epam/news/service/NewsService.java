package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.NewsDTO;
import com.epam.news.util.SearchCriteria;

/**
 * Interacts with newsDAO.
 */
public interface NewsService extends BasicService<NewsDTO> {

	/**
	 * Finds news entity by search criteria.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the list
	 */
	List<NewsDTO> findBySearchCriteria(SearchCriteria searchObject);

	/**
	 * Counts all news.
	 *
	 * @return the long
	 */
	Long countNews();

	/**
	 * Counts news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the int
	 */
	int countNewsBySearchCriteria(SearchCriteria searchCriteria);

	/**
	 * Finds list which contains news id by search criteria object.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 */
	List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria);

	/**
	 * Finds previous news entity.
	 *
	 * @param newsId
	 *            the current news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 */
	NewsDTO findPrevNews(Long newsId, SearchCriteria searchCriteria);

	/**
	 * Finds next news entity.
	 *
	 * @param newsId
	 *            the current news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 */
	NewsDTO findNextNews(Long newsId, SearchCriteria searchCriteria);

	/**
	 * Deletes list of news.
	 *
	 * @param newsIdList
	 *            the news id list
	 */
	void deleteList(List<Long> newsIdList);

}
