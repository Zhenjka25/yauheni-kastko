package com.epam.news.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The Class CommentDTO.
 */
public class CommentDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long commentId;

	@NotNull
	@Size(min = 3, max = 100, message = "{commenttext.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{commenttext.error.pattern}")
	private String commentText;

	private Date creationDate;

	private Long newsId;

	/**
	 * Instantiates a new comment dto.
	 *
	 * @param commentId
	 *            the comment id
	 * @param commentText
	 *            the comment text
	 * @param creationDate
	 *            the creation date
	 * @param newsId
	 *            the news id
	 */
	public CommentDTO(Long commentId, String commentText, Date creationDate, Long newsId) {
		this.commentId = commentId;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	/**
	 * Instantiates a new comment dto.
	 */
	public CommentDTO() {

	}

	/**
	 * Gets the comment id.
	 *
	 * @return the comment id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Sets the comment id.
	 *
	 * @param commentId
	 *            the new comment id
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the comment text
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text.
	 *
	 * @param commentText
	 *            the new comment text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentDTO other = (CommentDTO) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "CommentDTO [commentId=" + commentId + ", commentText=" + commentText + ", creationDate=" + creationDate
				+ ", newsId=" + newsId + "]";
	}

}
