package com.epam.news.entity;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The Class Tag. Represents tag business entity. Can be deleted.
 */
@Entity
@Table(name = "TAG")
public class Tag implements Serializable {

	private static final long serialVersionUID = 5007970396419349770L;

	/** The tag id. Primary key for tag table in database. */
	@Id
	@SequenceGenerator(name = "TAG_SEQ", sequenceName = "TAG_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "TAG_SEQ")
	@Column(name = "TAG_ID", nullable = false)
	private Long tagId;

	/** The tag name. Displayed with news, to which refers this tag. */
	@NotNull
	@Size(min = 3, max = 30, message = "{tagname.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{tagname.error.pattern}")
	@Column(name = "TAG_NAME", nullable = false)
	private String tagName;

	@Version
	@Column(name = "VERSION", nullable = false)
	private Long version;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "tags")
	private Set<News> news = new HashSet<>();

	/**
	 * Gets the tag id.
	 *
	 * @return the tag id
	 */
	public Long getTagId() {
		return tagId;
	}

	/**
	 * Sets the tag id.
	 *
	 * @param tagId
	 *            the new tag id
	 */
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	/**
	 * Gets the tag name.
	 *
	 * @return the tag name
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the tag name.
	 *
	 * @param tagName
	 *            the new tag name
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public Set<News> getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news
	 *            the new news
	 */
	public void setNews(Set<News> news) {
		this.news = news;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Tag [tagId=" + tagId + ", tagName=" + tagName + ", version=" + version + "]";
	}

}
