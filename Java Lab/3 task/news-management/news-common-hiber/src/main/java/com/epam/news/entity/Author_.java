package com.epam.news.entity;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Author.class)
public abstract class Author_ {

	public static volatile SetAttribute<Author, News> news;
	public static volatile SingularAttribute<Author, Date> expired;
	public static volatile SingularAttribute<Author, String> authorName;
	public static volatile SingularAttribute<Author, Long> authorId;
	public static volatile SingularAttribute<Author, Long> version;

}

