package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Tag;

/**
 * Classes that implements this interface, interacts with
 * the table "TAG" in the database.
 */
public interface TagDAO extends BasicDAO<Tag> {

	/**
	 * Gets entity the by its name.
	 *
	 * @param name
	 *            the name
	 * @return the by name
	 */
	Tag getByName(String name);

	/**
	 * Gets entities list the by tag id list.
	 *
	 * @param tagList
	 *            the tag list
	 * @return the by tag id list
	 */
	List<Tag> getByTagIdList(List<Tag> tagList);

	/**
	 * Gets entity from database by field values or null.
	 *
	 * @param entity
	 *            the entity
	 * @return the by field values
	 */
	Tag getByFieldValues(Tag tag);

}
