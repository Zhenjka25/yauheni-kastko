package com.epam.news.dao.jpa;

import static com.epam.news.constant.ConstantsCommon.NEWS_PER_PAGE_KEY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.Author_;
import com.epam.news.entity.Comment;
import com.epam.news.entity.Comment_;
import com.epam.news.entity.News;
import com.epam.news.entity.News_;
import com.epam.news.entity.Tag;
import com.epam.news.entity.Tag_;
import com.epam.news.util.SearchCriteria;

/**
 * JPA implementation of NewsDAO interface.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class NewsDAOJPA implements NewsDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private Environment env;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News saveOrUpdate(News entity) {
		entityManagerFactory.getCache().evict(News.class, entity.getNewsId());
		try {
			entity = entityManager.merge(entity);
		} catch (OptimisticLockException e) {
			// For EclipseLink
			throw new OptimisticLockException(entity);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News getById(long id) {
		return entityManager.find(News.class, new Long(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		News newsToDelete = entityManager.getReference(News.class, id);
		entityManager.remove(newsToDelete);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> getAll() {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<News> criteria = builder.createQuery(News.class);
		Root<News> newsRoot = criteria.from(News.class);
		criteria.select(newsRoot);
		return entityManager.createQuery(criteria).getResultList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> findBySearchCriteria(SearchCriteria searchObject) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();

		// Tuple query is used due to bug in eclipse link. Group by function is
		// ignored by simple criteria query and using this function is possible
		// only with the tuple query
		CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
		Root<News> newsRoot = criteria.from(News.class);
		Join<News, Comment> newsComments = newsRoot.join(News_.comments, JoinType.LEFT);
		criteria.where(getPredicates(searchObject, builder, newsRoot).toArray(new Predicate[] {}));
		addSortingToTupleQuery(builder, newsRoot, newsComments, criteria);
		
		Expression<Long> newsId = newsRoot.get(News_.newsId);
		criteria.multiselect(newsId);
		
		TypedQuery<Tuple> tq = entityManager.createQuery(criteria);
		addPaginationForTupleQuery(tq, searchObject.getPage());

		List<News> newsList = new ArrayList<>();
		for (Tuple t : tq.getResultList()) {
			newsList.add(getById(t.get(newsId)));
		}

		return newsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countNews() {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
		Root<News> newsRoot = criteria.from(News.class);

		criteria.multiselect(builder.count(newsRoot.get(News_.newsId)));

		return (Long) entityManager.createQuery(criteria).getSingleResult().get(0);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<News> newsRoot = criteria.from(News.class);

		criteria.where(getPredicates(searchCriteria, builder, newsRoot).toArray(new Predicate[] {}));

		criteria.select(builder.count(newsRoot.get(News_.newsId)));

		return entityManager.createQuery(criteria).getSingleResult().intValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();

		CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
		Root<News> newsRoot = criteria.from(News.class);

		Join<News, Comment> newsComments = newsRoot.join(News_.comments, JoinType.LEFT);

		criteria.where(getPredicates(searchCriteria, builder, newsRoot).toArray(new Predicate[] {}));

		addSortingToTupleQuery(builder, newsRoot, newsComments, criteria);

		Expression<Long> newsId = newsRoot.get(News_.newsId);
		criteria.multiselect(newsId);
		TypedQuery<Tuple> tq = entityManager.createQuery(criteria);

		List<Long> idList = new ArrayList<>();
		for (Tuple t : tq.getResultList()) {
			idList.add(t.get(newsId));
		}

		return idList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findPrevNews(Long newsId, SearchCriteria searchCriteria) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
		Root<News> newsRoot = criteria.from(News.class);

		Join<News, Comment> newsComments = newsRoot.join(News_.comments, JoinType.LEFT);

		criteria.where(getPredicates(searchCriteria, builder, newsRoot).toArray(new Predicate[] {}));

		addSortingToTupleQuery(builder, newsRoot, newsComments, criteria);

		Expression<Long> newsIdEx = newsRoot.get(News_.newsId);
		criteria.multiselect(newsIdEx);
		TypedQuery<Tuple> tq = entityManager.createQuery(criteria);

		return getPrevNewsFromList(tq.getResultList(), newsIdEx, newsId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findNextNews(Long newsId, SearchCriteria searchCriteria) {

		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<Tuple> criteria = builder.createTupleQuery();
		Root<News> newsRoot = criteria.from(News.class);

		Join<News, Comment> newsComments = newsRoot.join(News_.comments, JoinType.LEFT);

		criteria.where(getPredicates(searchCriteria, builder, newsRoot).toArray(new Predicate[] {}));

		addSortingToTupleQuery(builder, newsRoot, newsComments, criteria);

		Expression<Long> newsIdEx = newsRoot.get(News_.newsId);
		criteria.multiselect(newsIdEx);
		TypedQuery<Tuple> tq = entityManager.createQuery(criteria);

		return getNextNewsFromList(tq.getResultList(), newsIdEx, newsId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteList(List<Long> newsIdList) {
		if (!newsIdList.isEmpty()) {

			CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();

			CriteriaDelete<Comment> criteriaComments = builder.createCriteriaDelete(Comment.class);
			Root<Comment> commentRoot = criteriaComments.from(Comment.class);

			criteriaComments.where(commentRoot.get(Comment_.newsId).in(newsIdList));

			entityManager.createQuery(criteriaComments).executeUpdate();

			CriteriaDelete<News> criteriaNews = builder.createCriteriaDelete(News.class);
			Root<News> newsRoot = criteriaNews.from(News.class);

			criteriaNews.where(newsRoot.get(News_.newsId).in(newsIdList));

			entityManager.createQuery(criteriaNews).executeUpdate();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News getByFieldValues(News news) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<News> criteria = builder.createQuery(News.class);
		Root<News> newsRoot = criteria.from(News.class);

		criteria.where(getPredicatesByFieldValues(news, builder, newsRoot).toArray(new Predicate[] {}));

		return getSingleOrNoneResult(criteria);
	}

	/**
	 * Gets the predicates list by searchCriteria object.
	 *
	 * @param searchObject
	 *            the search object
	 * @param builder
	 *            the builder
	 * @param newsRoot
	 *            the news root
	 * @return the predicates
	 */
	private List<Predicate> getPredicates(SearchCriteria searchObject, CriteriaBuilder builder, Root<News> newsRoot) {

		List<Predicate> predicates = new ArrayList<>();

		if (searchObject.getAuthorId() != null) {
			Join<News, Author> newsAuthors = newsRoot.join(News_.authors);
			predicates.add(builder.equal(newsAuthors.get(Author_.authorId), searchObject.getAuthorId()));
		}

		if (!searchObject.getTagsId().isEmpty()) {
			Join<News, Tag> newsTags = newsRoot.join(News_.tags);
			predicates.add(newsTags.get(Tag_.tagId).in(searchObject.getTagsId()));
		}

		return predicates;
	}

	/**
	 * Adds sort by number of comments and news date to the tuple query.
	 *
	 * @param builder
	 *            the builder
	 * @param newsRoot
	 *            the news root
	 * @param newsComments
	 *            the news comments
	 * @param criteria
	 *            the criteria
	 */
	private void addSortingToTupleQuery(CriteriaBuilder builder, Root<News> newsRoot, Join<News, Comment> newsComments,
			CriteriaQuery<Tuple> criteria) {
		
		Expression<Long> count = builder.count(newsComments.get(Comment_.commentId));
		Expression<Date> newsDate = newsRoot.get(News_.modificationDate);
		Expression<String> newsTitle = newsRoot.get(News_.title);
		
		criteria.groupBy(newsRoot.get(News_.newsId), newsRoot.get(News_.title), newsRoot.get(News_.shortText),
				newsRoot.get(News_.fullText), newsRoot.get(News_.creationDate), newsRoot.get(News_.modificationDate),
				newsRoot.get(News_.version));
		
		criteria.orderBy(builder.desc(count), builder.desc(newsDate), builder.asc(newsTitle));
	}

	/**
	 * Adds pagination for the tuple query.
	 *
	 * @param tq
	 *            the tq
	 * @param page
	 *            the page
	 */
	private void addPaginationForTupleQuery(TypedQuery<Tuple> tq, int page) {
		int newsPerPage = Integer.parseInt(env.getProperty(NEWS_PER_PAGE_KEY));
		tq.setFirstResult((page - 1) * newsPerPage);
		tq.setMaxResults(newsPerPage);
	}

	/**
	 * Gets the previous news from list.
	 *
	 * @param tupleList
	 *            the tuple list
	 * @param newsIdEx
	 *            the news id ex
	 * @param newsId
	 *            the news id
	 * @return the prev news from list
	 */
	private News getPrevNewsFromList(List<Tuple> tupleList, Expression<Long> newsIdEx, Long newsId) {
		if (tupleList.get(0).get(newsIdEx).equals(newsId)) {
			return null;
		}
		for (int i = 1; i < tupleList.size(); i++) {
			if (tupleList.get(i).get(newsIdEx).equals(newsId)) {
				return getById(tupleList.get(i - 1).get(newsIdEx));
			}
		}
		return null;
	}

	/**
	 * Gets the next news from list.
	 *
	 * @param tupleList
	 *            the tuple list
	 * @param newsIdEx
	 *            the news id ex
	 * @param newsId
	 *            the news id
	 * @return the next news from list
	 */
	private News getNextNewsFromList(List<Tuple> tupleList, Expression<Long> newsIdEx, Long newsId) {
		if (tupleList.get(tupleList.size() - 1).get(newsIdEx).equals(newsId)) {
			return null;
		}
		for (int i = 0; i < tupleList.size() - 1; i++) {
			if (tupleList.get(i).get(newsIdEx).equals(newsId)) {
				return getById(tupleList.get(i + 1).get(newsIdEx));
			}
		}
		return null;
	}

	/**
	 * Gets predicates by field values.
	 *
	 * @param news
	 *            the news
	 * @param builder
	 *            the builder
	 * @param newsRoot
	 *            the news root
	 * @return the predicates by field values
	 */
	private List<Predicate> getPredicatesByFieldValues(News news, CriteriaBuilder builder, Root<News> newsRoot) {
		List<Predicate> predicates = new ArrayList<>();
		
		predicates.add(builder.equal(newsRoot.get(News_.modificationDate), news.getModificationDate()));
		predicates.add(builder.equal(newsRoot.get(News_.fullText), news.getFullText()));
		predicates.add(builder.equal(newsRoot.get(News_.shortText), news.getShortText()));
		predicates.add(builder.equal(newsRoot.get(News_.title), news.getTitle()));
		
		return predicates;
	}

	/**
	 * Gets the single or null result from query.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the single or none result
	 */
	private News getSingleOrNoneResult(CriteriaQuery<News> criteria) {
		List<News> newsList = entityManager.createQuery(criteria).getResultList();
		if (!newsList.isEmpty()) {
			return newsList.get(0);
		}
		return null;
	}

}
