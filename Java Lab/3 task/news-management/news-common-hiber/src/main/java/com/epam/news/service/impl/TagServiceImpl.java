package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

import com.epam.news.dao.TagDAO;
import com.epam.news.dto.TagDTO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DTOIntegrityException;
import com.epam.news.service.TagService;

/**
 * All methods work in transactional context.
 */
@Service
@Transactional
public class TagServiceImpl implements TagService {

	@Autowired
	private TagDAO tagDAO;

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	private Validator validator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TagDTO save(TagDTO entity) {
		validate(entity);
		Tag tagEntity = reverse(entity);
		Tag tag = tagDAO.getByFieldValues(tagEntity);
		if (tag == null) {
			tag = tagDAO.saveOrUpdate(tagEntity);
		}
		return reverse(tag);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public TagDTO findById(long id) {
		return reverse(tagDAO.getById(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(TagDTO entity) {
		validate(entity);
		tagDAO.saveOrUpdate(reverse(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		tagDAO.deleteById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<TagDTO> findAll() {
		List<TagDTO> tagDTOList = new ArrayList<>();
		List<Tag> tagList = tagDAO.getAll();
		for (Tag tag : tagList) {
			tagDTOList.add(reverse(tag));
		}
		return tagDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public TagDTO findByName(String name) {
		return reverse(tagDAO.getByName(name));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<TagDTO> findByTagIdList(List<TagDTO> tagList) {
		List<Tag> helperList = new ArrayList<>();
		for (TagDTO tagDTO : tagList) {
			helperList.add(reverse(tagDTO));
		}
		List<Tag> resultTagList = tagDAO.getByTagIdList(helperList);

		List<TagDTO> resultDTOList = new ArrayList<>();
		for (Tag tag : resultTagList) {
			resultDTOList.add(reverse(tag));
		}
		return resultDTOList;
	}

	private Tag reverse(TagDTO tagDTO) {
		if (tagDTO == null) {
			return null;
		}
		return mapper.map(tagDTO, Tag.class);
	}

	private TagDTO reverse(Tag tag) {
		if (tag == null) {
			return null;
		}
		return mapper.map(tag, TagDTO.class);
	}

	private void validate(TagDTO tagDTO) {
		DataBinder binder = new DataBinder(tagDTO);
		binder.setValidator(validator);
		binder.validate();
		BindingResult results = binder.getBindingResult();
		if (results.hasErrors()) {
			throw new DTOIntegrityException(results);
		}
	}

}
