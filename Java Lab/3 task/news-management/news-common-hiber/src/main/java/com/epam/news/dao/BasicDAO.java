package com.epam.news.dao;

import java.util.List;

/**
 * Basic interface for C.R.U.D. operations.
 *
 * @param <T>
 *            the generic type
 */
public interface BasicDAO<T> {

	/**
	 * Saves or updates entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the t
	 */
	T saveOrUpdate(T entity);

	/**
	 * Gets entity by its id.
	 *
	 * @param id
	 *            the id
	 * @return the by id
	 */
	T getById(long id);

	/**
	 * Deletes entity by its id.
	 *
	 * @param id
	 *            the id
	 */
	void deleteById(long id);

	/**
	 * Gets all entities.
	 *
	 * @return the all
	 */
	List<T> getAll();

}
