package com.epam.news.entity;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

/**
 * The Class News. Represents news business entity. It can have only one author
 * and any number of tags.
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {

	private static final long serialVersionUID = 2371023011673989446L;

	/** The news id. Primary key for news table in database. */
	@Id
	@SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "NEWS_SEQ")
	@Column(name = "NEWS_ID", nullable = false)
	private Long newsId;

	/** The news title. */
	@Column(name = "TITLE", nullable = false)
	private String title;

	/** The short text of news. Contains a brief content of the news. */
	@Column(name = "SHORT_TEXT", nullable = false)
	private String shortText;

	/** The full text of news. */
	@Column(name = "FULL_TEXT", nullable = false)
	private String fullText;

	/** The news creation date. Never changes after news creation. */
	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	/**
	 * The news modification date. After news creation it matches with creation
	 * date. Changes after editing of news.
	 */
	@Column(name = "MODIFICATION_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date modificationDate;

	@Version
	@Column(name = "VERSION", nullable = false)
	private Long version;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinTable(name = "NEWS_AUTHOR", joinColumns = {
			@JoinColumn(name = "NEWS_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "AUTHOR_ID", nullable = false, updatable = false) })
	private Set<Author> authors = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "news", cascade = CascadeType.REMOVE)
	@OrderBy("creationDate DESC")
	private List<Comment> comments = new ArrayList<>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinTable(name = "NEWS_TAG", joinColumns = {
			@JoinColumn(name = "NEWS_ID", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "TAG_ID", nullable = false, updatable = false) })
	private Set<Tag> tags = new HashSet<>();

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the short text.
	 *
	 * @return the short text
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text.
	 *
	 * @param shortText
	 *            the new short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the full text
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText
	 *            the new full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate
	 *            the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the authors of this news.
	 *
	 * @return the authors
	 */
	public Set<Author> getAuthors() {
		return authors;
	}

	/**
	 * Sets the authors.
	 *
	 * @param authors
	 *            the new authors
	 */
	public void setAuthors(Set<Author> authors) {
		this.authors = authors;
	}

	/**
	 * Gets the comments of this news.
	 *
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments
	 *            the new comments
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Gets the tags of this news.
	 *
	 * @return the tags
	 */
	public Set<Tag> getTags() {
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags
	 *            the new tags
	 */
	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (creationDate.compareTo(other.creationDate) != 0)
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (modificationDate.compareTo(other.modificationDate) != 0)
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

}
