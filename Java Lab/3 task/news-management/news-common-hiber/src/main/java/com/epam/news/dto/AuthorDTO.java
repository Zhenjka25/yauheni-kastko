package com.epam.news.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The Class AuthorDTO.
 */
public class AuthorDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long authorId;

	@NotNull
	@Size(min = 3, max = 30, message = "{authorname.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{authorname.error.pattern}")
	private String authorName;

	private Date expired;

	private Long version;

	/**
	 * Instantiates a new author dto.
	 *
	 * @param authorId
	 *            the author id
	 * @param authorName
	 *            the author name
	 * @param expired
	 *            the expired
	 * @param version
	 *            the version
	 */
	public AuthorDTO(Long authorId, String authorName, Date expired, Long version) {
		this.authorId = authorId;
		this.authorName = authorName;
		this.expired = expired;
		this.version = version;
	}

	/**
	 * Instantiates a new author dto.
	 */
	public AuthorDTO() {

	}

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the author name.
	 *
	 * @return the author name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the author name.
	 *
	 * @param authorName
	 *            the new author name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Gets the expired.
	 *
	 * @return the expired
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Sets the expired.
	 *
	 * @param expired
	 *            the new expired
	 */
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorDTO other = (AuthorDTO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "AuthorDTO [authorId=" + authorId + ", authorName=" + authorName + ", expired=" + expired + ", version="
				+ version + "]";
	}

}
