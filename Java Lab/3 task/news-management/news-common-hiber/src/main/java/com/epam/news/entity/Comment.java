package com.epam.news.entity;

import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 * The Class Comment. Represents comment business entity. Comment always refers
 * to some news.
 */
@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

	private static final long serialVersionUID = 1606023331974965346L;

	/** The comment id. Primary key for comment table in database. */
	@Id
	@SequenceGenerator(name = "COMMENTS_SEQ", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "COMMENTS_SEQ")
	@Column(name = "COMMENT_ID", nullable = false)
	private Long commentId;

	/** The id of news to which comment refers. */
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "NEWS_ID", nullable = false)
	private News news;

	/** The comment text. */
	@Column(name = "COMMENT_TEXT", nullable = false)
	private String commentText;

	/** The creation date. Never changes after comment creation. */
	@Column(name = "CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name = "NEWS_ID", insertable = false, updatable = false)
	private Long newsId;

	/**
	 * Gets the comment id.
	 *
	 * @return the comment id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Sets the comment id.
	 *
	 * @param commentId
	 *            the new comment id
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * Gets the news to which comment is attached.
	 *
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news
	 *            the new news
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the comment text
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text.
	 *
	 * @param commentText
	 *            the new comment text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (creationDate.compareTo(other.creationDate) != 0)
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", commentText=" + commentText + ", creationDate=" + creationDate
				+ ", newsId=" + newsId + "]";
	}

}
