package com.epam.news.service;

import com.epam.news.dto.CommentDTO;

/**
 * Service which interacts with CommentDAO
 */
public interface CommentService extends BasicService<CommentDTO> {

}
