package com.epam.news.dao;

import com.epam.news.entity.Comment;

/**
 * Classes that implements this interface, interacts
 * with the table "COMMENTS" in the database.
 */
public interface CommentDAO extends BasicDAO<Comment> {

}
