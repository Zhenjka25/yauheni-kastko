package com.epam.news.service;

import java.util.List;

import com.epam.news.dto.AuthorDTO;

/**
 * Service for interacting with Author DAO layer.
 */
public interface AuthorService extends BasicService<AuthorDTO> {

	/**
	 * Finds entity by its name.
	 *
	 * @param name
	 *            the name
	 * @return the author
	 */
	AuthorDTO findByName(String name);

	/**
	 * Finds actual authors list (their expire field is null).
	 *
	 * @return the list
	 */
	List<AuthorDTO> findActualAuthors();

	/**
	 * Expires author.
	 *
	 * @param entity
	 *            the entity
	 */
	void expireAuthor(AuthorDTO entity);

}
