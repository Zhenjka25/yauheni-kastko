package com.epam.news.dao;

import com.epam.news.entity.UserInfo;

/**
 * It is responsible for retrieving data for Spring
 * Security.
 */
public interface UserInfoDAO {

	/**
	 * Gets the user info entity.
	 *
	 * @param username
	 *            the username
	 * @return the user info
	 */
	UserInfo getUserInfo(String username);

}
