package com.epam.news.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class NewsDTO.
 */
public class NewsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long newsId;

	@NotNull
	@Size(min = 3, max = 30, message = "{title.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{title.error.pattern}")
	private String title;

	@NotNull
	@Size(min = 3, max = 100, message = "{shorttext.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{shorttext.error.pattern}")
	private String shortText;

	@NotNull
	@Size(min = 3, max = 200, message = "{fulltext.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{fulltext.error.pattern}")
	private String fullText;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "{date.error}")
	private Date creationDate;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = "{date.error}")
	private Date modificationDate;

	private Long version;

	private AuthorDTO author;
	private List<CommentDTO> comments;
	private List<TagDTO> tags;

	/**
	 * Instantiates a new news dto.
	 *
	 * @param newsId
	 *            the news id
	 * @param title
	 *            the title
	 * @param shortText
	 *            the short text
	 * @param fullText
	 *            the full text
	 * @param creationDate
	 *            the creation date
	 * @param modificationDate
	 *            the modification date
	 * @param version
	 *            the version
	 * @param authors
	 *            the authors
	 * @param comments
	 *            the comments
	 * @param tags
	 *            the tags
	 */
	public NewsDTO(Long newsId, String title, String shortText, String fullText, Date creationDate,
			Date modificationDate, Long version, AuthorDTO author, List<CommentDTO> comments, List<TagDTO> tags) {
		this.newsId = newsId;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.version = version;
		this.author = author;
		this.comments = comments;
		this.tags = tags;
	}

	/**
	 * Instantiates a new news dto.
	 */
	public NewsDTO() {

	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the short text.
	 *
	 * @return the short text
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text.
	 *
	 * @param shortText
	 *            the new short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the full text
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText
	 *            the new full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate
	 *            the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public Long getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(Long version) {
		this.version = version;
	}

	/**
	 * Gets the authors.
	 *
	 * @return the authors
	 */
	public AuthorDTO getAuthor() {
		return author;
	}

	/**
	 * Sets the authors.
	 *
	 * @param authors
	 *            the new authors
	 */
	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public List<CommentDTO> getComments() {
		if (comments == null) {
			comments = new ArrayList<>();
		}
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments
	 *            the new comments
	 */
	public void setComments(List<CommentDTO> comments) {
		this.comments = comments;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<TagDTO> getTags() {
		if (tags == null) {
			tags = new ArrayList<>();
		}
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags
	 *            the new tags
	 */
	public void setTags(List<TagDTO> tags) {
		this.tags = tags;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsDTO other = (NewsDTO) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "NewsDTO [newsId=" + newsId + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + ", version=" + version
				+ "]";
	}

}
