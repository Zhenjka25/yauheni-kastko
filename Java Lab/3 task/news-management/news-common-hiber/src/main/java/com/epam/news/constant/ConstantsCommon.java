package com.epam.news.constant;

public class ConstantsCommon {
	
	public static final String FIND_AUTHOR_BY_ID = "findAuthorById";
	public static final String FIND_AUTHOR_BY_NAME = "findAuthorByName";
	
	public static final String NEWS_PER_PAGE_KEY = "news.per.page";
	public static final String DIGITS_PER_PAGE = "digits.per.page";
	
	public static final String SPRING_PROFILE_HIBERNATE = "hibernate";
	public static final String SPRING_PROFILE_ECLIPSE_LINK = "eclipse-link";

}
