package com.epam.news.entity;

import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.metamodel.*;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(News.class)
public abstract class News_ {

	public static volatile SingularAttribute<News, Long> newsId;
	public static volatile SingularAttribute<News, Date> modificationDate;
	public static volatile ListAttribute<News, Comment> comments;
	public static volatile SingularAttribute<News, String> shortText;
	public static volatile SingularAttribute<News, String> fullText;
	public static volatile SingularAttribute<News, String> title;
	public static volatile SingularAttribute<News, Date> creationDate;
	public static volatile SingularAttribute<News, Long> version;
	public static volatile SetAttribute<News, Author> authors;
	public static volatile SetAttribute<News, Tag> tags;

}

