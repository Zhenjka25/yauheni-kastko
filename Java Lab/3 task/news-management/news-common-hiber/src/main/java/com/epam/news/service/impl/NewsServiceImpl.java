package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

import com.epam.news.dao.NewsDAO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.entity.News;
import com.epam.news.exception.DTOIntegrityException;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * All methods work in transactional context.
 */
@Service
@Transactional
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsDAO newsDAO;

	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private Validator validator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NewsDTO save(NewsDTO entity) {
		validate(entity);
		News newsEntity = reverse(entity);
		News news = newsDAO.getByFieldValues(newsEntity);
		if (news == null) {
			news = newsDAO.saveOrUpdate(newsEntity);
		}
		return reverse(news);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public NewsDTO findById(long id) {
		return reverse(newsDAO.getById(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(NewsDTO entity) {
		validate(entity);
		newsDAO.saveOrUpdate(reverse(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		newsDAO.deleteById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<NewsDTO> findAll() {
		List<NewsDTO> newsDTOList = new ArrayList<>();
		List<News> newsList = newsDAO.getAll();
		for (News news : newsList) {
			newsDTOList.add(reverse(news));
		}
		return newsDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<NewsDTO> findBySearchCriteria(SearchCriteria searchObject) {
		List<NewsDTO> newsDTOList = new ArrayList<>();
		List<News> newsList = newsDAO.findBySearchCriteria(searchObject);
		for (News news : newsList) {
			newsDTOList.add(reverse(news));
		}
		return newsDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public Long countNews() {
		return newsDAO.countNews();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) {
		return newsDAO.countNewsBySearchCriteria(searchCriteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) {
		return newsDAO.findIdListBySearchCriteria(searchCriteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public NewsDTO findPrevNews(Long newsId, SearchCriteria searchCriteria) {
		return reverse(newsDAO.findPrevNews(newsId, searchCriteria));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public NewsDTO findNextNews(Long newsId, SearchCriteria searchCriteria) {
		return reverse(newsDAO.findNextNews(newsId, searchCriteria));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteList(List<Long> newsIdList) {
		newsDAO.deleteList(newsIdList);
	}

	private News reverse(NewsDTO newsDTO) {
		if (newsDTO == null) {
			return null;
		}
		return mapper.map(newsDTO, News.class);
	}

	private NewsDTO reverse(News news) {
		if (news == null) {
			return null;
		}
		return mapper.map(news, NewsDTO.class);
	}
	
	private void validate(NewsDTO newsDTO) {
		DataBinder binder = new DataBinder(newsDTO);
		binder.setValidator(validator);
		binder.validate();
		BindingResult results = binder.getBindingResult();
		if (results.hasErrors()) {
			throw new DTOIntegrityException(results);
		}
	}

}
