package com.epam.news.service;

import org.hibernate.service.spi.ServiceException;

import com.epam.news.dto.UserInfoDTO;

/**
 * Interacts with UserInfoDAO. Used by Spring Security.
 */
public interface UserInfoService {

	/**
	 * Gets the user info entity.
	 *
	 * @param username
	 *            the username
	 * @return the user info
	 * @throws ServiceException
	 *             the service exception
	 */
	UserInfoDTO getUserInfo(String username);

}
