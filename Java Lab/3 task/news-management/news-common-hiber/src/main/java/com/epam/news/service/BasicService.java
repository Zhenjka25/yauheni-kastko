package com.epam.news.service;

import java.util.List;

/**
 * Basic service for interacting with basic C.R.U.D. DAO.
 *
 * @param <T>
 *            the generic type entity
 * 
 */
public interface BasicService<T> {

	/**
	 * Saves the entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the t
	 */
	T save(T entity);

	/**
	 * Finds entity by its id.
	 *
	 * @param id
	 *            the id
	 * @return the t
	 */
	T findById(long id);

	/**
	 * Updates the entity.
	 *
	 * @param entity
	 *            the entity
	 */
	void update(T entity);

	/**
	 * Deletes entity by its id.
	 *
	 * @param id
	 *            the id
	 */
	void deleteById(long id);

	/**
	 * Finds all entities.
	 *
	 * @return the list
	 */
	List<T> findAll();

}
