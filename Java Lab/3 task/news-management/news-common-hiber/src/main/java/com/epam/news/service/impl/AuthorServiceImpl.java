package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dto.AuthorDTO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DTOIntegrityException;
import com.epam.news.service.AuthorService;

/**
 * All methods work in transactional context.
 */
@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorDAO authorDAO;

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	private Validator validator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AuthorDTO save(AuthorDTO entity) {
		validate(entity);
		Author authorEntity = convert(entity);
		Author author = authorDAO.getByFieldValues(authorEntity);
		if (author == null) {
			author = authorDAO.saveOrUpdate(authorEntity);
		}
		return convert(author);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public AuthorDTO findById(long id) {
		return convert(authorDAO.getById(id));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(AuthorDTO entity) {
		validate(entity);
		authorDAO.saveOrUpdate(convert(entity));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) {
		authorDAO.deleteById(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<AuthorDTO> findAll() {
		List<AuthorDTO> authorDTOList = new ArrayList<>();
		List<Author> authorList = authorDAO.getAll();
		for (Author author : authorList) {
			authorDTOList.add(convert(author));
		}
		return authorDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public AuthorDTO findByName(String name) {
		return convert(authorDAO.getByName(name));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<AuthorDTO> findActualAuthors() {
		List<AuthorDTO> authorDTOList = new ArrayList<>();
		List<Author> authorList = authorDAO.getActualAuthors();
		for (Author author : authorList) {
			authorDTOList.add(convert(author));
		}
		return authorDTOList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void expireAuthor(AuthorDTO entity) {
		validate(entity);
		entity.setExpired(new Date());
		authorDAO.saveOrUpdate(convert(entity));
	}

	private Author convert(AuthorDTO authorDTO) {
		if (authorDTO == null) {
			return null;
		}
		return mapper.map(authorDTO, Author.class);
	}

	private AuthorDTO convert(Author author) {
		if (author == null) {
			return null;
		}
		return mapper.map(author, AuthorDTO.class);
	}

	private void validate(AuthorDTO authorDTO) {
		DataBinder binder = new DataBinder(authorDTO);
		binder.setValidator(validator);
		binder.validate();
		BindingResult results = binder.getBindingResult();
		if (results.hasErrors()) {
			throw new DTOIntegrityException(results);
		}
	}

}
