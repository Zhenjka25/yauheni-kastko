package com.epam.news.exception;

import org.springframework.validation.BindingResult;

/**
 * The Class DTOIntegrityException.
 */
public class DTOIntegrityException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private BindingResult results;

	/**
	 * Instantiates a new DTO integrity exception.
	 *
	 * @param results
	 *            the results
	 */
	public DTOIntegrityException(BindingResult results) {
		this.results = results;
	}

	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public BindingResult getResults() {
		return results;
	}

}
