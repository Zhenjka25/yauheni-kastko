package com.epam.news.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.UserInfoDAO;
import com.epam.news.entity.UserInfo;
import com.epam.news.entity.UserInfo_;

/**
 * JPA implementation of UserInfoDAO interface.
 */
@Repository
@Transactional(propagation = Propagation.MANDATORY)
public class UserInfoDAOJPA implements UserInfoDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInfo getUserInfo(String username) {
		CriteriaBuilder builder = entityManagerFactory.getCriteriaBuilder();
		CriteriaQuery<UserInfo> criteria = builder.createQuery(UserInfo.class);
		Root<UserInfo> userInfoRoot = criteria.from(UserInfo.class);

		criteria.where(builder.equal(userInfoRoot.get(UserInfo_.login), username));

		return getSingleOrNoneResult(criteria);
	}

	/**
	 * Gets the single or null result.
	 *
	 * @param criteria
	 *            the criteria
	 * @return the single or none result
	 */
	private UserInfo getSingleOrNoneResult(CriteriaQuery<UserInfo> criteria) {
		List<UserInfo> userInfoList = entityManager.createQuery(criteria).getResultList();
		if (!userInfoList.isEmpty()) {
			return userInfoList.get(0);
		}
		return null;
	}

}
