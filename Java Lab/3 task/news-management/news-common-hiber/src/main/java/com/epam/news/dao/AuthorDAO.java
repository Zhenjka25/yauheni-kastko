package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;

/**
 * Classes that implements this interface, interacts
 * with the table "AUTHOR" in the database.
 */
public interface AuthorDAO extends BasicDAO<Author> {

	/**
	 * Gets the entity the by its name.
	 *
	 * @param name
	 *            the name
	 * @return the by name
	 */
	Author getByName(String name);

	/**
	 * Gets the actual author entities(where EXPIRE column is null).
	 *
	 * @return the actual authors
	 */
	List<Author> getActualAuthors();

	/**
	 * Gets entity from database by field values or null.
	 *
	 * @param entity
	 *            the entity
	 * @return the by field values
	 */
	Author getByFieldValues(Author author);

}
