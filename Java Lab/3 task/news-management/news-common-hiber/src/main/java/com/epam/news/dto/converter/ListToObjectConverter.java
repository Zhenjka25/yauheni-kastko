package com.epam.news.dto.converter;

import java.util.HashSet;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.dozer.DozerConverter;

import com.epam.news.dto.AuthorDTO;
import com.epam.news.entity.Author;

@SuppressWarnings("rawtypes")
public class ListToObjectConverter extends DozerConverter<Set, AuthorDTO> {

	private DozerBeanMapper mapper = new DozerBeanMapper();

	public ListToObjectConverter() {
		super(Set.class, AuthorDTO.class);
	}

	@Override
	public AuthorDTO convertTo(Set source, AuthorDTO destination) {
		if (source != null && !source.isEmpty()) {
			destination = mapper.map((Author) source.iterator().next(), AuthorDTO.class);
		}
		return destination;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Author> convertFrom(AuthorDTO source, Set destination) {
		if (source != null) {
			destination = new HashSet<>();
			destination.add(mapper.map(source, Author.class));
		}
		return destination;
	}

}
