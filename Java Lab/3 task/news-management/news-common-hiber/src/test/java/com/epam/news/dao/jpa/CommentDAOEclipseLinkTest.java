package com.epam.news.dao.jpa;

import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_ECLIPSE_LINK;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.config.TestConfigJPA;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.Comment;
import com.epam.news.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfigJPA.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles(SPRING_PROFILE_ECLIPSE_LINK)
public class CommentDAOEclipseLinkTest extends AbstractTest {

	@Autowired
	private CommentDAO commentDAO;

	@Autowired
	private NewsDAO newsDAO;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/commentData.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		TestTransaction.end();
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDelete() {
		News news = newsDAO.getById(1L);
		Comment comment = new Comment();
		comment.setCommentText("text");
		comment.setCreationDate(new Date());
		comment.setNews(news);
		comment = commentDAO.saveOrUpdate(comment);
		assertNotNull(comment.getCommentId());
		assertThat(commentDAO.getAll().size(), is(3));
		commentDAO.deleteById(comment.getCommentId());
		assertThat(commentDAO.getAll().size(), is(2));
	}

	@Test(expected = PersistenceException.class)
	public void saveFail() {
		News news = newsDAO.getById(1L);
		Comment comment = new Comment();
		comment.setCommentText("textaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
				+ "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		comment.setCreationDate(new Date());
		comment.setNews(news);
		comment = commentDAO.saveOrUpdate(comment);
		entityManager.flush();
	}

	@Test(expected = PersistenceException.class)
	public void constraintFail() {
		Comment comment = new Comment();
		comment.setCommentText("text");
		comment.setCreationDate(new Date());
		comment.setNews(new News());
		comment = commentDAO.saveOrUpdate(comment);
		entityManager.flush();
	}

	@Test
	public void getById() {
		Comment comment = commentDAO.getById(1);
		assertEquals("Some text here", comment.getCommentText());
		assertEquals(Timestamp.valueOf("2015-08-09 11:41:29.77").getTime(), comment.getCreationDate().getTime());
		assertThat(comment.getNews().getNewsId(), is(1L));
	}

	@Test
	public void getAll() {
		assertThat(commentDAO.getAll().size(), is(2));
	}

	@Test
	public void getNullResult() {
		assertNull(commentDAO.getById(10L));
	}

}
