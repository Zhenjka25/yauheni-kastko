package com.epam.news.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Validator;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.dto.AuthorDTO;
import com.epam.news.entity.Author;

/**
 * The Class AuthorServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplTest {

	@Mock
	private AuthorDAO authorDAOmock;

	@Mock
	private DozerBeanMapper mapperMock;

	@Mock
	private Validator validatorMock;

	@InjectMocks
	private AuthorServiceImpl service;

	@Before
	public void setupValidator() {
		when(validatorMock.supports(any())).thenReturn(true);
	}

	@Test
	public void saveAuthor() {
		service.save(new AuthorDTO());
		verify(authorDAOmock).saveOrUpdate(any(Author.class));
		verify(mapperMock).map(any(Author.class), any());
		verify(mapperMock).map(any(AuthorDTO.class), any());
	}

	@Test
	public void getById() {
		service.findById(1);
		verify(authorDAOmock).getById(1);
	}

	@Test
	public void updateAuthor() {
		service.update(new AuthorDTO());
		verify(authorDAOmock).saveOrUpdate(any(Author.class));
		verify(mapperMock).map(any(Author.class), any());
		verify(mapperMock).map(any(AuthorDTO.class), any());
	}

	@Test
	public void expireAuthor() {
		service.expireAuthor(new AuthorDTO());
		verify(authorDAOmock).saveOrUpdate(any(Author.class));
		verify(mapperMock).map(any(Author.class), any());
	}

	@Test
	public void deleteAuthor() {
		long id = 1;
		service.deleteById(id);
		verify(authorDAOmock).deleteById(id);
	}

	@Test
	public void findAllAuthors() {
		service.findAll();
		verify(authorDAOmock).getAll();
	}

	@Test
	public void findByName() {
		String name = "name";
		service.findByName(name);
		verify(authorDAOmock).getByName(name);
	}

	@Test
	public void findActualAuthors() {
		service.findActualAuthors();
		verify(authorDAOmock).getActualAuthors();
	}

}
