package com.epam.news.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Validator;

import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dto.CommentDTO;
import com.epam.news.entity.Comment;

/**
 * The Class CommentServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

	@Mock
	private CommentDAO commentDAOmock;

	@Mock
	private NewsDAO newsDAOmock;

	@Mock
	private DozerBeanMapper mapperMock;
	
	@Mock
	private Validator validatorMock;

	@InjectMocks
	private CommentServiceImpl service;
	
	@Before
	public void setupValidator() {
		when(validatorMock.supports(any())).thenReturn(true);
	}

	@Test
	public void getById() {
		service.findById(1);
		verify(commentDAOmock).getById(1);
	}

	@Test
	public void updateComment() {
		service.update(new CommentDTO());
		verify(commentDAOmock).saveOrUpdate(any(Comment.class));
		verify(mapperMock).map(any(Comment.class), any());
	}

	@Test
	public void deleteComment() {
		service.deleteById(1);
		verify(commentDAOmock).deleteById(1);
	}

	@Test
	public void findAllComments() {
		service.findAll();
		verify(commentDAOmock).getAll();
	}
}
