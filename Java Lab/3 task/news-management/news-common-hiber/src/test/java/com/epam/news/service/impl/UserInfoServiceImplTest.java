package com.epam.news.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.UserInfoDAO;
import com.epam.news.dto.UserInfoDTO;
import com.epam.news.entity.UserInfo;

@RunWith(MockitoJUnitRunner.class)
public class UserInfoServiceImplTest {

	@Mock
	private UserInfoDAO userInfoDAOMock;

	@Mock
	private DozerBeanMapper mapperMock;

	@InjectMocks
	private UserInfoServiceImpl userInfoService;

	private UserInfo userInfo;
	private UserInfoDTO userInfoDTO;

	@Before
	public void doSetup() {
		userInfo = new UserInfo();
		userInfo.setUsername("Wasya");
		userInfo.setPassword("12345");
		userInfo.setRole("user");

		userInfoDTO = new UserInfoDTO();
		userInfoDTO.setUsername("Wasya");
		userInfoDTO.setPassword("12345");
		userInfoDTO.setRole("user");

		when(userInfoDAOMock.getUserInfo(any(String.class))).thenAnswer(new Answer<UserInfo>() {
			@Override
			public UserInfo answer(InvocationOnMock invocation) throws Throwable {
				return userInfo;
			}
		});
		when(mapperMock.map(any(UserInfo.class), any())).thenReturn(userInfoDTO);
	}

	@Test
	public void getUserInfo() {
		UserInfoDTO userInfoFinded = userInfoService.getUserInfo("Login");
		assertEquals(userInfoDTO, userInfoFinded);
	}

}
