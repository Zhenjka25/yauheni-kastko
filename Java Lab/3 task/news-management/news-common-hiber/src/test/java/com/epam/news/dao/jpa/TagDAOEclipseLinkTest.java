package com.epam.news.dao.jpa;

import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_ECLIPSE_LINK;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.config.TestConfigJPA;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfigJPA.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles(SPRING_PROFILE_ECLIPSE_LINK)
public class TagDAOEclipseLinkTest extends AbstractTest {

	@Autowired
	private TagDAO tagDAO;

	@Autowired
	private NewsDAO newsDAO;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tagData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tagData.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		TestTransaction.end();
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteTag() {
		Tag tag = new Tag();
		tag.setTagName("Tag");
		tag = tagDAO.saveOrUpdate(tag);
		assertNotNull(tag.getTagId());
		assertThat(tagDAO.getAll().size(), is(3));
		tagDAO.deleteById(tag.getTagId());
		assertThat(tagDAO.getAll().size(), is(2));
	}

	@Test
	public void getById() {
		Tag tag = tagDAO.getById(1L);
		assertEquals("sport", tag.getTagName());
		assertTrue(tag.getNews().contains(newsDAO.getById(1L)));
	}

	@Test
	public void getByName() {
		Tag tag = tagDAO.getByName("politics");
		assertThat(tag.getTagId(), is(2L));
	}

	@Test
	public void getByTagIdList() {
		Tag tag1 = new Tag();
		tag1.setTagId(1L);
		Tag tag2 = new Tag();
		tag2.setTagId(2L);
		List<Tag> tagList = new ArrayList<>();
		tagList.add(tag1);
		tagList.add(tag2);
		assertThat(tagDAO.getByTagIdList(tagList).size(), is(2));
	}

	@Test
	public void getNullResult() {
		assertNull(tagDAO.getById(10L));
	}

	@Test(expected = PersistenceException.class)
	public void saveFail() {
		Tag tag = new Tag();
		tag.setTagName("Tagaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		tag = tagDAO.saveOrUpdate(tag);
		entityManager.flush();
	}

}
