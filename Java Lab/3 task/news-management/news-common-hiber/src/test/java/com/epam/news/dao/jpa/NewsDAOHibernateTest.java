package com.epam.news.dao.jpa;

import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_HIBERNATE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.config.TestConfigJPA;
import com.epam.news.dao.*;
import com.epam.news.entity.News;
import com.epam.news.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfigJPA.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles(SPRING_PROFILE_HIBERNATE)
public class NewsDAOHibernateTest extends AbstractTest {

	@Autowired
	private NewsDAO newsDAO;

	@Autowired
	private AuthorDAO authorDAO;

	@Autowired
	private TagDAO tagDAO;

	@Autowired
	private CommentDAO commentDAO;

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/commentData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tagData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tagData.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		TestTransaction.end();
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDelete() {
		News news = new News();
		news.setTitle("New title");
		news.setShortText("Some text");
		news.setFullText("some text");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news = newsDAO.saveOrUpdate(news);
		assertThat(newsDAO.getAll().size(), is(5));
		newsDAO.deleteById(news.getNewsId());
		assertThat(newsDAO.getAll().size(), is(4));
	}

	@Test
	public void getById() {
		News news = newsDAO.getById(1L);
		assertEquals("Some title", news.getTitle());
		assertTrue(news.getComments().contains(commentDAO.getById(1L)));
		assertTrue(news.getTags().contains(tagDAO.getById(1L)));
		assertTrue(news.getAuthors().contains(authorDAO.getById(3L)));
	}

	@Test
	public void getAll() {
		assertThat(newsDAO.getAll().size(), is(4));
	}

	@Test
	public void findBySearchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.getTagsId().add(1L);
		searchCriteria.getTagsId().add(2L);
		List<News> newsList = newsDAO.findBySearchCriteria(searchCriteria);
		assertThat(newsList.size(), is(2));
	}

	@Test
	public void countNews() {
		assertThat(newsDAO.countNews(), is(4L));
	}

	@Test
	public void countNewsBySearchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.getTagsId().add(1L);
		searchCriteria.getTagsId().add(2L);
		assertThat(newsDAO.countNewsBySearchCriteria(searchCriteria), is(2));
	}

	@Test
	public void findIdListBySearchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.getTagsId().add(1L);
		searchCriteria.getTagsId().add(2L);
		assertThat(newsDAO.findIdListBySearchCriteria(searchCriteria).size(), is(2));
	}

	@Test
	public void findPrevNews() {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.getTagsId().add(1L);
		searchCriteria.getTagsId().add(2L);
		assertThat(newsDAO.findPrevNews(1L, searchCriteria).getNewsId(), is(2L));
	}

	public void findNextNews() {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.getTagsId().add(1L);
		searchCriteria.getTagsId().add(2L);
		assertThat(newsDAO.findNextNews(2L, searchCriteria).getNewsId(), is(1L));
	}

	@Test
	public void deleteIdList() {
		List<Long> idList = new ArrayList<>();
		idList.add(3L);
		idList.add(4L);
		newsDAO.deleteList(idList);
		assertThat(newsDAO.getAll().size(), is(2));
	}

	@Test(expected = PersistenceException.class)
	public void saveFail() {
		News news = new News();
		news.setTitle("New titleaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		news.setShortText("Some text");
		news.setFullText("some text");
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
		news = newsDAO.saveOrUpdate(news);
		entityManager.flush();
	}

	@Test
	public void getNullResult() {
		assertNull(newsDAO.getById(10L));
	}

}
