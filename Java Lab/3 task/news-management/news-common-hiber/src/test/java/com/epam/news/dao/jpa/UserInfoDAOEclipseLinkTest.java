package com.epam.news.dao.jpa;

import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_ECLIPSE_LINK;
import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.config.TestConfigJPA;
import com.epam.news.dao.UserInfoDAO;
import com.epam.news.entity.UserInfo;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfigJPA.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles(SPRING_PROFILE_ECLIPSE_LINK)
public class UserInfoDAOEclipseLinkTest extends AbstractTest {

	@Autowired
	private UserInfoDAO userInfoDAO;

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/userData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/roleData.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void getUserInfo() {
		UserInfo userExpected = new UserInfo();
		userExpected.setUserId(1L);
		userExpected.setPassword("qweasd");
		userExpected.setRole("some role");
		userExpected.setUsername("Eugen");
		userExpected.setLogin("Eugen33");
		UserInfo userFinded = userInfoDAO.getUserInfo("Eugen33");
		assertEquals(userExpected, userFinded);
	}

}
