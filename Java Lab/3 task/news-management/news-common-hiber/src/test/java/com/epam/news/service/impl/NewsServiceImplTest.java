package com.epam.news.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Validator;

import com.epam.news.dao.NewsDAO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.entity.News;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplTest {

	@Mock
	private NewsDAO newsDAOmock;

	@Mock
	private DozerBeanMapper mapperMock;
	
	@Mock
	private Validator validatorMock;

	@InjectMocks
	private NewsServiceImpl service;
	
	@Before
	public void setupValidator() {
		when(validatorMock.supports(any())).thenReturn(true);
	}

	@Test
	public void saveNews() {
		service.save(new NewsDTO());
		verify(newsDAOmock).saveOrUpdate(any(News.class));
		verify(mapperMock).map(any(News.class), any());
		verify(mapperMock).map(any(NewsDTO.class), any());
	}

	@Test
	public void getById() {
		service.findById(1);
		verify(newsDAOmock).getById(1);
	}

	@Test
	public void updateNews() {
		service.update(new NewsDTO());
		verify(newsDAOmock).saveOrUpdate(any(News.class));
		verify(mapperMock).map(any(News.class), any());
	}

	@Test
	public void deleteNews() {
		service.deleteById(1);
		verify(newsDAOmock).deleteById(1);
	}

	@Test
	public void findAllNews() {
		service.findAll();
		verify(newsDAOmock).getAll();
	}

	@Test
	public void findBySearchCriteria() {
		SearchCriteria searchCriteria = new SearchCriteria();
		service.findBySearchCriteria(searchCriteria);
		verify(newsDAOmock).findBySearchCriteria(searchCriteria);
	}

	@Test
	public void countNews() {
		service.countNews();
		verify(newsDAOmock).countNews();
	}
}
