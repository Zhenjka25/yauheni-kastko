package com.epam.news.dao.jpa;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * The Class AbstractTest. Required for configuration of DBUnit.
 */
public abstract class AbstractTest {

	@Autowired
	DataSource dataSource;

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 * @throws Exception
	 *             the exception
	 */
	public IDatabaseConnection getConnection() throws Exception {
		Connection con = dataSource.getConnection();
		DatabaseMetaData databaseMetaData = con.getMetaData();
		IDatabaseConnection connection = new DatabaseConnection(con, databaseMetaData.getUserName().toUpperCase());
		return connection;
	}

	/**
	 * Gets the data set for insert.
	 *
	 * @return the data set for insert
	 * @throws DataSetException
	 *             the data set exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public abstract IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException;

	/**
	 * Gets the data set for delete.
	 *
	 * @return the data set for delete
	 * @throws DataSetException
	 *             the data set exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public IDataSet getDataSetForDelete() throws DataSetException, FileNotFoundException {
		return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/deleteData.xml"));
	}

	/**
	 * Configures database testcase.
	 *
	 * @throws DatabaseUnitException
	 *             the database unit exception
	 * @throws SQLException
	 *             the SQL exception
	 * @throws Exception
	 *             the exception
	 */
	public abstract void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception;

	/**
	 * Cleans data base.
	 *
	 * @throws DatabaseUnitException
	 *             the database unit exception
	 * @throws SQLException
	 *             the SQL exception
	 * @throws Exception
	 *             the exception
	 */
	public abstract void cleanDataBase() throws DatabaseUnitException, SQLException, Exception;
}