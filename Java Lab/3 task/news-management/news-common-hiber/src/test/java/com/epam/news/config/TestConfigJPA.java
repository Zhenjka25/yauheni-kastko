package com.epam.news.config;

import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_ECLIPSE_LINK;
import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_HIBERNATE;

import java.util.Properties;

import javax.sql.DataSource;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.instrument.classloading.InstrumentationLoadTimeWeaver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:app_test.properties" })
@ComponentScan("com.epam.news.dao, com.epam.news.service")
public class TestConfigJPA {

	private static final String PROP_DATABASE_DRIVER = "db.driver";
	private static final String PROP_DATABASE_PASSWORD = "db.password";
	private static final String PROP_DATABASE_URL = "db.url";
	private static final String PROP_DATABASE_USERNAME = "db.username";
	private static final String PROP_ENTITYMANAGER_PACKAGES_TO_SCAN = "db.entitymanager.packages.to.scan";

	private static final String PROP_HIBERNATE_DIALECT = "db.hibernate.dialect";
	private static final String PROP_HIBERNATE_SHOW_SQL = "db.hibernate.show_sql";

	private static final String PROP_ECLIPSE_DIALECT = "eclipselink.target-database";
	private static final String PROP_ECLIPSE_WEAVING = "eclipselink.weaving";
	private static final String PROP_ECLIPSE_LOGGING_LEVEL = "eclipselink.logging.level.sql";
	private static final String PROP_ECLIPSE_LOGGING_PARAMETERS = "eclipselink.logging.parameters";
	private static final String PROP_VALIDATION = "javax.persistence.validation.mode";

	@Autowired
	private Environment env;

	/**
	 * Data source. Returns data source without connection pool. It done for
	 * tests only.
	 *
	 * @return the data source
	 */
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty(PROP_DATABASE_DRIVER));
		dataSource.setUrl(env.getRequiredProperty(PROP_DATABASE_URL));
		dataSource.setUsername(env.getRequiredProperty(PROP_DATABASE_USERNAME));
		dataSource.setPassword(env.getRequiredProperty(PROP_DATABASE_PASSWORD));
		return dataSource;
	}

	@Bean
	@Profile(SPRING_PROFILE_HIBERNATE)
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryHiberNate() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] { env.getRequiredProperty(PROP_ENTITYMANAGER_PACKAGES_TO_SCAN) });
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(getHibernateProperties());
		return em;
	}

	@Bean
	@Profile(SPRING_PROFILE_ECLIPSE_LINK)
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryEclipseLink() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setLoadTimeWeaver(new InstrumentationLoadTimeWeaver());
		em.setPackagesToScan(new String[] { env.getRequiredProperty(PROP_ENTITYMANAGER_PACKAGES_TO_SCAN) });
		JpaVendorAdapter vendorAdapter = new EclipseLinkJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(getEclipseProperties());
		return em;
	}

	@Bean
	public JpaTransactionManager transactionManager() {
		return new JpaTransactionManager();
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
		localValidatorFactoryBean.setValidationMessageSource(messageSource());
		return localValidatorFactoryBean;
	}

	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename("locale");
		source.setDefaultEncoding("UTF-8");
		return source;
	}

	@Bean
	public DozerBeanMapper dozerBeanMapper() {
		return new DozerBeanMapper();
	}

	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put(PROP_HIBERNATE_DIALECT, env.getRequiredProperty(PROP_HIBERNATE_DIALECT));
		properties.put(PROP_HIBERNATE_SHOW_SQL, env.getRequiredProperty(PROP_HIBERNATE_SHOW_SQL));
		properties.put(PROP_VALIDATION, env.getRequiredProperty(PROP_VALIDATION));
		return properties;
	}

	private Properties getEclipseProperties() {
		Properties properties = new Properties();
		properties.put(PROP_ECLIPSE_DIALECT, env.getRequiredProperty(PROP_ECLIPSE_DIALECT));
		properties.put(PROP_ECLIPSE_WEAVING, env.getRequiredProperty(PROP_ECLIPSE_WEAVING));
		properties.put(PROP_ECLIPSE_LOGGING_LEVEL, env.getRequiredProperty(PROP_ECLIPSE_LOGGING_LEVEL));
		properties.put(PROP_ECLIPSE_LOGGING_PARAMETERS, env.getRequiredProperty(PROP_ECLIPSE_LOGGING_PARAMETERS));
		properties.put(PROP_VALIDATION, env.getRequiredProperty(PROP_VALIDATION));
		return properties;
	}

}
