package com.epam.news.config;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TestContextListener {

	public static final String PATH = "log4j_test.properties";

	@EventListener
	public void handleContextRefresh(ContextRefreshedEvent event) {
		PropertyConfigurator.configure(PATH);
	}

}
