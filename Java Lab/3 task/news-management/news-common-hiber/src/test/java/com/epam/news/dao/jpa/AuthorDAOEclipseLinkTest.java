package com.epam.news.dao.jpa;

import static com.epam.news.constant.ConstantsCommon.SPRING_PROFILE_ECLIPSE_LINK;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.config.TestConfigJPA;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfigJPA.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class,
		TransactionalTestExecutionListener.class })
@Transactional
@ActiveProfiles(SPRING_PROFILE_ECLIPSE_LINK)
public class AuthorDAOEclipseLinkTest extends AbstractTest {

	@Autowired
	private AuthorDAO authorDAO;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private NewsDAO newsDAO;

	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_authorData.xml")) };
		return new CompositeDataSet(datasets);
	}

	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		TestTransaction.end();
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteAuthor() {
		Author author = new Author();
		author.setAuthorName("wasilii");
		author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		author = authorDAO.saveOrUpdate(author);
		assertNotNull(author.getAuthorId());
		assertThat(authorDAO.getAll().size(), is(4));
		authorDAO.deleteById(author.getAuthorId());
		assertThat(authorDAO.getAll().size(), is(3));
	}

	@Test(expected = PersistenceException.class)
	public void saveFail() {
		Author author = new Author();
		author.setAuthorName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		author = authorDAO.saveOrUpdate(author);
		entityManager.flush();
	}

	@Test
	public void updateAuthor() {
		Author author = authorDAO.getById(1L);
		author.setAuthorName("Pupkin");
		authorDAO.saveOrUpdate(author);
		assertEquals("Pupkin", authorDAO.getById(1L).getAuthorName());
	}

	@Test
	public void getById() throws ParseException {
		Author author = authorDAO.getById(1L);
		assertEquals("Artur Babaev", author.getAuthorName());
		assertEquals(Timestamp.valueOf("2015-08-10 12:46:29.77").getTime(), author.getExpired().getTime());
		assertTrue(author.getNews().contains(newsDAO.getById(1L)));
		assertTrue(author.getNews().contains(newsDAO.getById(2L)));
	}

	@Test
	public void getByName() {
		Author author = authorDAO.getByName("Artur Babaev");
		assertThat(author.getAuthorId(), is(1L));
	}

	@Test
	public void getActualAuthors() {
		assertThat(authorDAO.getActualAuthors().size(), is(0));
		Author author = new Author();
		author.setAuthorName("Petya");
		authorDAO.saveOrUpdate(author);
		assertThat(authorDAO.getActualAuthors().size(), is(1));
	}

	@Test
	public void getNullResult() {
		assertNull(authorDAO.getById(10L));
	}

}
