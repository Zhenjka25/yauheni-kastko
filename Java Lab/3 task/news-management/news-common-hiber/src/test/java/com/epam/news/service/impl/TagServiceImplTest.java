package com.epam.news.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Validator;

import com.epam.news.dao.TagDAO;
import com.epam.news.dto.TagDTO;
import com.epam.news.entity.Tag;

/**
 * The Class TagServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {

	@Mock
	private TagDAO tagDAOmock;

	@Mock
	private DozerBeanMapper mapperMock;
	
	@Mock
	private Validator validatorMock;

	@InjectMocks
	private TagServiceImpl service;
	
	@Before
	public void setupValidator() {
		when(validatorMock.supports(any())).thenReturn(true);
	}

	@Test
	public void saveTag() {
		service.save(new TagDTO());
		verify(tagDAOmock).saveOrUpdate(any(Tag.class));
		verify(mapperMock).map(any(Tag.class), any());
		verify(mapperMock).map(any(TagDTO.class), any());
	}

	@Test
	public void getById() {
		service.findById(1);
		verify(tagDAOmock).getById(1);
	}

	@Test
	public void updateTag() {
		service.update(new TagDTO());
		verify(tagDAOmock).saveOrUpdate(any(Tag.class));
		verify(mapperMock).map(any(Tag.class), any());
	}

	@Test
	public void deleteTag() {
		service.deleteById(1);
		verify(tagDAOmock).deleteById(1);
	}

	@Test
	public void findAllTags() {
		service.findAll();
		verify(tagDAOmock).getAll();
	}

	@Test
	public void findByName() {
		String name = "name";
		service.findByName(name);
		verify(tagDAOmock).getByName(name);
	}

}
