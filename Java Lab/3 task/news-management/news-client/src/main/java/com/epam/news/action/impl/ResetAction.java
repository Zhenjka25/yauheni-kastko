package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;

/**
 * The Class ResetAction. Resets the filter settings.
 */
@Component(value = ACTION_RESET)
public class ResetAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	/**
	 * {@inheritDoc} Removes search criteria object from session.
	 */
	public ResponsePage execute(HttpServletRequest request) {
		request.getSession().removeAttribute(ATTRIBUTE_SEARCH_CRITERIA);
		request.getSession().removeAttribute(ATTRIBUTE_NUMBER_PAGIN);
		return new ResponsePage(configurationManager.getProperty(PAGE_INDEX), false);
	}

}
