package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.dto.NewsDTO;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import com.epam.news.util.Paginator;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsListAction. Jumps to the news list page.
 */
@Component(value = ACTION_VIEW_NEWS_LIST)
public class ViewNewsListAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private Paginator paginator;

	/**
	 * {@inheritDoc} Displays news list filtered by search criteria.
	 */
	public ResponsePage execute(HttpServletRequest request) {
		SearchCriteria searchCriteria = request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		paginator.processPagination(request, searchCriteria);
		List<NewsDTO> newsList = newsService.findBySearchCriteria(searchCriteria);

		request.setAttribute(ATTRIBUTE_NEWS_LIST, newsList);
		request.setAttribute(ATTRIBUTE_AUTHOR_LIST, authorService.findAll());
		request.setAttribute(ATTRIBUTE_TAG_LIST, tagService.findAll());

		request.getSession().setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE, PAGE_VIEW_NEWS_LIST_ACTION);

		return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS_LIST), false);
	}

}
