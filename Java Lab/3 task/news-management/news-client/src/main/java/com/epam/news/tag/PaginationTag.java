package com.epam.news.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * The Class PaginationTag. Uses for pagination on news list page.
 */
public class PaginationTag extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8464263595117061744L;

	/** The pages count. */
	private int pagesCount;

	/** The current page number. */
	private int currentPage;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			out.write(
					"<td><a class=\"arrows\" href=\"Controller?action=switchPage&switch=first\">&laquo;</a>&nbsp;&nbsp;</td>");
			out.write(
					"<td><a class=\"arrows\" href=\"Controller?action=switchPage&switch=decr\">&#8249;</a>&nbsp;&nbsp;</td>");
			out.write("<input id=\"inPage\" type=\"number\" name=\"page\" min=\"1\" max=\"" + pagesCount + "\" value=\""
					+ currentPage + "\">");
			// out.write("<label id=\"lab\" for=\"inPage\">" + currentPage + "/"
			// + pagesCount + "</label>");
			out.write(
					"<td>&nbsp;&nbsp;<a class=\"arrows\" href=\"Controller?action=switchPage&switch=incr\">&#8250;</a>&nbsp;&nbsp;</td>");
			out.write(
					"<td><a class=\"arrows\" href=\"Controller?action=switchPage&switch=last\">&#187;</a>&nbsp;&nbsp;</td>");
		} catch (IOException e) {
			throw new JspException(e);
		}
		return SKIP_BODY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	/**
	 * Sets the pages count.
	 *
	 * @param pagesCount
	 *            the new pages count
	 */
	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	/**
	 * Sets the current page.
	 *
	 * @param currentPage
	 *            the new current page
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

}
