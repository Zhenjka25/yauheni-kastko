package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.util.Paginator;
import com.epam.news.util.SearchCriteria;

@Component(value = ACTION_SWITCH_PAGE)
public class SwitchPageAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	@Autowired
	private Paginator paginator;

	@Override
	public ResponsePage execute(HttpServletRequest request) {
		HttpSession session = request.getSession();

		SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		Integer page = null;
		if (request.getParameter(PARAM_PAGE) != null) {
			page = Integer.parseInt(request.getParameter(PARAM_PAGE));
		}

		String switchTo = request.getParameter(PARAM_SWITCH_PAGE);

		paginator.switchPage(page, switchTo, searchCriteria);

		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);

		return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS_LIST_ACTION), true);
	}

}
