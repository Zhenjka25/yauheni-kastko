package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.dto.NewsDTO;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsAction. Jumps to the news page.
 */
@Component(value = ACTION_VIEW_NEWS)
public class ViewNewsAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	@Autowired
	private NewsService service;

	/**
	 * {@inheritDoc} Jumps to the chosen news page.
	 */
	public ResponsePage execute(HttpServletRequest request) {
		long newsId = request.getParameter(PARAM_NEWS_ID) == null
				? (long) request.getSession().getAttribute(PARAM_NEWS_ID)
				: Long.parseLong(request.getParameter(PARAM_NEWS_ID));

		SearchCriteria searchCriteria = request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		NewsDTO news = service.findById(newsId);

		request.setAttribute(ATTRIBUTE_NEWS, news);
		request.setAttribute(ATTRIBUTE_PREV_NEWS, service.findPrevNews(newsId, searchCriteria));
		request.setAttribute(ATTRIBUTE_NEXT_NEWS, service.findNextNews(newsId, searchCriteria));

		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE, PAGE_VIEW_NEWS_ACTION);
		request.getSession().setAttribute(ATTRIBUTE_NEWS_ID, newsId);

		return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS), false);
	}

}
