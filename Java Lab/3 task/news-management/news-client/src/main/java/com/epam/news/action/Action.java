
package com.epam.news.action;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.service.spi.ServiceException;

import com.epam.news.page.ResponsePage;

/**
 * The Interface Action. Base interface for all actions available to user.
 */
public interface Action {

	/**
	 * Executes action.
	 *
	 * @param request
	 *            the request
	 * @return the string
	 * @throws ServiceException
	 *             the service exception
	 */
	ResponsePage execute(HttpServletRequest request);

}
