package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;

/**
 * The Class ChangeLocaleAction. Changes the language.
 */
@Component(value = ACTION_CHANGE_LOCALE)
public class ChangeLocaleAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	/**
	 * {@inheritDoc} Changes locale.
	 */
	public ResponsePage execute(HttpServletRequest request) {
		String lang = request.getParameter(PARAM_LOCALE);
		request.getSession().setAttribute(ATTRIBUTE_LOCALE, lang);
		String page = configurationManager
				.getProperty((String) request.getSession().getAttribute(ATTRIBUTE_ACTUAL_PAGE));
		return new ResponsePage(page, false);
	}

}
