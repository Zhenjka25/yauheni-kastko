package com.epam.news.tag;

import java.util.List;

import com.epam.news.dto.AuthorDTO;
import com.epam.news.dto.TagDTO;

/**
 * The Class ListChecker. Checks equivalence of authors or tags by its id.
 */
public class ListChecker {

	/**
	 * Checks if author has the same id.
	 *
	 * @param author
	 *            the author
	 * @param authorId
	 *            the author id
	 * @return true, if successful
	 */
	public static boolean containsAuthor(AuthorDTO authorDTO, Long authorId) {
		return authorDTO.getAuthorId().equals(authorId);
	}

	/**
	 * Checks if tag id contained in id list.
	 *
	 * @param tag
	 *            the tag
	 * @param tagIdList
	 *            the tag id list
	 * @return true, if successful
	 */
	public static boolean containsTag(TagDTO tagDTO, List<Long> tagIdList) {
		for (Long tagId : tagIdList) {
			if (tagDTO.getTagId().equals(tagId)) {
				return true;
			}
		}
		return false;
	}

}
