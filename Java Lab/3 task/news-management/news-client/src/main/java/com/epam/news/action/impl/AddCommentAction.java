package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.dto.CommentDTO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.exception.DTOIntegrityException;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class AddCommentAction. Add comment to the news.
 */
@Component(value = ACTION_ADD_COMMENT)
public class AddCommentAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	@Autowired
	private CommentService commentService;

	@Autowired
	private NewsService newsService;

	/**
	 * {@inheritDoc} Adds comment to news.
	 */
	public ResponsePage execute(HttpServletRequest request) {

		Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));

		CommentDTO comment = new CommentDTO();
		comment.setCommentText(request.getParameter(PARAM_COMMENT_TEXT));
		comment.setCreationDate(new Date());
		comment.setNewsId(newsId);

		try {
			commentService.save(comment);
		} catch (DTOIntegrityException e) {
			request.setAttribute(ATTRIBUTE_COMMENT_TEXT_VALIDATION_ERROR,
					e.getResults().getFieldError(FIELD_COMMENT_TEXT).getDefaultMessage());
			initializeNews(request, newsId);
			return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS), false);
		}

		NewsDTO newsDTO = newsService.findById(newsId);

		request.setAttribute(ATTRIBUTE_NEWS, newsDTO);

		return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS_ACTION), true);
	}

	private void initializeNews(HttpServletRequest request, Long newsId) {
		SearchCriteria searchCriteria = request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		NewsDTO news = newsService.findById(newsId);

		request.setAttribute(ATTRIBUTE_NEWS, news);
		request.setAttribute(ATTRIBUTE_PREV_NEWS, newsService.findPrevNews(newsId, searchCriteria));
		request.setAttribute(ATTRIBUTE_NEXT_NEWS, newsService.findNextNews(newsId, searchCriteria));

		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE, PAGE_VIEW_NEWS_ACTION);
		request.getSession().setAttribute(ATTRIBUTE_NEWS_ID, newsId);
	}

}
