package com.epam.news.constant;

/**
 * The Class Constants. Contains all constants available to the client module.
 */
public final class ConstantsClient {

	// Actions
	public static final String ACTION_CHANGE_LOCALE = "changeLocale";
	public static final String ACTION_VIEW_NEWS = "viewNews";
	public static final String ACTION_VIEW_NEWS_LIST = "viewNewsList";
	public static final String ACTION_ADD_COMMENT = "addComment";
	public static final String ACTION_FILTER = "filter";
	public static final String ACTION_RESET = "reset";
	public static final String ACTION_SWITCH_PAGE = "switchPage";

	// Attributes
	public static final String ATTRIBUTE_LOCALE = "locale";
	public static final String ATTRIBUTE_NEWS = "news";
	public static final String ATTRIBUTE_NEWS_LIST = "newsList";
	public static final String ATTRIBUTE_PAGES_COUNT = "pagesCount";
	public static final String ATTRIBUTE_CURRENT_PAGE = "currentPage";
	public static final String ATTRIBUTE_AUTHOR_LIST = "authorList";
	public static final String ATTRIBUTE_TAG_LIST = "tagList";
	public static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
	public static final String ATTRIBUTE_NEWS_ID_LIST = "idList";
	public static final String ATTRIBUTE_PREV_NEWS = "prevNews";
	public static final String ATTRIBUTE_NEXT_NEWS = "nextNews";
	public static final String ATTRIBUTE_ERROR = "error";
	public static final String ATTRIBUTE_ACTUAL_PAGE = "actualPage";
	public static final String ATTRIBUTE_NEWS_ID = "newsId";
	public static final String ATTRIBUTE_TIME_ZONE_OFFSET = "timezoneOffset";
	public static final String ATTRIBUTE_NUMBER_PAGIN = "numberPagin";
	public static final String ATTRIBUTE_DIGITS_PER_PAGE = "digitsPerPage";
	public static final String ATTRIBUTE_NUMBER_OF_ALL_PAGIN = "numberOfAllPagin";
	public static final String ATTRIBUTE_COMMENT_TEXT_VALIDATION_ERROR = "commentTextError";

	// Pages
	public static final String PAGE_VIEW_NEWS = "path.page.news";
	public static final String PAGE_VIEW_NEWS_LIST = "path.page.newslist";
	public static final String PAGE_INDEX = "path.page.index";
	public static final String PAGE_ERROR = "path.page.error";
	public static final String PAGE_VIEW_NEWS_ACTION = "path.action.viewnews";
	public static final String PAGE_VIEW_NEWS_LIST_ACTION = "path.action.viewnewslist";

	// Params
	public static final String PARAM_LOCALE = "locale";
	public static final String PARAM_NEWS_ID = "newsId";
	public static final String PARAM_AUTHOR_ID = "authorId";
	public static final String PARAM_TAGS = "tags";
	public static final String PARAM_COMMENT_TEXT = "commentText";
	public static final String PARAM_PAGE = "page";
	public static final String PARAM_ACTION = "action";
	public static final String PARAM_INCR_PAGE = "incrPage";
	public static final String PARAM_NUMBER_PAGIN = "numberPaginParam";
	public static final String PARAM_SWITCH_PAGE = "switch";

	// Cookies
	public static final String COOKIE_OFFSET = "timezoneOffset";

	public static final String DECR = "decr";
	public static final String INCR = "incr";
	
	public static final String FIELD_COMMENT_TEXT = "commentText";

}
