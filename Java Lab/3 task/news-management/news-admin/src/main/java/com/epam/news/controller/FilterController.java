package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.service.spi.ServiceException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.util.SearchCriteria;

/**
 * The Class FilterController. Controller to set and reset search filter.
 */
@Controller
@RequestMapping(value = "/admin")
public class FilterController {

	private static final String REDIRECT_PATH = "redirect:/admin/newslist";

	/**
	 * Filter.
	 *
	 * @param authorId
	 *            the author id
	 * @param tagIdArray
	 *            the tag id array
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public ModelAndView filter(@RequestParam(value = PARAM_AUTHOR_ID, required = false) Long authorId,
			@RequestParam(value = PARAM_TAGS, required = false) Long[] tagIdArray, HttpSession session) {

		List<Long> tagList = new ArrayList<>();
		if (tagIdArray != null && tagIdArray.length != 0) {
			tagList = Arrays.asList(tagIdArray);
		}

		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagsId(tagList);

		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		session.removeAttribute(ATTRIBUTE_NUMBER_PAGIN);

		return new ModelAndView(REDIRECT_PATH);
	}

	/**
	 * Reset.
	 *
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public ModelAndView reset(HttpSession session) {
		session.removeAttribute(ATTRIBUTE_SEARCH_CRITERIA);
		session.removeAttribute(ATTRIBUTE_NUMBER_PAGIN);
		return new ModelAndView(REDIRECT_PATH);
	}

}
