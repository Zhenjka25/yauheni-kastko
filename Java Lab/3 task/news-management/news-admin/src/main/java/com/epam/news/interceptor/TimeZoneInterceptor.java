package com.epam.news.interceptor;

import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_TIME_ZONE_OFFSET;
import static com.epam.news.constant.ConstantsAdmin.COOKIE_OFFSET;

import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * The Class TimeZoneInterceptor. Gets the client time zone from the cookies and
 * adds the time zone offset to the user session. Therefore user can see date in
 * its time zone, rather than servers's time zone.
 */
public class TimeZoneInterceptor extends HandlerInterceptorAdapter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		if (request.getSession().getAttribute(ATTRIBUTE_TIME_ZONE_OFFSET) == null && request.getCookies() != null) {

			Cookie cookie = findTimeZoneOffsetCookie(request);

			if (cookie != null) {
				int timezoneOffsetMinutes = Integer.parseInt(cookie.getValue());
				TimeZone timeZone = TimeZone.getTimeZone("UTC");
				timeZone.setRawOffset((int) (timezoneOffsetMinutes * DateUtils.MILLIS_PER_MINUTE));
				Config.set(request.getSession(), Config.FMT_TIME_ZONE, timeZone);
				request.getSession().setAttribute(ATTRIBUTE_TIME_ZONE_OFFSET, timezoneOffsetMinutes);
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}

		return true;
	}

	/**
	 * Finds the time zone offset cookie from request.
	 *
	 * @param request
	 *            the request
	 * @return the cookie
	 */
	private Cookie findTimeZoneOffsetCookie(HttpServletRequest request) {
		for (Cookie cookie : request.getCookies()) {
			if (COOKIE_OFFSET.equals(cookie.getName())) {
				return cookie;
			}
		}
		return null;
	}
}
