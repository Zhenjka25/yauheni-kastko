package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.VIEW_LOGIN;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class HomeController. Controller for home page. If user is logged in, it
 * goes to the page with a list of news. If not, it goes to the login page.
 */
@Controller
public class HomeController {

	private static final String REDIRECT_PATH = "redirect:/admin/newslist";

	/**
	 * Home.
	 *
	 * @return the model and view
	 */
	@RequestMapping(value = "/")
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName(REDIRECT_PATH);
		} else {
			model.setViewName(VIEW_LOGIN);
		}
		
		return model;
	}

}
