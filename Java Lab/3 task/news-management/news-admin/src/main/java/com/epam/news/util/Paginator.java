package com.epam.news.util;

import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_CURRENT_PAGE;
import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_PAGES_COUNT;
import static com.epam.news.constant.ConstantsCommon.NEWS_PER_PAGE_KEY;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.service.NewsService;

@Component
public class Paginator {

	private static final String SWITCH_FIRST = "first";
	private static final String SWITCH_DECR = "decr";
	private static final String SWITCH_INCR = "incr";
	private static final String SWITCH_LAST = "last";

	@Autowired
	private NewsService service;

	@Autowired
	private Environment env;

	public void processPagination(ModelAndView model, SearchCriteria searchCriteria) {
		model.addObject(ATTRIBUTE_CURRENT_PAGE, searchCriteria.getPage());
		int pagesCount = getPagesCount(searchCriteria);
		model.addObject(ATTRIBUTE_PAGES_COUNT, pagesCount);
	}

	public void switchPage(Integer page, String switchTo, SearchCriteria searchCriteria) {
		if (page != null) {
			searchCriteria.setPage(page);
		} else {
			switch (switchTo) {
			case SWITCH_FIRST:
				searchCriteria.setPage(1);
				break;
			case SWITCH_DECR:
				searchCriteria.prevPage();
				break;
			case SWITCH_INCR:
				int pagesCount = getPagesCount(searchCriteria);
				if (searchCriteria.getPage() < pagesCount) {
					searchCriteria.nextPage();
				}
				break;
			case SWITCH_LAST:
				searchCriteria.setPage(getPagesCount(searchCriteria));
				break;
			}
		}
	}

	private int getPagesCount(SearchCriteria searchCriteria) {
		int newsPerPage = Integer.parseInt(env.getProperty(NEWS_PER_PAGE_KEY));
		int newsNumber = service.countNewsBySearchCriteria(searchCriteria);
		int pagesCount = newsNumber / newsPerPage;
		if ((newsNumber % newsPerPage) != 0) {
			pagesCount++;
		}
		return pagesCount;
	}

}
