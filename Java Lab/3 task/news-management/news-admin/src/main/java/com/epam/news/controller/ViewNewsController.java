package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.dto.CommentDTO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.exception.NotFoundException;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsController. Controller to view the news page.
 */
@Controller
@RequestMapping(value = "/admin")
public class ViewNewsController {

	@Autowired
	private NewsService service;

	/**
	 * View news.
	 *
	 * @param newsId
	 *            the news id
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 * @throws NotFoundException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}")
	public ModelAndView viewNews(@PathVariable(value = "newsId") Long newsId, HttpSession session) {

		NewsDTO newsDTO = service.findById(newsId);

		if (newsDTO == null) {
			throw new NotFoundException();
		}

		ModelAndView model = new ModelAndView();

		SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		CommentDTO comment = new CommentDTO();
		comment.setCreationDate(new Date());
		comment.setNewsId(newsId);

		model.addObject(ATTRIBUTE_NEWS, newsDTO);
		model.addObject(ATTRIBUTE_PREV_NEWS, service.findPrevNews(newsId, searchCriteria));
		model.addObject(ATTRIBUTE_NEXT_NEWS, service.findNextNews(newsId, searchCriteria));
		model.addObject(ATTRIBUTE_COMMENT, comment);
		model.setViewName(VIEW_NEWS);

		return model;
	}

}
