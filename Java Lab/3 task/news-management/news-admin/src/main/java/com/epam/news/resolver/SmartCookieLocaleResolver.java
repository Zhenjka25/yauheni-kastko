package com.epam.news.resolver;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * The Class SmartCookieLocaleResolver. Falls back to Spring's
 * {@link SessionLocaleResolver} when no cookie is found. This allows users with
 * cookies disabled to still have a custom Locale for the duration of their
 * session.
 */
public class SmartCookieLocaleResolver extends CookieLocaleResolver {

	/** The session locale resolver. */
	private SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected Locale determineDefaultLocale(HttpServletRequest request) {
		return sessionLocaleResolver.resolveLocale(request);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
		super.setLocale(request, response, locale);
		sessionLocaleResolver.setLocale(request, response, locale);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDefaultLocale(Locale defaultLocale) {
		sessionLocaleResolver.setDefaultLocale(defaultLocale);
	}

}
