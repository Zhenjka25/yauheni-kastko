package com.epam.news.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.epam.news.service.UserService;

/**
 * The Class SecurityConfig. Spring Security configuration for admin application.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	/**
	 * Registers global authentication.
	 *
	 * @param auth the auth
	 * @throws Exception the exception
	 */
	@Autowired
	public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(new Md5PasswordEncoder());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
        .antMatchers("/resources/**").permitAll();
		
		 http.formLogin()
		 .loginPage("/login")
		 .loginProcessingUrl("/j_spring_security_check")
		 .failureUrl("/login?error")
		 .usernameParameter("login")
		 .passwordParameter("password")
		 .permitAll();
		 
		 http.authorizeRequests()
		 .antMatchers("/admin/**")
		 .access("hasRole('ROLE_ADMIN')");
		
		 http.logout()
		 .permitAll()
		 .logoutUrl("/logout")
		 .logoutSuccessUrl("/login?logout")
		 .invalidateHttpSession(true);

	}

}
