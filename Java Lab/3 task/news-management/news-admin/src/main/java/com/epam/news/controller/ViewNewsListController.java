package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.dto.NewsDTO;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import com.epam.news.util.Paginator;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsListController. Controller to view the list of news page.
 */
@Controller
@RequestMapping(value = "/admin")
public class ViewNewsListController {

	private static final String CURRENT = "newsList";

	private static final String REDIRECT_NEWS_LIST_PATH = "redirect:/admin/newslist";

	@Autowired
	private NewsService service;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private Paginator paginator;

	/**
	 * View news list.
	 *
	 * @param page
	 *            the page
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/newslist")
	public ModelAndView viewNewsList(HttpSession session) {

		ModelAndView model = new ModelAndView();

		SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);

		paginator.processPagination(model, searchCriteria);

		List<NewsDTO> newsList = service.findBySearchCriteria(searchCriteria);

		model.addObject(ATTRIBUTE_NEWS_LIST, newsList);
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.findAll());
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_NEWS_LIST);

		return model;
	}

	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/changePage")
	public ModelAndView changePage(@RequestParam(value = PARAM_PAGE, required = false) Integer page,
			@RequestParam(value = PARAM_SWITCH_PAGE, required = false) String switchTo, HttpSession session) {

		SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();

		paginator.switchPage(page, switchTo, searchCriteria);

		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);

		return new ModelAndView(REDIRECT_NEWS_LIST_PATH);
	}

}
