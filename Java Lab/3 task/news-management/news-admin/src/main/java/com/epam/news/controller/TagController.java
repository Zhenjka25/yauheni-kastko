package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.List;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.epam.news.dto.TagDTO;
import com.epam.news.entity.Tag;
import com.epam.news.service.TagService;

/**
 * The Class TagController. Controller to add, edit and delete tags.
 */
@Controller
@RequestMapping(value = "/admin")
public class TagController {

	private static Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	private static final String CURRENT = "tags";
	private static final String REDIRECT_PATH = "redirect:/admin/view/edittags";

	@Autowired
	private TagService tagService;

	/**
	 * View edit tags page.
	 *
	 * @param editTagId
	 *            the edit tag id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.GET)
	public ModelAndView viewEditTagsPage(@RequestParam(value = PARAM_EDIT_TAG_ID, required = false) Long editTagId) {

		ModelAndView model = new ModelAndView();

		List<TagDTO> tags = tagService.findAll();

		if (editTagId != null) {
			TagDTO editTag = tagService.findById(editTagId);
			model.addObject(ATTRIBUTE_EDIT_TAG, editTag);
		}

		model.addObject(ATTRIBUTE_TAG_LIST, tags);
		model.addObject(ATTRIBUTE_SAVE_TAG, new Tag());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_EDIT_TAGS);

		return model;
	}

	/**
	 * Edits the tag.
	 *
	 * @param tag
	 *            the tag
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.POST, params = PARAM_EDIT)
	public ModelAndView editTag(@Valid @ModelAttribute(ATTRIBUTE_EDIT_TAG) TagDTO tag, BindingResult bindingResult) {

		ModelAndView model = new ModelAndView();

		if (bindingResult.hasErrors()) {
			List<TagDTO> tags = tagService.findAll();

			model.addObject(ATTRIBUTE_EDIT_TAG, tag);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES, bindingResult.getFieldErrors());
			model.addObject(ATTRIBUTE_TAG_LIST, tags);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_TAGS);

			return model;
		} else {
			tagService.update(tag);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * Save author.
	 *
	 * @param tag
	 *            the tag
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.POST, params = PARAM_SAVE)
	public ModelAndView saveTag(@Valid @ModelAttribute(ATTRIBUTE_SAVE_TAG) TagDTO tag, BindingResult bindingResult) {

		ModelAndView model = new ModelAndView();

		if (bindingResult.hasErrors()) {
			List<TagDTO> tags = tagService.findAll();

			model.addObject(ATTRIBUTE_EDIT_TAG, tag);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES, bindingResult.getFieldErrors());
			model.addObject(ATTRIBUTE_TAG_LIST, tags);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_TAGS);

			return model;
		} else {
			tagService.save(tag);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * Expire author.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", params = PARAM_DELETE, method = RequestMethod.POST)
	public ModelAndView deleteTag(@RequestParam(value = PARAM_TAG_ID, required = true) Long tagId) {
		tagService.deleteById(tagId);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH);
		return model;
	}

	/**
	 * Optimistic lock exception handler.
	 *
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@ExceptionHandler(OptimisticLockException.class)
	public ModelAndView optimLockExceptionHandler(OptimisticLockException e, HttpServletRequest request) {
		log.error(e);
		FlashMap map = RequestContextUtils.getOutputFlashMap(request);
		map.put(ATTRIBUTE_ERROR, ERROR_MESSAGE_OPTIM_LOCK_TAG);
		return new ModelAndView(REDIRECT_PATH);
	}

}
