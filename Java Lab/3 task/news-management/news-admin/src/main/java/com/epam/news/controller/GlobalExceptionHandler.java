package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.epam.news.exception.NotFoundException;

/**
 * The Class GlobalExceptionHandler. Handles all exceptions thrown by
 * controllers.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	/**
	 * Default error handler.
	 *
	 * @param e
	 *            the e
	 * @return the model and view
	 * @throws Exception
	 *             the exception
	 */
	@ExceptionHandler(value = Exception.class)
	public ModelAndView defaultExceptionHandler(Exception e) throws Exception {
		log.error(e);

		if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null) {
			throw e;
		}

		ModelAndView model = new ModelAndView();
		model.addObject(ATTRIBUTE_ERROR, ERROR_MESSAGE_DEFAULT);
		model.setViewName(VIEW_ERROR);

		return model;
	}

	/**
	 * No handler found exception handler.
	 *
	 * @param e
	 *            the e
	 * @return the model and view
	 */
	@ExceptionHandler(value = NoHandlerFoundException.class)
	public ModelAndView NoHandlerFoundExceptionHandler(NoHandlerFoundException e) {
		log.error(e);

		ModelAndView model = new ModelAndView();
		model.addObject(ATTRIBUTE_ERROR, ERROR_MESSAGE_NOT_FOUND);
		model.setViewName(VIEW_ERROR);

		return model;
	}

	/**
	 * Not found exception handler.
	 *
	 * @param e
	 *            the e
	 * @return the model and view
	 */
	@ExceptionHandler(value = NotFoundException.class)
	public ModelAndView notFoundExceptionHandler(NotFoundException e) {
		log.error(e);

		ModelAndView model = new ModelAndView();
		model.addObject(ATTRIBUTE_ERROR, ERROR_MESSAGE_NOT_FOUND);
		model.setViewName(VIEW_ERROR);

		return model;
	}

}