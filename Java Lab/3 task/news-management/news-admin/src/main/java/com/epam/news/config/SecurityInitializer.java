package com.epam.news.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * The Class SecurityInitializer. Registers Spring Security filters.
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
