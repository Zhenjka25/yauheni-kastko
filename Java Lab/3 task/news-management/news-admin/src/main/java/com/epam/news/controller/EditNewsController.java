package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.List;

import javax.persistence.OptimisticLockException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.epam.news.dto.AuthorDTO;
import com.epam.news.dto.NewsDTO;
import com.epam.news.dto.TagDTO;
import com.epam.news.entity.News;
import com.epam.news.exception.NotFoundException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;

/**
 * The Class EditNewsController. Controller to edit news.
 */
@Controller
@RequestMapping(value = "/admin")
public class EditNewsController {

	private static Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	private static final String REDIRECT_PATH_SUCCESS = "redirect:/admin/news/";
	private static final String REDIRECT_PATH_EDIT = "redirect:/admin/view/editnews/";

	@Autowired
	private NewsService service;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	/**
	 * View edit news page.
	 *
	 * @param newsId
	 *            the news id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editnews/{newsId}", method = RequestMethod.GET)
	public ModelAndView viewEditNewsPage(@PathVariable(value = "newsId") Long newsId) {
		ModelAndView model = new ModelAndView();

		NewsDTO newsDTO = service.findById(newsId);
		if (newsDTO == null) {
			throw new NotFoundException();
		}

		List<AuthorDTO> authors = authorService.findActualAuthors();
		if (newsDTO.getAuthor() != null) {
			authors.add(newsDTO.getAuthor());
		}

		model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
		model.addObject(ATTRIBUTE_NEWS_VALUE, newsDTO);
		model.setViewName(VIEW_EDIT_NEWS);

		return model;
	}

	/**
	 * Edits the news.
	 *
	 * @param newsValue
	 *            the news value
	 * @param bindingResult
	 *            the binding result
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editnews/{newsId}", method = RequestMethod.POST)
	public ModelAndView editNews(@Valid @ModelAttribute(value = ATTRIBUTE_NEWS_VALUE) NewsDTO newsDTO,
			BindingResult bindingResult, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();

		if (bindingResult.hasErrors()) {

			if (newsDTO.getTags() != null && !newsDTO.getTags().isEmpty()) {
				newsDTO.setTags(tagService.findByTagIdList(newsDTO.getTags()));
			}

			model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.findActualAuthors());
			model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
			model.setViewName(VIEW_EDIT_NEWS);

			return model;
		}

		newsDTO.setAuthor(authorService.findById(newsDTO.getAuthor().getAuthorId()));
		if (newsDTO.getTags() != null) {
			newsDTO.setTags(tagService.findByTagIdList(newsDTO.getTags()));
		}
		service.update(newsDTO);

		FlashMap map = RequestContextUtils.getOutputFlashMap(request);
		map.put(ATTRIBUTE_CREATE_OR_UPDATE_NEWS, true);

		model.setViewName(REDIRECT_PATH_SUCCESS + newsDTO.getNewsId());

		return model;
	}

	/**
	 * Optimistic lock exception handler.
	 *
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@ExceptionHandler(OptimisticLockException.class)
	public ModelAndView optimLockExceptionHandler(OptimisticLockException e, HttpServletRequest request) {
		log.error(e);
		News news = (News) e.getEntity();
		FlashMap map = RequestContextUtils.getOutputFlashMap(request);
		map.put(ATTRIBUTE_ERROR, ERROR_MESSAGE_OPTIM_LOCK_NEWS);
		return new ModelAndView(REDIRECT_PATH_EDIT + news.getNewsId());
	}

	/**
	 * Inits the binder.
	 *
	 * @param binder
	 *            the binder
	 */
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(List.class, "tags", new CustomCollectionEditor(List.class) {
			@Override
			protected Object convertElement(Object element) {
				if (element instanceof TagDTO) {
					return element;
				}
				if (element instanceof String) {
					TagDTO tagDTO = new TagDTO();
					tagDTO.setTagId(Long.parseLong((String) element));
					return tagDTO;
				} else {
					return null;
				}
			}
		});
	}

}
