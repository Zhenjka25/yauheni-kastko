<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ctg" uri="customtags"%>

<div id="content">
	<div id="content-search">
		<form method="POST" action='<c:url value="/admin/filter" />'>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div id="content-authors">
				<select id="ss" name="authorId"><option></option>
					<c:forEach items="${authorList}" var="author">
						<c:choose>
							<c:when
								test="${ctg:containsAuthor(author, searchCriteria.authorId) }">
								<option selected="selected" value="${author.authorId }">${ctg:escapeHtml4(author.authorName) }</option>
							</c:when>
							<c:otherwise>
								<option value="${author.authorId }">${ctg:escapeHtml4(author.authorName) }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach></select>
			</div>
			<div id="content-tags">
				<select id="ms" name="tags" multiple="multiple"><c:forEach
						items="${tagList}" var="tag">
						<c:choose>
							<c:when test="${ctg:containsTag(tag, searchCriteria.tagsId) }">
								<option selected="selected" value="${tag.tagId }">${ctg:escapeHtml4(tag.tagName) }</option>
							</c:when>
							<c:otherwise>
								<option value="${tag.tagId }">${ctg:escapeHtml4(tag.tagName) }</option>
							</c:otherwise>
						</c:choose>
					</c:forEach></select>
			</div>
			<script>
				$('#ms').multipleSelect();
			</script>
			<div id="content-filter-button">
				<input type="submit" value="<fmt:message key="search.filter" />" />
			</div>
		</form>
		<div id="content-reset-button">
			<form method="POST" action='<c:url value="/admin/reset" />'>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /> <input type="submit"
					value="<fmt:message key="search.reset" />">
			</form>
		</div>
	</div>
	<div id="news-list-content">
		<form method="POST" action='<c:url value="/admin/news/deletenews" />'>
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<c:forEach items="${newsList}" var="news">
				<div class="short-news">
					<div class="short-news-title">
						<strong>${ctg:escapeHtml4(news.title) }</strong>
					</div>
					<div class="short-news-author">
						(
						<fmt:message key="news.author" />
						${ctg:escapeHtml4(news.author.authorName) })
					</div>
					<div class="short-news-date">
						<fmt:formatDate pattern="dd-MM-yyy "
							value="${news.modificationDate}" />
					</div>
					<div style="clear: both"></div>
					<div class="short-news-text">${ctg:escapeHtml4(news.shortText) }</div>
					<div class="short-news-view-link">
						<a href="news/${news.newsId }"><fmt:message
								key="view.news.link" /></a>
					</div>
					<div class="short-news-comments-number">
						<fmt:message key="comments.count" />
						(${news.comments.size() })
					</div>
					<div class="short-news-tag-list">
						<c:forEach items="${news.tags}" var="tag">
					${ctg:escapeHtml4(tag.tagName) }
				</c:forEach>
					</div>
					<div style="clear: both"></div>
				</div>
				<div class="short-news-delete-checkbox">
					<input type="checkbox" name="newsIdList"
						value="${news.newsId }">
				</div>
				<div class="short-news-edit-link">
					<a
						href="<c:url value="/admin/view/editnews/${news.newsId }" />"><fmt:message
							key="news.edit.link" /></a>
				</div>
				<div style="clear: both"></div>
			</c:forEach>
			<input id="delete-news-button" name="submit" type="submit"
				value="<fmt:message key="newslist.delete" />" />
			<div style="clear: right"></div>
		</form>
	</div>
	<div id="navigation-by-list">
		<div id="paginationDigits">
			<form method="POST" action='<c:url value="/admin/changePage" />'>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<ctg:pagination currentPage="${currentPage }"
					pagesCount="${pagesCount }" />
			</form>
		</div>
	</div>
</div>
<div style="clear: both"></div>