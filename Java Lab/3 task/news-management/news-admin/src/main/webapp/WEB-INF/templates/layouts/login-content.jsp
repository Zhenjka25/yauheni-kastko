<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div id="login-content">
	<div id="login-form">
		<c:if test="${not empty error}">
			<div id="login-error">
				<fmt:message key="${error}" />
			</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div id="login-message">
				<fmt:message key="${msg}" />
			</div>
		</c:if>
		<c:url value="/j_spring_security_check" var="loginUrl" />
		<form name='loginForm' action="${loginUrl }" method='POST'>

			<table>
				<tr>
					<td><fmt:message key="login.user" /></td>
					<td><input type='text' name='login'></td>
				</tr>
				<tr>
					<td><fmt:message key="login.password" /></td>
					<td><input type='password' name='password' /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit"
						value="<fmt:message key="login.enter" />" /></td>
				</tr>
			</table>

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

		</form>
	</div>
</div>
<div style="clear: both"></div>