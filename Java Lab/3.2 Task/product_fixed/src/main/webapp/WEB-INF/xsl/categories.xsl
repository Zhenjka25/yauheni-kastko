<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://www.w3schools.com">
	<xsl:output method="html" indent="yes" />

	<xsl:param name="errorMessage" />

	<xsl:template match="/">
		<html>
			<head>
				<title>Categories</title>
			</head>
			<body>
				<div style="color: red; font-style: italic">
					<xsl:value-of select="$errorMessage" />
				</div>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="a:categories">
		<xsl:for-each select="a:category">
			<div>
				<xsl:variable name="category" select="@name"></xsl:variable>
				<a href="/product/{$category}">
					<xsl:value-of select="$category" />
					(
					<xsl:value-of select="count(a:subcategory/a:product)" />
					)
				</a>
			</div>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>