package com.epam.product.constant;

/**
 * The Class FilePathes. Contains path to xml and xsl files.
 */
public class FilePathes {

	public static final String XML_FILE = "/resources/xml/products.xml";
	public static final String XSL_ADD_PRODUCT = "/WEB-INF/xsl/add-product.xsl";
	public static final String XSL_ADD_PRODUCT_PAGE = "/WEB-INF/xsl/add-product-page.xsl";
	public static final String XSL_CATEGORIES = "/WEB-INF/xsl/categories.xsl";
	public static final String XSL_PRODUCT_LIST = "/WEB-INF/xsl/product-list.xsl";
	public static final String XSL_SUBCATEGORY = "/WEB-INF/xsl/subcategory.xsl";

}
