package com.epam.product.constant;

public class BeanNames {
	
	public static final String CATEGORY_BEAN = "category";
	public static final String SUBCATEGORY_BEAN = "subcategory";
	public static final String PRODUCT_LIST_BEAN = "productList";
	public static final String ADD_PRODUCT_PAGE_BEAN = "addProductPage";
	public static final String ADD_PRODUCT_BEAN = "addProduct";

}
