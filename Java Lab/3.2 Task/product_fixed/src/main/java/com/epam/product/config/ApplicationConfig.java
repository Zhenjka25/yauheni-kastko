package com.epam.product.config;

import static com.epam.product.constant.BeanNames.*;
import static com.epam.product.constant.FilePathes.*;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.ServletContext;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.epam.product.validator.ProductValidator;

/**
 * The Class ApplicationConfig. Spring configuration for application.
 */
@EnableWebMvc
@Configuration
@ComponentScan("com.epam.product.controller, com.epam.product.parser, com.epam.product.validator")
public class ApplicationConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private ServletContext serevletContext;

	/**
	 * Validator.
	 *
	 * @return the product validator
	 */
	@Bean
	public ProductValidator validator() {
		return new ProductValidator();
	}

	/**
	 * Lock.
	 *
	 * @return the lock
	 */
	@Bean
	public ReadWriteLock lock() {
		return new ReentrantReadWriteLock(true);
	}

	/**
	 * Transformer factory.
	 *
	 * @return the transformer factory
	 */
	@Bean
	public TransformerFactory transformerFactory() {
		return TransformerFactory.newInstance();
	}

	/**
	 * Category template.
	 *
	 * @return the templates
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	@Bean(name = CATEGORY_BEAN)
	public Templates categoryTemplate() throws TransformerConfigurationException {
		String xslPath = serevletContext.getRealPath(XSL_CATEGORIES);
		return transformerFactory().newTemplates(new StreamSource(xslPath));
	}

	/**
	 * Subcategory template.
	 *
	 * @return the templates
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	@Bean(name = SUBCATEGORY_BEAN)
	public Templates subcategoryTemplate() throws TransformerConfigurationException {
		String xslPath = serevletContext.getRealPath(XSL_SUBCATEGORY);
		return transformerFactory().newTemplates(new StreamSource(xslPath));
	}

	/**
	 * Product list template.
	 *
	 * @return the templates
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	@Bean(name = PRODUCT_LIST_BEAN)
	public Templates productListTemplate() throws TransformerConfigurationException {
		String xslPath = serevletContext.getRealPath(XSL_PRODUCT_LIST);
		return transformerFactory().newTemplates(new StreamSource(xslPath));
	}

	/**
	 * Adds the product page template.
	 *
	 * @return the templates
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	@Bean(name = ADD_PRODUCT_PAGE_BEAN)
	public Templates addProductPageTemplate() throws TransformerConfigurationException {
		String xslPath = serevletContext.getRealPath(XSL_ADD_PRODUCT_PAGE);
		return transformerFactory().newTemplates(new StreamSource(xslPath));
	}

	/**
	 * Adds the product template.
	 *
	 * @return the templates
	 * @throws TransformerConfigurationException
	 *             the transformer configuration exception
	 */
	@Bean(name = ADD_PRODUCT_BEAN)
	public Templates addProductTemplate() throws TransformerConfigurationException {
		String xslPath = serevletContext.getRealPath(XSL_ADD_PRODUCT);
		return transformerFactory().newTemplates(new StreamSource(xslPath));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #addResourceHandlers(org.springframework.web.servlet.config.annotation.
	 * ResourceHandlerRegistry)
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

}
