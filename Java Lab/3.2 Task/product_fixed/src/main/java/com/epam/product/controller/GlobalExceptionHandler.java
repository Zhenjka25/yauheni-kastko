package com.epam.product.controller;

import static com.epam.product.constant.Attributes.ERROR_MESSAGE;
import static com.epam.product.constant.BeanNames.CATEGORY_BEAN;
import static com.epam.product.constant.FilePathes.XML_FILE;

import java.io.*;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * The Class GlobalExceptionHandler. Handles all exceptions which may be throwed
 * by controllers.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private ReadWriteLock lock;

	@Autowired
	@Qualifier(CATEGORY_BEAN)
	private Templates categoryTemplate;

	private static Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	private static final String TRANSFORMATION_ERROR = "XSL transformation exception";
	private static final String IO_ERRROR = "Input/Output exception";
	private static final String PATH_ERRROR = "Path not found";

	/**
	 * Transformation exception handler.
	 *
	 * @param te
	 *            the te
	 * @return the string
	 * @throws TransformerException
	 *             the transformer exception
	 */
	@ExceptionHandler(value = TransformerException.class)
	@ResponseBody
	public String transformationExceptionHandler(TransformerException te) throws TransformerException {
		log.error(te);
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = categoryTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			Writer result = new StringWriter();
			transformer.setParameter(ERROR_MESSAGE, TRANSFORMATION_ERROR);
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * Io exception handler.
	 *
	 * @param ioe
	 *            the ioe
	 * @return the string
	 * @throws TransformerException
	 *             the transformer exception
	 */
	@ExceptionHandler(value = IOException.class)
	@ResponseBody
	public String ioExceptionHandler(IOException ioe) throws TransformerException {
		log.error(ioe);
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = categoryTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			Writer result = new StringWriter();
			transformer.setParameter(ERROR_MESSAGE, IO_ERRROR);
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * No handler found exception handler.
	 *
	 * @param e
	 *            the e
	 * @param request
	 *            the request
	 * @return the string
	 * @throws TransformerException
	 *             the transformer exception
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseBody
	public String noHandlerFoundExceptionHandler(NoHandlerFoundException e, HttpServletRequest request)
			throws TransformerException {
		log.error(e);
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = categoryTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			Writer result = new StringWriter();
			transformer.setParameter(ERROR_MESSAGE, PATH_ERRROR);
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

}
