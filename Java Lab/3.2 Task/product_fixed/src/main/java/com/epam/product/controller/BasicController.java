package com.epam.product.controller;

import static com.epam.product.constant.Attributes.CATEGORY;
import static com.epam.product.constant.Attributes.ERRORS;
import static com.epam.product.constant.Attributes.PRODUCT;
import static com.epam.product.constant.Attributes.SUBCATEGORY;
import static com.epam.product.constant.Attributes.VALIDATOR;
import static com.epam.product.constant.BeanNames.ADD_PRODUCT_BEAN;
import static com.epam.product.constant.BeanNames.ADD_PRODUCT_PAGE_BEAN;
import static com.epam.product.constant.BeanNames.CATEGORY_BEAN;
import static com.epam.product.constant.BeanNames.PRODUCT_LIST_BEAN;
import static com.epam.product.constant.BeanNames.SUBCATEGORY_BEAN;
import static com.epam.product.constant.FilePathes.XML_FILE;
import static com.epam.product.constant.Params.ADD;
import static com.epam.product.constant.Params.COLOR;
import static com.epam.product.constant.Params.DATE_OF_ISSUE;
import static com.epam.product.constant.Params.MODEL;
import static com.epam.product.constant.Params.PRICE;
import static com.epam.product.constant.Params.PRODUCER;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.epam.product.entity.Product;
import com.epam.product.validator.ProductValidator;

/**
 * The Class MainController. Controller for all application requests.
 */
@Controller
public class BasicController {

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private ReadWriteLock lock;

	@Autowired
	private ProductValidator validator;

	@Autowired
	@Qualifier(CATEGORY_BEAN)
	private Templates categoryTemplate;

	@Autowired
	@Qualifier(SUBCATEGORY_BEAN)
	private Templates subcategoryTemplate;

	@Autowired
	@Qualifier(PRODUCT_LIST_BEAN)
	private Templates productListTemplate;

	@Autowired
	@Qualifier(ADD_PRODUCT_PAGE_BEAN)
	private Templates addProductPageTemplate;

	@Autowired
	@Qualifier(ADD_PRODUCT_BEAN)
	private Templates addProductTemplate;

	/**
	 * Category page.
	 *
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws TransformerException
	 *             the transformer exception
	 */
	@RequestMapping(value = "/")
	@ResponseBody
	public String categoryPage() throws IOException, TransformerException {
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = categoryTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			StringWriter result = new StringWriter();
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * Subcategory page.
	 *
	 * @param category
	 *            the category
	 * @return the string
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "{category}")
	@ResponseBody
	public String subcategoryPage(@PathVariable(value = "category") String category)
			throws TransformerException, IOException {
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = subcategoryTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			StringWriter result = new StringWriter();
			transformer.setParameter(CATEGORY, category);
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * Product list page.
	 *
	 * @param category
	 *            the category
	 * @param subcategory
	 *            the subcategory
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws TransformerException
	 *             the transformer exception
	 */
	@RequestMapping(value = "{category}/{subcategory}")
	@ResponseBody
	public String productListPage(@PathVariable(value = "category") String category,
			@PathVariable(value = "subcategory") String subcategory) throws IOException, TransformerException {
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = productListTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			StringWriter result = new StringWriter();
			transformer.setParameter(CATEGORY, category);
			transformer.setParameter(SUBCATEGORY, subcategory);
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * Adds the product page.
	 *
	 * @param category
	 *            the category
	 * @param subcategory
	 *            the subcategory
	 * @return the string
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "{category}/{subcategory}", params = ADD)
	@ResponseBody
	public String addProductPage(@PathVariable(value = "category") String category,
			@PathVariable(value = "subcategory") String subcategory) throws TransformerException, IOException {
		String xmlPath = servletContext.getRealPath(XML_FILE);
		lock.readLock().lock();
		try {
			Transformer transformer = addProductPageTemplate.newTransformer();
			Source source = new StreamSource(new File(xmlPath));
			StringWriter result = new StringWriter();
			transformer.setParameter(CATEGORY, category);
			transformer.setParameter(SUBCATEGORY, subcategory);
			transformer.setParameter(ERRORS, validator.getErrors());
			transformer.setParameter(PRODUCT, new Product());
			transformer.transform(source, new StreamResult(result));
			return result.toString();
		} finally {
			lock.readLock().unlock();
		}
	}

	/**
	 * Adds the product.
	 *
	 * @param category
	 *            the category
	 * @param subcategory
	 *            the subcategory
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @return the string
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "{category}/{subcategory}", method = RequestMethod.POST)
	@ResponseBody
	public String addProduct(@PathVariable(value = "category") String category,
			@PathVariable(value = "subcategory") String subcategory, HttpServletRequest request,
			HttpServletResponse response) throws TransformerException, IOException {
		String xmlFilePath = servletContext.getRealPath(XML_FILE);
		lock.writeLock().lock();
		try {
			StringWriter result = new StringWriter();
			File xmlFile = new File(xmlFilePath);
			Source source = new StreamSource(xmlFile);
			Product product = buildProduct(request);
			Transformer transformer = addProductTemplate.newTransformer();
			transformer.setParameter(CATEGORY, category);
			transformer.setParameter(SUBCATEGORY, subcategory);
			transformer.setParameter(PRODUCT, product);
			transformer.setParameter(VALIDATOR, validator);
			transformer.transform(source, new StreamResult(result));
			if (validator.isLastValidationSuccesful()) {
				rewriteXML(xmlFile, result.toString());
				response.sendRedirect(subcategory);
				return null;
			} else {
				result.getBuffer().setLength(0);
				transformer = addProductPageTemplate.newTransformer();
				transformer.setParameter(CATEGORY, category);
				transformer.setParameter(SUBCATEGORY, subcategory);
				transformer.setParameter(ERRORS, validator.getErrors());
				transformer.setParameter(PRODUCT, product);
				transformer.transform(source, new StreamResult(result));
				validator.clearErrorData();
				return result.toString();
			}
		} finally {
			lock.writeLock().unlock();
		}
	}

	/**
	 * Builds the product.
	 *
	 * @param request
	 *            the request
	 * @return the product
	 */
	private Product buildProduct(HttpServletRequest request) {
		Product product = new Product();
		product.setProducer(request.getParameter(PRODUCER));
		product.setModel(request.getParameter(MODEL));
		product.setDateOfIssue(request.getParameter(DATE_OF_ISSUE));
		product.setColor(request.getParameter(COLOR));
		product.setPrice(request.getParameter(PRICE));
		return product;
	}

	/**
	 * Rewrite xml.
	 *
	 * @param xmlFile
	 *            the xml file
	 * @param result
	 *            the result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void rewriteXML(File xmlFile, String result) throws IOException {
		try (PrintWriter fileWriter = new PrintWriter(xmlFile)) {
			fileWriter.write(result);
		}
	}

}
