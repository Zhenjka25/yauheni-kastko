<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://www.w3schools.com">
	<xsl:output method="html" indent="yes" />
	<xsl:param name="category" />
	<xsl:param name="subcategory" />
	<xsl:template match="/">
		<html>
			<head>
				<title>Products</title>
			</head>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="a:categories">
		<div>
			<a href="/product/{$category}">Back</a>
		</div>
		<xsl:for-each select="a:category">
			<xsl:if test="$category=@name">
				<xsl:value-of select="$category" />
				/
				<xsl:for-each select="a:subcategory">
					<xsl:if test="$subcategory=@name">
						<xsl:value-of select="$subcategory" />
						:
						<div>
							<table border="1" width="100%" cellpadding="5">
								<tr>
									<th>
										Producer
									</th>
									<th>
										Model
									</th>
									<th>
										Date of issue
									</th>
									<th>
										Color
									</th>
									<th>
										Price
									</th>
								</tr>
								<xsl:for-each select="a:product">
									<tr>
										<td>
											<xsl:value-of select="a:producer" />
										</td>
										<td>
											<xsl:value-of select="a:model" />
										</td>
										<td>
											<xsl:value-of select="a:date-of-issue" />
										</td>
										<td>
											<xsl:value-of select="a:color" />
										</td>
										<td>
											<xsl:choose>
												<xsl:when test="a:price">
													<xsl:value-of select="a:price" />
												</xsl:when>
												<xsl:when test="a:not-in-stock">
													Not in stock
												</xsl:when>
											</xsl:choose>
										</td>
									</tr>
								</xsl:for-each>
							</table>
						</div>
					</xsl:if>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
		<div>
			<a href="?add">Add</a>
		</div>
	</xsl:template>
</xsl:stylesheet>