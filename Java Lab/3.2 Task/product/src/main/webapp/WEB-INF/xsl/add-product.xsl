<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://www.w3schools.com"
	xmlns:validator="xalan://com.epam.product.validator.ProductValidator"
	xmlns:product="xalan://com.epam.product.entity.Product">

	<xsl:output method="xml" indent="yes" />

	<xsl:param name="category" />
	<xsl:param name="subcategory" />
	<xsl:param name="product" />
	<xsl:param name="validator" />

	<xsl:template match="/|node()|@*">
		<xsl:if test="validator:validate($validator, $product)">
			<xsl:copy>
				<xsl:apply-templates select="node()|@*" />
			</xsl:copy>
		</xsl:if>
	</xsl:template>

	<xsl:template
		match="a:categories/a:category[@name=$category]/a:subcategory[@name=$subcategory]">
		<xsl:copy>
			<xsl:apply-templates select="node()|@*" />
			<xsl:element name="a:product">
				<xsl:element name="a:producer">
					<xsl:value-of select="product:getProducer($product)" />
				</xsl:element>
				<xsl:element name="a:model">
					<xsl:value-of select="product:getModel($product)" />
				</xsl:element>
				<xsl:element name="a:date-of-issue">
					<xsl:value-of select="product:getDateOfIssue($product)" />
				</xsl:element>
				<xsl:element name="a:color">
					<xsl:value-of select="product:getColor($product)" />
				</xsl:element>
				<xsl:choose>
					<xsl:when test="product:getPrice($product)=0">
						<xsl:element name="a:not-in-stock" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="a:price">
							<xsl:value-of select="product:getPrice($product)" />
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>