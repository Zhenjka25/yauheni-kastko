<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://www.w3schools.com">
	<xsl:output method="html" indent="yes" />
	<xsl:param name="category" />
	<xsl:template match="/">
		<html>
			<head>
				<title>Subcategories</title>
			</head>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="a:categories">
		<div>
			<a href="/product">Back</a>
		</div>
		<xsl:for-each select="a:category">
			<xsl:if test="$category=@name">
				<xsl:for-each select="a:subcategory">
					<div>
						<xsl:variable name="subcategory" select="@name"></xsl:variable>
						<a href="{$category}/{$subcategory}">
							<xsl:value-of select="$subcategory" />
							(
							<xsl:value-of select="count(a:product)" />
							)
						</a>
					</div>
				</xsl:for-each>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>