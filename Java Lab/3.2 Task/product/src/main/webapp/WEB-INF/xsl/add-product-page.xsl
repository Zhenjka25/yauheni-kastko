<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://www.w3schools.com"
	xmlns:map="xalan://java.util.Map" xmlns:product="xalan://com.epam.product.entity.Product">
	<xsl:output method="html" indent="yes" />
	<xsl:param name="category" />
	<xsl:param name="subcategory" />
	<xsl:param name="errors" />
	<xsl:param name="product" />

	<xsl:template match="/">
		<html>
			<head>
				<title>Products</title>
			</head>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	<xsl:template match="a:categories">
		<div>
			<a href="/product/{$category}/{$subcategory}">Back</a>
		</div>
		<xsl:value-of select="$category" />
		/
		<xsl:value-of select="$subcategory" />
		:
		<div>
			<form name='addProduct' action="{$subcategory}" method='POST'>
				<table>
					<tr>
						<td>
							Producer:
						</td>
						<td>
							<xsl:variable name="producer" select="product:getProducer($product)"></xsl:variable>
							<input type='text' name='producer' value='{$producer}' />
						</td>
						<td style="color: red; font-style: italic">
							<xsl:value-of select="map:get($errors, 'producerKey')" />
						</td>
					</tr>
					<tr>
						<td>
							Model:
						</td>
						<td>
							<xsl:variable name="model" select="product:getModel($product)"></xsl:variable>
							<input type='text' name='model' value='{$model}' />
						</td>
						<td style="color: red; font-style: italic">
							<xsl:value-of select="map:get($errors, 'modelKey')" />
						</td>
					</tr>
					<tr>
						<td>
							Date of issue:
						</td>
						<td>
							<xsl:variable name="date"
								select="product:getDateOfIssue($product)"></xsl:variable>
							<input type='text' name='dateOfIssue' value='{$date}' />
						</td>
						<td style="color: red; font-style: italic">
							<xsl:value-of select="map:get($errors, 'dateKey')" />
						</td>
					</tr>
					<tr>
						<td>
							Color:
						</td>
						<td>
							<xsl:variable name="color" select="product:getColor($product)"></xsl:variable>
							<input type='text' name='color' value='{$color}' />
						</td>
						<td style="color: red; font-style: italic">
							<xsl:value-of select="map:get($errors, 'colorKey')" />
						</td>
					</tr>
					<tr>
						<td>
							Price:
						</td>
						<td>
							<xsl:variable name="price" select="product:getPrice($product)"></xsl:variable>
							<input type='text' name='price' value='{$price}' />
						</td>
						<td style="color: red; font-style: italic">
							<xsl:value-of select="map:get($errors, 'priceKey')" />
						</td>
					</tr>
				</table>
				<input name="submit" type="submit" value="Add product" />
			</form>
		</div>
	</xsl:template>
</xsl:stylesheet>