package com.epam.product.constant;

/**
 * The Class ViewNames. Contains xsl names.
 */
public class ViewNames {

	public static final String CATEGORIES_VIEW = "categories";
	public static final String SUBCATEGORY_VIEW = "subcategory";
	public static final String PRODUCT_LIST_VIEW = "product-list";
	public static final String ADD_PRODUCT_PAGE_VIEW = "add-product-page";
	public static final String REDIRECT = "redirect:/";

}
