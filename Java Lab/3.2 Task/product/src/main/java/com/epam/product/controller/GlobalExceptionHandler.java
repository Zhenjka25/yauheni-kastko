package com.epam.product.controller;

import static com.epam.product.constant.Attributes.ERROR_MESSAGE;
import static com.epam.product.constant.Attributes.XML_SOURCE;
import static com.epam.product.constant.FilePathes.XML_FILE;
import static com.epam.product.constant.ViewNames.CATEGORIES_VIEW;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * The Class GlobalExceptionHandler. Handles all exceptions which may be throwed by controllers.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	private static Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	private static final String TRANSFORMATION_ERROR = "XSL transformation exception";
	private static final String IO_ERRROR = "Input/Output exception";
	private static final String PATH_ERRROR = "Path not found";

	/**
	 * Transformation exception handler.
	 *
	 * @param te the te
	 * @param request the request
	 * @return the model and view
	 */
	@ExceptionHandler(value = TransformerException.class)
	public ModelAndView transformationExceptionHandler(TransformerException te, HttpServletRequest request) {
		log.error(te);
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		Source source = new StreamSource(new File(xmlFilePath));
		ModelAndView model = new ModelAndView(CATEGORIES_VIEW);
		model.addObject(XML_SOURCE, source);
		model.addObject(ERROR_MESSAGE, TRANSFORMATION_ERROR);
		return model;
	}

	/**
	 * Io exception handler.
	 *
	 * @param ioe the ioe
	 * @param request the request
	 * @return the model and view
	 */
	@ExceptionHandler(value = IOException.class)
	public ModelAndView ioExceptionHandler(IOException ioe, HttpServletRequest request) {
		log.error(ioe);
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		Source source = new StreamSource(new File(xmlFilePath));
		ModelAndView model = new ModelAndView(CATEGORIES_VIEW);
		model.addObject(XML_SOURCE, source);
		model.addObject(ERROR_MESSAGE, IO_ERRROR);
		return model;
	}

	/**
	 * No handler found exception handler.
	 *
	 * @param e the e
	 * @param request the request
	 * @return the model and view
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView noHandlerFoundExceptionHandler(NoHandlerFoundException e, HttpServletRequest request) {
		log.error(e);
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		Source source = new StreamSource(new File(xmlFilePath));
		ModelAndView model = new ModelAndView(CATEGORIES_VIEW);
		model.addObject(XML_SOURCE, source);
		model.addObject(ERROR_MESSAGE, PATH_ERRROR);
		return model;
	}

}
