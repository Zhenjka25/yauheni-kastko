package com.epam.product.constant;

/**
 * The Class Attributes. Contains request attribute names.
 */
public class Attributes {

	public static final String XML_SOURCE = "xmlSource";
	public static final String CATEGORY = "category";
	public static final String SUBCATEGORY = "subcategory";
	public static final String VALIDATOR = "validator";
	public static final String PRODUCT = "product";
	public static final String ERRORS = "errors";
	public static final String ERROR_MESSAGE = "errorMessage";

}
