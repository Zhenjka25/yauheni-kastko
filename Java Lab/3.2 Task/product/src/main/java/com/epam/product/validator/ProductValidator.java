package com.epam.product.validator;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.epam.product.entity.Product;

/**
 * The Class ProductValidator. Validates product entity and provides error messages if errors will be founded.
 */
@Component
public class ProductValidator {

	private HashMap<String, String> errors;

	private boolean flag;

	private static final String PRODUCER_PATTERN = "[A-Z\\d][\\w\\W\\s]{1,15}";
	private static final String MODEL_PATTERN = "[A-Za-z]{2}\\d{3}";
	private static final String DATE_PATTERN = "\\d{2}[-]\\d{2}[-]\\d{4}";

	private static final String PRODUCER_KEY = "producerKey";
	private static final String MODEL_KEY = "modelKey";
	private static final String DATE_KEY = "dateKey";
	private static final String COLOR_KEY = "colorKey";
	private static final String PRICE_KEY = "priceKey";

	private static final String PRODUCER_MESSAGE = "The first character must be an uppercase letter or digit. Producer must contain at least 2 symbols up to 16";
	private static final String MODEL_MESSAGE = "Model must contain two letters and three digits. Example: SX353";
	private static final String DATE_MESSAGE = "Date format: dd-mm-yyyy";
	private static final String COLOR_MESSAGE = "Such color didn't found";
	private static final String PRICE_MESSAGE = "Price must be a digit";

	/**
	 * Instantiates a new product validator.
	 */
	public ProductValidator() {
		errors = new HashMap<>();
	}

	/**
	 * Validate.
	 *
	 * @param product the product
	 * @return true, if successful
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean validate(Product product) throws SAXException, IOException {
		flag = true;
		Pattern pattern = null;
		pattern = Pattern.compile(PRODUCER_PATTERN);
		if (!pattern.matcher(product.getProducer()).find()) {
			flag = false;
			errors.put(PRODUCER_KEY, PRODUCER_MESSAGE);
		} else {
			errors.put(PRODUCER_KEY, null);
		}
		pattern = Pattern.compile(MODEL_PATTERN);
		if (!pattern.matcher(product.getModel()).find()) {
			flag = false;
			errors.put(MODEL_KEY, MODEL_MESSAGE);
		} else {
			errors.put(MODEL_KEY, null);
		}
		pattern = Pattern.compile(DATE_PATTERN);
		if (!pattern.matcher(product.getDateOfIssue()).find()) {
			flag = false;
			errors.put(DATE_KEY, DATE_MESSAGE);
		} else {
			errors.put(DATE_KEY, null);
		}
		try {
			Integer.parseInt(product.getPrice());
			errors.put(PRICE_KEY, null);
		} catch (NumberFormatException e) {
			flag = false;
			errors.put(PRICE_KEY, PRICE_MESSAGE);
		}
		if (!EnumUtils.isValidEnum(Colors.class, product.getColor().toUpperCase())) {
			flag = false;
			errors.put(COLOR_KEY, COLOR_MESSAGE);
		} else {
			errors.put(COLOR_KEY, null);
		}
		return flag;
	}

	/**
	 * Gets the errors.
	 *
	 * @return the errors
	 */
	public HashMap<String, String> getErrors() {
		return errors;
	}

	/**
	 * Checks if is last validation succesful.
	 *
	 * @return true, if is last validation succesful
	 */
	public boolean isLastValidationSuccesful() {
		return flag;
	}

	/**
	 * The Enum Colors.
	 */
	private enum Colors {
		BLACK, BLUE, BROWN, GRAY, GREEN, ORANGE, PINK, PURPLE, RED, WHITE, YELLOW
	}

}
