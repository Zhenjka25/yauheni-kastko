package com.epam.product.config;

import static com.epam.product.constant.ViewNames.ADD_PRODUCT_PAGE_VIEW;
import static com.epam.product.constant.ViewNames.CATEGORIES_VIEW;
import static com.epam.product.constant.ViewNames.PRODUCT_LIST_VIEW;
import static com.epam.product.constant.ViewNames.SUBCATEGORY_VIEW;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.xslt.XsltView;
import org.springframework.web.servlet.view.xslt.XsltViewResolver;

import com.epam.product.validator.ProductValidator;

/**
 * The Class ApplicationConfig. Spring configuration for application.
 */
@EnableWebMvc
@Configuration
@ComponentScan("com.epam.product.controller, com.epam.product.parser, com.epam.product.validator")
public class ApplicationConfig extends WebMvcConfigurerAdapter {

	/**
	 * Gets the XSLT view resolver.
	 *
	 * @return the XSLT view resolver
	 */
	@Bean
	public ViewResolver getXSLTViewResolver() {
		XsltViewResolver xsltResolover = new XsltViewResolver();
		xsltResolover.setSourceKey("xmlSource");
		xsltResolover.setViewClass(XsltView.class);
		xsltResolover.setViewNames(
				new String[] { CATEGORIES_VIEW, SUBCATEGORY_VIEW, PRODUCT_LIST_VIEW, ADD_PRODUCT_PAGE_VIEW });
		xsltResolover.setPrefix("/WEB-INF/xsl/");
		xsltResolover.setSuffix(".xsl");
		xsltResolover.setOrder(1);
		return xsltResolover;
	}

	/**
	 * Gets the JSP view resolver.
	 *
	 * @return the JSP view resolver
	 */
	@Bean
	public ViewResolver getJSPViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setOrder(2);
		return resolver;
	}

	/**
	 * Validator.
	 *
	 * @return the product validator
	 */
	@Bean
	public ProductValidator validator() {
		return new ProductValidator();
	}

	/**
	 * Lock.
	 *
	 * @return the lock
	 */
	@Bean
	public ReadWriteLock lock() {
		return new ReentrantReadWriteLock(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
	 * #addResourceHandlers(org.springframework.web.servlet.config.annotation.
	 * ResourceHandlerRegistry)
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

}
