package com.epam.product.constant;

/**
 * The Class FilePathes. Contains path to xml and xsl files.
 */
public class FilePathes {

	public static final String XML_FILE = "/resources/xml/products.xml";
	public static final String XSL_TRANSFORM_FILE = "/WEB-INF/xsl/add-product.xsl";

}
