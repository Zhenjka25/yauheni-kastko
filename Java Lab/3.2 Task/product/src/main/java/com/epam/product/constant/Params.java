package com.epam.product.constant;

/**
 * The Class Params. Contains request parameter names.
 */
public class Params {

	public static final String ADD = "add";
	public static final String PRODUCER = "producer";
	public static final String MODEL = "model";
	public static final String DATE_OF_ISSUE = "dateOfIssue";
	public static final String COLOR = "color";
	public static final String PRICE = "price";

}
