package com.epam.product.controller;

import static com.epam.product.constant.Attributes.*;
import static com.epam.product.constant.FilePathes.XML_FILE;
import static com.epam.product.constant.FilePathes.XSL_TRANSFORM_FILE;
import static com.epam.product.constant.Params.*;
import static com.epam.product.constant.ViewNames.*;

import java.io.*;
import java.util.concurrent.locks.ReadWriteLock;

import javax.servlet.http.HttpServletRequest;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.product.entity.Product;
import com.epam.product.validator.ProductValidator;

/**
 * The Class MainController. Controller for all application requests.
 */
@Controller
public class BasicController {

	@Autowired
	private ReadWriteLock lock;

	@Autowired
	private ProductValidator validator;

	/**
	 * Home page.
	 *
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "/")
	public ModelAndView homePage(HttpServletRequest request) throws IOException {
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		ModelAndView model = new ModelAndView(CATEGORIES_VIEW);
		lock.readLock().lock();
		try {
			Source source = new StreamSource(new File(xmlFilePath));
			model.addObject(XML_SOURCE, source);
		} finally {
			lock.readLock().unlock();
		}
		return model;
	}

	/**
	 * Category page.
	 *
	 * @param category
	 *            the category
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@RequestMapping(value = "{category}")
	public ModelAndView categoryPage(@PathVariable(value = "category") String category, HttpServletRequest request) {
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		ModelAndView model = new ModelAndView(SUBCATEGORY_VIEW);
		lock.readLock().lock();
		try {
			Source source = new StreamSource(new File(xmlFilePath));
			model.addObject(XML_SOURCE, source);
			model.addObject(CATEGORY, category);
		} finally {
			lock.readLock().unlock();
		}
		return model;
	}

	/**
	 * Sub category page.
	 *
	 * @param category
	 *            the category
	 * @param subcategory
	 *            the subcategory
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@RequestMapping(value = "{category}/{subcategory}")
	public ModelAndView subCategoryPage(@PathVariable(value = "category") String category,
			@PathVariable(value = "subcategory") String subcategory, HttpServletRequest request) {
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		ModelAndView model = new ModelAndView(PRODUCT_LIST_VIEW);
		lock.readLock().lock();
		try {
			Source source = new StreamSource(new File(xmlFilePath));
			model.addObject(XML_SOURCE, source);
			model.addObject(CATEGORY, category);
			model.addObject(SUBCATEGORY, subcategory);
		} finally {
			lock.readLock().unlock();
		}
		return model;
	}

	/**
	 * Adds the product page.
	 *
	 * @param category
	 *            the category
	 * @param subcategory
	 *            the subcategory
	 * @param request
	 *            the request
	 * @return the model and view
	 */
	@RequestMapping(value = "{category}/{subcategory}", params = ADD)
	public ModelAndView addProductPage(@PathVariable(value = "category") String category,
			@PathVariable(value = "subcategory") String subcategory, HttpServletRequest request) {
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		ModelAndView model = new ModelAndView(ADD_PRODUCT_PAGE_VIEW);
		lock.readLock().lock();
		try {
			Source source = new StreamSource(new File(xmlFilePath));
			model.addObject(XML_SOURCE, source);
			model.addObject(CATEGORY, category);
			model.addObject(SUBCATEGORY, subcategory);
			model.addObject(ERRORS, validator.getErrors());
			model.addObject(PRODUCT, new Product());
		} finally {
			lock.readLock().unlock();
		}
		return model;
	}

	/**
	 * Adds the product.
	 *
	 * @param category
	 *            the category
	 * @param subcategory
	 *            the subcategory
	 * @param request
	 *            the request
	 * @return the model and view
	 * @throws TransformerException
	 *             the transformer exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@RequestMapping(value = "{category}/{subcategory}", method = RequestMethod.POST)
	public ModelAndView addProduct(@PathVariable(value = "category") String category,
			@PathVariable(value = "subcategory") String subcategory, HttpServletRequest request)
					throws TransformerException, IOException {
		TransformerFactory factory = TransformerFactory.newInstance();
		String contextPath = request.getServletContext().getRealPath("");
		String xmlFilePath = contextPath + XML_FILE;
		String xslPath = contextPath + XSL_TRANSFORM_FILE;
		ModelAndView model = new ModelAndView();
		lock.writeLock().lock();
		try (Writer result = new StringWriter()) {
			File xmlFile = new File(xmlFilePath);
			Source source = new StreamSource(xmlFile);
			Product product = buildProduct(request);
			Transformer transformer = factory.newTransformer(new StreamSource(xslPath));
			transformer.setParameter(CATEGORY, category);
			transformer.setParameter(SUBCATEGORY, subcategory);
			transformer.setParameter(PRODUCT, product);
			transformer.setParameter(VALIDATOR, validator);
			transformer.transform(source, new StreamResult(result));
			if (validator.isLastValidationSuccesful()) {
				rewriteXML(xmlFile, result.toString());
				model.setViewName(REDIRECT + category + "/" + subcategory);
				model.addObject(XML_SOURCE, source);
				model.addObject(CATEGORY, category);
				model.addObject(SUBCATEGORY, subcategory);
			} else {
				model.setViewName(ADD_PRODUCT_PAGE_VIEW);
				model.addObject(XML_SOURCE, source);
				model.addObject(CATEGORY, category);
				model.addObject(SUBCATEGORY, subcategory);
				model.addObject(ERRORS, validator.getErrors());
				model.addObject(PRODUCT, product);
			}
		} finally {
			lock.writeLock().unlock();
		}
		return model;
	}

	/**
	 * Builds the product.
	 *
	 * @param request
	 *            the request
	 * @return the product
	 */
	private Product buildProduct(HttpServletRequest request) {
		Product product = new Product();
		product.setProducer(request.getParameter(PRODUCER));
		product.setModel(request.getParameter(MODEL));
		product.setDateOfIssue(request.getParameter(DATE_OF_ISSUE));
		product.setColor(request.getParameter(COLOR));
		product.setPrice(request.getParameter(PRICE));
		return product;
	}

	/**
	 * Rewrite xml.
	 *
	 * @param xmlFile
	 *            the xml file
	 * @param result
	 *            the result
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void rewriteXML(File xmlFile, String result) throws IOException {
		try (PrintWriter fileWriter = new PrintWriter(xmlFile)) {
			fileWriter.write(result);
		}
	}

}
