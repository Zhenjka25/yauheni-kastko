<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<tiles:importAttribute name="javascripts" />
<tiles:importAttribute name="stylesheets" />
<tiles:importAttribute name="title" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title><fmt:message key="${title }" /></title>

<c:forEach var="css" items="${stylesheets}">
	<link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
</c:forEach>

<c:forEach var="script" items="${javascripts}">
	<script src="<c:url value="${script}"/>"></script>
</c:forEach>

<c:if test="${empty sessionScope.timezoneOffset}">
	<script type="text/javascript">
		var tzo = new Date().getTimezoneOffset();
		document.cookie = "timezoneOffset=" + encodeURI(tzo * (-1));
	</script>
</c:if>

</head>
<body>

	<tiles:insertAttribute name="header" />
	<div id="side-and-content">
		<tiles:insertAttribute name="sidebar" />
		<tiles:insertAttribute name="content" />
	</div>
	<tiles:insertAttribute name="footer" />

</body>
</html>