<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ctg" uri="customtags"%>

<script type="text/javascript">
	authorError = '<fmt:message key="authorname.error.pattern" />';
</script>

<div id="content">
	<div id="author-content">
		<c:forEach items="${authorList}" var="author">
			<c:choose>
				<c:when
					test="${not empty editAuthor and editAuthor.authorId eq author.authorId}">
					<sf:form modelAttribute="editAuthor" action="?edit"
						onsubmit="return validateAuthor()">
						<sf:hidden path="authorId" />
						<div class="editauthor-author-text">
							<fmt:message key="editauthor.author.text" />
						</div>
						<div class="editauthor-inputname">
							<sf:input path="authorName" />
						</div>
						<div class="editauthor-update-button">
							<input type="submit"
								value="<fmt:message key="editauthor.update.button" />">
						</div>
					</sf:form>
					<div class="editauthor-expire-link">
						<form action="?authorId=${author.authorId }&expire" method="POST">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input type="submit"
								value="<fmt:message
									key="editauthor.expire.link" />">
						</form>
					</div>
					<div class="editauthor-cancel-link">
						<a href="?"><fmt:message key="editauthor.cancel.link" /></a>
					</div>
				</c:when>
				<c:otherwise>
					<div class="editauthor-author-text">
						<fmt:message key="editauthor.author.text" />
					</div>
					<div class="editauthor-inputname-blocked">
						<input type="text" value="${ctg:escapeHtml4(author.authorName) }"
							disabled="disabled" />
					</div>
					<div class="editauthor-edit-link">
						<a href="?editAuthorId=${author.authorId }"><fmt:message
								key="editauthor.edit.link" /></a>
					</div>
				</c:otherwise>
			</c:choose>
			<div style="clear: both;"></div>
		</c:forEach>
	</div>
	<div>
		<c:if test="${empty editAuthor or empty editAuthor.authorId }">
			<div id="editauthor-addauthor-text">
				<fmt:message key="editauthor.addauthor.text" />
			</div>
			<sf:form modelAttribute="saveAuthor" action="?save"
				onsubmit="return validateAuthor()">
				<div id="editauthor-inputname-save">
					<sf:input path="authorName" />
				</div>
				<div id="editauthor-save-button">
					<input type="submit"
						value="<fmt:message key="editauthor.save.button" />">
				</div>
			</sf:form>
			<div style="clear: both;"></div>
		</c:if>
	</div>
	<div class="addnews-error-message" id="editauthor-message">
		<c:forEach items="${validationMessages}" var="error">
				${error.getDefaultMessage() }
			</c:forEach>
	</div>
</div>
<div style="clear: both"></div>