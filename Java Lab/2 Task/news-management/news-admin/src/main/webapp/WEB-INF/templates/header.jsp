<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<header id="header">
	<div id="header-title">
		<strong><fmt:message key="title.title" /></strong>
	</div>
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<div id="header-greeting-message">
			<fmt:message key="header.greeting" />
			<security:authentication property="principal.username" />
			!
		</div>
		<div id="header-logout-button">
			<c:url value="/logout" var="logoutUrl" />
			<form method="POST" action="${logoutUrl }">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" /> <input type="submit"
					value="<fmt:message key="header.logout" />">
			</form>
		</div>
	</security:authorize>
	<div id="header-locale">
		<div id="header-localeEN">
			<a href="?locale=en_US"><fmt:message key="title.en" /></a>
		</div>
		<div id="header-localeRU">
			<a href="?locale=ru_RU"><fmt:message key="title.ru" /></a>
		</div>
	</div>
	<div style="clear: left"></div>
</header>