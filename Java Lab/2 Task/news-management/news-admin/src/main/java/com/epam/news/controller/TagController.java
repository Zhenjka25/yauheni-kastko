package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

/**
 * The Class TagController. Controller to add, edit and delete tags.
 */
@Controller
@RequestMapping(value = "/admin")
public class TagController {

	private static final String CURRENT = "tags";
	private static final String REDIRECT_PATH = "redirect:/admin/view/edittags";

	@Autowired
	private TagService tagService;

	/**
	 * View edit tags page.
	 *
	 * @param editTagId
	 *            the edit tag id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.GET)
	public ModelAndView viewEditTagsPage(@RequestParam(value = PARAM_EDIT_TAG_ID, required = false) Long editTagId)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Tag> tags = tagService.findAll();
		model.addObject(ATTRIBUTE_TAG_LIST, tags);
		if (editTagId != null) {
			Tag editTag = tagService.findById(editTagId);
			editTag.setTagName(editTag.getTagName());
			model.addObject(ATTRIBUTE_EDIT_TAG, editTag);
		}
		model.addObject(ATTRIBUTE_SAVE_TAG, new Tag());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_EDIT_TAGS);
		return model;
	}

	/**
	 * Edits the tag.
	 *
	 * @param tag
	 *            the tag
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.POST, params = PARAM_EDIT)
	public ModelAndView editTag(@Valid @ModelAttribute(ATTRIBUTE_EDIT_TAG) Tag tag, BindingResult bindingResult)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_TAG, tag);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES, bindingResult.getFieldErrors());
			List<Tag> tags = tagService.findAll();
			model.addObject(ATTRIBUTE_TAG_LIST, tags);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_TAGS);
			return model;
		} else {
			tagService.update(tag);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * Save author.
	 *
	 * @param tag
	 *            the tag
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", method = RequestMethod.POST, params = PARAM_SAVE)
	public ModelAndView saveTag(@Valid @ModelAttribute(ATTRIBUTE_SAVE_TAG) Tag tag, BindingResult bindingResult)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_TAG, tag);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES, bindingResult.getFieldErrors());
			List<Tag> tags = tagService.findAll();
			model.addObject(ATTRIBUTE_TAG_LIST, tags);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_TAGS);
			return model;
		} else {
			tagService.save(tag);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * Expire author.
	 *
	 * @param tagId
	 *            the tag id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/edittags", params = PARAM_DELETE, method = RequestMethod.POST)
	public ModelAndView deleteTag(@RequestParam(value = PARAM_TAG_ID, required = true) Long tagId)
			throws ServiceException {
		tagService.deleteById(tagId);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH);
		return model;
	}

}
