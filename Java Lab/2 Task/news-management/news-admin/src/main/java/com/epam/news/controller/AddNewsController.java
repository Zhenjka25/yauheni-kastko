package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.News;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsManagementService;
import com.epam.news.service.TagService;

/**
 * The Class AddNewsController. Controller to add and delete news.
 */
@Controller
@RequestMapping(value = "/admin")
public class AddNewsController {

	private static final String CURRENT = "addNews";
	private static final String REDIRECT_NEWS_PATH = "redirect:/admin/news/";
	private static final String REDIRECT_NEWS_LIST_PATH = "redirect:/admin/newslist";

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	/**
	 * View add news page.
	 *
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/addnews", method = RequestMethod.GET)
	public ModelAndView viewAddNewsPage() throws ServiceException {
		ModelAndView model = new ModelAndView();
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.findActualAuthors());
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
		News news = new News();
		news.setCreationDate(new Date());
		news.setModificationDate(news.getCreationDate());
		NewsValueObject newsValue = new NewsValueObject();
		newsValue.setNews(news);
		model.addObject(ATTRIBUTE_NEWS_VALUE, newsValue);
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_ADD_NEWS);
		return model;
	}

	/**
	 * Adds the news.
	 *
	 * @param newsValue
	 *            the news value
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/addnews", method = RequestMethod.POST)
	public ModelAndView addNews(@Valid @ModelAttribute(value = ATTRIBUTE_NEWS_VALUE) NewsValueObject newsValue,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			if (newsValue.getTags() != null) {
				newsValue.setTags(tagService.findByTagIdList(newsValue.getTags()));
			}
			model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.findActualAuthors());
			model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_ADD_NEWS);
			return model;
		}
		newsValue.getNews().setModificationDate(newsValue.getNews().getCreationDate());
		newsValue.getNews().setTitle(newsValue.getNews().getTitle());
		newsValue.getNews().setShortText(newsValue.getNews().getShortText());
		newsValue.getNews().setFullText(newsValue.getNews().getFullText());
		newsManagementService.saveNews(newsValue);
		model.setViewName(REDIRECT_NEWS_PATH + newsValue.getNews().getNewsId());
		return model;
	}

	/**
	 * Delete news.
	 *
	 * @param newsIdArray
	 *            the news id array
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/deletenews")
	public ModelAndView deleteNews(@RequestParam(value = PARAM_NEWS_ID_LIST, required = false) Long[] newsIdArray)
			throws ServiceException {
		List<Long> newsIdList = new ArrayList<>();
		if (newsIdArray != null && newsIdArray.length != 0) {
			newsIdList = Arrays.asList(newsIdArray);
		}
		newsManagementService.deleteNews(newsIdList);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_NEWS_LIST_PATH);
		return model;
	}

	/**
	 * Inits the binder.
	 *
	 * @param binder
	 *            the binder
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(List.class, "tags", new CustomCollectionEditor(List.class) {
			@Override
			protected Object convertElement(Object element) {
				if (element instanceof Tag) {
					return element;
				}
				if (element instanceof String) {
					Tag tag = new Tag();
					tag.setTagId(Long.parseLong((String) element));
					return tag;
				} else {
					return null;
				}
			}
		});
	}

}
