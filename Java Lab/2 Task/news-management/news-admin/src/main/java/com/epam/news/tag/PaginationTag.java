package com.epam.news.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class PaginationTag extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8464263595117061744L;

	/** The pages count. */
	private int pagesCount;

	/** The current page number. */
	private int currentPage;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			for (int i = 1; i <= pagesCount; i++) {
				if (i == currentPage) {
					out.write("<td>" + i + "</td>");
				} else {
					out.write("<td><a href=\"" + pageContext.getServletContext().getContextPath()
							+ "/admin/newslist?page=" + i + "\">" + i + "</a></td>");
				}
			}
		} catch (IOException e) {
			throw new JspException(e);
		}
		return SKIP_BODY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	/**
	 * Sets the pages count.
	 *
	 * @param pagesCount
	 *            the new pages count
	 */
	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	/**
	 * Sets the current page.
	 *
	 * @param currentPage
	 *            the new current page
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

}
