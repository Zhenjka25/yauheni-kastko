package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsManagementService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class CommentController. Controller to add and delete comments.
 */
@Controller
@RequestMapping(value = "/admin")
public class CommentController {

	private static final String REDIRECT_PATH = "redirect:/admin/news/";

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	private CommentService commentService;

	/**
	 * Adds the comment.
	 *
	 * @param text
	 *            the text
	 * @param newsId
	 *            the news id
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.POST, params = PARAM_ADD)
	public ModelAndView addComment(@Valid Comment comment, BindingResult bindingResult, HttpSession session)
			throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
					? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();
			model.addObject(ATTRIBUTE_NEWS, newsManagementService.findSingleNews(comment.getNewsId()));
			model.addObject(ATTRIBUTE_PREV_NEWS,
					newsManagementService.findPrevNews(comment.getNewsId(), searchCriteria));
			model.addObject(ATTRIBUTE_NEXT_NEWS,
					newsManagementService.findNextNews(comment.getNewsId(), searchCriteria));
			model.setViewName(VIEW_NEWS);
			return model;
		}
		comment.setCommentText(comment.getCommentText());
		commentService.save(comment);
		model.setViewName(REDIRECT_PATH + comment.getNewsId());
		return model;
	}

	/**
	 * Delete comment.
	 *
	 * @param commentId
	 *            the comment id
	 * @param newsId
	 *            the news id
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}", method = RequestMethod.POST, params = PARAM_DELETE)
	public ModelAndView deleteComment(@RequestParam(value = PARAM_COMMENT_ID, required = true) Long commentId,
			@RequestParam(value = PARAM_NEWS_ID, required = true) Long newsId, HttpSession session)
					throws ServiceException {
		commentService.deleteById(commentId);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH + newsId);
		return model;
	}

}
