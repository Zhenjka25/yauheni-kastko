package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Class LoginController. Controller for authorization.
 */
@Controller
public class LoginController {

	private static final String ERROR_KEY = "login.error.message";
	private static final String MSG_KEY = "login.msg.message";

	/**
	 * Login.
	 *
	 * @param error
	 *            the error
	 * @param logout
	 *            the logout
	 * @return the model and view
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = PARAM_ERROR_MESSAGE, required = false) String error,
			@RequestParam(value = PARAM_LOGOUT_MESSAGE, required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject(ATTRIBUTE_ERROR, ERROR_KEY);
		}
		if (logout != null) {
			model.addObject(ATTRIBUTE_MSG, MSG_KEY);
		}
		model.setViewName(VIEW_LOGIN);
		return model;
	}

}
