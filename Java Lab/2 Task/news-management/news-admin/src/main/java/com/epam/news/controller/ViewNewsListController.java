package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;
import static com.epam.news.constant.ConstantsCommon.NEWS_PER_PAGE_KEY;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.NewsValueObject;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsManagementService;
import com.epam.news.service.TagService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsListController. Controller to view the list of news page.
 */
@Controller
@RequestMapping(value = "/admin")
public class ViewNewsListController {

	private static final String CURRENT = "newsList";

	@Autowired
	private NewsManagementService service;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private Environment env;

	/**
	 * View news list.
	 *
	 * @param page
	 *            the page
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/newslist")
	public ModelAndView viewNewsList(@RequestParam(value = PARAM_PAGE, required = false) Integer page,
			HttpSession session) throws ServiceException {
		ModelAndView model = new ModelAndView();
		SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();
		if (page != null) {
			searchCriteria.setPage(page);
		}
		List<NewsValueObject> newsList = service.findBySearchCriteria(searchCriteria);
		model.addObject(ATTRIBUTE_NEWS_LIST, newsList);
		int newsPerPage = Integer.parseInt(env.getProperty(NEWS_PER_PAGE_KEY));
		long pagesCount = service.countNewsBySearchCriteria(searchCriteria) / newsPerPage;
		if ((service.countNewsBySearchCriteria(searchCriteria) % newsPerPage) != 0) {
			pagesCount++;
		}
		session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		model.addObject(ATTRIBUTE_PAGES_COUNT, pagesCount);
		model.addObject(ATTRIBUTE_CURRENT_PAGE, searchCriteria.getPage());
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.findAll());
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_NEWS_LIST);
		return model;
	}

}
