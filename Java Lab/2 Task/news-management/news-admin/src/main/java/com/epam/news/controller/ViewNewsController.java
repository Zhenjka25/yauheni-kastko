package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_COMMENT;
import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_NEWS;
import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_NEXT_NEWS;
import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_PREV_NEWS;
import static com.epam.news.constant.ConstantsAdmin.ATTRIBUTE_SEARCH_CRITERIA;
import static com.epam.news.constant.ConstantsAdmin.ROLE_ADMIN;
import static com.epam.news.constant.ConstantsAdmin.VIEW_NEWS;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Comment;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.exception.NotFoundException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsManagementService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsController. Controller to view the news page.
 */
@Controller
@RequestMapping(value = "/admin")
public class ViewNewsController {

	@Autowired
	private NewsManagementService service;

	/**
	 * View news.
	 *
	 * @param newsId
	 *            the news id
	 * @param session
	 *            the session
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 * @throws NotFoundException
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/news/{newsId}")
	public ModelAndView viewNews(@PathVariable(value = "newsId") Long newsId, HttpSession session)
			throws ServiceException {
		NewsValueObject news = service.findSingleNews(newsId);
		if (news.getNews() == null) {
			throw new NotFoundException();
		}
		ModelAndView model = new ModelAndView();
		SearchCriteria searchCriteria = session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();
		model.addObject(ATTRIBUTE_NEWS, news);
		model.addObject(ATTRIBUTE_PREV_NEWS, service.findPrevNews(newsId, searchCriteria));
		model.addObject(ATTRIBUTE_NEXT_NEWS, service.findNextNews(newsId, searchCriteria));
		Comment comment = new Comment();
		comment.setCreationDate(new Date());
		comment.setNewsId(newsId);
		model.addObject(ATTRIBUTE_COMMENT, comment);
		model.setViewName(VIEW_NEWS);
		return model;
	}

}
