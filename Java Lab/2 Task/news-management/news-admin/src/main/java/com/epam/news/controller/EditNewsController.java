package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.*;
import com.epam.news.exception.NotFoundException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsManagementService;
import com.epam.news.service.TagService;

/**
 * The Class EditNewsController. Controller to edit news.
 */
@Controller
@RequestMapping(value = "/admin")
public class EditNewsController {

	private static final String REDIRECT_PATH = "redirect:/admin/news/";

	@Autowired
	private NewsManagementService service;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	/**
	 * View edit news page.
	 *
	 * @param newsId
	 *            the news id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editnews/{newsId}", method = RequestMethod.GET)
	public ModelAndView viewEditNewsPage(@PathVariable(value = "newsId") Long newsId) throws ServiceException {
		ModelAndView model = new ModelAndView();
		NewsValueObject newsValueObject = service.findSingleNews(newsId);
		News news = newsValueObject.getNews();
		news.setTitle(news.getTitle());
		news.setShortText(news.getShortText());
		news.setFullText(news.getFullText());
		if (newsValueObject.getNews() == null) {
			throw new NotFoundException();
		}
		List<Author> authors = authorService.findActualAuthors();
		if (newsValueObject.getAuthor() != null && newsValueObject.getAuthor().getExpired() != null) {
			authors.add(newsValueObject.getAuthor());
		}
		model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
		model.addObject(ATTRIBUTE_NEWS_VALUE, newsValueObject);
		model.setViewName(VIEW_EDIT_NEWS);
		return model;
	}

	/**
	 * Edits the news.
	 *
	 * @param newsValue
	 *            the news value
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editnews/{newsId}", method = RequestMethod.POST)
	public ModelAndView editNews(@Valid @ModelAttribute(value = ATTRIBUTE_NEWS_VALUE) NewsValueObject newsValue,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			if (newsValue.getTags() != null) {
				newsValue.setTags(tagService.findByTagIdList(newsValue.getTags()));
			}
			model.addObject(ATTRIBUTE_AUTHOR_LIST, authorService.findActualAuthors());
			model.addObject(ATTRIBUTE_TAG_LIST, tagService.findAll());
			model.setViewName(VIEW_EDIT_NEWS);
			return model;
		}
		newsValue.getNews().setTitle(newsValue.getNews().getTitle());
		newsValue.getNews().setShortText(newsValue.getNews().getShortText());
		newsValue.getNews().setFullText(newsValue.getNews().getFullText());
		service.updateNews(newsValue);
		model.setViewName(REDIRECT_PATH + newsValue.getNews().getNewsId());
		return model;
	}

	/**
	 * Inits the binder.
	 *
	 * @param binder
	 *            the binder
	 */
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(List.class, "tags", new CustomCollectionEditor(List.class) {
			@Override
			protected Object convertElement(Object element) {
				if (element instanceof Tag) {
					return element;
				}
				if (element instanceof String) {
					Tag tag = new Tag();
					tag.setTagId(Long.parseLong((String) element));
					return tag;
				} else {
					return null;
				}
			}
		});
	}

}
