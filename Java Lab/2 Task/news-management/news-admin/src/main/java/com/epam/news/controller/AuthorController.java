package com.epam.news.controller;

import static com.epam.news.constant.ConstantsAdmin.*;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

/**
 * The Class AuthorController. Controller to add, edit and expire authors.
 */
@Controller
@RequestMapping(value = "/admin")
public class AuthorController {

	private static final String CURRENT = "authors";
	private static final String REDIRECT_PATH = "redirect:/admin/view/editauthors";

	@Autowired
	private AuthorService authorService;

	/**
	 * View edit authors page.
	 *
	 * @param editAuthorId
	 *            the edit author id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", method = RequestMethod.GET)
	public ModelAndView viewEditAuthorsPage(
			@RequestParam(value = PARAM_EDIT_AUTHOR_ID, required = false) Long editAuthorId) throws ServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authors = authorService.findActualAuthors();
		model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
		if (editAuthorId != null) {
			Author editAuthor = authorService.findById(editAuthorId);
			editAuthor.setAuthorName(editAuthor.getAuthorName());
			model.addObject(ATTRIBUTE_EDIT_AUTHOR, editAuthor);
		}
		model.addObject(ATTRIBUTE_SAVE_AUTHOR, new Author());
		model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
		model.setViewName(VIEW_EDIT_AUTHORS);
		return model;
	}

	/**
	 * Edits the author.
	 *
	 * @param author
	 *            the author
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", method = RequestMethod.POST, params = PARAM_EDIT)
	public ModelAndView editAuthor(@Valid @ModelAttribute(ATTRIBUTE_EDIT_AUTHOR) Author author,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_AUTHOR, author);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES, bindingResult.getFieldErrors());
			List<Author> authors = authorService.findActualAuthors();
			model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_AUTHORS);
			return model;
		} else {
			authorService.update(author);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * Save author.
	 *
	 * @param author
	 *            the author
	 * @param bindingResult
	 *            the binding result
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", method = RequestMethod.POST, params = PARAM_SAVE)
	public ModelAndView saveAuthor(@Valid @ModelAttribute(ATTRIBUTE_SAVE_AUTHOR) Author author,
			BindingResult bindingResult) throws ServiceException {
		ModelAndView model = new ModelAndView();
		if (bindingResult.hasErrors()) {
			model.addObject(ATTRIBUTE_EDIT_AUTHOR, author);
			model.addObject(ATTRIBUTE_VALIDATION_ERROR_MESSAGES, bindingResult.getFieldErrors());
			List<Author> authors = authorService.findActualAuthors();
			model.addObject(ATTRIBUTE_AUTHOR_LIST, authors);
			model.addObject(ATTRIBUTE_SIDE_BAR_PAGE, CURRENT);
			model.setViewName(VIEW_EDIT_AUTHORS);
			return model;
		} else {
			authorService.save(author);
			model.setViewName(REDIRECT_PATH);
			return model;
		}
	}

	/**
	 * Expire author.
	 *
	 * @param authorId
	 *            the author id
	 * @return the model and view
	 * @throws ServiceException
	 *             the service exception
	 */
	@Secured({ ROLE_ADMIN })
	@RequestMapping(value = "/view/editauthors", params = PARAM_EXPIRE, method = RequestMethod.POST)
	public ModelAndView expireAuthor(@RequestParam(value = PARAM_AUTHOR_ID, required = true) Long authorId)
			throws ServiceException {
		Author author = authorService.findById(authorId);
		author.setAuthorName(author.getAuthorName());
		authorService.expireAuthor(author);
		ModelAndView model = new ModelAndView();
		model.setViewName(REDIRECT_PATH);
		return model;
	}

}
