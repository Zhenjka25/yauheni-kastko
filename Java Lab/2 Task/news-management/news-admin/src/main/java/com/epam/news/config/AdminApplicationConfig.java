package com.epam.news.config;

import java.util.Locale;

import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.epam.news.interceptor.TimeZoneInterceptor;
import com.epam.news.resolver.SmartCookieLocaleResolver;

/**
 * The Class AdminApplicationConfig. Spring configuration for admin application.
 */
@EnableWebMvc
@Configuration
@Import({ ApplicationConfig.class })
@ComponentScan("com.epam.news.controller, com.epam.news.service")
public class AdminApplicationConfig extends WebMvcConfigurerAdapter {

	/**
	 * Validator.
	 *
	 * @return the local validator factory bean
	 */
	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
		validator.setValidationMessageSource(messageSource());
		return validator;
	}

	/**
	 * View resolver.
	 *
	 * @return the tiles view resolver
	 */
	@Bean
	public TilesViewResolver viewResolver() {
		return new TilesViewResolver();
	}

	/**
	 * Tiles configurer.
	 *
	 * @return the tiles configurer
	 */
	@Bean
	public TilesConfigurer tilesConfigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setCompleteAutoload(true);
		return tilesConfigurer;
	}

	/**
	 * Message source.
	 *
	 * @return the resource bundle message source
	 */
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasename("locale");
		source.setDefaultEncoding("UTF-8");
		return source;
	}

	/**
	 * Locale change interceptor.
	 *
	 * @return the locale change interceptor
	 */
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		return new LocaleChangeInterceptor();
	}

	/**
	 * Time zone interceptor.
	 *
	 * @return the time zone interceptor
	 */
	@Bean
	public TimeZoneInterceptor timeZoneInterceptor() {
		return new TimeZoneInterceptor();
	}

	/**
	 * Locale resolver.
	 *
	 * @return the smart cookie locale resolver
	 */
	@Bean
	public SmartCookieLocaleResolver localeResolver() {
		SmartCookieLocaleResolver localeResolver = new SmartCookieLocaleResolver();
		localeResolver.setDefaultLocale(new Locale("en_US"));
		localeResolver.setCookieName("locale");
		return localeResolver;
	}

	/**
	 * Handler mapping.
	 *
	 * @return the request mapping handler mapping
	 */
	@Bean
	public RequestMappingHandlerMapping handlerMapping() {
		RequestMappingHandlerMapping handlerMapping = new RequestMappingHandlerMapping();
		Object[] interceptors = { localeChangeInterceptor() };
		handlerMapping.setInterceptors(interceptors);
		return handlerMapping;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
		registry.addInterceptor(timeZoneInterceptor());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Validator getValidator() {
		return validator();
	}

}
