package com.epam.news.service.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.entity.*;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.*;
import com.epam.news.service.impl.NewsManagementServiceImpl;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsValueServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceImplTest {

	@Mock
	private NewsService newsServiceMock;

	@Mock
	private AuthorService authorServiceMock;

	@Mock
	private TagService tagServiceMock;

	@Mock
	private CommentService commentServiceMock;

	@InjectMocks
	private NewsManagementServiceImpl service;

	private News news;

	private Author author;

	private List<Tag> tags;

	private List<Comment> comments;

	/**
	 * Setup mocks before running tests.
	 *
	 * @throws ServiceException
	 *             the service exception
	 */
	@Before
	public void setupMocks() throws ServiceException {
		when(newsServiceMock.findById(any(Long.class))).thenAnswer(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				news.setNewsId((Long) invocation.getArguments()[0]);
				return news;
			}
		});
		when(newsServiceMock.save(any(News.class))).thenAnswer(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news = (News) invocation.getArguments()[0];
				news.setNewsId(1L);
				return news;
			}
		});
		when(authorServiceMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				author.setAuthorId((Long) invocation.getArguments()[0]);
				return author;
			}
		});
		when(authorServiceMock.save(any(Author.class))).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = (Author) invocation.getArguments()[0];
				author.setAuthorId(1L);
				return author;
			}
		});
		when(authorServiceMock.findById(any(Long.class))).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = new Author();
				author.setAuthorId((Long) invocation.getArguments()[0]);
				author.setAuthorName("Eugen");
				author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
				return author;
			}
		});
		when(authorServiceMock.findAll()).thenAnswer(new Answer<List<Author>>() {
			@Override
			public List<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<>();
				authors.add(new Author());
				authors.add(new Author());
				return authors;
			}
		});
		when(authorServiceMock.findActualAuthors()).thenAnswer(new Answer<List<Author>>() {
			@Override
			public List<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<>();
				authors.add(new Author());
				return authors;
			}
		});
		when(tagServiceMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				return tags;
			}
		});
		when(tagServiceMock.save(any(Tag.class))).thenAnswer(new Answer<Tag>() {
			@Override
			public Tag answer(InvocationOnMock invocation) throws Throwable {
				Tag tag = (Tag) invocation.getArguments()[0];
				tag.setTagId(1L);
				return tag;
			}
		});
		when(commentServiceMock.findByNewsId(any(Long.class))).thenAnswer(new Answer<List<Comment>>() {
			@Override
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				for (Comment comment : comments) {
					comment.setCommentId((Long) invocation.getArguments()[0]);
				}
				return comments;
			}
		});
		when(newsServiceMock.findBySearchCriteria(any(SearchCriteria.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<>();
				News singleNews = new News();
				singleNews.setNewsId(1L);
				news.add(singleNews);
				return news;
			}
		});
		when(commentServiceMock.save(any(Comment.class))).thenAnswer(new Answer<Comment>() {
			@Override
			public Comment answer(InvocationOnMock invocation) throws Throwable {
				Comment comment = (Comment) invocation.getArguments()[0];
				comment.setCommentId(1L);
				return comment;
			}
		});
		when(tagServiceMock.findAll()).thenAnswer(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<>();
				tags.add(new Tag());
				tags.add(new Tag());
				return tags;
			}
		});
	}

	/**
	 * Setup objects before running tests.
	 */
	@Before
	public void setupObjects() {
		news = new News();
		news.setTitle("Some title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		news.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("Eugen");
		author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		tags = new ArrayList<>();
		Tag tagOne = new Tag();
		tagOne.setTagId(1L);
		tagOne.setTagName("name one");
		Tag tagTwo = new Tag();
		tagTwo.setTagId(2L);
		tagTwo.setTagName("name two");
		tags.add(tagOne);
		tags.add(tagTwo);
		comments = new ArrayList<>();
		Comment commentOne = new Comment();
		commentOne.setCommentId(1L);
		commentOne.setCommentText("some text");
		commentOne.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		Comment commentTwo = new Comment();
		commentTwo.setCommentId(2L);
		commentTwo.setCommentText("another text");
		commentTwo.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		comments.add(commentOne);
		comments.add(commentTwo);
	}

	@Test
	public void findNewsValueObject() throws ServiceException {
		NewsValueObject newsValueObject = service.findSingleNews(1L);
		assertEquals(news, newsValueObject.getNews());
		assertEquals(author, newsValueObject.getAuthor());
		assertEquals(tags, newsValueObject.getTags());
		assertEquals(comments, newsValueObject.getComments());
	}

	@Test
	public void saveNewsAuthorTags() throws ServiceException {
		List<Long> tagList = new ArrayList<>();
		for (Tag tag : tags) {
			tagList.add(tag.getTagId());
		}
		NewsValueObject newsValueObject = new NewsValueObject();
		newsValueObject.setNews(news);
		newsValueObject.setAuthor(author);
		newsValueObject.setTags(tags);
		service.saveNews(newsValueObject);
		assertThat(news.getNewsId(), is(1L));
	}

	@Test
	public void findBySearchCriteria() throws ServiceException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<NewsValueObject> news = service.findBySearchCriteria(searchCriteria);
		assertThat(news.size(), is(1));
	}

}
