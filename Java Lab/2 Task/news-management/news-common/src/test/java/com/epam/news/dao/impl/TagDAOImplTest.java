package com.epam.news.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.news.config.TestConfig;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * The Class TagDAOImplementationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class TagDAOImplTest extends AbstractTest {

	@Autowired()
	private TagDAO tagDAO;

	@Autowired
	private NewsDAO newsDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#getDataSetForInsert()
	 */
	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tagData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tagData.xml")) };
		return new CompositeDataSet(datasets);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#configureDbTestcase()
	 */
	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#cleanDataBase()
	 */
	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteTag() throws DAOException {
		Tag tag = new Tag();
		tag.setTagName("some tag");
		long id = tagDAO.save(tag);
		assertNotEquals(0, id);
		assertNotNull(tagDAO.getById(id));
		tagDAO.deleteById(id);
		assertNull(tagDAO.getById(id));
	}

	@Test(expected = DAOException.class)
	public void testWrongTagName() throws DAOException {
		Tag tag = new Tag();
		tag.setTagName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		tagDAO.save(tag);
	}

	@Test
	public void getById() throws DAOException {
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("sport");
		assertEquals(tag, tagDAO.getById(1));
	}

	@Test
	public void updateTag() throws DAOException {
		Tag tag = tagDAO.getById(2);
		tag.setTagName("not politics");
		tagDAO.update(tag);
		assertEquals("not politics", tagDAO.getById(2).getTagName());
	}

	@Test
	public void getByName() throws DAOException {
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("sport");
		assertEquals(tag, tagDAO.getByName("sport"));
	}

	@Test
	public void findAllComments() throws DAOException {
		List<Tag> tags = tagDAO.getAll();
		assertThat(tags.size(), is(2));
	}

	@Test
	public void findByNewsId() throws DAOException {
		List<Tag> tags = tagDAO.getByNewsId(1);
		assertThat(tags.size(), is(1));
	}

	@Test
	public void deleteNewsTag() throws DAOException {
		tagDAO.deleteNewsTagByTagId(1L);
		assertThat(newsDAO.getByTagId(1L).size(), is(0));
	}

	@Test
	public void getByTagIdList() throws DAOException {
		Tag tagOne = new Tag();
		tagOne.setTagId(1L);
		Tag tagTwo = new Tag();
		tagTwo.setTagId(2L);
		List<Tag> tagList = new ArrayList<>();
		tagList.add(tagOne);
		tagList.add(tagTwo);
		tagList = tagDAO.getByTagIdList(tagList);
		assertEquals("sport", tagList.get(0).getTagName());
		assertEquals("politics", tagList.get(1).getTagName());
	}

}
