package com.epam.news.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.news.config.TestConfig;
import com.epam.news.dao.CommentDAO;
import com.epam.news.dao.NewsDAO;
import com.epam.news.dao.TagDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * The Class NewsDAOImplementationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class NewsDAOImplTest extends AbstractTest {

	@Autowired()
	private NewsDAO newsDAO;

	@Autowired
	private CommentDAO commentDAO;

	@Autowired
	private TagDAO tagDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#getDataSetForInsert()
	 */
	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/commentData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tagData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tagData.xml")) };
		return new CompositeDataSet(datasets);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#configureDbTestcase()
	 */
	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#cleanDataBase()
	 */
	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteNews() throws DAOException {
		News news = new News();
		news.setTitle("title");
		news.setShortText("short");
		news.setFullText("full");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		long id = newsDAO.save(news);
		assertNotEquals(0, id);
		assertNotNull(newsDAO.getById(id));
		newsDAO.deleteById(id);
		assertNull(newsDAO.getById(id));
	}

	@Test(expected = DAOException.class)
	public void testWrongTitle() throws DAOException {
		News news = new News();
		news.setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		news.setShortText("short");
		news.setFullText("full");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		newsDAO.save(news);
	}

	@Test
	public void getById() throws DAOException {
		News news = new News();
		news.setNewsId(1L);
		news.setTitle("Some title");
		news.setShortText("Some short text");
		news.setFullText("Some full text");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		assertEquals(news, newsDAO.getById(1));
	}

	@Test
	public void updateNews() throws DAOException {
		News news = newsDAO.getById(2);
		news.setFullText("Another full text");
		newsDAO.update(news);
		assertEquals("Another full text", newsDAO.getById(2).getFullText());
	}

	@Test
	public void findAllNews() throws DAOException {
		List<News> news = newsDAO.getAll();
		assertThat(news.size(), is(2));
	}

	@Test
	public void findByAuthorId() throws DAOException {
		List<News> news = newsDAO.getByAuthorId(1);
		assertThat(news.size(), is(2));
	}

	@Test
	public void findByTagId() throws DAOException {
		List<News> news = newsDAO.getByTagId(1);
		assertThat(news.size(), is(2));
	}

	@Test
	public void searchNews() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.addTagId(1L);
		searchCriteria.addTagId(2L);
		List<News> news = newsDAO.findBySearchCriteria(searchCriteria);
		assertThat(news.size(), is(2));
	}

	@Test
	public void countNews() throws DAOException {
		assertThat(newsDAO.countNews(), is(2L));
	}

	@Test
	public void saveNewsAuthor() throws DAOException {
		newsDAO.saveNewsAuthor(1, 2);
		newsDAO.saveNewsAuthor(2, 2);
		assertThat(newsDAO.getByAuthorId(2).size(), is(2));
	}

	@Test(expected = DAOException.class)
	public void saveNewsAuthorException() throws DAOException {
		newsDAO.saveNewsAuthor(1, 5);
	}

	@Test
	public void saveNewsTag() throws DAOException {
		newsDAO.saveNewsTag(1, 2);
		newsDAO.saveNewsTag(2, 2);
		assertThat(newsDAO.getByTagId(2).size(), is(2));
	}

	@Test(expected = DAOException.class)
	public void saveNewsTagException() throws DAOException {
		newsDAO.saveNewsTag(1, 5);
	}

	@Test
	public void deleteNewsAuthor() throws DAOException {
		assertThat(newsDAO.getByAuthorId(1).size(), is(2));
		newsDAO.deleteNewsAuthorByNewsId(2);
		newsDAO.deleteNewsAuthorByNewsId(1);
		assertThat(newsDAO.getByAuthorId(1).size(), is(0));
	}

	@Test
	public void deleteNewsTag() throws DAOException {
		assertThat(newsDAO.getByTagId(1).size(), is(2));
		newsDAO.deleteNewsTagByNewsId(2);
		newsDAO.deleteNewsTagByNewsId(1);
		assertThat(newsDAO.getByTagId(1).size(), is(0));
	}

	@Test
	public void deleteCommentsByNewsId() throws DAOException {
		assertThat(commentDAO.getByNewsId(1).size(), is(1));
		newsDAO.deleteCommentsByNewsId(1);
		assertThat(commentDAO.getByNewsId(1).size(), is(0));
	}

	@Test
	public void findIdListBySearchCriteria() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		ArrayList<Long> tagList = new ArrayList<>();
		tagList.add(1L);
		searchCriteria.setTagsId(tagList);
		assertThat(newsDAO.countNewsBySearchCriteria(searchCriteria), is(2));
		List<Long> newsIdList = newsDAO.findIdListBySearchCriteria(searchCriteria);
		assertThat(newsIdList.size(), is(2));
		assertTrue(newsIdList.contains(new Long(1)));
		assertTrue(newsIdList.contains(new Long(2)));
	}

	@Test
	public void findPrevNews() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		ArrayList<Long> tagList = new ArrayList<>();
		tagList.add(1L);
		searchCriteria.setTagsId(tagList);
		assertThat(newsDAO.findPrevNews(2L, searchCriteria).getNewsId(), is(1L));
	}

	@Test
	public void findNextNews() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		ArrayList<Long> tagList = new ArrayList<>();
		tagList.add(1L);
		searchCriteria.setTagsId(tagList);
		assertThat(newsDAO.findNextNews(1L, searchCriteria).getNewsId(), is(2L));
	}

	@Test
	public void deleteList() throws DAOException {
		ArrayList<Long> idList = new ArrayList<>();
		idList.add(1L);
		idList.add(2L);
		newsDAO.deleteCommentsListByNewsId(idList);
		newsDAO.deleteNewsAuthorListByNewsId(idList);
		newsDAO.deleteNewsTagListByNewsId(idList);
		newsDAO.deleteList(idList);
		assertThat(newsDAO.getAll().size(), is(0));
		assertThat(newsDAO.getByAuthorId(1L).size(), is(0));
		assertThat(newsDAO.getByTagId(1L).size(), is(0));
	}

	@Test
	public void saveNewsTagList() throws DAOException {
		Tag tag = new Tag();
		tag.setTagId(2L);
		ArrayList<Tag> tagList = new ArrayList<>();
		tagList.add(tag);
		newsDAO.saveNewsTagList(1L, tagList);
		assertThat(tagDAO.getByNewsId(1L).size(), is(2));
	}

}
