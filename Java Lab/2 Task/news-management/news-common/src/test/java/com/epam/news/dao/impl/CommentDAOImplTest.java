package com.epam.news.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.news.config.TestConfig;
import com.epam.news.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * The Class CommentDAOImplementationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class CommentDAOImplTest extends AbstractTest {

	@Autowired()
	private CommentDAO commentDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#getDataSetForInsert()
	 */
	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/commentData.xml")) };
		return new CompositeDataSet(datasets);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#configureDbTestcase()
	 */
	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#cleanDataBase()
	 */
	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteComment() throws DAOException {
		Comment comment = new Comment();
		comment.setNewsId(2L);
		comment.setCommentText("some text");
		comment.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		long id = commentDAO.save(comment);
		assertNotEquals(0, id);
		assertNotNull(commentDAO.getById(id));
		commentDAO.deleteById(id);
		assertNull(commentDAO.getById(id));
	}

	@Test(expected = DAOException.class)
	public void testWrongNewsId() throws DAOException {
		Comment comment = new Comment();
		comment.setNewsId(100L);
		comment.setCommentText("some text");
		comment.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		commentDAO.save(comment);
	}

	@Test
	public void getById() throws DAOException {
		Comment comment = new Comment();
		comment.setCommentId(1L);
		comment.setNewsId(1L);
		comment.setCommentText("Some text here");
		comment.setCreationDate(Timestamp.valueOf("2015-08-09 11:41:29.77"));
		assertEquals(comment, commentDAO.getById(1));
	}

	@Test
	public void updateAuthor() throws DAOException {
		Comment comment = commentDAO.getById(2);
		comment.setCommentText("Text");
		commentDAO.update(comment);
		assertEquals("Text", commentDAO.getById(2).getCommentText());
	}

	@Test
	public void findAllComments() throws DAOException {
		List<Comment> comments = commentDAO.getAll();
		assertThat(comments.size(), is(2));
	}

	@Test
	public void findByNewsId() throws DAOException {
		List<Comment> comments = commentDAO.getByNewsId(1);
		assertThat(comments.size(), is(1));
	}

}
