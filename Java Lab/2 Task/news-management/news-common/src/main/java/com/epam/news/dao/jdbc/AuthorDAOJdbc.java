package com.epam.news.dao.jdbc;

import static com.epam.news.constant.ConstantsCommon.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * The Class AuthorDAOImplementation. Implementation of AuthorDAO interface by
 * using simple JDBC.
 */
@Repository
public class AuthorDAOJdbc implements AuthorDAO {

	private static final String SQL_SELECT_AUTHOR_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_SELECT_AUTHOR_BY_NAME = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_NAME = ?";
	private static final String SQL_SELECT_AUTHOR_BY_NEWS_ID = "SELECT AUTHOR.AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM NEWS_AUTHOR JOIN AUTHOR ON (NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID) WHERE NEWS_ID = ?";
	private static final String SQL_SELECT_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
	private static final String SQL_SELECT_ACTUAL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE EXPIRED IS NULL";
	private static final String SQL_CREATE_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (?,?,?)";
	private static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String SQL_DELETE_AUTHOR = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID = "DELETE FROM NEWS_AUTHOR WHERE AUTHOR_ID = ?";
	private static final String SQL_SELECT_NEXT_VAL = "SELECT AUTHOR_SEQ.NEXTVAL FROM DUAL";

	@Autowired
	private DataSource dataSource;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long save(Author entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_AUTHOR)) {
			entity.setAuthorId(getNextValue());
			preparedStatement.setLong(1, entity.getAuthorId());
			preparedStatement.setString(2, entity.getAuthorName());
			preparedStatement.setTimestamp(3,
					entity.getExpired() != null ? new Timestamp(entity.getExpired().getTime()) : null);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return entity.getAuthorId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author getById(long id) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID)) {
			preparedStatement.setLong(1, id);
			List<Author> authors = parseResultSet(preparedStatement.executeQuery());
			if (!authors.isEmpty()) {
				author = authors.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Author entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
			preparedStatement.setString(1, entity.getAuthorName());
			preparedStatement.setTimestamp(2,
					entity.getExpired() != null ? new Timestamp(entity.getExpired().getTime()) : null);
			preparedStatement.setLong(3, entity.getAuthorId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsAuthorByAuthorId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Author> getAll() throws DAOException {
		List<Author> authors = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			authors = parseResultSet(statement.executeQuery(SQL_SELECT_ALL_AUTHORS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authors;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author getByName(String name) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NAME)) {
			preparedStatement.setString(1, name);
			List<Author> authors = parseResultSet(preparedStatement.executeQuery());
			if (!authors.isEmpty()) {
				author = authors.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Author> getActualAuthors() throws DAOException {
		List<Author> authors = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			authors = parseResultSet(statement.executeQuery(SQL_SELECT_ACTUAL_AUTHORS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authors;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author getByNewsId(long id) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NEWS_ID)) {
			preparedStatement.setLong(1, id);
			List<Author> authors = parseResultSet(preparedStatement.executeQuery());
			if (!authors.isEmpty()) {
				author = authors.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/**
	 * Parses the result set.
	 *
	 * @param rs
	 *            the rs
	 * @return the list
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<Author> parseResultSet(ResultSet rs) throws SQLException {
		List<Author> authors = new ArrayList<>();
		while (rs.next()) {
			Author author = new Author();
			author.setAuthorId(rs.getLong(AUTHOR_ID));
			author.setAuthorName(rs.getString(AUTHOR_NAME));
			author.setExpired(rs.getTimestamp(EXPIRED));
			authors.add(author);
		}
		return authors;
	}

	/**
	 * Gets the next value of author id from database sequence.
	 *
	 * @return the next value
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Long getNextValue() throws SQLException {
		Long value = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEXT_VAL);
			if (resultSet.next()) {
				value = resultSet.getLong(NEXTVAL);
			}
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return value;
	}

}
