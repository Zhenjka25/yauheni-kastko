package com.epam.news.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.CommentService;

/**
 * The Class CommentServiceImplementation. Interacts with Comment JdbcDAO.
 */
@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDAO commentDAO;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Comment save(Comment entity) throws ServiceException {
		try {
			entity.setCommentId(commentDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Comment findById(long id) throws ServiceException {
		try {
			return commentDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Comment entity) throws ServiceException {
		try {
			commentDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			commentDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Comment> findAll() throws ServiceException {
		try {
			return commentDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Comment> findByNewsId(long id) throws ServiceException {
		try {
			return commentDAO.getByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
