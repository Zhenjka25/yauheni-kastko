package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The Class Author. Represents news author business entity. Can be expired.
 */
public class Author implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5657673558616042306L;

	/** The author id. Primary key for author table in database. */
	private Long authorId;

	/** The author name. Displayed with news, written by this author */
	@NotNull
	@Size(min = 3, max = 30, message = "{authorname.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{authorname.error.pattern}")
	private String authorName;

	/**
	 * The expired date. Any author can be expired. After that he cannot write
	 * news but he still does displayed with news, written by him earlier.
	 */
	private Date expired;

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the author name.
	 *
	 * @return the author name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the author name.
	 *
	 * @param authorName
	 *            the new author name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Gets the expired date.
	 *
	 * @return the expired
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Sets the expired date.
	 *
	 * @param expired
	 *            the new expired
	 */
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result + ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName + ", expired=" + expired + "]";
	}

}
