package com.epam.news.dao.jdbc;

import static com.epam.news.constant.ConstantsCommon.PASSWORD;
import static com.epam.news.constant.ConstantsCommon.ROLE_NAME;
import static com.epam.news.constant.ConstantsCommon.USER_NAME;

import java.sql.*;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.UserInfoDAO;
import com.epam.news.entity.UserInfo;
import com.epam.news.exception.DAOException;

/**
 * The Class UserInfoDAOImplementation. Implementation of UserInfoDAO interface
 * by using simple JDBC.
 */
@Repository
public class UserInfoDAOJdbc implements UserInfoDAO {

	@Autowired
	private DataSource dataSource;

	private static final String SQL_SELECT_USER = "SELECT USER_NAME, PASSWORD, ROLE_NAME FROM ROLES JOIN USERS ON (ROLES.USER_ID = USERS.USER_ID) WHERE LOGIN = ?";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInfo getUserInfo(String username) throws DAOException {
		UserInfo userInfo = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER)) {
			preparedStatement.setString(1, username);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				userInfo = new UserInfo();
				userInfo.setUsername(rs.getString(USER_NAME));
				userInfo.setPassword(rs.getString(PASSWORD));
				userInfo.setRole(rs.getString(ROLE_NAME));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return userInfo;
	}

}
