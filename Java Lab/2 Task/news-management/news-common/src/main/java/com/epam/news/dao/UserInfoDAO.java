
package com.epam.news.dao;

import com.epam.news.entity.UserInfo;
import com.epam.news.exception.DAOException;

/**
 * The Interface UserInfoDAO. Classes implementing this interface, interacts
 * with the tables "USERS" and "ROLES" in the database.
 */
public interface UserInfoDAO {

	/**
	 * Gets the user info.
	 *
	 * @param username
	 *            the username
	 * @return the user info
	 * @throws DAOException 
	 */
	UserInfo getUserInfo(String username) throws DAOException;

}
