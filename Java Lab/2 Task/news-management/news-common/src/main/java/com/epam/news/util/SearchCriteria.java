package com.epam.news.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class SearchCriteria. Contains list of tag ids and author id for news
 * searching.
 */
public class SearchCriteria implements Serializable {

	private static final long serialVersionUID = 2045825113854080268L;
	private Long authorId;
	private List<Long> tagsId = new ArrayList<>();
	private int page = 1;

	/**
	 * Gets the author id.
	 *
	 * @return the author id
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the author id.
	 *
	 * @param authorId
	 *            the new author id
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Gets the tags id.
	 *
	 * @return the tags id
	 */
	public List<Long> getTagsId() {
		return tagsId;
	}

	/**
	 * Sets the tags id.
	 *
	 * @param tagsId
	 *            the new tags id
	 */
	public void setTagsId(List<Long> tagsId) {
		this.tagsId = tagsId;
	}

	/**
	 * Adds the tag id.
	 *
	 * @param tagId
	 *            the tag id
	 */
	public void addTagId(Long tagId) {
		tagsId.add(tagId);
	}

	/**
	 * Gets the page.
	 *
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * Sets the page.
	 *
	 * @param page
	 *            the new page
	 */
	public void setPage(int page) {
		this.page = page;
	}

}
