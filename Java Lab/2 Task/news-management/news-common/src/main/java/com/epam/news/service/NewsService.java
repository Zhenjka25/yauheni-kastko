package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;
import com.epam.news.util.SearchCriteria;

/**
 * The Interface NewsService. Service for interacting with News DAO layer.
 */
public interface NewsService extends BasicService<News> {

	/**
	 * Saves data to the news_author table.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws ServiceException
	 *             the service exception
	 */
	void saveNewsAuthor(long newsId, long authorId) throws ServiceException;

	/**
	 * Saves data to the news_tag table.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws ServiceException
	 *             the service exception
	 */
	void saveNewsTag(long newsId, long tagId) throws ServiceException;

	/**
	 * Finds news by author id.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findByAuthorId(long id) throws ServiceException;

	/**
	 * Finds news by tag id.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findByTagId(long id) throws ServiceException;

	/**
	 * Deletes data from news_author table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsAuthorByNewsId(long id) throws ServiceException;

	/**
	 * Deletes data from news_tag table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsTagByNewsId(long id) throws ServiceException;

	/**
	 * Deletes comments by news id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteCommentsByNewsId(long id) throws ServiceException;

	/**
	 * Finds news by search criteria.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findBySearchCriteria(SearchCriteria searchObject) throws ServiceException;

	/**
	 * Count news.
	 *
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long countNews() throws ServiceException;

	/**
	 * Count news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the int
	 * @throws ServiceException
	 *             the service exception
	 */
	int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Find id list by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Find prev news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 * @throws ServiceException
	 *             the service exception
	 */
	News findPrevNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Find next news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 * @throws ServiceException
	 *             the service exception
	 */
	News findNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Delete list.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteList(List<Long> newsIdList) throws ServiceException;

	/**
	 * Delete news author list by news id.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsAuthorListByNewsId(List<Long> newsIdList) throws ServiceException;

	/**
	 * Delete news tag list by news id.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsTagListByNewsId(List<Long> newsIdList) throws ServiceException;

	/**
	 * Delete comments list by news id.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteCommentsListByNewsId(List<Long> newsIdList) throws ServiceException;

	/**
	 * Save newsTag list.
	 *
	 * @param newsId
	 *            the news id
	 * @param tags
	 *            the tags
	 * @throws ServiceException
	 *             the service exception
	 */
	void saveNewsTagList(long newsId, List<Tag> tags) throws ServiceException;
}
