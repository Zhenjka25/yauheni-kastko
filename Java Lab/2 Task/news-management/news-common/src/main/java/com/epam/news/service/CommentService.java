package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.ServiceException;

/**
 * The Interface CommentService. Service for interacting with Comment DAO layer.
 */
public interface CommentService extends BasicService<Comment> {

	/**
	 * Finds comments by news id.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Comment> findByNewsId(long id) throws ServiceException;
}
