package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.exception.ServiceException;
import com.epam.news.util.SearchCriteria;

/**
 * The Interface NewsManagementService. Contains business logic of news
 * management. Implementations must support transactions.
 */
public interface NewsManagementService {

	/**
	 * Finds news DTO by news object.
	 *
	 * @param news
	 *            the news
	 * @return the news value object
	 * @throws ServiceException
	 *             the service exception
	 */
	NewsValueObject findSingleNews(News news) throws ServiceException;

	/**
	 * Find news DTO by news id.
	 *
	 * @param id
	 *            the id
	 * @return the news value object
	 * @throws ServiceException
	 *             the service exception
	 */
	NewsValueObject findSingleNews(long id) throws ServiceException;

	/**
	 * Saves news.
	 *
	 * @param newsValueObject
	 *            the news value object
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long saveNews(NewsValueObject newsValueObject) throws ServiceException;

	/**
	 * Deletes news.
	 *
	 * @param newsidList
	 *            the news id list
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNews(List<Long> newsidList) throws ServiceException;

	/**
	 * Updates news.
	 *
	 * @param newsValueObject
	 *            the news value object
	 * @throws ServiceException
	 *             the service exception
	 */
	void updateNews(NewsValueObject newsValueObject) throws ServiceException;

	/**
	 * Finds news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<NewsValueObject> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Finds all news.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findAllNews() throws ServiceException;

	/**
	 * Count news.
	 *
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long countNews() throws ServiceException;

	/**
	 * Count news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the int
	 * @throws ServiceException
	 *             the service exception
	 */
	int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Find id list by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Find prev news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 * @throws ServiceException
	 *             the service exception
	 */
	News findPrevNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Find next news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 * @throws ServiceException
	 *             the service exception
	 */
	News findNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException;

}
