package com.epam.news.dao.jdbc;

import static com.epam.news.constant.ConstantsCommon.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsDAOImplementation. Implementation of NewsDAO interface by using
 * simple JDBC.
 */
@Repository
public class NewsDAOJdbc implements NewsDAO {

	private static final String SQL_SELECT_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_SELECT_ALL_NEWS = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
	private static final String SQL_SELECT_NEWS_BY_AUTHOR_ID = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE AUTHOR_ID = ? GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
	private static final String SQL_SELECT_NEWS_BY_TAG_ID = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE TAG_ID = ? GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
	private static final String SQL_SELECT_PREV_NEXT_NEWS = "SELECT * FROM (SELECT NEWS_ID, LAG(NEWS_ID, 1, 0) OVER (ORDER BY RN) AS NEWS_ID_PREV, LEAD(NEWS_ID, 1, 0) OVER (ORDER BY RN) AS NEWS_ID_NEXT FROM (SELECT NEWSID AS NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, RN FROM (SELECT NEWSID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, ROWNUM RN FROM(SELECT NEWS.NEWS_ID AS NEWSID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, MODIFICATION_DATE DESC)) LEFT JOIN NEWS_AUTHOR ON (NEWS_AUTHOR.NEWS_ID = NEWSID) LEFT JOIN NEWS_TAG ON (NEWS_TAG.NEWS_ID = NEWSID)";
	private static final String SQL_CREATE_NEWS = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (?,?,?,?,?,?)";
	private static final String SQL_CREATE_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";
	private static final String SQL_CREATE_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";
	private static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_COMMENTS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	private static final String SQL_SELECT_NEXT_VAL = "SELECT NEWS_SEQ.NEXTVAL FROM DUAL";
	private static final String SQL_SEARCH_NEWS = "SELECT NEWS_ID NEWS_ID, TITLE TITLE, SHORT_TEXT SHORT_TEXT, FULL_TEXT FULL_TEXT, CREATION_DATE CREATION_DATE, MODIFICATION_DATE MODIFICATION_DATE, RNN RNN FROM (SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, ROWNUM RNN FROM (SELECT NEWSID AS NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, RN FROM (SELECT NEWSID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, ROWNUM RN FROM(SELECT NEWS.NEWS_ID AS NEWSID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENTS.COMMENT_ID) DESC, MODIFICATION_DATE DESC)) LEFT JOIN NEWS_AUTHOR ON (NEWS_AUTHOR.NEWS_ID = NEWSID) LEFT JOIN NEWS_TAG ON (NEWS_TAG.NEWS_ID = NEWSID)";
	private static final String SQL_SORT_QUERY = " GROUP BY NEWSID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, RN ORDER BY RN";
	private static final String SQL_COUNT_NEWS = "SELECT COUNT(NEWS_ID) FROM NEWS";

	@Autowired
	private DataSource dataSource;

	@Autowired
	private Environment env;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long save(News entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS)) {
			entity.setNewsId(getNextValue());
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getTitle());
			preparedStatement.setString(3, entity.getShortText());
			preparedStatement.setString(4, entity.getFullText());
			preparedStatement.setTimestamp(5,
					entity.getCreationDate() != null ? new Timestamp(entity.getCreationDate().getTime()) : null);
			preparedStatement.setDate(6,
					entity.getModificationDate() != null ? new Date(entity.getModificationDate().getTime()) : null);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return entity.getNewsId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News getById(long id) throws DAOException {
		News singleNews = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_ID)) {
			preparedStatement.setLong(1, id);
			List<News> news = parseResultSet(preparedStatement.executeQuery());
			if (!news.isEmpty()) {
				singleNews = news.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return singleNews;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(News entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS)) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4,
					entity.getCreationDate() != null ? new Timestamp(entity.getCreationDate().getTime()) : null);
			preparedStatement.setDate(5,
					entity.getModificationDate() != null ? new Date(entity.getModificationDate().getTime()) : null);
			preparedStatement.setLong(6, entity.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> getAll() throws DAOException {
		List<News> news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			news = parseResultSet(statement.executeQuery(SQL_SELECT_ALL_NEWS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveNewsAuthor(long newsId, long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_AUTHOR)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveNewsTag(long newsId, long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_TAG)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> getByAuthorId(long id) throws DAOException {
		List<News> news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedDtatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_AUTHOR_ID)) {
			preparedDtatement.setLong(1, id);
			news = parseResultSet(preparedDtatement.executeQuery());
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> getByTagId(long id) throws DAOException {
		List<News> news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedDtatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_TAG_ID)) {
			preparedDtatement.setLong(1, id);
			news = parseResultSet(preparedDtatement.executeQuery());
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsTagByNewsId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCommentsByNewsId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENTS)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> findBySearchCriteria(SearchCriteria searchObject) throws DAOException {
		String query = buildSearchQuery(searchObject);
		List<News> newsList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			newsList = parseResultSet(statement.executeQuery(query));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countNews() throws DAOException {
		Long count = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(SQL_COUNT_NEWS);
			if (rs.next()) {
				count = rs.getLong(1);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		int count = 0;
		try (Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(buildQueryForCountNews(searchCriteria));
			while (rs.next()) {
				count++;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return count;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		List<Long> idList = new ArrayList<>();
		try (Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(buildQueryForCountNews(searchCriteria));
			while (rs.next()) {
				idList.add(rs.getLong(1));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return idList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findPrevNews(Long newsId, SearchCriteria searchCriteria) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		News singleNews = null;
		try (Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(buildQueryForPrevNextNews(newsId, searchCriteria));
			if (rs.next()) {
				singleNews = getById(rs.getLong(NEWS_ID_PREV));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return singleNews;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findNextNews(Long newsId, SearchCriteria searchCriteria) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		News singleNews = null;
		try (Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(buildQueryForPrevNextNews(newsId, searchCriteria));
			if (rs.next()) {
				singleNews = getById(rs.getLong(NEWS_ID_NEXT));
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		return singleNews;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteList(List<Long> newsIdList) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS)) {
			for (Long id : newsIdList) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsAuthorListByNewsId(List<Long> newsIdList) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR)) {
			for (Long id : newsIdList) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsTagListByNewsId(List<Long> newsIdList) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG)) {
			for (Long id : newsIdList) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCommentsListByNewsId(List<Long> newsIdList) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENTS)) {
			for (Long id : newsIdList) {
				preparedStatement.setLong(1, id);
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveNewsTagList(long newsId, List<Tag> tags) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_TAG)) {
			for (Tag tag : tags) {
				preparedStatement.setLong(1, newsId);
				preparedStatement.setLong(2, tag.getTagId());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/**
	 * Parses the result set.
	 *
	 * @param rs
	 *            the rs
	 * @return the list
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<News> parseResultSet(ResultSet rs) throws SQLException {
		List<News> news = new ArrayList<>();
		while (rs.next()) {
			News singleNews = new News();
			singleNews.setNewsId(rs.getLong(NEWS_ID));
			singleNews.setTitle(rs.getString(TITLE));
			singleNews.setShortText(rs.getString(SHORT_TEXT));
			singleNews.setFullText(rs.getString(FULL_TEXT));
			singleNews.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
			singleNews.setModificationDate(rs.getDate(MODIFICATION_DATE));
			news.add(singleNews);
		}
		return news;
	}

	/**
	 * Gets the next value for news id from database sequence.
	 *
	 * @return the next value
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Long getNextValue() throws SQLException {
		Long value = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEXT_VAL);
			if (resultSet.next()) {
				value = resultSet.getLong(NEXTVAL);
			}
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return value;
	}

	/**
	 * Builds the search query by search criteria object.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the string
	 */
	private String buildSearchQuery(SearchCriteria searchObject) {
		StringBuilder query = new StringBuilder(SQL_SEARCH_NEWS);
		if (searchObject.getAuthorId() != null || !searchObject.getTagsId().isEmpty()) {
			query.append(includeAuthorInQuery(searchObject));
			query.append(includeTagsInQuery(searchObject));
		}
		query.append(SQL_SORT_QUERY);
		query.append(")) WHERE RNN > ");
		int newsPerPage = Integer.parseInt(env.getProperty(NEWS_PER_PAGE_KEY));
		int firstNews = newsPerPage * (searchObject.getPage() - 1);
		int lastNews = firstNews + newsPerPage;
		query.append(firstNews);
		query.append(" AND RNN <= ");
		query.append(lastNews);
		return query.toString();
	}

	/**
	 * Builds the query for counting news.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the string
	 */
	private String buildQueryForCountNews(SearchCriteria searchObject) {
		StringBuilder query = new StringBuilder(SQL_SEARCH_NEWS);
		if (searchObject.getAuthorId() != null || !searchObject.getTagsId().isEmpty()) {
			query.append(includeAuthorInQuery(searchObject));
			query.append(includeTagsInQuery(searchObject));
		}
		query.append(SQL_SORT_QUERY);
		query.append("))");
		return query.toString();
	}

	/**
	 * Builds the query for prev and next news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchObject
	 *            the search object
	 * @return the string
	 */
	private String buildQueryForPrevNextNews(Long newsId, SearchCriteria searchObject) {
		StringBuilder query = new StringBuilder(SQL_SELECT_PREV_NEXT_NEWS);
		if (searchObject.getAuthorId() != null || !searchObject.getTagsId().isEmpty()) {
			query.append(includeAuthorInQuery(searchObject));
			query.append(includeTagsInQuery(searchObject));
		}
		query.append(SQL_SORT_QUERY);
		query.append(")) WHERE NEWS_ID = ");
		query.append(newsId);
		return query.toString();
	}

	/**
	 * Include author in search query.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the string
	 */
	private String includeAuthorInQuery(SearchCriteria searchObject) {
		StringBuilder query = new StringBuilder();
		query.append(" WHERE");
		if (searchObject.getAuthorId() != null) {
			query.append(" AUTHOR_ID = ");
			query.append(searchObject.getAuthorId());
		}
		if (!searchObject.getTagsId().isEmpty() && searchObject.getAuthorId() != null) {
			query.append(" AND");
		}
		return query.toString();
	}

	/**
	 * Include tags in search query.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the string
	 */
	private String includeTagsInQuery(SearchCriteria searchObject) {
		StringBuilder query = new StringBuilder();
		if (!searchObject.getTagsId().isEmpty()) {
			query.append(" TAG_ID IN (");
			int i;
			for (i = 0; i < searchObject.getTagsId().size() - 1; i++) {
				query.append(searchObject.getTagsId().get(i));
				query.append(", ");
			}
			query.append(searchObject.getTagsId().get(i));
			query.append(")");
		}
		return query.toString();
	}

}
