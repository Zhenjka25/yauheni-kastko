package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Interface TagDAO. Classes implementing this interface, interacts with the
 * table "TAG" in the database.
 */
public interface TagDAO extends BasicDAO<Tag> {

	/**
	 * Gets tag by name.
	 *
	 * @param name
	 *            the name
	 * @return Tag by name
	 * @throws DAOException
	 *             the DAO exception
	 */
	Tag getByName(String name) throws DAOException;

	/**
	 * Deletes data from news_tag table by tag id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsTagByTagId(Long id) throws DAOException;

	/**
	 * Gets tags by news id.
	 *
	 * @param id
	 *            the id
	 * @return List<Tag> by news id
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Tag> getByNewsId(long id) throws DAOException;

	/**
	 * Gets list of tags by tag id list. Tag list parameter contains tags with
	 * only id.
	 *
	 * @param tagList
	 *            the tag list
	 * @return the by tag id list
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Tag> getByTagIdList(List<Tag> tagList) throws DAOException;
}
