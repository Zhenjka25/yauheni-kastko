package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.util.SearchCriteria;

/**
 * The Interface NewsDAO. Classes implementing this interface, interacts with
 * the tables "NEWS", "NEWS_AUTHOR" and "NEWS_TAG" in the database.
 */
public interface NewsDAO extends BasicDAO<News> {

	/**
	 * Saves data to the news_author table.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void saveNewsAuthor(long newsId, long authorId) throws DAOException;

	/**
	 * Saves data to the news_tag table.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void saveNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * Gets news by author id.
	 *
	 * @param id
	 *            the id
	 * @return List<News> by author id
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<News> getByAuthorId(long id) throws DAOException;

	/**
	 * Gets news by tag id.
	 *
	 * @param id
	 *            the id
	 * @return List<News> by tag id
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<News> getByTagId(long id) throws DAOException;

	/**
	 * Deletes data from news_author table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsAuthorByNewsId(long id) throws DAOException;

	/**
	 * Deletes data from news_tag table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsTagByNewsId(long id) throws DAOException;

	/**
	 * Deletes comments by news id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteCommentsByNewsId(long id) throws DAOException;

	/**
	 * Finds news by search criteria.
	 *
	 * @param searchObject
	 *            the search object
	 * @return List<News> by search criteria
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<News> findBySearchCriteria(SearchCriteria searchObject) throws DAOException;

	/**
	 * Counts news.
	 *
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
	Long countNews() throws DAOException;

	/**
	 * Counts news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
	int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;

	/**
	 * Finds id list by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;

	/**
	 * Finds prev news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 * @throws DAOException
	 *             the DAO exception
	 */
	News findPrevNews(Long newsId, SearchCriteria searchCriteria) throws DAOException;

	/**
	 * Finds next news.
	 *
	 * @param newsId
	 *            the news id
	 * @param searchCriteria
	 *            the search criteria
	 * @return the news
	 * @throws DAOException
	 *             the DAO exception
	 */
	News findNextNews(Long newsId, SearchCriteria searchCriteria) throws DAOException;

	/**
	 * Deletes list.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteList(List<Long> newsIdList) throws DAOException;

	/**
	 * Deletes news author list by news id.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsAuthorListByNewsId(List<Long> newsIdList) throws DAOException;

	/**
	 * Deletes news tag list by news id.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsTagListByNewsId(List<Long> newsIdList) throws DAOException;

	/**
	 * Deletes comments list by news id.
	 *
	 * @param newsIdList
	 *            the news id list
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteCommentsListByNewsId(List<Long> newsIdList) throws DAOException;

	/**
	 * Saves newsTag list.
	 *
	 * @param newsId
	 *            the news id
	 * @param tags
	 *            the tags
	 * @throws DAOException
	 *             the DAO exception
	 */
	void saveNewsTagList(long newsId, List<Tag> tags) throws DAOException;

}
