package com.epam.news.entity;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

/**
 * The Class NewsValueObject. NewsDTO object for transfer the data to the view
 * layer. Contains news, author of this news and list of news tags.
 */
public class NewsValueObject {

	/** The news. */
	@Valid
	private News news;

	/** The news author. */
	private Author author;

	/** List of tags, to which refers news. */
	private List<Tag> tags = new ArrayList<>();

	/** List of comments. Contains all news comments. */
	private List<Comment> comments = new ArrayList<>();

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news
	 *            the new news
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author
	 *            the new author
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<Tag> getTags() {
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags
	 *            the new tags
	 */
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	/**
	 * Adds the tag.
	 *
	 * @param tag
	 *            the tag
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments
	 *            the new comments
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Adds the comment.
	 *
	 * @param comment
	 *            the comment
	 */
	public void addComment(Comment comment) {
		comments.add(comment);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NewsValueObject [news=" + news + ", author=" + author + ", tags=" + tags + ", comments=" + comments
				+ "]";
	}

}
