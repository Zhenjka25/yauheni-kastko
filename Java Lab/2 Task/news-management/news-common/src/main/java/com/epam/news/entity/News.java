package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * The Class News. Represents news business entity. It can have only one author
 * and any number of tags.
 */
public class News implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1050542954652592841L;

	/** The news id. Primary key for news table in database. */
	private Long newsId;

	/** The news title. */
	@NotNull
	@Size(min = 3, max = 30, message = "{title.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{title.error.pattern}")
	private String title;

	/** The short text of news. Contains a brief content of the news. */
	@NotNull
	@Size(min = 3, max = 100, message = "{shorttext.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{shorttext.error.pattern}")
	private String shortText;

	/** The full text of news. */
	@NotNull
	@Size(min = 3, max = 200, message = "{fulltext.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{fulltext.error.pattern}")
	private String fullText;

	/** The news creation date. Never changes after news creation. */
	@NotNull(message = "{date.error}")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date creationDate;

	/**
	 * The news modification date. After news creation it matches with creation
	 * date. Changes after editing of news.
	 */
	@NotNull(message = "{date.error}")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date modificationDate;

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the short text.
	 *
	 * @return the short text
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text.
	 *
	 * @param shortText
	 *            the new short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text.
	 *
	 * @return the full text
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text.
	 *
	 * @param fullText
	 *            the new full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date.
	 *
	 * @return the modification date
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date.
	 *
	 * @param modificationDate
	 *            the new modification date
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", title=" + title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate=" + modificationDate + "]";
	}

}
