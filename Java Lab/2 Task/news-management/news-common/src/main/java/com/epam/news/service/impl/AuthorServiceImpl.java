package com.epam.news.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

/**
 * The Class AuthorServiceImplementation. Interacts with Author JdbcDAO.
 */
@Service
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorDAO authorDAO;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author save(Author entity) throws ServiceException {
		try {
			entity.setAuthorId(authorDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author findById(long id) throws ServiceException {
		try {
			return authorDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Author entity) throws ServiceException {
		try {
			authorDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void expireAuthor(Author entity) throws ServiceException {
		try {
			entity.setExpired(new Date());
			authorDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			authorDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Author> findAll() throws ServiceException {
		try {
			return authorDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author findByName(String name) throws ServiceException {
		try {
			return authorDAO.getByName(name);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Author> findActualAuthors() throws ServiceException {
		try {
			return authorDAO.getActualAuthors();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsAuthorByAuthorId(long id) throws ServiceException {
		try {
			authorDAO.deleteNewsAuthorByAuthorId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Author findByNewsId(long id) throws ServiceException {
		try {
			return authorDAO.getByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
