package com.epam.news.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.UserInfoDAO;
import com.epam.news.entity.UserInfo;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.UserInfoService;

/**
 * The Class UserInfoServiceImplementation. Interacts with UserInfo JdbcDAO.
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	private UserInfoDAO userInfoDAO;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInfo getUserInfo(String username) throws ServiceException {
		try {
			return userInfoDAO.getUserInfo(username);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
