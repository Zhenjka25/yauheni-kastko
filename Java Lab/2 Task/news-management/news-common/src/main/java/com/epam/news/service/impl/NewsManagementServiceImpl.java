package com.epam.news.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.News;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsManagementService;
import com.epam.news.service.NewsService;
import com.epam.news.service.TagService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsManagementServiceImplementation. Interacts with News Management
 * JdbcDAO.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService {

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private CommentService commentService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public NewsValueObject findSingleNews(News news) throws ServiceException {
		NewsValueObject newsValueObject = new NewsValueObject();
		newsValueObject.setNews(news);
		newsValueObject.setAuthor(authorService.findByNewsId(news.getNewsId()));
		newsValueObject.setTags(tagService.findByNewsId(news.getNewsId()));
		newsValueObject.setComments(commentService.findByNewsId(news.getNewsId()));
		return newsValueObject;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public NewsValueObject findSingleNews(long id) throws ServiceException {
		NewsValueObject newsValueObject = new NewsValueObject();
		newsValueObject.setNews(newsService.findById(id));
		newsValueObject.setAuthor(authorService.findByNewsId(id));
		newsValueObject.setTags(tagService.findByNewsId(id));
		newsValueObject.setComments(commentService.findByNewsId(id));
		return newsValueObject;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long saveNews(NewsValueObject newsValueObject) throws ServiceException {
		newsValueObject.setNews(newsService.save(newsValueObject.getNews()));
		newsService.saveNewsAuthor(newsValueObject.getNews().getNewsId(), newsValueObject.getAuthor().getAuthorId());
		if (newsValueObject.getTags() != null) {
			newsService.saveNewsTagList(newsValueObject.getNews().getNewsId(), newsValueObject.getTags());
		}
		return newsValueObject.getNews().getNewsId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNews(List<Long> newsidList) throws ServiceException {
		newsService.deleteCommentsListByNewsId(newsidList);
		newsService.deleteNewsAuthorListByNewsId(newsidList);
		newsService.deleteNewsTagListByNewsId(newsidList);
		newsService.deleteList(newsidList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateNews(NewsValueObject newsValueObject) throws ServiceException {
		newsService.deleteNewsAuthorByNewsId(newsValueObject.getNews().getNewsId());
		newsService.deleteNewsTagByNewsId(newsValueObject.getNews().getNewsId());
		newsService.update(newsValueObject.getNews());
		newsService.saveNewsAuthor(newsValueObject.getNews().getNewsId(), newsValueObject.getAuthor().getAuthorId());
		if (newsValueObject.getTags() != null) {
			newsService.saveNewsTagList(newsValueObject.getNews().getNewsId(), newsValueObject.getTags());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<NewsValueObject> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		List<NewsValueObject> newsValueObjectList = new ArrayList<>();
		List<News> newsList = newsService.findBySearchCriteria(searchCriteria);
		for (News news : newsList) {
			newsValueObjectList.add(findSingleNews(news));
		}
		return newsValueObjectList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<News> findAllNews() throws ServiceException {
		return newsService.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public Long countNews() throws ServiceException {
		return newsService.countNews();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		return newsService.countNewsBySearchCriteria(searchCriteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		return newsService.findIdListBySearchCriteria(searchCriteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public News findPrevNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		return newsService.findPrevNews(newsId, searchCriteria);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional(readOnly = true)
	public News findNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		return newsService.findNextNews(newsId, searchCriteria);
	}

}
