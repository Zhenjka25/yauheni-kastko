package com.epam.news.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The Class Comment. Represents comment business entity. Comment always refers
 * to some news.
 */
public class Comment implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8233657719514270830L;

	/** The comment id. Primary key for comment table in database. */
	private Long commentId;

	/** The id of news to which comment refers. */
	private Long newsId;

	/** The comment text. */
	@NotNull
	@Size(min = 3, max = 100, message = "{commenttext.error.size}")
	@Pattern(regexp = "[A-Z0-9][\\w\\s,.!?&-_<>*'\"\\/()]+", message = "{commenttext.error.pattern}")
	private String commentText;

	/** The creation date. Never changes after comment creation. */
	private Date creationDate;

	/**
	 * Gets the comment id.
	 *
	 * @return the comment id
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Sets the comment id.
	 *
	 * @param commentId
	 *            the new comment id
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * Gets the news id.
	 *
	 * @return the news id
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the news id.
	 *
	 * @param newsId
	 *            the new news id
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Gets the comment text.
	 *
	 * @return the comment text
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text.
	 *
	 * @param commentText
	 *            the new comment text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date.
	 *
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commentId == null) ? 0 : commentId.hashCode());
		result = prime * result + ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentId == null) {
			if (other.commentId != null)
				return false;
		} else if (!commentId.equals(other.commentId))
			return false;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Comment [commentId=" + commentId + ", newsId=" + newsId + ", commentText=" + commentText
				+ ", creationDate=" + creationDate + "]";
	}

}
