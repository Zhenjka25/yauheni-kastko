package com.epam.news.service;

import com.epam.news.entity.UserInfo;
import com.epam.news.exception.ServiceException;

/**
 * The Interface UserInfoService. Service for interacting with UserInfo DAO
 * layer.
 */
public interface UserInfoService {

	/**
	 * Gets the user info.
	 *
	 * @param username
	 *            the username
	 * @return the user info
	 * @throws ServiceException
	 *             the service exception
	 */
	UserInfo getUserInfo(String username) throws ServiceException;

}
