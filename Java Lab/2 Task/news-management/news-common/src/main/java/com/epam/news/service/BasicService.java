package com.epam.news.service;

import java.util.List;

import com.epam.news.exception.ServiceException;

/**
 * The Interface BasicService. Basic service for interacting with DAO layer.
 *
 * @param <T>
 *            the generic type
 */
public interface BasicService<T> {

	/**
	 * Saves entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the t
	 * @throws ServiceException
	 *             the service exception
	 */
	T save(T entity) throws ServiceException;

	/**
	 * Finds entity by id.
	 *
	 * @param id
	 *            the id
	 * @return the t
	 * @throws ServiceException
	 *             the service exception
	 */
	T findById(long id) throws ServiceException;

	/**
	 * Updates entity.
	 *
	 * @param entity
	 *            the entity
	 * @throws ServiceException
	 *             the service exception
	 */
	void update(T entity) throws ServiceException;

	/**
	 * Deletes entity by id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteById(long id) throws ServiceException;

	/**
	 * Finds all entities.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<T> findAll() throws ServiceException;
}
