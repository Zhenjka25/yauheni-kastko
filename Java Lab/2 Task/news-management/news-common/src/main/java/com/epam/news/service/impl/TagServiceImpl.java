package com.epam.news.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

/**
 * The Class TagServiceImplementation. Interacts with Tag JdbcDAO.
 */
@Service
public class TagServiceImpl implements TagService {

	@Autowired
	private TagDAO tagDAO;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag save(Tag entity) throws ServiceException {
		try {
			entity.setTagId(tagDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag findById(long id) throws ServiceException {
		try {
			return tagDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(Tag entity) throws ServiceException {
		try {
			tagDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			tagDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tag> findAll() throws ServiceException {
		try {
			return tagDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Tag findByName(String name) throws ServiceException {
		try {
			return tagDAO.getByName(name);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsTagByTagId(Long id) throws ServiceException {
		try {
			tagDAO.deleteNewsTagByTagId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tag> findByNewsId(long id) throws ServiceException {
		try {
			return tagDAO.getByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Tag> findByTagIdList(List<Tag> tagList) throws ServiceException {
		try {
			return tagDAO.getByTagIdList(tagList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
