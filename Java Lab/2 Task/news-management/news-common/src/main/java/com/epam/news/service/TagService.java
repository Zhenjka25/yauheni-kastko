package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Tag;
import com.epam.news.exception.ServiceException;

/**
 * The Interface TagService. Service for interacting with Tag DAO layer.
 */
public interface TagService extends BasicService<Tag> {

	/**
	 * Finds tag by name.s
	 *
	 * @param name
	 *            the name
	 * @return the tag
	 * @throws ServiceException
	 *             the service exception
	 */
	Tag findByName(String name) throws ServiceException;

	/**
	 * Deletes data from news_tag table by tag id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsTagByTagId(Long id) throws ServiceException;

	/**
	 * Finds tags by news id.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Tag> findByNewsId(long id) throws ServiceException;

	/**
	 * Finds list of tags by tag id list. Tag list parameter contains tags with
	 * only id.
	 *
	 * @param tagList
	 *            the tag list
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Tag> findByTagIdList(List<Tag> tagList) throws ServiceException;
}
