package com.epam.news.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The Class ApplicationConfig. Declares @Bean methods that may be processed by
 * the Spring container for application.
 */
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:oracle_db.properties", "classpath:app_config.properties" })
@ComponentScan("com.epam.news.dao, com.epam.news.service")
public class ApplicationConfig {
	private static final String DRIVER_KEY = "db.driver";
	private static final String PASSWORD_KEY = "db.password";
	private static final String DATABASE_URL_KEY = "db.url";
	private static final String USER_NAME_KEY = "db.username";
	private static final String POOL_SIZE_KEY = "db.initialsize";

	@Autowired
	private Environment env;

	/**
	 * Data source. Returns data source with connection pool.
	 *
	 * @return the data source
	 */
	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(env.getProperty(DRIVER_KEY));
		dataSource.setUrl(env.getProperty(DATABASE_URL_KEY));
		dataSource.setUsername(env.getProperty(USER_NAME_KEY));
		dataSource.setPassword(env.getProperty(PASSWORD_KEY));
		dataSource.setInitialSize(Integer.parseInt(env.getProperty(POOL_SIZE_KEY)));
		return dataSource;
	}

	/**
	 * Transaction manager. Allows using of Spring transactions.
	 *
	 * @return the platform transaction manager
	 */
	@Bean
	public PlatformTransactionManager txManager() {
		return new DataSourceTransactionManager(dataSource());
	}

}
