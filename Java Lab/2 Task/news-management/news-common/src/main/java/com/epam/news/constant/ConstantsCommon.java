package com.epam.news.constant;

/**
 * The Class ColumnNames. Contains database column names.
 */
public final class ConstantsCommon {

	// AUTHOR
	public static final String AUTHOR_ID = "AUTHOR_ID";
	public static final String AUTHOR_NAME = "AUTHOR_NAME";
	public static final String EXPIRED = "EXPIRED";

	// COMMENTS
	public static final String COMMENT_ID = "COMMENT_ID";
	public static final String COMMENT_TEXT = "COMMENT_TEXT";
	public static final String COMMENT_CREATION_DATE = "CREATION_DATE";

	// NEWS
	public static final String NEWS_ID = "NEWS_ID";
	public static final String TITLE = "TITLE";
	public static final String SHORT_TEXT = "SHORT_TEXT";
	public static final String FULL_TEXT = "FULL_TEXT";
	public static final String NEWS_CREATION_DATE = "CREATION_DATE";
	public static final String MODIFICATION_DATE = "MODIFICATION_DATE";
	public static final String NEWS_ID_PREV = "NEWS_ID_PREV";
	public static final String NEWS_ID_NEXT = "NEWS_ID_NEXT";

	// TAG
	public static final String TAG_ID = "TAG_ID";
	public static final String TAG_NAME = "TAG_NAME";

	// DUAL
	public static final String NEXTVAL = "NEXTVAL";

	// USERS and ROLES
	public static final String USER_NAME = "USER_NAME";
	public static final String PASSWORD = "PASSWORD";
	public static final String ROLE_NAME = "ROLE_NAME";
	
	// News number per page
	public static final String NEWS_PER_PAGE_KEY = "news.per.page";

}
