package com.epam.news.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsServiceImplementation. Interacts with News JdbcDAO.
 */
@Service
public class NewsServiceImpl implements NewsService {

	@Autowired
	private NewsDAO newsDAO;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News save(News entity) throws ServiceException {
		try {
			entity.setNewsId(newsDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findById(long id) throws ServiceException {
		try {
			return newsDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			newsDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> findAll() throws ServiceException {
		try {
			return newsDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveNewsAuthor(long newsId, long authorId) throws ServiceException {
		try {
			newsDAO.saveNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveNewsTag(long newsId, long tagId) throws ServiceException {
		try {
			newsDAO.saveNewsTag(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> findByAuthorId(long id) throws ServiceException {
		try {
			return newsDAO.getByAuthorId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> findByTagId(long id) throws ServiceException {
		try {
			return newsDAO.getByTagId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long id) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthorByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsTagByNewsId(long id) throws ServiceException {
		try {
			newsDAO.deleteNewsTagByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCommentsByNewsId(long id) throws ServiceException {
		try {
			newsDAO.deleteCommentsByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<News> findBySearchCriteria(SearchCriteria searchObject) throws ServiceException {
		try {
			return newsDAO.findBySearchCriteria(searchObject);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long countNews() throws ServiceException {
		try {
			return newsDAO.countNews();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int countNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.countNewsBySearchCriteria(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Long> findIdListBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.findIdListBySearchCriteria(searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findPrevNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.findPrevNews(newsId, searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public News findNextNews(Long newsId, SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.findNextNews(newsId, searchCriteria);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteList(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.deleteList(newsIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsAuthorListByNewsId(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthorListByNewsId(newsIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteNewsTagListByNewsId(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.deleteNewsTagListByNewsId(newsIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteCommentsListByNewsId(List<Long> newsIdList) throws ServiceException {
		try {
			newsDAO.deleteCommentsListByNewsId(newsIdList);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveNewsTagList(long newsId, List<Tag> tags) throws ServiceException {
		try {
			newsDAO.saveNewsTagList(newsId, tags);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
