package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.util.SearchCriteria;

/**
 * The Class FilterAction. Filters list of news by author and tag list.
 */
@Component(value = ACTION_FILTER)
public class FilterAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	/**
	 * {@inheritDoc} Adds search criteria object to session by chosen author and
	 * tag list.
	 */
	public ResponsePage execute(HttpServletRequest request) throws ServiceException {
		request.getSession().setAttribute(ATTRIBUTE_SEARCH_CRITERIA, getSearchCriteria(request));
		return new ResponsePage(configurationManager.getProperty(PAGE_INDEX), false);
	}

	/**
	 * Gets the search criteria.
	 *
	 * @param request
	 *            the request
	 * @return the search criteria
	 */
	private SearchCriteria getSearchCriteria(HttpServletRequest request) {
		Long authorId = request.getParameter(PARAM_AUTHOR_ID) != null
				&& !request.getParameter(PARAM_AUTHOR_ID).isEmpty()
						? Long.parseLong(request.getParameter(PARAM_AUTHOR_ID)) : null;
		List<Long> tagList = new ArrayList<Long>();
		String[] tagArray = request.getParameterValues(PARAM_TAGS);
		if (tagArray != null) {
			for (String tag : tagArray) {
				tagList.add(Long.parseLong(tag));
			}
		}
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(authorId);
		searchCriteria.setTagsId(tagList);
		return searchCriteria;
	}

}
