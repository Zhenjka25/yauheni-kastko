package com.epam.news.manager;

import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Class ConfigurationManager.
 */
@Component
public class ConfigurationManager {

	@Autowired
	private ResourceBundle resourceBundle;

	/**
	 * Gets the property.
	 *
	 * @param key
	 *            the key
	 * @return the property
	 */
	public String getProperty(String key) {
		return resourceBundle.getString(key);
	}

}
