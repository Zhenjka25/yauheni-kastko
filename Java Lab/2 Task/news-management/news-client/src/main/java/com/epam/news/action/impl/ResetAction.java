package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.ACTION_RESET;
import static com.epam.news.constant.ConstantsClient.ATTRIBUTE_SEARCH_CRITERIA;
import static com.epam.news.constant.ConstantsClient.PAGE_INDEX;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;

/**
 * The Class ResetAction. Resets the filter settings.
 */
@Component(value = ACTION_RESET)
public class ResetAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	/**
	 * {@inheritDoc} Removes search criteria object from session.
	 */
	public ResponsePage execute(HttpServletRequest request) throws ServiceException {
		request.getSession().removeAttribute(ATTRIBUTE_SEARCH_CRITERIA);
		return new ResponsePage(configurationManager.getProperty(PAGE_INDEX), false);
	}

}
