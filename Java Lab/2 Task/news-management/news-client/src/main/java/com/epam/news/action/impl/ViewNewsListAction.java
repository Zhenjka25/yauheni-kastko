package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;
import static com.epam.news.constant.ConstantsCommon.NEWS_PER_PAGE_KEY;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.AuthorService;
import com.epam.news.service.NewsManagementService;
import com.epam.news.service.TagService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class ViewNewsListAction. Jumps to the news list page.
 */
@Component(value = ACTION_VIEW_NEWS_LIST)
public class ViewNewsListAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	@Autowired
	private NewsManagementService newsManagementService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private Environment env;

	/**
	 * {@inheritDoc} Displays news list filtered by search criteria.
	 */
	public ResponsePage execute(HttpServletRequest request) throws ServiceException {
		SearchCriteria searchCriteria = request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) != null
				? (SearchCriteria) request.getSession().getAttribute(ATTRIBUTE_SEARCH_CRITERIA) : new SearchCriteria();
		if (request.getParameter(PARAM_PAGE) != null) {
			searchCriteria.setPage(Integer.parseInt(request.getParameter(PARAM_PAGE)));
		}
		List<NewsValueObject> newsList = newsManagementService.findBySearchCriteria(searchCriteria);
		request.setAttribute(ATTRIBUTE_NEWS_LIST, newsList);
		int newsPerPage = Integer.parseInt(env.getProperty(NEWS_PER_PAGE_KEY));
		long pagesCount = newsManagementService.countNewsBySearchCriteria(searchCriteria) / newsPerPage;
		if ((newsManagementService.countNewsBySearchCriteria(searchCriteria) % newsPerPage) != 0) {
			pagesCount++;
		}
		request.setAttribute(ATTRIBUTE_PAGES_COUNT, pagesCount);
		request.setAttribute(ATTRIBUTE_CURRENT_PAGE, searchCriteria.getPage());
		request.setAttribute(ATTRIBUTE_AUTHOR_LIST, authorService.findAll());
		request.setAttribute(ATTRIBUTE_TAG_LIST, tagService.findAll());
		request.getSession().setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
		request.getSession().setAttribute(ATTRIBUTE_ACTUAL_PAGE, PAGE_VIEW_NEWS_LIST_ACTION);
		return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS_LIST), false);
	}

}
