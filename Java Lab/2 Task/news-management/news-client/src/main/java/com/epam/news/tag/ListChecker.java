package com.epam.news.tag;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.entity.Tag;

/**
 * The Class ListChecker. Checks equivalence of authors or tags by its id.
 */
public class ListChecker {

	/**
	 * Checks if author has the same id.
	 *
	 * @param author
	 *            the author
	 * @param authorId
	 *            the author id
	 * @return true, if successful
	 */
	public static boolean containsAuthor(Author author, Long authorId) {
		return author.getAuthorId().equals(authorId);
	}

	/**
	 * Checks if tag id contained in id list.
	 *
	 * @param tag
	 *            the tag
	 * @param tagIdList
	 *            the tag id list
	 * @return true, if successful
	 */
	public static boolean containsTag(Tag tag, List<Long> tagIdList) {
		for (Long tagId : tagIdList) {
			if (tag.getTagId().equals(tagId)) {
				return true;
			}
		}
		return false;
	}

}
