package com.epam.news.config;

import java.util.ResourceBundle;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * The Class ClientApplicationConfig. Spring configuration for client
 * application.
 */
@Configuration
@ComponentScan("com.epam.news.action, com.epam.news.manager")
@Import(ApplicationConfig.class)
public class ClientApplicationConfig {

	private static final String PAGES_PROPERTY_FILE = "config";

	/**
	 * Resource bundle.
	 *
	 * @return the resource bundle
	 */
	@Bean
	public ResourceBundle resourceBundle() {
		return ResourceBundle.getBundle(PAGES_PROPERTY_FILE);
	}
}
