package com.epam.news.action.impl;

import static com.epam.news.constant.ConstantsClient.*;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.news.action.Action;
import com.epam.news.entity.Comment;
import com.epam.news.entity.NewsValueObject;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;
import com.epam.news.service.CommentService;
import com.epam.news.service.NewsManagementService;

/**
 * The Class AddCommentAction. Add comment to the news.
 */
@Component(value = ACTION_ADD_COMMENT)
public class AddCommentAction implements Action {

	@Autowired
	private ConfigurationManager configurationManager;

	@Autowired
	private CommentService commentService;

	@Autowired
	private NewsManagementService newsManagementService;

	/**
	 * {@inheritDoc} Adds comment to news.
	 */
	public ResponsePage execute(HttpServletRequest request) throws ServiceException {
		Comment comment = new Comment();
		comment.setCommentText(request.getParameter(PARAM_COMMENT_TEXT));
		comment.setCreationDate(new Date());
		Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
		comment.setNewsId(newsId);
		commentService.save(comment);
		NewsValueObject news = newsManagementService.findSingleNews(newsId);
		request.setAttribute(ATTRIBUTE_NEWS, news);
		return new ResponsePage(configurationManager.getProperty(PAGE_VIEW_NEWS_ACTION), true);
	}

}
