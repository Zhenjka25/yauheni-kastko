package com.epam.news.page;

/**
 * The Class ResponsePage. Contains page String and boolean value, which means,
 * is redirect needed after action processing.
 */
public class ResponsePage {

	/** The result page after action processing. */
	private String page;

	/** The boolean value, which means, is redirect needed. */
	private boolean isRedirect;

	/**
	 * Instantiates a new response page.
	 *
	 * @param page
	 *            the page
	 * @param isRedirect
	 *            the is redirect
	 */
	public ResponsePage(String page, boolean isRedirect) {
		this.page = page;
		this.isRedirect = isRedirect;
	}

	/**
	 * Checks if redirect needed.
	 *
	 * @return true, if is redirect
	 */
	public boolean isRedirect() {
		return isRedirect;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return page;
	}

}
