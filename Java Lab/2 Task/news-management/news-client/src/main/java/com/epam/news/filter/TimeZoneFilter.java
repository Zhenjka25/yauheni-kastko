package com.epam.news.filter;

import static com.epam.news.constant.ConstantsClient.ATTRIBUTE_TIME_ZONE_OFFSET;
import static com.epam.news.constant.ConstantsClient.COOKIE_OFFSET;

import java.io.IOException;
import java.util.TimeZone;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.lang3.time.DateUtils;

/**
 * The Class TimeZoneFilter. Gets the client time zone from the cookies and adds
 * the time zone offset to the user session. Therefore user can see date in its
 * time zone, rather than servers's time zone.
 */
@WebFilter(urlPatterns = { "/*" })
public class TimeZoneFilter implements Filter {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		if (req.getSession().getAttribute(ATTRIBUTE_TIME_ZONE_OFFSET) == null && req.getCookies() != null) {
			Cookie cookie = findTimeZoneOffsetCookie(req);
			if (cookie != null) {
				int timezoneOffsetMinutes = Integer.parseInt(cookie.getValue());
				TimeZone timeZone = TimeZone.getTimeZone("UTC");
				timeZone.setRawOffset((int) (timezoneOffsetMinutes * DateUtils.MILLIS_PER_MINUTE));
				Config.set(req.getSession(), Config.FMT_TIME_ZONE, timeZone);
				req.getSession().setAttribute(ATTRIBUTE_TIME_ZONE_OFFSET, timezoneOffsetMinutes);
				cookie.setMaxAge(0);
				res.addCookie(cookie);
			}
		}
		chain.doFilter(request, response);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
	}

	/**
	 * Finds the time zone offset cookie from request.
	 *
	 * @param request
	 *            the request
	 * @return the cookie
	 */
	private Cookie findTimeZoneOffsetCookie(HttpServletRequest request) {
		for (Cookie cookie : request.getCookies()) {
			if (COOKIE_OFFSET.equals(cookie.getName())) {
				return cookie;
			}
		}
		return null;
	}

}
