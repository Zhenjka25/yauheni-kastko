package com.epam.news.controller;

import static com.epam.news.constant.ConstantsClient.ATTRIBUTE_ERROR;
import static com.epam.news.constant.ConstantsClient.PAGE_ERROR;
import static com.epam.news.constant.ConstantsClient.PARAM_ACTION;

import java.io.IOException;
import java.util.TimeZone;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.epam.news.action.Action;
import com.epam.news.config.ClientApplicationConfig;
import com.epam.news.exception.ServiceException;
import com.epam.news.manager.ConfigurationManager;
import com.epam.news.page.ResponsePage;

/**
 * The Class Controller. Receives requests, causes the needed action and
 * forwards to the result page.
 */
@WebServlet("/Controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(Controller.class);
	
	private ApplicationContext context;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init() throws ServletException {
		super.init();
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		context = new AnnotationConfigApplicationContext(ClientApplicationConfig.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Process the request.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ResponsePage page = null;
		Action action = (Action) context.getBean(request.getParameter(PARAM_ACTION));
		ConfigurationManager configurationManager = (ConfigurationManager) context.getBean(ConfigurationManager.class);
		try {
			page = action.execute(request);
		} catch (ServiceException e) {
			log.error(e);
			request.setAttribute(ATTRIBUTE_ERROR, e.getMessage());
			page = new ResponsePage(configurationManager.getProperty(PAGE_ERROR), false);
		}
		if (page.isRedirect()) {
			response.sendRedirect(page.toString());
		} else {
			RequestDispatcher rd = request.getRequestDispatcher(page.toString());
			rd.forward(request, response);
		}
	}

}
