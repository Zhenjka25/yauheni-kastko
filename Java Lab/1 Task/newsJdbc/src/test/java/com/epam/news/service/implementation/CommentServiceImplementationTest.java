package com.epam.news.service.implementation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

/**
 * The Class CommentServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplementationTest {

	@Mock
	private CommentDAO commentDAOmock;

	@InjectMocks
	private CommentServiceImplementation service;

	/**
	 * Setups mock before running tests.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Before
	public void doSetup() throws DAOException {

		when(commentDAOmock.save(any(Comment.class))).thenReturn(1L);
		when(commentDAOmock.getById(any(Long.class))).thenAnswer(new Answer<Comment>() {
			@Override
			public Comment answer(InvocationOnMock invocation) throws Throwable {
				Comment comment = new Comment();
				comment.setCommentId(1L);
				comment.setNewsId((Long) invocation.getArguments()[0]);
				comment.setCommentText("some text");
				comment.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
				return comment;
			}
		});
		when(commentDAOmock.getAll()).thenAnswer(new Answer<List<Comment>>() {
			@Override
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> comments = new ArrayList<>();
				comments.add(new Comment());
				comments.add(new Comment());
				return comments;
			}
		});
		when(commentDAOmock.getByNewsId(any(Long.class))).thenAnswer(new Answer<List<Comment>>() {
			@Override
			public List<Comment> answer(InvocationOnMock invocation) throws Throwable {
				List<Comment> comments = new ArrayList<>();
				comments.add(new Comment());
				return comments;
			}
		});
	}

	@Test
	public void saveComment() throws ServiceException {
		assertTrue(service.save(new Comment()).getCommentId() == 1);
	}

	@Test
	public void getById() throws DAOException, ServiceException {
		Comment comment = new Comment();
		comment.setCommentId(1L);
		comment.setNewsId(1L);
		comment.setCommentText("some text");
		comment.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		assertEquals(comment, service.findById(1));
	}

	@Test
	public void updateComment() throws ServiceException, DAOException {
		Comment comment = new Comment();
		service.update(comment);
		verify(commentDAOmock).update(comment);
	}

	@Test
	public void deleteComment() throws ServiceException, DAOException {
		long id = 1;
		service.deleteById(id);
		verify(commentDAOmock).deleteById(id);
	}

	@Test
	public void findAllComments() throws DAOException, ServiceException {
		List<Comment> comments = service.findAll();
		assertThat(comments.size(), is(2));
	}

	@Test
	public void findByNewsId() throws DAOException, ServiceException {
		List<Comment> comments = service.findByNewsId(1);
		assertThat(comments.size(), is(1));
	}
}
