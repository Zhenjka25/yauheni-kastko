package com.epam.news.dao.implementation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.news.config.TestConfig;
import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * The Class AuthorDAOImplementationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class AuthorDAOImplementationTest extends AbstractTest {

	@Autowired()
	private AuthorDAO authorDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#getDataSetForInsert()
	 */
	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_authorData.xml")) };
		return new CompositeDataSet(datasets);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#configureDbTestcase()
	 */
	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#cleanDataBase()
	 */
	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteAuthor() throws DAOException {
		Author author = new Author();
		author.setAuthorName("eugen");
		author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		long id = authorDAO.save(author);
		assertNotEquals(0, id);
		assertNotNull(authorDAO.getById(id));
		authorDAO.deleteById(id);
		assertNull(authorDAO.getById(id));
	}

	@Test(expected = DAOException.class)
	public void testWrongAuthorName() throws DAOException {
		Author author = new Author();
		author.setAuthorName("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		authorDAO.save(author);
	}

	@Test
	public void getById() throws DAOException {
		Author author = new Author();
		author.setAuthorId(2L);
		author.setAuthorName("Daniil Ishutin");
		author.setExpired(Timestamp.valueOf("2015-08-09 12:46:29.77"));
		assertEquals(author, authorDAO.getById(2));
	}

	@Test
	public void updateAuthor() throws DAOException {
		Author author = authorDAO.getById(1);
		author.setAuthorName("Wasya");
		authorDAO.update(author);
		assertEquals("Wasya", authorDAO.getById(1).getAuthorName());
	}

	@Test
	public void expireAuthor() throws DAOException {
		Author author = new Author();
		author.setAuthorName("some name");
		author.setAuthorId(authorDAO.save(author));
		assertNull(author.getExpired());
		authorDAO.expireAuthor(author);
		assertNotNull(author.getExpired());
	}

	@Test
	public void findAllAuthors() throws DAOException {
		List<Author> authors = authorDAO.getAll();
		assertThat(authors.size(), is(2));
	}

	@Test
	public void findByName() throws DAOException {
		Author author = authorDAO.getByName("Artur Babaev");
		assertEquals(new Long(1), author.getAuthorId());
	}

	@Test
	public void findActualAuthors() throws DAOException {
		List<Author> authors = authorDAO.getActualAuthors();
		assertTrue(authors.isEmpty());
	}

	@Test
	public void findByNewsId() throws DAOException {
		Author author = authorDAO.getByNewsId(1);
		assertNotNull(author);
	}

}
