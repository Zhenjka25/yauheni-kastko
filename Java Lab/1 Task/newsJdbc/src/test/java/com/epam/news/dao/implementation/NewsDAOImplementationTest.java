package com.epam.news.dao.implementation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.news.config.TestConfig;
import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

/**
 * The Class NewsDAOImplementationTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestConfig.class })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class })
public class NewsDAOImplementationTest extends AbstractTest {

	@Autowired()
	private NewsDAO newsDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#getDataSetForInsert()
	 */
	@Override
	public IDataSet getDataSetForInsert() throws DataSetException, FileNotFoundException {
		IDataSet[] datasets = new IDataSet[] {
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/newsData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/commentData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tagData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_authorData.xml")),
				new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tagData.xml")) };
		return new CompositeDataSet(datasets);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#configureDbTestcase()
	 */
	@Override
	@Before
	public void configureDbTestcase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSetForInsert());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.implementation.AbstractTest#cleanDataBase()
	 */
	@Override
	@After
	public void cleanDataBase() throws DatabaseUnitException, SQLException, Exception {
		DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSetForDelete());
	}

	@Test
	public void saveAndDeleteNews() throws DAOException {
		News news = new News();
		news.setTitle("title");
		news.setShortText("short");
		news.setFullText("full");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		long id = newsDAO.save(news);
		assertNotEquals(0, id);
		assertNotNull(newsDAO.getById(id));
		newsDAO.deleteById(id);
		assertNull(newsDAO.getById(id));
	}

	@Test(expected = DAOException.class)
	public void testWrongTitle() throws DAOException {
		News news = new News();
		news.setTitle("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		news.setShortText("short");
		news.setFullText("full");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		newsDAO.save(news);
	}

	@Test
	public void getById() throws DAOException {
		News news = new News();
		news.setNewsId(1L);
		news.setTitle("Some title");
		news.setShortText("Some short text");
		news.setFullText("Some full text");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		assertEquals(news, newsDAO.getById(1));
	}

	@Test
	public void updateNews() throws DAOException {
		News news = newsDAO.getById(2);
		news.setFullText("Another full text");
		newsDAO.update(news);
		assertEquals("Another full text", newsDAO.getById(2).getFullText());
	}

	@Test
	public void findAllNews() throws DAOException {
		List<News> news = newsDAO.getAll();
		assertThat(news.size(), is(2));
	}

	@Test
	public void findByAuthorId() throws DAOException {
		List<News> news = newsDAO.getByAuthorId(1);
		assertThat(news.size(), is(2));
	}

	@Test
	public void findByTagId() throws DAOException {
		List<News> news = newsDAO.getByTagId(1);
		assertThat(news.size(), is(2));
	}

	@Test
	public void searchNews() throws DAOException {
		SearchCriteria searchCriteria = new SearchCriteria();
		searchCriteria.setAuthorId(1L);
		searchCriteria.addTagId(1L);
		searchCriteria.addTagId(2L);
		List<News> news = newsDAO.findBySearchCriteria(searchCriteria);
		assertThat(news.size(), is(2));
	}

}
