package com.epam.news.service.implementation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

/**
 * The Class AuthorServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceImplementationTest {

	@Mock
	private AuthorDAO authorDAOmock;

	@InjectMocks
	private AuthorServiceImplementation service;

	/**
	 * Setups mock before running tests.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Before
	public void doSetup() throws DAOException {
		when(authorDAOmock.save(any(Author.class))).thenReturn(1L);
		when(authorDAOmock.getById(any(Long.class))).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = new Author();
				author.setAuthorId((Long) invocation.getArguments()[0]);
				author.setAuthorName("Eugen");
				author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
				return author;
			}
		});
		when(authorDAOmock.getAll()).thenAnswer(new Answer<List<Author>>() {
			@Override
			public List<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<>();
				authors.add(new Author());
				authors.add(new Author());
				return authors;
			}
		});
		when(authorDAOmock.getByName(any(String.class))).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				Author author = new Author();
				author.setAuthorId(2L);
				author.setAuthorName((String) invocation.getArguments()[0]);
				author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
				return author;
			}
		});
		when(authorDAOmock.getActualAuthors()).thenAnswer(new Answer<List<Author>>() {
			@Override
			public List<Author> answer(InvocationOnMock invocation) throws Throwable {
				List<Author> authors = new ArrayList<>();
				authors.add(new Author());
				return authors;
			}
		});
		when(authorDAOmock.getByNewsId(any(Long.class))).thenAnswer(new Answer<Author>() {
			@Override
			public Author answer(InvocationOnMock invocation) throws Throwable {
				return new Author();
			}
		});
	}

	@Test
	public void saveAuthor() throws ServiceException {
		assertTrue(service.save(new Author()).getAuthorId() == 1);
	}

	@Test
	public void getById() throws DAOException, ServiceException {
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("Eugen");
		author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		assertEquals(author, service.findById(1));
	}

	@Test
	public void updateAuthor() throws DAOException, ServiceException {
		Author author = new Author();
		service.update(author);
		verify(authorDAOmock).update(author);
	}

	@Test
	public void expireAuthor() throws ServiceException, DAOException {
		Author author = new Author();
		author.setAuthorId(1L);
		author.setAuthorName("Eugen");
		author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		service.expireAuthor(author);
		verify(authorDAOmock).expireAuthor(author);
	}

	@Test
	public void deleteAuthor() throws ServiceException, DAOException {
		long id = 1;
		service.deleteById(id);
		verify(authorDAOmock).deleteById(id);
	}

	@Test
	public void findAllAuthors() throws DAOException, ServiceException {
		List<Author> authors = service.findAll();
		assertThat(authors.size(), is(2));
	}

	@Test
	public void findByName() throws DAOException, ServiceException {
		Author author = new Author();
		author.setAuthorId(2L);
		author.setAuthorName("Wasya");
		author.setExpired(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		assertEquals(author, service.findByName("Wasya"));
	}

	@Test
	public void findActualAuthors() throws DAOException, ServiceException {
		List<Author> authors = service.findActualAuthors();
		assertThat(authors.size(), is(1));
	}

	@Test
	public void deleteNewsAuthor() throws ServiceException, DAOException {
		long id = 1;
		service.deleteNewsAuthorByAuthorId(id);
		verify(authorDAOmock).deleteNewsAuthorByAuthorId(id);
	}

	@Test
	public void findByNewsId() throws DAOException, ServiceException {
		assertNotNull(service.findByNewsId(1));
	}

}
