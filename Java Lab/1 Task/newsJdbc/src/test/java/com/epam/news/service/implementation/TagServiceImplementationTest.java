package com.epam.news.service.implementation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;

/**
 * The Class TagServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplementationTest {

	@Mock
	private TagDAO tagDAOmock;

	@InjectMocks
	private TagServiceImplementation service;

	/**
	 * Setup mock before running tests.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Before
	public void doSetup() throws DAOException {
		when(tagDAOmock.save(any(Tag.class))).thenReturn(1L);
		when(tagDAOmock.getById(any(Long.class))).thenAnswer(new Answer<Tag>() {
			@Override
			public Tag answer(InvocationOnMock invocation) throws Throwable {
				Tag tag = new Tag();
				tag.setTagId((Long) invocation.getArguments()[0]);
				tag.setTagName("name");
				return tag;
			}
		});
		when(tagDAOmock.getAll()).thenAnswer(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<>();
				tags.add(new Tag());
				tags.add(new Tag());
				return tags;
			}
		});
		when(tagDAOmock.getByName(any(String.class))).thenAnswer(new Answer<Tag>() {
			@Override
			public Tag answer(InvocationOnMock invocation) throws Throwable {
				Tag tag = new Tag();
				tag.setTagId(1L);
				tag.setTagName((String) invocation.getArguments()[0]);
				return tag;
			}
		});
		when(tagDAOmock.getByNewsId(any(Long.class))).thenAnswer(new Answer<List<Tag>>() {
			@Override
			public List<Tag> answer(InvocationOnMock invocation) throws Throwable {
				List<Tag> tags = new ArrayList<>();
				tags.add(new Tag());
				return tags;
			}
		});
	}

	@Test
	public void saveTag() throws ServiceException {
		assertTrue(service.save(new Tag()).getTagId() == 1);
	}

	@Test
	public void getById() throws DAOException, ServiceException {
		Tag tag = new Tag();
		tag.setTagId(2L);
		tag.setTagName("name");
		assertEquals(tag, service.findById(2));
	}

	@Test
	public void updateTag() throws ServiceException, DAOException {
		Tag tag = new Tag();
		service.update(tag);
		verify(tagDAOmock).update(tag);
	}

	@Test
	public void deleteTag() throws ServiceException, DAOException {
		long id = 1;
		service.deleteById(id);
		verify(tagDAOmock).deleteById(id);
	}

	@Test
	public void deleteNewsTag() throws ServiceException, DAOException {
		long id = 1;
		service.deleteNewsTagByTagId(id);
		verify(tagDAOmock).deleteNewsTagByTagId(id);
	}

	@Test
	public void findAllTags() throws DAOException, ServiceException {
		List<Tag> tags = service.findAll();
		assertThat(tags.size(), is(2));
	}

	@Test
	public void findByName() throws DAOException, ServiceException {
		Tag tag = new Tag();
		tag.setTagId(1L);
		tag.setTagName("name");
		assertEquals(tag, service.findByName("name"));
	}

	@Test
	public void findByNewsId() throws DAOException, ServiceException {
		List<Tag> tags = service.findByNewsId(1);
		assertThat(tags.size(), is(1));
	}
}
