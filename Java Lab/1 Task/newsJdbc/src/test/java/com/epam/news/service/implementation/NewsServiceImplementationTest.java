package com.epam.news.service.implementation;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsServiceImplementationTest.
 */
@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplementationTest {

	@Mock
	private NewsDAO newsDAOmock;

	@InjectMocks
	private NewsServiceImplementation service;

	/**
	 * Setups mock before running tests.
	 *
	 * @throws DAOException
	 *             the DAO exception
	 */
	@Before
	public void doSetup() throws DAOException {
		when(newsDAOmock.save(any(News.class))).thenReturn(1L);
		when(newsDAOmock.getById(any(Long.class))).thenAnswer(new Answer<News>() {
			@Override
			public News answer(InvocationOnMock invocation) throws Throwable {
				News news = new News();
				news.setNewsId((Long) invocation.getArguments()[0]);
				news.setTitle("Some title");
				news.setShortText("Short text");
				news.setFullText("Full text");
				news.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
				news.setModificationDate(Date.valueOf("2015-08-11"));
				return news;
			}
		});
		when(newsDAOmock.getAll()).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<>();
				news.add(new News());
				news.add(new News());
				return news;
			}
		});
		when(newsDAOmock.getByAuthorId(any(Long.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<>();
				news.add(new News());
				return news;
			}
		});
		when(newsDAOmock.getByTagId(any(Long.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<>();
				news.add(new News());
				return news;
			}
		});
		when(newsDAOmock.findBySearchCriteria(any(SearchCriteria.class))).thenAnswer(new Answer<List<News>>() {
			@Override
			public List<News> answer(InvocationOnMock invocation) throws Throwable {
				List<News> news = new ArrayList<>();
				news.add(new News());
				return news;
			}
		});
	}

	@Test
	public void saveNews() throws ServiceException {
		assertTrue(service.save(new News()).getNewsId() == 1);
	}

	@Test
	public void getById() throws DAOException, ServiceException {
		News news = new News();
		news.setNewsId(1L);
		news.setTitle("Some title");
		news.setShortText("Short text");
		news.setFullText("Full text");
		news.setCreationDate(Timestamp.valueOf("2015-08-16 10:53:56.589"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		assertEquals(news, service.findById(1));
	}

	@Test
	public void updateNews() throws ServiceException, DAOException {
		News news = new News();
		service.update(news);
		verify(newsDAOmock).update(news);
	}

	@Test
	public void deleteNews() throws ServiceException, DAOException {
		long id = 1;
		service.deleteById(id);
		verify(newsDAOmock).deleteById(id);
	}

	@Test
	public void saveNewsAuthor() throws ServiceException, DAOException {
		long newsId = 1;
		long authorId = 1;
		service.saveNewsAuthor(newsId, authorId);
		verify(newsDAOmock).saveNewsAuthor(newsId, authorId);
	}

	@Test
	public void saveNewsTag() throws ServiceException, DAOException {
		long newsId = 1;
		long tagId = 1;
		service.saveNewsTag(newsId, tagId);
		verify(newsDAOmock).saveNewsTag(newsId, tagId);
	}

	@Test
	public void deleteNewsAuthor() throws ServiceException, DAOException {
		long id = 1;
		service.deleteNewsAuthorByNewsId(id);
		verify(newsDAOmock).deleteNewsAuthorByNewsId(id);
	}

	@Test
	public void deleteNewsTag() throws ServiceException, DAOException {
		long id = 1;
		service.deleteNewsTagByNewsId(id);
		verify(newsDAOmock).deleteNewsTagByNewsId(id);
	}

	@Test
	public void deleteComments() throws ServiceException, DAOException {
		long id = 1;
		service.deleteCommentsByNewsId(id);
		verify(newsDAOmock).deleteCommentsByNewsId(id);
	}

	@Test
	public void findAllNews() throws DAOException, ServiceException {
		List<News> news = service.findAll();
		assertThat(news.size(), is(2));
	}

	@Test
	public void findByAuthorId() throws DAOException, ServiceException {
		List<News> news = service.findByAuthorId(1);
		assertThat(news.size(), is(1));
	}

	@Test
	public void findByTagId() throws DAOException, ServiceException {
		List<News> news = service.findByTagId(1);
		assertThat(news.size(), is(1));
	}

	@Test
	public void findBySearchCriteria() throws ServiceException {
		SearchCriteria searchCriteria = new SearchCriteria();
		List<News> news = service.findBySearchCriteria(searchCriteria);
		assertThat(news.size(), is(1));
	}
}
