package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.exception.ServiceException;
import com.epam.news.util.SearchCriteria;

/**
 * The Interface NewsService. Service for work with NewsDAO.
 */
public interface NewsService extends BasicService<News> {

	/**
	 * Saves data to the news_author table.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws ServiceException
	 *             the service exception
	 */
	void saveNewsAuthor(long newsId, long authorId) throws ServiceException;

	/**
	 * Saves data to the news_tag table.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws ServiceException
	 *             the service exception
	 */
	void saveNewsTag(long newsId, long tagId) throws ServiceException;

	/**
	 * Finds news by author id.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findByAuthorId(long id) throws ServiceException;

	/**
	 * Finds news by tag id.
	 *
	 * @param id
	 *            the id
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findByTagId(long id) throws ServiceException;

	/**
	 * Deletes data from news_author table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsAuthorByNewsId(long id) throws ServiceException;

	/**
	 * Deletes data from news_tag table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsTagByNewsId(long id) throws ServiceException;

	/**
	 * Deletes comments by news id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteCommentsByNewsId(long id) throws ServiceException;

	/**
	 * Finds news by search criteria.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findBySearchCriteria(SearchCriteria searchObject) throws ServiceException;
}
