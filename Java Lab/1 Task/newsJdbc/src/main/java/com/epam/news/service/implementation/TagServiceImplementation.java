package com.epam.news.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.TagService;

/**
 * The Class TagServiceImplementation.
 */
@Service
public class TagServiceImplementation implements TagService {

	@Autowired
	private TagDAO tagDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#save(java.lang.Object)
	 */
	@Override
	public Tag save(Tag entity) throws ServiceException {
		try {
			entity.setTagId(tagDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#findById(long)
	 */
	@Override
	public Tag findById(long id) throws ServiceException {
		try {
			return tagDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#update(java.lang.Object)
	 */
	@Override
	public void update(Tag entity) throws ServiceException {
		try {
			tagDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			tagDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#findAll()
	 */
	@Override
	public List<Tag> findAll() throws ServiceException {
		try {
			return tagDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.TagService#findByName(java.lang.String)
	 */
	@Override
	public Tag findByName(String name) throws ServiceException {
		try {
			return tagDAO.getByName(name);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.news.service.TagService#deleteNewsTagByTagId(java.lang.Long)
	 */
	@Override
	public void deleteNewsTagByTagId(Long id) throws ServiceException {
		try {
			tagDAO.deleteNewsTagByTagId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.TagService#findByNewsId(long)
	 */
	@Override
	public List<Tag> findByNewsId(long id) throws ServiceException {
		try {
			return tagDAO.getByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
