package com.epam.news.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.AuthorService;

/**
 * The Class AuthorServiceImplementation.
 */
@Service
public class AuthorServiceImplementation implements AuthorService {

	@Autowired
	private AuthorDAO authorDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#save(java.lang.Object)
	 */
	@Override
	public Author save(Author entity) throws ServiceException {
		try {
			entity.setAuthorId(authorDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#findById(long)
	 */
	@Override
	public Author findById(long id) throws ServiceException {
		try {
			return authorDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#update(java.lang.Object)
	 */
	@Override
	public void update(Author entity) throws ServiceException {
		try {
			authorDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.news.service.AuthorService#expireAuthor(com.epam.news.entity.
	 * Author)
	 */
	@Override
	public void expireAuthor(Author entity) throws ServiceException {
		try {
			authorDAO.expireAuthor(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			authorDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#findAll()
	 */
	@Override
	public List<Author> findAll() throws ServiceException {
		try {
			return authorDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.AuthorService#findByName(java.lang.String)
	 */
	@Override
	public Author findByName(String name) throws ServiceException {
		try {
			return authorDAO.getByName(name);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.AuthorService#findActualAuthors()
	 */
	@Override
	public List<Author> findActualAuthors() throws ServiceException {
		try {
			return authorDAO.getActualAuthors();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.AuthorService#deleteNewsAuthorByAuthorId(long)
	 */
	@Override
	public void deleteNewsAuthorByAuthorId(long id) throws ServiceException {
		try {
			authorDAO.deleteNewsAuthorByAuthorId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.AuthorService#findByNewsId(long)
	 */
	@Override
	public Author findByNewsId(long id) throws ServiceException {
		try {
			return authorDAO.getByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
