package com.epam.news.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.*;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.*;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsManagementServiceImplementation.
 */
@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImplementation implements NewsManagementService {

	private static Logger log = Logger.getLogger(NewsManagementServiceImplementation.class);

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private TagService tagService;

	@Autowired
	private CommentService commentService;

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#findSingleNews(long)
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public NewsValueObject findSingleNews(long id) throws ServiceException {
		NewsValueObject newsValueObject = new NewsValueObject();
		try {
			newsValueObject.setNews(newsService.findById(id));
			newsValueObject.setAuthor(authorService.findByNewsId(id));
			newsValueObject.setTags(tagService.findByNewsId(id));
			newsValueObject.setComments(commentService.findByNewsId(id));
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
		return newsValueObject;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#saveNews(com.epam.news.entity.News, java.lang.Long, java.util.List)
	 */
	@Override
	public Long saveNews(News news, Long authorId, List<Long> tags) throws ServiceException {
		try {
			news = newsService.save(news);
			newsService.saveNewsAuthor(news.getNewsId(), authorId);
			for (Long tag : tags) {
				newsService.saveNewsTag(news.getNewsId(), tag);
			}
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
		return news.getNewsId();
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#deleteNews(java.util.List)
	 */
	@Override
	public void deleteNews(List<Long> newsidList) throws ServiceException {
		try {
			for (Long newsId : newsidList) {
				newsService.deleteCommentsByNewsId(newsId);
				newsService.deleteNewsAuthorByNewsId(newsId);
				newsService.deleteNewsTagByNewsId(newsId);
				newsService.deleteById(newsId);
			}
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#updateNews(com.epam.news.entity.News, java.lang.Long, java.util.List)
	 */
	@Override
	public void updateNews(News news, Long authorId, List<Long> tags) throws ServiceException {
		try {
			newsService.deleteNewsAuthorByNewsId(news.getNewsId());
			newsService.deleteNewsTagByNewsId(news.getNewsId());
			newsService.update(news);
			newsService.saveNewsAuthor(news.getNewsId(), authorId);
			for (Long tag : tags) {
				newsService.saveNewsTag(news.getNewsId(), tag);
			}
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#findBySearchCriteria(com.epam.news.util.SearchCriteria)
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<NewsValueObject> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
		List<NewsValueObject> newsValueObjects = new ArrayList<>();
		try {
			List<News> news = newsService.findBySearchCriteria(searchCriteria);
			for (News singleNews : news) {
				newsValueObjects.add(findSingleNews(singleNews.getNewsId()));
			}
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
		return newsValueObjects;
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#findAllNews()
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<News> findAllNews() throws ServiceException {
		try {
			return newsService.findAll();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#saveAuthor(com.epam.news.entity.Author)
	 */
	@Override
	public Long saveAuthor(Author author) throws ServiceException {
		try {
			return authorService.save(author).getAuthorId();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#updateAuthor(com.epam.news.entity.Author)
	 */
	@Override
	public void updateAuthor(Author entity) throws ServiceException {
		try {
			authorService.update(entity);
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#findAllAuthors()
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Author> findAllAuthors() throws ServiceException {
		try {
			return authorService.findAll();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#findActualAuthors()
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Author> findActualAuthors() throws ServiceException {
		try {
			return authorService.findActualAuthors();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#expireAuthor(com.epam.news.entity.Author)
	 */
	@Override
	public void expireAuthor(Author entity) throws ServiceException {
		try {
			authorService.expireAuthor(entity);
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#addComment(com.epam.news.entity.Comment)
	 */
	@Override
	public Long addComment(Comment comment) throws ServiceException {
		try {
			return commentService.save(comment).getCommentId();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#deleteComment(long)
	 */
	@Override
	public void deleteComment(long id) throws ServiceException {
		try {
			commentService.deleteById(id);
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#saveTag(com.epam.news.entity.Tag)
	 */
	@Override
	public Long saveTag(Tag entity) throws ServiceException {
		try {
			return tagService.save(entity).getTagId();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#updateTag(com.epam.news.entity.Tag)
	 */
	@Override
	public void updateTag(Tag entity) throws ServiceException {
		try {
			tagService.update(entity);
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see com.epam.news.service.NewsManagementService#findAllTags()
	 */
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public List<Tag> findAllTags() throws ServiceException {
		try {
			return tagService.findAll();
		} catch (ServiceException e) {
			log.error(e);
			throw e;
		}
	}

}
