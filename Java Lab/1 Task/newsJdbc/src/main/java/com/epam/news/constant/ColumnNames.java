package com.epam.news.constant;

public class ColumnNames {
	
	//AUTHOR
	public static final String AUTHOR_ID = "AUTHOR_ID";
	public static final String AUTHOR_NAME = "AUTHOR_NAME";
	public static final String EXPIRED = "EXPIRED";
	
	//COMMENTS
	public static final String COMMENT_ID = "COMMENT_ID";
	public static final String COMMENT_TEXT = "COMMENT_TEXT";
	public static final String COMMENT_CREATION_DATE = "CREATION_DATE";
	
	//NEWS
	public static final String NEWS_ID = "NEWS_ID";
	public static final String TITLE = "TITLE";
	public static final String SHORT_TEXT = "SHORT_TEXT";
	public static final String FULL_TEXT = "FULL_TEXT";
	public static final String NEWS_CREATION_DATE = "CREATION_DATE";
	public static final String MODIFICATION_DATE = "MODIFICATION_DATE";
	
	//TAG
	public static final String TAG_ID = "TAG_ID";
	public static final String TAG_NAME = "TAG_NAME";
	
	//DUAL
	public static final String NEXTVAL = "NEXTVAL";
}
