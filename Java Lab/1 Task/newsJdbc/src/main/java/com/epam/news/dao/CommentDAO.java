package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

/**
 * The Interface CommentDAO. Classes implementing this interface, interacts with
 * the table "COMMENTS" in the database.
 */
public interface CommentDAO extends BasicDAO<Comment> {

	/**
	 * Gets comments by news id.
	 *
	 * @param id
	 *            the id
	 * @return List<Comment> by news id
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Comment> getByNewsId(long id) throws DAOException;

}
