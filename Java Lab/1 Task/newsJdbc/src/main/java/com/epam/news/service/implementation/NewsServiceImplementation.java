package com.epam.news.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsService;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsServiceImplementation.
 */
@Service
public class NewsServiceImplementation implements NewsService {

	@Autowired
	private NewsDAO newsDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#save(java.lang.Object)
	 */
	@Override
	public News save(News entity) throws ServiceException {
		try {
			entity.setNewsId(newsDAO.save(entity));
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#findById(long)
	 */
	@Override
	public News findById(long id) throws ServiceException {
		try {
			return newsDAO.getById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#update(java.lang.Object)
	 */
	@Override
	public void update(News entity) throws ServiceException {
		try {
			newsDAO.update(entity);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws ServiceException {
		try {
			newsDAO.deleteById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.BasicService#findAll()
	 */
	@Override
	public List<News> findAll() throws ServiceException {
		try {
			return newsDAO.getAll();
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#saveNewsAuthor(long, long)
	 */
	@Override
	public void saveNewsAuthor(long newsId, long authorId) throws ServiceException {
		try {
			newsDAO.saveNewsAuthor(newsId, authorId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#saveNewsTag(long, long)
	 */
	@Override
	public void saveNewsTag(long newsId, long tagId) throws ServiceException {
		try {
			newsDAO.saveNewsTag(newsId, tagId);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#findByAuthorId(long)
	 */
	@Override
	public List<News> findByAuthorId(long id) throws ServiceException {
		try {
			return newsDAO.getByAuthorId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#findByTagId(long)
	 */
	@Override
	public List<News> findByTagId(long id) throws ServiceException {
		try {
			return newsDAO.getByTagId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#deleteNewsAuthorByNewsId(long)
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long id) throws ServiceException {
		try {
			newsDAO.deleteNewsAuthorByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#deleteNewsTagByNewsId(long)
	 */
	@Override
	public void deleteNewsTagByNewsId(long id) throws ServiceException {
		try {
			newsDAO.deleteNewsTagByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.service.NewsService#deleteCommentsByNewsId(long)
	 */
	@Override
	public void deleteCommentsByNewsId(long id) throws ServiceException {
		try {
			newsDAO.deleteCommentsByNewsId(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.news.service.NewsService#findBySearchCriteria(com.epam.news.util
	 * .SearchCriteria)
	 */
	@Override
	public List<News> findBySearchCriteria(SearchCriteria searchObject) throws ServiceException {
		try {
			return newsDAO.findBySearchCriteria(searchObject);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

}
