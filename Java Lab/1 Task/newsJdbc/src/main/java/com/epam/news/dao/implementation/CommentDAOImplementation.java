package com.epam.news.dao.implementation;

import static com.epam.news.constant.ColumnNames.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.CommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.exception.DAOException;

/**
 * The Class CommentDAOImplementation. Implementation of CommentDAO interface
 * using simple JDBC.
 */
@Repository
public class CommentDAOImplementation implements CommentDAO {

	public static final String SQL_SELECT_COMMENT_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
	public static final String SQL_SELECT_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS";
	public static final String SQL_SELECT_COMMENT_BY_NEWS_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";
	public static final String SQL_CREATE_COMMENT = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (?,?,?,?)";
	public static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET NEWS_ID = ?, COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
	public static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	public static final String SQL_SELECT_NEXT_VAL = "SELECT COMMENTS_SEQ.NEXTVAL FROM DUAL";

	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(Comment entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_COMMENT)) {
			entity.setCommentId(getNextValue());
			preparedStatement.setLong(1, entity.getCommentId());
			preparedStatement.setLong(2, entity.getNewsId());
			preparedStatement.setString(3, entity.getCommentText());
			preparedStatement.setTimestamp(4,
					entity.getCreationDate() != null ? new Timestamp(entity.getCreationDate().getTime()) : null);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return entity.getCommentId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getById(long)
	 */
	@Override
	public Comment getById(long id) throws DAOException {
		Comment comment = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT_BY_ID)) {
			preparedStatement.setLong(1, id);
			List<Comment> comments = parseResultSet(preparedStatement.executeQuery());
			if (!comments.isEmpty()) {
				comment = comments.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comment;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Comment entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT)) {
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getCommentText());
			preparedStatement.setTimestamp(3,
					entity.getCreationDate() != null ? new Timestamp(entity.getCreationDate().getTime()) : null);
			preparedStatement.setLong(4, entity.getCommentId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getAll()
	 */
	@Override
	public List<Comment> getAll() throws DAOException {
		List<Comment> comments = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			comments = parseResultSet(statement.executeQuery(SQL_SELECT_ALL_COMMENTS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comments;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.CommentDAO#getByNewsId(long)
	 */
	@Override
	public List<Comment> getByNewsId(long id) throws DAOException {
		List<Comment> comments = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_COMMENT_BY_NEWS_ID)) {
			preparedStatement.setLong(1, id);
			comments = parseResultSet(preparedStatement.executeQuery());
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return comments;
	}

	/**
	 * Parses the result set.
	 *
	 * @param rs
	 *            the rs
	 * @return the list
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<Comment> parseResultSet(ResultSet rs) throws SQLException {
		List<Comment> comments = new ArrayList<>();
		while (rs.next()) {
			Comment comment = new Comment();
			comment.setCommentId(rs.getLong(COMMENT_ID));
			comment.setNewsId(rs.getLong(NEWS_ID));
			comment.setCommentText(rs.getString(COMMENT_TEXT));
			comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
			comments.add(comment);
		}
		return comments;
	}

	/**
	 * Gets the next value for comment id.
	 *
	 * @return the next value
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Long getNextValue() throws SQLException {
		Long value = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEXT_VAL);
			if (resultSet.next()) {
				value = resultSet.getLong(NEXTVAL);
			}
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return value;
	}

}
