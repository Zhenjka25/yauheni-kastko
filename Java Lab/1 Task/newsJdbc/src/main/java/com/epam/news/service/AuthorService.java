package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.ServiceException;

/**
 * The Interface AuthorService. Service for work with AuthorDAO.
 */
public interface AuthorService extends BasicService<Author> {

	/**
	 * Expires author.
	 *
	 * @param entity
	 *            the entity
	 * @throws ServiceException
	 *             the service exception
	 */
	void expireAuthor(Author entity) throws ServiceException;

	/**
	 * Finds author by name.
	 *
	 * @param name
	 *            the name
	 * @return the author
	 * @throws ServiceException
	 *             the service exception
	 */
	Author findByName(String name) throws ServiceException;

	/**
	 * Finds actual authors.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Author> findActualAuthors() throws ServiceException;

	/**
	 * Deletes data from news_author by author id.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNewsAuthorByAuthorId(long id) throws ServiceException;

	/**
	 * Finds author by news id.
	 *
	 * @param id
	 *            the id
	 * @return the author
	 * @throws ServiceException
	 *             the service exception
	 */
	Author findByNewsId(long id) throws ServiceException;
}
