package com.epam.news.dao;

import java.util.List;

import com.epam.news.exception.DAOException;

/**
 * The Interface BasicDAO. Basic interface for C.R.U.D. operations.
 *
 * @param <T>
 *            the generic type
 */
public interface BasicDAO<T> {

	/**
	 * Saves entity.
	 *
	 * @param entity
	 *            the entity
	 * @return the long
	 * @throws DAOException
	 *             the DAO exception
	 */
	Long save(T entity) throws DAOException;

	/**
	 * Gets entity by id.
	 *
	 * @param id
	 *            the id
	 * @return the by id
	 * @throws DAOException
	 *             the DAO exception
	 */
	T getById(long id) throws DAOException;

	/**
	 * Updates entity.
	 *
	 * @param entity
	 *            the entity
	 * @throws DAOException
	 *             the DAO exception
	 */
	void update(T entity) throws DAOException;

	/**
	 * Deletes entity by id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteById(long id) throws DAOException;

	/**
	 * Gets the all entities.
	 *
	 * @return the all
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<T> getAll() throws DAOException;
}
