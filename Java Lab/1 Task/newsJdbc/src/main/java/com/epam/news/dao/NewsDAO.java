package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.util.SearchCriteria;

/**
 * The Interface NewsDAO. Classes implementing this interface, interacts with
 * the tables "NEWS", "NEWS_AUTHOR" and "NEWS_TAG" in the database.
 */
public interface NewsDAO extends BasicDAO<News> {

	/**
	 * Saves data to the news_author table.
	 *
	 * @param newsId
	 *            the news id
	 * @param authorId
	 *            the author id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void saveNewsAuthor(long newsId, long authorId) throws DAOException;

	/**
	 * Saves data to the news_tag table.
	 *
	 * @param newsId
	 *            the news id
	 * @param tagId
	 *            the tag id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void saveNewsTag(long newsId, long tagId) throws DAOException;

	/**
	 * Gets news by author id.
	 *
	 * @param id
	 *            the id
	 * @return List<News> by author id
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<News> getByAuthorId(long id) throws DAOException;

	/**
	 * Gets news by tag id.
	 *
	 * @param id
	 *            the id
	 * @return List<News> by tag id
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<News> getByTagId(long id) throws DAOException;

	/**
	 * Deletes data from news_author table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsAuthorByNewsId(long id) throws DAOException;

	/**
	 * Deletes data from news_tag table by news id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsTagByNewsId(long id) throws DAOException;

	/**
	 * Deletes comments by news id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteCommentsByNewsId(long id) throws DAOException;

	/**
	 * Finds news by search criteria.
	 *
	 * @param searchObject
	 *            the search object
	 * @return List<News> by search criteria
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<News> findBySearchCriteria(SearchCriteria searchObject) throws DAOException;

}
