package com.epam.news.service;

import java.util.List;

import com.epam.news.entity.*;
import com.epam.news.exception.ServiceException;
import com.epam.news.util.SearchCriteria;

/**
 * The Interface NewsManagementService. Main service.
 */
public interface NewsManagementService {

	/**
	 * Finds single news.
	 *
	 * @param id
	 *            the id
	 * @return the news value object
	 * @throws ServiceException
	 *             the service exception
	 */
	NewsValueObject findSingleNews(long id) throws ServiceException;

	/**
	 * Saves news.
	 *
	 * @param news
	 *            the news
	 * @param authorId
	 *            the author id
	 * @param tags
	 *            the tags
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long saveNews(News news, Long authorId, List<Long> tags) throws ServiceException;

	/**
	 * Deletes news.
	 *
	 * @param newsidList
	 *            the news id list
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteNews(List<Long> newsidList) throws ServiceException;

	/**
	 * Updates news.
	 *
	 * @param news
	 *            the news
	 * @param authorId
	 *            the author id
	 * @param tags
	 *            the tags
	 * @throws ServiceException
	 *             the service exception
	 */
	void updateNews(News news, Long authorId, List<Long> tags) throws ServiceException;

	/**
	 * Finds news by search criteria.
	 *
	 * @param searchCriteria
	 *            the search criteria
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<NewsValueObject> findBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

	/**
	 * Finds all news.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<News> findAllNews() throws ServiceException;

	/**
	 * Saves author.
	 *
	 * @param author
	 *            the author
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long saveAuthor(Author author) throws ServiceException;

	/**
	 * Updates author.
	 *
	 * @param entity
	 *            the entity
	 * @throws ServiceException
	 *             the service exception
	 */
	void updateAuthor(Author entity) throws ServiceException;

	/**
	 * Finds all authors.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Author> findAllAuthors() throws ServiceException;

	/**
	 * Finds actual authors.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Author> findActualAuthors() throws ServiceException;

	/**
	 * Expires author.
	 *
	 * @param entity
	 *            the entity
	 * @throws ServiceException
	 *             the service exception
	 */
	void expireAuthor(Author entity) throws ServiceException;

	/**
	 * Adds the comment.
	 *
	 * @param comment
	 *            the comment
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long addComment(Comment comment) throws ServiceException;

	/**
	 * Deletes comment.
	 *
	 * @param id
	 *            the id
	 * @throws ServiceException
	 *             the service exception
	 */
	void deleteComment(long id) throws ServiceException;

	/**
	 * Saves tag.
	 *
	 * @param entity
	 *            the entity
	 * @return the long
	 * @throws ServiceException
	 *             the service exception
	 */
	Long saveTag(Tag entity) throws ServiceException;

	/**
	 * Updates tag.
	 *
	 * @param entity
	 *            the entity
	 * @throws ServiceException
	 *             the service exception
	 */
	void updateTag(Tag entity) throws ServiceException;

	/**
	 * Finds all tags.
	 *
	 * @return the list
	 * @throws ServiceException
	 *             the service exception
	 */
	List<Tag> findAllTags() throws ServiceException;

}
