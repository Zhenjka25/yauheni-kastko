package com.epam.news.dao.implementation;

import static com.epam.news.constant.ColumnNames.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.NewsDAO;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.util.SearchCriteria;

/**
 * The Class NewsDAOImplementation. Implementation of NewsDAO interface using
 * simple JDBC.
 */
@Repository
public class NewsDAOImplementation implements NewsDAO {

	public static final String SQL_SELECT_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	public static final String SQL_SELECT_ALL_NEWS = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) GROUP BY NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
	public static final String SQL_SELECT_NEWS_BY_AUTHOR_ID = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE AUTHOR_ID = ? GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
	public static final String SQL_SELECT_NEWS_BY_TAG_ID = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID) WHERE TAG_ID = ? GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";
	public static final String SQL_CREATE_NEWS = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (?,?,?,?,?,?)";
	public static final String SQL_CREATE_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?,?)";
	public static final String SQL_CREATE_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?,?)";
	public static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	public static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	public static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
	public static final String SQL_DELETE_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
	public static final String SQL_DELETE_COMMENTS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
	public static final String SQL_SELECT_NEXT_VAL = "SELECT NEWS_SEQ.NEXTVAL FROM DUAL";
	public static final String SQL_SEARCH_NEWS = "SELECT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR ON (NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID) JOIN NEWS_TAG ON (NEWS.NEWS_ID = NEWS_TAG.NEWS_ID) LEFT JOIN COMMENTS ON (NEWS.NEWS_ID = COMMENTS.NEWS_ID)";
	public static final String SQL_SORT_QUERY = " GROUP BY NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, NEWS.CREATION_DATE, MODIFICATION_DATE ORDER BY COUNT(COMMENT_ID) DESC, NEWS.MODIFICATION_DATE DESC";

	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(News entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS)) {
			entity.setNewsId(getNextValue());
			preparedStatement.setLong(1, entity.getNewsId());
			preparedStatement.setString(2, entity.getTitle());
			preparedStatement.setString(3, entity.getShortText());
			preparedStatement.setString(4, entity.getFullText());
			preparedStatement.setTimestamp(5,
					entity.getCreationDate() != null ? new Timestamp(entity.getCreationDate().getTime()) : null);
			preparedStatement.setDate(6,
					entity.getModificationDate() != null ? new Date(entity.getModificationDate().getTime()) : null);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return entity.getNewsId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getById(long)
	 */
	@Override
	public News getById(long id) throws DAOException {
		News singleNews = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_ID)) {
			preparedStatement.setLong(1, id);
			List<News> news = parseResultSet(preparedStatement.executeQuery());
			if (!news.isEmpty()) {
				singleNews = news.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return singleNews;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#update(java.lang.Object)
	 */
	@Override
	public void update(News entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS)) {
			preparedStatement.setString(1, entity.getTitle());
			preparedStatement.setString(2, entity.getShortText());
			preparedStatement.setString(3, entity.getFullText());
			preparedStatement.setTimestamp(4,
					entity.getCreationDate() != null ? new Timestamp(entity.getCreationDate().getTime()) : null);
			preparedStatement.setDate(5,
					entity.getModificationDate() != null ? new Date(entity.getModificationDate().getTime()) : null);
			preparedStatement.setLong(6, entity.getNewsId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getAll()
	 */
	@Override
	public List<News> getAll() throws DAOException {
		List<News> news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			news = parseResultSet(statement.executeQuery(SQL_SELECT_ALL_NEWS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#saveNewsAuthor(long, long)
	 */
	@Override
	public void saveNewsAuthor(long newsId, long authorId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_AUTHOR)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, authorId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#saveNewsTag(long, long)
	 */
	@Override
	public void saveNewsTag(long newsId, long tagId) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS_TAG)) {
			preparedStatement.setLong(1, newsId);
			preparedStatement.setLong(2, tagId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#getByAuthorId(long)
	 */
	@Override
	public List<News> getByAuthorId(long id) throws DAOException {
		List<News> news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedDtatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_AUTHOR_ID)) {
			preparedDtatement.setLong(1, id);
			news = parseResultSet(preparedDtatement.executeQuery());
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#getByTagId(long)
	 */
	@Override
	public List<News> getByTagId(long id) throws DAOException {
		List<News> news = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedDtatement = connection.prepareStatement(SQL_SELECT_NEWS_BY_TAG_ID)) {
			preparedDtatement.setLong(1, id);
			news = parseResultSet(preparedDtatement.executeQuery());
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return news;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#deleteNewsAuthorByNewsId(long)
	 */
	@Override
	public void deleteNewsAuthorByNewsId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#deleteNewsTagByNewsId(long)
	 */
	@Override
	public void deleteNewsTagByNewsId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#deleteCommentsByNewsId(long)
	 */
	@Override
	public void deleteCommentsByNewsId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENTS)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.NewsDAO#findBySearchCriteria(com.epam.news.util.
	 * SearchCriteria)
	 */
	@Override
	public List<News> findBySearchCriteria(SearchCriteria searchObject) throws DAOException {
		String query = buildSearchQuery(searchObject);
		List<News> newsList = new ArrayList<>();
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			newsList = parseResultSet(statement.executeQuery(query));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return newsList;
	}

	/**
	 * Parses the result set.
	 *
	 * @param rs
	 *            the rs
	 * @return the list
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<News> parseResultSet(ResultSet rs) throws SQLException {
		List<News> news = new ArrayList<>();
		while (rs.next()) {
			News singleNews = new News();
			singleNews.setNewsId(rs.getLong(NEWS_ID));
			singleNews.setTitle(rs.getString(TITLE));
			singleNews.setShortText(rs.getString(SHORT_TEXT));
			singleNews.setFullText(rs.getString(FULL_TEXT));
			singleNews.setCreationDate(rs.getTimestamp(NEWS_CREATION_DATE));
			singleNews.setModificationDate(rs.getDate(MODIFICATION_DATE));
			news.add(singleNews);
		}
		return news;
	}

	/**
	 * Gets the next value for news id.
	 *
	 * @return the next value
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Long getNextValue() throws SQLException {
		Long value = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEXT_VAL);
			if (resultSet.next()) {
				value = resultSet.getLong(NEXTVAL);
			}
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return value;
	}

	/**
	 * Builds the search query by search criteria object.
	 *
	 * @param searchObject
	 *            the search object
	 * @return the string
	 */
	private String buildSearchQuery(SearchCriteria searchObject) {
		StringBuilder query = new StringBuilder(SQL_SEARCH_NEWS);
		if (searchObject.getAuthorId() != null || !searchObject.getTagsId().isEmpty()) {
			query.append(" WHERE");
			if (searchObject.getAuthorId() != null) {
				query.append(" AUTHOR_ID = ");
				query.append(searchObject.getAuthorId());
			}
			if (!searchObject.getTagsId().isEmpty()) {
				query.append(" AND");
			}
			Iterator<Long> tagIterator = searchObject.getTagsId().iterator();
			query.append(" TAG_ID IN (");
			while (tagIterator.hasNext()) {
				query.append(tagIterator.next());
				if (tagIterator.hasNext()) {
					query.append(", ");
				} else {
					query.append(")");
				}
			}
		}
		query.append(SQL_SORT_QUERY);
		return query.toString();
	}

}
