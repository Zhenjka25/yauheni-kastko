package com.epam.news.dao.implementation;

import static com.epam.news.constant.ColumnNames.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.AuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * The Class AuthorDAOImplementation. Implementation of AuthorDAO interface
 * using simple JDBC.
 */
@Repository
public class AuthorDAOImplementation implements AuthorDAO {

	public static final String SQL_SELECT_AUTHOR_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
	public static final String SQL_SELECT_AUTHOR_BY_NAME = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_NAME = ?";
	public static final String SQL_SELECT_AUTHOR_BY_NEWS_ID = "SELECT AUTHOR.AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM NEWS_AUTHOR JOIN AUTHOR ON (NEWS_AUTHOR.AUTHOR_ID = AUTHOR.AUTHOR_ID) WHERE NEWS_ID = ?";
	public static final String SQL_SELECT_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
	public static final String SQL_SELECT_ACTUAL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE EXPIRED != null";
	public static final String SQL_CREATE_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (?,?,?)";
	public static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ?, EXPIRED = ? WHERE AUTHOR_ID = ?";
	public static final String SQL_SET_EXPIRED = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	public static final String SQL_DELETE_AUTHOR = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
	public static final String SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID = "DELETE FROM NEWS_AUTHOR WHERE AUTHOR_ID = ?";
	public static final String SQL_SELECT_NEXT_VAL = "SELECT AUTHOR_SEQ.NEXTVAL FROM DUAL";

	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(Author entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_AUTHOR)) {
			entity.setAuthorId(getNextValue());
			preparedStatement.setLong(1, entity.getAuthorId());
			preparedStatement.setString(2, entity.getAuthorName());
			preparedStatement.setTimestamp(3,
					entity.getExpired() != null ? new Timestamp(entity.getExpired().getTime()) : null);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return entity.getAuthorId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getById(long)
	 */
	@Override
	public Author getById(long id) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_ID)) {
			preparedStatement.setLong(1, id);
			List<Author> authors = parseResultSet(preparedStatement.executeQuery());
			if (!authors.isEmpty()) {
				author = authors.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Author entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
			preparedStatement.setString(1, entity.getAuthorName());
			preparedStatement.setTimestamp(2,
					entity.getExpired() != null ? new Timestamp(entity.getExpired().getTime()) : null);
			preparedStatement.setLong(3, entity.getAuthorId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.epam.news.dao.AuthorDAO#expireAuthor(com.epam.news.entity.Author)
	 */
	@Override
	public void expireAuthor(Author entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_EXPIRED)) {
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			preparedStatement.setTimestamp(1, timestamp);
			preparedStatement.setLong(2, entity.getAuthorId());
			preparedStatement.executeUpdate();
			entity.setExpired(timestamp);
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_AUTHOR)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.AuthorDAO#deleteNewsAuthorByAuthorId(long)
	 */
	@Override
	public void deleteNewsAuthorByAuthorId(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_AUTHOR_ID)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getAll()
	 */
	@Override
	public List<Author> getAll() throws DAOException {
		List<Author> authors = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			authors = parseResultSet(statement.executeQuery(SQL_SELECT_ALL_AUTHORS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.AuthorDAO#getByName(java.lang.String)
	 */
	@Override
	public Author getByName(String name) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NAME)) {
			preparedStatement.setString(1, name);
			List<Author> authors = parseResultSet(preparedStatement.executeQuery());
			if (!authors.isEmpty()) {
				author = authors.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.AuthorDAO#getActualAuthors()
	 */
	@Override
	public List<Author> getActualAuthors() throws DAOException {
		List<Author> authors = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			authors = parseResultSet(statement.executeQuery(SQL_SELECT_ACTUAL_AUTHORS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return authors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.AuthorDAO#getByNewsId(long)
	 */
	@Override
	public Author getByNewsId(long id) throws DAOException {
		Author author = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_AUTHOR_BY_NEWS_ID)) {
			preparedStatement.setLong(1, id);
			List<Author> authors = parseResultSet(preparedStatement.executeQuery());
			if (!authors.isEmpty()) {
				author = authors.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return author;
	}

	/**
	 * Parses the result set.
	 *
	 * @param rs
	 *            the rs
	 * @return the list
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<Author> parseResultSet(ResultSet rs) throws SQLException {
		List<Author> authors = new ArrayList<>();
		while (rs.next()) {
			Author author = new Author();
			author.setAuthorId(rs.getLong(AUTHOR_ID));
			author.setAuthorName(rs.getString(AUTHOR_NAME));
			author.setExpired(rs.getTimestamp(EXPIRED));
			authors.add(author);
		}
		return authors;
	}

	/**
	 * Gets the next value of author id.
	 *
	 * @return the next value
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Long getNextValue() throws SQLException {
		Long value = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEXT_VAL);
			if (resultSet.next()) {
				value = resultSet.getLong(NEXTVAL);
			}
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return value;
	}

}
