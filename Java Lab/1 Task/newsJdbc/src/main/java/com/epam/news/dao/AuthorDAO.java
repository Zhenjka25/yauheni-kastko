package com.epam.news.dao;

import java.util.List;

import com.epam.news.entity.Author;
import com.epam.news.exception.DAOException;

/**
 * The Interface AuthorDAO. Classes implementing this interface, interacts with
 * the table "AUTHOR" in the database.
 */
public interface AuthorDAO extends BasicDAO<Author> {

	/**
	 * Expires author.
	 *
	 * @param entity
	 *            the entity
	 * @throws DAOException
	 *             the DAO exception
	 */
	void expireAuthor(Author entity) throws DAOException;

	/**
	 * Gets author by name.
	 *
	 * @param name
	 *            the name
	 * @return by name
	 * @throws DAOException
	 *             the DAO exception
	 */
	Author getByName(String name) throws DAOException;

	/**
	 * Gets the actual authors.
	 *
	 * @return the actual authors
	 * @throws DAOException
	 *             the DAO exception
	 */
	List<Author> getActualAuthors() throws DAOException;

	/**
	 * Deletes news author by author id.
	 *
	 * @param id
	 *            the id
	 * @throws DAOException
	 *             the DAO exception
	 */
	void deleteNewsAuthorByAuthorId(long id) throws DAOException;

	/**
	 * Gets author by news id.
	 *
	 * @param id
	 *            the id
	 * @return the by news id
	 * @throws DAOException
	 *             the DAO exception
	 */
	Author getByNewsId(long id) throws DAOException;

}
