package com.epam.news.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class NewsValueObject.
 */
public class NewsValueObject {

	private News news;
	private Author author;
	private List<Tag> tags = new ArrayList<>();
	private List<Comment> comments = new ArrayList<>();

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public News getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news
	 *            the new news
	 */
	public void setNews(News news) {
		this.news = news;
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author
	 *            the new author
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<Tag> getTags() {
		return tags;
	}

	/**
	 * Sets the tags.
	 *
	 * @param tags
	 *            the new tags
	 */
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	/**
	 * Adds the tag.
	 *
	 * @param tag
	 *            the tag
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments
	 *            the new comments
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Adds the comment.
	 *
	 * @param comment
	 *            the comment
	 */
	public void addComment(Comment comment) {
		comments.add(comment);
	}

}
