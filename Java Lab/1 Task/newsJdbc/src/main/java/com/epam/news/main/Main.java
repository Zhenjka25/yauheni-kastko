package com.epam.news.main;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import com.epam.news.config.ApplicationConfig;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.exception.DAOException;
import com.epam.news.exception.ServiceException;
import com.epam.news.service.NewsManagementService;

@Component
public class Main {

	private static NewsManagementService newsManagementService;

	public static void main(String[] args) throws ServiceException, DAOException {
		@SuppressWarnings("resource")
		ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
		newsManagementService = context.getBean(NewsManagementService.class);
		saveAuthor();
		saveNews();
	}

	private static void saveAuthor() throws ServiceException {
		Author author = new Author();
		author.setAuthorName("aaa");
		newsManagementService.saveAuthor(author);
	}

	private static void saveNews() throws ServiceException {
		News news = new News();
		news.setTitle("title");
		news.setShortText("short");
		news.setFullText("full");
		news.setCreationDate(Timestamp.valueOf("2015-08-09 09:41:29.77"));
		news.setModificationDate(Date.valueOf("2015-08-11"));
		Long authorId = new Long(46L);
		List<Long> tags = new ArrayList<>();
		tags.add(33L);
		tags.add(34L);
		tags.add(155L);
		newsManagementService.saveNews(news, authorId, tags);
	}
}
