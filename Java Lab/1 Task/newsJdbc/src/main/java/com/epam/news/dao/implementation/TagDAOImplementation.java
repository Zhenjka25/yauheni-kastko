package com.epam.news.dao.implementation;

import static com.epam.news.constant.ColumnNames.NEXTVAL;
import static com.epam.news.constant.ColumnNames.TAG_ID;
import static com.epam.news.constant.ColumnNames.TAG_NAME;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import com.epam.news.dao.TagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.exception.DAOException;

/**
 * The Class TagDAOImplementation. Implementation of TagDAO interface using
 * simple JDBC.
 */
@Repository
public class TagDAOImplementation implements TagDAO {

	public static final String SQL_SELECT_TAG_BY_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
	public static final String SQL_SELECT_TAG_BY_NAME = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_NAME = ?";
	public static final String SQL_SELECT_TAG_BY_NEWS_ID = "SELECT TAG.TAG_ID, TAG_NAME FROM NEWS_TAG JOIN TAG ON (NEWS_TAG.TAG_ID = TAG.TAG_ID) WHERE NEWS_ID = ?";
	public static final String SQL_SELECT_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAG";
	public static final String SQL_CREATE_TAG = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (?,?)";
	public static final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
	public static final String SQL_DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID = ?";
	public static final String SQL_DELETE_NEWS_TAG_BY_TAG_ID = "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";
	public static final String SQL_SELECT_NEXT_VAL = "SELECT TAG_SEQ.NEXTVAL FROM DUAL";

	@Autowired
	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#save(java.lang.Object)
	 */
	@Override
	public Long save(Tag entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_TAG)) {
			entity.setTagId(getNextValue());
			preparedStatement.setLong(1, entity.getTagId());
			preparedStatement.setString(2, entity.getTagName());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return entity.getTagId();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getById(long)
	 */
	@Override
	public Tag getById(long id) throws DAOException {
		Tag tag = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_BY_ID)) {
			preparedStatement.setLong(1, id);
			List<Tag> tags = parseResultSet(preparedStatement.executeQuery());
			if (!tags.isEmpty()) {
				tag = tags.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Tag entity) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG)) {
			preparedStatement.setString(1, entity.getTagName());
			preparedStatement.setLong(2, entity.getTagId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#deleteById(long)
	 */
	@Override
	public void deleteById(long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TAG)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.BasicDAO#getAll()
	 */
	@Override
	public List<Tag> getAll() throws DAOException {
		List<Tag> tags = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			tags = parseResultSet(statement.executeQuery(SQL_SELECT_ALL_TAGS));
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tags;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.TagDAO#getByName(java.lang.String)
	 */
	@Override
	public Tag getByName(String name) throws DAOException {
		Tag tag = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_BY_NAME)) {
			preparedStatement.setString(1, name);
			List<Tag> tags = parseResultSet(preparedStatement.executeQuery());
			if (!tags.isEmpty()) {
				tag = tags.get(0);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.TagDAO#deleteNewsTagByTagId(java.lang.Long)
	 */
	@Override
	public void deleteNewsTagByTagId(Long id) throws DAOException {
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_BY_TAG_ID)) {
			preparedStatement.setLong(1, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.epam.news.dao.TagDAO#getByNewsId(long)
	 */
	@Override
	public List<Tag> getByNewsId(long id) throws DAOException {
		List<Tag> tags = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_TAG_BY_NEWS_ID)) {
			preparedStatement.setLong(1, id);
			tags = parseResultSet(preparedStatement.executeQuery());
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return tags;
	}

	/**
	 * Gets the next value for tag id.
	 *
	 * @return the next value
	 * @throws SQLException
	 *             the SQL exception
	 */
	private Long getNextValue() throws SQLException {
		Long value = null;
		Connection connection = DataSourceUtils.getConnection(dataSource);
		try (Statement statement = connection.createStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_NEXT_VAL);
			if (resultSet.next()) {
				value = resultSet.getLong(NEXTVAL);
			}
		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		return value;
	}

	/**
	 * Parses the result set.
	 *
	 * @param rs
	 *            the rs
	 * @return the list
	 * @throws SQLException
	 *             the SQL exception
	 */
	private List<Tag> parseResultSet(ResultSet rs) throws SQLException {
		List<Tag> tags = new ArrayList<>();
		while (rs.next()) {
			Tag tag = new Tag();
			tag.setTagId(rs.getLong(TAG_ID));
			tag.setTagName(rs.getString(TAG_NAME));
			tags.add(tag);
		}
		return tags;
	}

}
