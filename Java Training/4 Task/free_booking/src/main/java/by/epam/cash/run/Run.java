package by.epam.cash.run;

import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import by.epam.cash.entity.Cash;
import by.epam.cash.entity.Client;
import by.epam.cash.entity.Shop;

public class Run {
	private static Logger log = Logger.getLogger(Run.class);
	static {
		PropertyConfigurator.configure("src/main/resources/log4j.properties");
	}

	public static void main(String[] args) {
		int amountClients = 100;
		int amountsCashes = 5;
		ConcurrentLinkedQueue<Cash> cashes = new ConcurrentLinkedQueue<>();
		for (int i = 0; i < amountsCashes; i++) {
			cashes.add(new Cash(i + 1));
		}
		Shop shop = new Shop(cashes);
		log.info("Start working of shop");
		for (int i = 0; i < amountClients; i++) {
			new Client(shop).start();
		}
	}

}
