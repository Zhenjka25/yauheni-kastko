package by.epam.cash.entity;

import java.util.ArrayDeque;
import java.util.Queue;

public class Shop {
	private final ArrayDeque<Cash> cashes = new ArrayDeque<>();

	public Shop(Queue<Cash> source) {
		cashes.addAll(source);
	}

	public ArrayDeque<Cash> getCashes() {
		return cashes;
	}

}
