package by.epam.cash.entity;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import by.epam.cash.exception.ResourceException;

public class Cash {
	private int id;
	private volatile int currentClientsAmount;
	private ReentrantLock lock = new ReentrantLock();

	public Cash(int id) {
		this.id = id;
	}

	public void serve() throws ResourceException {
		try {
			Thread.sleep(new Random().nextInt(500));
		} catch (InterruptedException e) {
			throw new ResourceException(e);
		}
	}

	public int getCurrentClientsAmount() {
		return currentClientsAmount;
	}

	public void addClientToQueue() {
		currentClientsAmount++;
	}

	public void removeClientFromQueue() {
		currentClientsAmount--;
	}

	public int getId() {
		return id;
	}

	public ReentrantLock getLock() {
		return lock;
	}
}
