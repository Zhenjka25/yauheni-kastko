package by.epam.cash.entity;

import java.util.ArrayDeque;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import by.epam.cash.exception.ResourceException;

public class Client extends Thread {
	private static Logger log = Logger.getLogger(Client.class);
	private boolean serving;
	private Shop shop;

	public Client(Shop shop) {
		this.shop = shop;
	}

	public void run() {
		Cash cash = null;
		Random random = new Random();
		try {
			while (!serving) {
				cash = findCashWithMinAmountClients();
				cash.addClientToQueue();
				log.info("Client #" + this.getId() + " went to the cash #"
						+ cash.getId());
				if (cash.getLock().tryLock(random.nextInt(2500),
						TimeUnit.MILLISECONDS)) {
					serving = true;
					log.info("Client #" + this.getId() + " is served on cash #"
							+ cash.getId());
					cash.serve();
					cash.getLock().unlock();
				}
				cash.removeClientFromQueue();
			}
		} catch (ResourceException | InterruptedException e) {
			log.error(e);
		} finally {
			log.info("Client #" + this.getId() + " was served on cash #"
					+ cash.getId());
		}
	}

	public Cash findCashWithMinAmountClients() {
		ArrayDeque<Cash> cashes = shop.getCashes();
		Cash cashWithMinAmountClients = shop.getCashes().peek();
		for (Cash cash : cashes) {
			if (cashWithMinAmountClients.getCurrentClientsAmount() > cash
					.getCurrentClientsAmount()) {
				cashWithMinAmountClients = cash;
			}
		}
		return cashWithMinAmountClients;
	}
}
