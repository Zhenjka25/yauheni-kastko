package by.epam.train.tool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import by.epam.train.entity.Coach;
import by.epam.train.entity.PassengerTrain;

public class TrainSort {
	public static List<Coach> sortByComfortLevel(PassengerTrain train) {
		List<Coach> list = new ArrayList<>();
		for (Coach coach : train.getWagons()) {
			list.add(coach);
		}
		Collections.sort(list, new ComfortLevelComparator());
		return list;
	}

	public static List<Coach> toListByPassengersAmount(int minAmount,
			int maxAmount, PassengerTrain train) {
		List<Coach> list = new ArrayList<>();
		for (Coach coach : train.getWagons()) {
			if (coach.getAmountPassengers() >= minAmount
					&& coach.getAmountPassengers() <= maxAmount) {
				list.add(coach);
			}
		}
		return list;
	}
}
