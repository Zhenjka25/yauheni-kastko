package by.epam.train.tool;

import java.util.Comparator;

import org.apache.log4j.Logger;

import by.epam.train.entity.Coach;
import by.epam.train.exception.TechnicalException;

public class ComfortLevelComparator implements Comparator<Coach> {
	private static Logger log = Logger.getLogger(ComfortLevelComparator.class);

	@Override
	public int compare(Coach o1, Coach o2) {
		int result = 0;
		try {
			result = o1.getComfortLevel() - o2.getComfortLevel();
		} catch (TechnicalException e) {
			log.error("It does not specify the types of compared coaches");
		}
		return result;
	}
}
