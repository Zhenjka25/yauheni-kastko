package by.epam.train.tool;

import org.apache.log4j.Logger;

import by.epam.train.entity.Coach;
import by.epam.train.entity.PassengerTrain;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

public class TrainService {
	private static Logger log = Logger.getLogger(TrainService.class);

	public static void repairTrain(PassengerTrain train)
			throws TechnicalException, LogicalException {
		for (Coach coach : train.getWagons()) {
			if (coach != null) {
				if (coach.getWearRate() > coach.getAllowableWearRate()) {
					coach.setWearRate(0);
					log.info("Coach id " + coach.getId() + " was repaired");
				}
			} else {
				throw new TechnicalException("Coach is not initialized");
			}
		}
	}
}
