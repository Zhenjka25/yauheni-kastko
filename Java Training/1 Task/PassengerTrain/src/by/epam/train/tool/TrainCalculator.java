package by.epam.train.tool;

import by.epam.train.entity.Coach;
import by.epam.train.entity.PassengerTrain;
import by.epam.train.exception.TechnicalException;

public class TrainCalculator {
	public static int calculateAmountPassengers(PassengerTrain train) {
		int sum = 0;
		for (Coach coach : train.getWagons()) {
			sum += coach.getAmountPassengers();
		}
		return sum;
	}

	public static int calculateAmountBaggage(PassengerTrain train) {
		int sum = 0;
		for (Coach coach : train.getWagons()) {
			sum += coach.getAmountBaggage();
		}
		return sum;
	}

	public static int calculateBaggageWeight(PassengerTrain train) {
		int weight = 0;
		for (Coach coach : train.getWagons()) {
			weight += coach.getAmountBaggage() * Coach.BAGGAGE_WEIGHT;
		}
		return weight;
	}

	public static int calculateTrainWeight(PassengerTrain train)
			throws TechnicalException {
		int weight = 0;
		for (Coach coach : train.getWagons()) {
			weight += coach.getWeight();
			weight += coach.getAmountPassengers() * Coach.PASSENGER_WEIGHT;
			weight += coach.getAmountBaggage() * Coach.BAGGAGE_WEIGHT;
		}
		return weight;
	}
}
