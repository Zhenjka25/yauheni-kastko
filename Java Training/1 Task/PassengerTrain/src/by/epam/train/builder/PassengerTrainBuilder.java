package by.epam.train.builder;

import java.util.List;

import by.epam.train.entity.Coach;
import by.epam.train.entity.PassengerTrain;
import by.epam.train.entity.PassengerTrainType;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

public class PassengerTrainBuilder {
	private PassengerTrain train = new PassengerTrain();

	public PassengerTrain getPassengerTrain() {
		return train;
	}

	public void buildId(int id) throws LogicalException {
		train.setId(id);
	}

	public void buildName(String name) {
		train.setName(name);
	}

	public void buildType(PassengerTrainType type) {
		train.setType(type);
	}

	public void buildSpeed(int speed) throws LogicalException,
			TechnicalException {
		train.setSpeed(speed);
	}

	public void buildCoaches(List<Coach> coaches) {
		train.setWagons(coaches);
	}
}
