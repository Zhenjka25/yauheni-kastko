package by.epam.train.builder;

import by.epam.train.entity.Coach;
import by.epam.train.entity.CoachType;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

public class CoachBuilder {
	private Coach coach = new Coach();

	public Coach getCoach() {
		return coach;
	}

	public void buildId(int id) throws LogicalException {
		coach.setId(id);
	}

	public void buildWearRate(int wearRate) throws LogicalException {
		coach.setWearRate(wearRate);
	}

	public void buildAmountPassengers(int amountPassengers)
			throws LogicalException, TechnicalException {
		coach.setAmountPassengers(amountPassengers);
	}

	public void buildAmountBaggage(int amountBaggage) throws LogicalException {
		coach.setAmountBaggage(amountBaggage);
	}

	public void buildCoachType(CoachType type) {
		coach.setType(type);
	}
}
