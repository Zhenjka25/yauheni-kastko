package by.epam.train.run;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import by.epam.train.creator.Creator;
import by.epam.train.entity.PassengerTrain;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;
import by.epam.train.reporter.Reporter;
import by.epam.train.reporter.WriterToFile;
import by.epam.train.tool.TrainService;

public class Run {
	private static Logger log = Logger.getLogger(Run.class);
	static {
		PropertyConfigurator.configure("resources/log4j.properties");
	}

	public static void main(String[] args) {
		try {
			Creator creator = new Creator();
			PassengerTrain train = creator.createTrain(1);
			TrainService.repairTrain(train);
			String information = Reporter.buildTrainInformation(train);
			String path = "information/train.txt";
			WriterToFile.writeToFile(information, path);
			log.info("information is recorded in the file " + path);
		} catch (LogicalException | TechnicalException e) {
			log.error(e);
		}
	}
}
