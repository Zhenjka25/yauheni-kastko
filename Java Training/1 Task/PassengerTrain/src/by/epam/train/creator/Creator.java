package by.epam.train.creator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import by.epam.train.builder.CoachBuilder;
import by.epam.train.builder.PassengerTrainBuilder;
import by.epam.train.entity.Coach;
import by.epam.train.entity.CoachType;
import by.epam.train.entity.PassengerTrain;
import by.epam.train.entity.PassengerTrainType;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;
import by.epam.train.tool.RandomEnum;

public class Creator {
	public Coach createCoach(int id) throws LogicalException,
			TechnicalException {
		Random random = new Random();
		Coach coach = null;
		CoachBuilder coachBuilder = new CoachBuilder();
		coachBuilder.buildId(id);
		coachBuilder.buildCoachType(RandomEnum.randomEnum(CoachType.class));
		coachBuilder.buildWearRate(random.nextInt(10));
		coachBuilder.buildAmountPassengers(random.nextInt(30));
		coachBuilder.buildAmountBaggage(random.nextInt(60));
		coach = coachBuilder.getCoach();
		return coach;
	}

	public PassengerTrain createTrain(int id) throws LogicalException,
			TechnicalException {
		PassengerTrain train;
		Random random = new Random();
		PassengerTrainBuilder trainBuilder = new PassengerTrainBuilder();
		List<Coach> coaches = new ArrayList<>();
		int amountOfCoaches = 10;
		for (int i = 0; i < amountOfCoaches; i++) {
			coaches.add(createCoach(i + 1));
		}
		trainBuilder.buildId(id);
		trainBuilder.buildName("Passenger_Train");
		trainBuilder.buildType(RandomEnum.randomEnum(PassengerTrainType.class));
		trainBuilder.buildSpeed(random.nextInt(40) + 20);
		trainBuilder.buildCoaches(coaches);
		train = trainBuilder.getPassengerTrain();
		return train;
	}
}
