package by.epam.train.entity;

public enum PassengerTrainType {
	PASSENGER_TRAIN(60), 
	FAST_TRAIN(80), 
	SPEEDING_TRAIN(140), 
	HIGH_SPEED_TRAIN(200);
	
	private int maxSpeed;

	PassengerTrainType(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

}
