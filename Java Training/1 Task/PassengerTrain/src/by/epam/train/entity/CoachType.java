package by.epam.train.entity;

public enum CoachType {
	SLEEPING_CAR(5, 8, 40000, 9, 18), 
	COMPARTMENT_CAR(5, 8, 45000, 7, 36), 
	CAR_WITH_RESERVED_SEATS(5, 8, 35000, 5, 54), 
	COMMON_CAR(5, 8, 30000, 3, 81), 
	SEDENTERY_CAR(5, 8, 40000, 6, 63);
	
	private int allowableWearRate;
	private int length;
	private int weight;
	private int comfortLevel;
	private int maxNumberPassangers;

	CoachType(int allowableWearRate, int length, int weight, int comfortLevel,
			int maxNumberPassengers) {
		this.allowableWearRate = allowableWearRate;
		this.length = length;
		this.weight = weight;
		this.comfortLevel = comfortLevel;
		this.maxNumberPassangers = maxNumberPassengers;
	}

	public int getAllowableWearRate() {
		return allowableWearRate;
	}

	public int getLength() {
		return length;
	}

	public int getWeight() {
		return weight;
	}

	public int getComfortLevel() {
		return comfortLevel;
	}

	public int getMaxNumberPassangers() {
		return maxNumberPassangers;
	}
}