package by.epam.train.entity;

import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

public class PassengerTrain extends BasicTrain<Coach> {
	private PassengerTrainType type;
	private int speed;

	public PassengerTrainType getType() {
		return type;
	}

	public void setType(PassengerTrainType type) {
		this.type = type;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) throws LogicalException, TechnicalException {
		if (type != null) {
			if (speed > 0 && speed < type.getMaxSpeed()) {
				this.speed = speed;
			} else {
				throw new LogicalException(
						"Speed is negative or equals 0 or larger than max speed");
			}
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	public int getMaxSpeed() throws TechnicalException {
		if (type != null) {
			return type.getMaxSpeed();
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + speed;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerTrain other = (PassengerTrain) obj;
		if (speed != other.speed)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder trainInformation = new StringBuilder();
		trainInformation.append("\nTrain id is " + this.getId());
		trainInformation.append("\nTrain " + this.getName());
		trainInformation.append("\nTrain type is " + this.getType());
		trainInformation.append("\ntrain speed is " + this.getSpeed() + "\n");
		for (Coach coach : this.getWagons()) {
			trainInformation.append(coach.toString());
		}
		return trainInformation.toString();
	}
}
