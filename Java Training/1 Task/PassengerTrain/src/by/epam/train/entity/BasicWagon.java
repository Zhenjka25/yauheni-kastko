package by.epam.train.entity;

import by.epam.train.exception.LogicalException;

public class BasicWagon {
	private int id;
	private int wearRate;

	public int getId() {
		return id;
	}

	public void setId(int id) throws LogicalException {
		if (id > 0) {
			this.id = id;
		} else {
			throw new LogicalException("Id is negative or equals 0");
		}
	}

	public int getWearRate() {
		return wearRate;
	}

	public void setWearRate(int wearRate) throws LogicalException {
		if (wearRate >= 0) {
			this.wearRate = wearRate;
		} else {
			throw new LogicalException("Wear rate is negative");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + wearRate;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicWagon other = (BasicWagon) obj;
		if (id != other.id)
			return false;
		if (wearRate != other.wearRate)
			return false;
		return true;
	}
}
