package by.epam.train.entity;

import java.util.Collections;
import java.util.List;

import by.epam.train.exception.LogicalException;

public class BasicTrain<T extends BasicWagon> {
	private int id;
	private String name;
	private List<T> wagons;

	public int getId() {
		return id;
	}

	public void setId(int id) throws LogicalException {
		if (id > 0) {
			this.id = id;
		} else {
			throw new LogicalException("Id is negative or equals 0");
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<T> getWagons() {
		return Collections.unmodifiableList(wagons);
	}

	public void setWagons(List<T> wagons) {
		this.wagons = wagons;
	}

	public void addWagon(T wagon) {
		wagons.add(wagon);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((wagons == null) ? 0 : wagons.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicTrain<?> other = (BasicTrain<?>) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (wagons == null) {
			if (other.wagons != null)
				return false;
		} else if (!wagons.equals(other.wagons))
			return false;
		return true;
	}

}
