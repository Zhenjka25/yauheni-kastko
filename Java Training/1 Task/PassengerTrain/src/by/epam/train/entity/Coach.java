package by.epam.train.entity;

import org.apache.log4j.Logger;

import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

public class Coach extends BasicWagon {
	private static Logger log = Logger.getLogger(Coach.class);
	private int amountPassengers;
	private int amountBaggage;
	private CoachType type;
	public static final int BAGGAGE_WEIGHT = 10;
	public static final int PASSENGER_WEIGHT = 80;

	public int getAmountPassengers() {
		return amountPassengers;
	}

	public void setAmountPassengers(int amountPassengers)
			throws LogicalException, TechnicalException {
		if (type != null) {
			if (amountPassengers >= 0
					&& amountPassengers < type.getMaxNumberPassangers()) {
				this.amountPassengers = amountPassengers;
			} else {
				throw new LogicalException(
						"Amount of passengers is negative or larger than max number of passengers");
			}
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	public int getAmountBaggage() {
		return amountBaggage;
	}

	public void setAmountBaggage(int amountBaggage) throws LogicalException {
		if (amountBaggage >= 0) {
			this.amountBaggage = amountBaggage;
		} else {
			throw new LogicalException("Amount of baggage is negative");
		}
	}

	public CoachType getType() {
		return type;
	}

	public void setType(CoachType type) {
		this.type = type;
	}

	public int getComfortLevel() throws TechnicalException {
		if (type != null) {
			return type.getComfortLevel();
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	public int getLength() throws TechnicalException {
		if (type != null) {
			return type.getLength();
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	public int getWeight() throws TechnicalException {
		if (type != null) {
			return type.getWeight();
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	public int getMaxAmountPassengers() throws TechnicalException {
		if (type != null) {
			return type.getMaxNumberPassangers();
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	public int getAllowableWearRate() throws TechnicalException {
		if (type != null) {
			return type.getAllowableWearRate();
		} else {
			throw new TechnicalException(
					"It does not specify the type of coach");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + amountBaggage;
		result = prime * result + amountPassengers;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coach other = (Coach) obj;
		if (amountBaggage != other.amountBaggage)
			return false;
		if (amountPassengers != other.amountPassengers)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder coachInformation = new StringBuilder();
		try {
			coachInformation.append("\nCoach " + this.getId());
			coachInformation.append("\nCoach length is " + this.getLength());
			coachInformation.append("\nCoach weight is " + this.getWeight());
			coachInformation.append("\nCoach type is " + this.getType());
			coachInformation.append("\nCoach wear rate is "
					+ this.getWearRate());
			coachInformation.append("\nCoach comfort level is "
					+ this.getComfortLevel());
			coachInformation.append("\nAmount of passengers is "
					+ this.getAmountPassengers());
			coachInformation.append("\nAmount of baggage is "
					+ this.getAmountBaggage());
			coachInformation.append("\n");
		} catch (TechnicalException te) {
			log.error(te);
		}
		return coachInformation.toString();
	}

}
