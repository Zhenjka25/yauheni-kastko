package by.epam.train.reporter;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import by.epam.train.exception.TechnicalException;

public class WriterToFile {

	public static void writeToFile(String information, String path)
			throws TechnicalException {
		try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(path)))) {
			w.write(information);
		} catch (IOException e) {
			throw new TechnicalException("Writing error");
		}
	}
}
