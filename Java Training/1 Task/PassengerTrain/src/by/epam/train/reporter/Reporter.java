package by.epam.train.reporter;

import java.util.List;

import by.epam.train.entity.Coach;
import by.epam.train.entity.PassengerTrain;
import by.epam.train.exception.TechnicalException;
import by.epam.train.tool.TrainCalculator;
import by.epam.train.tool.TrainSort;

public class Reporter {
	public static String buildTrainInformation(PassengerTrain train)
			throws TechnicalException {
		StringBuilder information = new StringBuilder();
		information.append(train.toString());
		information.append("\n\nSum amount of passengers is "
				+ TrainCalculator.calculateAmountPassengers(train));
		information.append("\nSum amount of baggage is "
				+ TrainCalculator.calculateAmountBaggage(train));
		information.append("\nSum baggage weight is "
				+ TrainCalculator.calculateBaggageWeight(train));
		information.append("\nSum weight of train is "
				+ TrainCalculator.calculateTrainWeight(train));
		information.append("\n\nCoaches, sorted by comfort level: "
				+ buildCoachesInformation(TrainSort.sortByComfortLevel(train)));
		information
				.append("\n\nCoaches for amount of passengers from 10 to 15: "
						+ buildCoachesInformation(TrainSort
								.toListByPassengersAmount(10, 15, train)));
		return information.toString();
	}

	public static String buildCoachesInformation(List<Coach> coaches) {
		StringBuilder coachesInformation = new StringBuilder();
		for (Coach coach : coaches) {
			coachesInformation.append(coach.toString());
		}
		return coachesInformation.toString();

	}
}
