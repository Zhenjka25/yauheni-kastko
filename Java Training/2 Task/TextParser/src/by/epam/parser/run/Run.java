package by.epam.parser.run;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import by.epam.parser.component.StringComposite;
import by.epam.parser.component.StringCompositeType;
import by.epam.parser.exception.TechnicalException;
import by.epam.parser.parser.FileWorker;
import by.epam.parser.parser.TextParser;
import by.epam.parser.parser.TextWorker;

public class Run {
	private static Logger log = Logger.getLogger(Run.class);
	static {
		PropertyConfigurator.configure("resources/log4j.properties");

	}

	public static void main(String[] args) {
		try {
			String readPath = "Text.txt";
			String parsedTextPath = "TextOut.txt";
			String modifiedTextPath = "ModifiedText.txt";
			TextParser parser = new TextParser();
			String text = FileWorker.readTextFile(readPath);
			StringComposite composite = new StringComposite(
					StringCompositeType.TEXT);
			parser.parseText(composite, text);
			log.info("Parsing is finished");
			FileWorker.writeTextFile(composite.buildText(), parsedTextPath);
			log.info("Parsed text is written to file " + parsedTextPath);
			TextWorker.swapFirstAndLastWords(composite);
			TextWorker.replaceWords(composite, 2, 7, "ЗАМЕНА");
			log.info("Operations with text are finished");
			FileWorker.writeTextFile(composite.buildText(), modifiedTextPath);
			log.info("Modified text is written to file " + modifiedTextPath);
		} catch (TechnicalException e) {
			log.error(e);
		}
	}
}
