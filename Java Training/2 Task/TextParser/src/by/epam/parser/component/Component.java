package by.epam.parser.component;

import java.util.List;

import by.epam.parser.exception.TechnicalException;

public interface Component {
	String buildText();

	void add(Component component) throws TechnicalException;

	void remove(Component component) throws TechnicalException;

	List<Component> getComponents() throws TechnicalException;

	StringCompositeType getType();

	void setText(String text) throws TechnicalException;
}
