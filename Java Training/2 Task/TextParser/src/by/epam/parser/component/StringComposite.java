package by.epam.parser.component;

import java.util.ArrayList;
import java.util.List;

import by.epam.parser.exception.TechnicalException;

public class StringComposite implements Component {
	private ArrayList<Component> stringComponents;

	private StringCompositeType type;

	public StringComposite(StringCompositeType type) {
		stringComponents = new ArrayList<>();
		this.type = type;
	}

	@Override
	public String buildText() {
		StringBuilder sb = new StringBuilder();
		for (Component component : stringComponents) {
			sb.append(component.buildText());
		}
		return sb.toString();
	}

	@Override
	public void add(Component component) {
		stringComponents.add(component);
	}

	@Override
	public void remove(Component component) {
		stringComponents.remove(component);
	}

	@Override
	public List<Component> getComponents() {
		return stringComponents;
	}

	@Override
	public StringCompositeType getType() {
		return type;
	}

	public void setType(StringCompositeType type) {
		this.type = type;
	}

	@Override
	public void setText(String text) throws TechnicalException {
		throw new TechnicalException("Operation is not supported");
	}
}
