package by.epam.parser.component;

import java.util.List;

import by.epam.parser.exception.TechnicalException;

public class StringLeaf implements Component {
	private String text;
	private StringCompositeType type;

	public StringLeaf(String text, StringCompositeType type) {
		this.text = text;
		this.type = type;
	}

	@Override
	public String buildText() {
		return text;
	}

	@Override
	public void add(Component component) throws TechnicalException {
		throw new TechnicalException("Operation is not supported");
	}

	@Override
	public void remove(Component component) throws TechnicalException {
		throw new TechnicalException("Operation is not supported");
	}

	@Override
	public List<Component> getComponents() throws TechnicalException {
		throw new TechnicalException("Operation is not supported");
	}

	@Override
	public StringCompositeType getType() {
		return type;
	}

	public void setType(StringCompositeType type) {
		this.type = type;
	}

	@Override
	public void setText(String text) {
		this.text = text;
	}
}
