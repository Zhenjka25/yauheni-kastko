package by.epam.parser.component;

public enum StringCompositeType {
	TEXT("[\\s\\S]+"), 
	PARAGRAPH_WITH_LISTING("[\\s]{6}[A-ZА-Я][\\wА-Яа-я\\s,:;.?!-]+[.!?]\\n(\\n)*(\\/\\/#[^А-Яа-я]+\\/\\/##\\n)*"), 
	LISTING("\\/\\/#[^А-Яа-я]+\\/\\/##\\n"), 
	PARAGRAPH("[\\s]{6}[А-ЯA-Z][А-Яа-я\\w,:;.!?\\s-]+[.!?]\\n"), 
	SENTENCE("[А-Я\\d][\\wА-Яа-я\\s,:;-]+[.][\\s]"), 
	LEXEME_WITH_SPACE("[\\wА-Яа-я,;:.?!()-]+[\\s]"), 
	WORD("[\\wА-Яа-я]+"), 
	SYMBOL("[,:;.!?(){}<>=]"),
	SPACE("\\s");
	
	private String regexp;

	private StringCompositeType(String regexp) {
		this.regexp = regexp;
	}

	public String getRegexp() {
		return regexp;
	}

}
