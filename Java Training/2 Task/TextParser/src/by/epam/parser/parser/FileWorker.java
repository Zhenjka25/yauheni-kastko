package by.epam.parser.parser;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import by.epam.parser.exception.TechnicalException;

public class FileWorker {
	public static String readTextFile(String path) throws TechnicalException {
		StringBuilder sb = new StringBuilder();
		try (Scanner sc = new Scanner(new File(path))) {
			while (sc.hasNext()) {
				sb.append(sc.nextLine() + "\n");
			}
		} catch (IOException e) {
			throw new TechnicalException("Reading error");
		}
		return sb.toString();
	}

	public static void writeTextFile(String text, String path)
			throws TechnicalException {
		try (BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(path)))) {
			w.write(text);
		} catch (IOException e) {
			throw new TechnicalException("Writing error");
		}
	}
}
