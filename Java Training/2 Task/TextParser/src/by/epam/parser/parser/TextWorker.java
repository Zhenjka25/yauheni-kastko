package by.epam.parser.parser;

import java.util.ArrayList;
import java.util.List;

import by.epam.parser.component.Component;
import by.epam.parser.component.StringCompositeType;
import by.epam.parser.exception.TechnicalException;

public class TextWorker {
	public static void swapFirstAndLastWords(Component component)
			throws TechnicalException {
		List<Component> sentences = new ArrayList<>();
		buildAllSentences(component, sentences);
		for (Component sentence : sentences) {
			Component firstLexeme = sentence.getComponents().get(0);
			Component lastLexeme = sentence.getComponents().get(
					sentence.getComponents().size() - 1);
			String word = firstLexeme.getComponents().get(0).buildText();
			firstLexeme.getComponents().get(0)
					.setText(lastLexeme.getComponents().get(0).buildText());
			lastLexeme.getComponents().get(0).setText(word);
		}
	}

	public static void replaceWords(Component component, int numberOfSentence,
			int wordLength, String word) throws TechnicalException {
		List<Component> sentences = new ArrayList<>();
		buildAllSentences(component, sentences);
		if (sentences.size() > numberOfSentence - 1) {
			Component sentence = sentences.get(numberOfSentence - 1);
			for (Component lexeme : sentence.getComponents()) {
				if (lexeme.getComponents().get(0).buildText().length() == wordLength) {
					lexeme.getComponents().get(0).setText(word);
				}
			}
		} else {
			throw new TechnicalException(
					"Sentence by this number does not exist");
		}
	}

	public static void buildAllSentences(Component component,
			List<Component> sentences) throws TechnicalException {
		if (component.getType() != StringCompositeType.LISTING) {
			for (Component childComponent : component.getComponents()) {
				if (childComponent.getType() == StringCompositeType.SENTENCE) {
					sentences.add(childComponent);
				} else {
					buildAllSentences(childComponent, sentences);
				}
			}
		}
	}
}
