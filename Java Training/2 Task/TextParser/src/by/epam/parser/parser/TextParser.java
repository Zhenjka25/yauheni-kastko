package by.epam.parser.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import by.epam.parser.component.StringComposite;
import by.epam.parser.component.StringCompositeType;
import by.epam.parser.component.StringLeaf;

public class TextParser {
	public void parseText(StringComposite composite, String text) {
		parseToParagraphWithListing(composite, text);
	}

	public void parseToParagraphWithListing(StringComposite composite,
			String text) {
		Pattern p = Pattern.compile(StringCompositeType.PARAGRAPH_WITH_LISTING
				.getRegexp());
		Matcher m = p.matcher(text);
		while (m.find()) {
			StringComposite childComposite = new StringComposite(
					StringCompositeType.PARAGRAPH_WITH_LISTING);
			composite.add(childComposite);
			parseToParagraph(childComposite, m.group());
			parseToLeaf(childComposite, StringCompositeType.LISTING, m.group());
		}
	}

	public void parseToParagraph(StringComposite composite, String text) {
		Pattern p = Pattern.compile(StringCompositeType.PARAGRAPH.getRegexp());
		Matcher m = p.matcher(text);
		while (m.find()) {
			StringComposite childComposite = new StringComposite(
					StringCompositeType.PARAGRAPH);
			composite.add(childComposite);
			parseToSentence(childComposite, m.group());
		}
	}

	public void parseToSentence(StringComposite composite, String text) {
		Pattern p = Pattern.compile(StringCompositeType.SENTENCE.getRegexp());
		Matcher m = p.matcher(text);
		while (m.find()) {
			StringComposite childComposite = new StringComposite(
					StringCompositeType.SENTENCE);
			composite.add(childComposite);
			parseToLexeme(childComposite, m.group());
		}
	}

	public void parseToLexeme(StringComposite composite, String text) {
		Pattern p = Pattern.compile(StringCompositeType.LEXEME_WITH_SPACE
				.getRegexp());
		Matcher m = p.matcher(text);
		while (m.find()) {
			StringComposite childComposite = new StringComposite(
					StringCompositeType.LEXEME_WITH_SPACE);
			composite.add(childComposite);
			parseToLeaf(childComposite, StringCompositeType.WORD, m.group());
			parseToLeaf(childComposite, StringCompositeType.SYMBOL, m.group());
			parseToLeaf(childComposite, StringCompositeType.SPACE, m.group());
		}
	}

	public void parseToLeaf(StringComposite composite,
			StringCompositeType type, String text) {
		Pattern p = Pattern.compile(type.getRegexp());
		Matcher m = p.matcher(text);
		while (m.find()) {
			StringLeaf childComposite = new StringLeaf(m.group(), type);
			composite.add(childComposite);
		}
	}
}
