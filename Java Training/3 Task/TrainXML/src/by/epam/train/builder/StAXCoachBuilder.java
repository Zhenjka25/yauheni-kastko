package by.epam.train.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;
import by.epam.train.parser.CoachStAXParser;

// TODO: Auto-generated Javadoc
/**
 * The Class StAXCoachBuilder.
 */
public class StAXCoachBuilder extends AbstractCoachBuilder {
	
	/** The log. */
	private static Logger log = Logger.getLogger(StAXCoachBuilder.class);

	/* (non-Javadoc)
	 * @see by.epam.train.builder.AbstractCoachBuilder#buildCoaches(java.lang.String)
	 */
	@Override
	public void buildCoaches(String path) throws TechnicalException,
			LogicalException {
		try (FileInputStream inStream = new FileInputStream(new File(path))) {
			XMLStreamReader reader = XMLInputFactory.newInstance()
					.createXMLStreamReader(inStream);
			CoachStAXParser parser = new CoachStAXParser();
			log.info("StAX parsing started");
			parser.buildCoaches(reader);
			coaches = parser.getCoaches();
			log.info("StAX parsing finished");
		} catch (IOException | XMLStreamException | FactoryConfigurationError e) {
			throw new TechnicalException(e);
		}
	}
}
