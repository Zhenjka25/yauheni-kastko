package by.epam.train.builder;

import java.util.ArrayList;

import java.util.List;

import by.epam.train.entity.Coach;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractCoachBuilder.
 */
public abstract class AbstractCoachBuilder {
	
	/** The coaches. */
	protected ArrayList<Coach> coaches;

	/**
	 * Instantiates a new abstract coach builder.
	 */
	public AbstractCoachBuilder() {
		this.coaches = new ArrayList<>();
	}

	/**
	 * Gets the coaches.
	 *
	 * @return the coaches
	 */
	public List<Coach> getCoaches() {
		return coaches;
	}

	/**
	 * Builds the coaches.
	 *
	 * @param path the path
	 * @throws TechnicalException the technical exception
	 * @throws LogicalException the logical exception
	 */
	public abstract void buildCoaches(String path) throws TechnicalException,
			LogicalException;
}
