package by.epam.train.builder;

import java.io.File;

import org.apache.log4j.Logger;

import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;
import by.epam.train.parser.CoachDOMParser;

// TODO: Auto-generated Javadoc
/**
 * The Class DOMCoachBuilder.
 */
public class DOMCoachBuilder extends AbstractCoachBuilder {
	
	/** The log. */
	private static Logger log = Logger.getLogger(DOMCoachBuilder.class);
	
	/** The parser. */
	private CoachDOMParser parser = new CoachDOMParser();

	/* (non-Javadoc)
	 * @see by.epam.train.builder.AbstractCoachBuilder#buildCoaches(java.lang.String)
	 */
	@Override
	public void buildCoaches(String path) throws TechnicalException,
			LogicalException {
		File f = new File(path);
		log.info("DOM parsing started");
		parser.buildListCoaches(f);
		coaches = parser.getCoaches();
		log.info("DOM parsing finished");
	}

}
