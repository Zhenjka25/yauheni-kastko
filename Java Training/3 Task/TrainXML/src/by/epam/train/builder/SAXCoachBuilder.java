package by.epam.train.builder;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import by.epam.train.exception.TechnicalException;
import by.epam.train.parser.CoachHandler;

// TODO: Auto-generated Javadoc
/**
 * The Class SAXCoachBuilder.
 */
public class SAXCoachBuilder extends AbstractCoachBuilder {
	
	/** The log. */
	private static Logger log = Logger.getLogger(SAXCoachBuilder.class);
	
	/** The h sax. */
	private CoachHandler hSAX = new CoachHandler();
	
	/** The Constant JAXP_SCHEMA_LANGUAGE. */
	static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	
	/** The Constant W3C_XML_SCHEMA. */
	static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

	/* (non-Javadoc)
	 * @see by.epam.train.builder.AbstractCoachBuilder#buildCoaches(java.lang.String)
	 */
	@Override
	public void buildCoaches(String path) throws TechnicalException {
		try {
			SAXParserFactory fact = SAXParserFactory.newInstance();
			fact.setNamespaceAware(true);
			fact.setValidating(true);
			SAXParser saxParser = fact.newSAXParser();
			saxParser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			File f = new File(path);
			log.info("SAX parsing started");
			saxParser.parse(f, hSAX);
			coaches = hSAX.getCoaches();
			log.info("SAX parsing finished");
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw new TechnicalException(e);
		}
	}
}
