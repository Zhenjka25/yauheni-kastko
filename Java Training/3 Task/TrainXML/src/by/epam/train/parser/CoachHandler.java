package by.epam.train.parser;

import java.util.ArrayList;
import java.util.EnumSet;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.train.entity.Coach;
import by.epam.train.entity.CoachEnum;
import by.epam.train.entity.CompartmentCar;
import by.epam.train.entity.SedentaryCar;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

// TODO: Auto-generated Javadoc
/**
 * The Class CoachHandler.
 */
public class CoachHandler extends DefaultHandler {
	
	/** The log. */
	private static Logger log = Logger.getLogger(CoachHandler.class);
	
	/** The wear rate. */
	private int comfortLevel, amountPassengers, amountBaggage, wearRate;
	
	/** The compartment car. */
	private CompartmentCar compartmentCar;
	
	/** The sedentary car. */
	private SedentaryCar sedentaryCar;
	
	/** The coach enum. */
	private CoachEnum coachEnum;
	
	/** The coaches. */
	private ArrayList<Coach> coaches;
	
	/** The with text. */
	private EnumSet<CoachEnum> withText;

	/**
	 * Instantiates a new coach handler.
	 */
	public CoachHandler() {
		coaches = new ArrayList<>();
		withText = EnumSet.range(CoachEnum.COMFORT_LEVEL,
				CoachEnum.PRESENCE_OF_TV);
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	@Override
	public void startElement(String uri, String name, String qname,
			Attributes attrs) {
		switch (name) {
		case "compartment-car":
			compartmentCar = new CompartmentCar();
			compartmentCar.setId(attrs.getValue("id"));
			break;
		case "sedentary-car":
			sedentaryCar = new SedentaryCar();
			sedentaryCar.setId(attrs.getValue("id"));
		default:
			CoachEnum temp = CoachEnum.valueOf(name.replace("-", "_")
					.toUpperCase());
			if (withText.contains(temp)) {
				coachEnum = temp;
			}
			break;
		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	@Override
	public void characters(char[] buf, int offset, int len) {
		String s = new String(buf, offset, len).trim();
		try {
			if (coachEnum != null) {
				switch (coachEnum) {
				case COMFORT_LEVEL:
					comfortLevel = Integer.parseInt(s);
					break;
				case AMOUNT_PASSENGERS:
					amountPassengers = Integer.parseInt(s);
					break;
				case AMOUNT_BAGGAGE:
					amountBaggage = Integer.parseInt(s);
					break;
				case WEAR_RATE:
					wearRate = Integer.parseInt(s);
					break;
				case SLEEPING_BERTH:
					compartmentCar.setSleepingBerthtype(s);
					break;
				case AMOUNT_COMPARTMENT:
					compartmentCar.setAmountCompartment(Integer.parseInt(s));
					break;
				case PRESENCE_OF_ROSETTE:
					compartmentCar
							.setPresenceOfRosette(Boolean.parseBoolean(s));
					break;
				case CONDITIONER:
					compartmentCar.setConditionerType(s);
					break;
				case LINENS:
					compartmentCar.setLinensType(s);
					break;
				case SEAT:
					sedentaryCar.setSeatType(s);
					break;
				case PRESENCE_OF_CONDITIONER:
					sedentaryCar.setPresenceOfConditioner(Boolean
							.parseBoolean(s));
					break;
				case PRESENCE_OF_TV:
					sedentaryCar.setPresenceOfTV(Boolean.parseBoolean(s));
					break;
				default:
					throw new TechnicalException("Such element does not exist");
				}
			}
		} catch (LogicalException | TechnicalException e) {
			log.error(e);
		}
	}

	/* (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void endElement(String uri, String name, String qname) {
		try {
			switch (name) {
			case "compartment-car":
				compartmentCar.setComfortLevel(comfortLevel);
				compartmentCar.setAmountPassengers(amountPassengers);
				compartmentCar.setAmountBaggage(amountBaggage);
				compartmentCar.setWearRate(wearRate);
				coaches.add(compartmentCar);
				compartmentCar = null;
				break;
			case "sedentary-car":
				sedentaryCar.setComfortLevel(comfortLevel);
				sedentaryCar.setAmountPassengers(amountPassengers);
				sedentaryCar.setAmountBaggage(amountBaggage);
				sedentaryCar.setWearRate(wearRate);
				coaches.add(sedentaryCar);
				sedentaryCar = null;
			default:
				break;
			}
		} catch (LogicalException e) {
			log.error(e);
		}
	}

	/**
	 * Gets the coaches.
	 *
	 * @return the coaches
	 */
	public ArrayList<Coach> getCoaches() {
		return coaches;
	}
}
