package by.epam.train.parser;

import java.util.ArrayList;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.epam.train.entity.Coach;
import by.epam.train.entity.CoachEnum;
import by.epam.train.entity.CompartmentCar;
import by.epam.train.entity.SedentaryCar;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

// TODO: Auto-generated Javadoc
/**
 * The Class CoachStAXParser.
 */
public class CoachStAXParser implements XMLStreamConstants {
	
	/** The compartment car. */
	private CompartmentCar compartmentCar;
	
	/** The sedentary car. */
	private SedentaryCar sedentaryCar;
	
	/** The coaches. */
	private ArrayList<Coach> coaches = new ArrayList<>();

	/**
	 * Builds the coaches.
	 *
	 * @param reader the reader
	 * @throws LogicalException the logical exception
	 * @throws TechnicalException the technical exception
	 */
	public void buildCoaches(XMLStreamReader reader) throws LogicalException,
			TechnicalException {
		try {
			String name;
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					switch (CoachEnum.valueOf(name.replace("-", "_")
							.toUpperCase())) {
					case COMPARTMENT_CAR:
						compartmentCar = buildCompartmentCar(reader);
						coaches.add(compartmentCar);
						break;
					case SEDENTARY_CAR:
						sedentaryCar = buildSedentaryCar(reader);
						coaches.add(sedentaryCar);
					default:
						break;
					}
				}
			}
		} catch (XMLStreamException e) {
			throw new TechnicalException(e);
		}
	}

	/**
	 * Builds the compartment car.
	 *
	 * @param reader the reader
	 * @return the compartment car
	 * @throws LogicalException the logical exception
	 * @throws TechnicalException the technical exception
	 * @throws XMLStreamException the XML stream exception
	 */
	private CompartmentCar buildCompartmentCar(XMLStreamReader reader)
			throws LogicalException, TechnicalException, XMLStreamException {
		CompartmentCar compartmentCar = new CompartmentCar();
		compartmentCar.setId(reader.getAttributeValue(null, "id"));
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (CoachEnum.valueOf(name.replace("-", "_").toUpperCase())) {
				case COMFORT_LEVEL:
					compartmentCar.setComfortLevel(Integer
							.parseInt(getXMLText(reader)));
					break;
				case AMOUNT_PASSENGERS:
					compartmentCar.setAmountPassengers(Integer
							.parseInt(getXMLText(reader)));
					break;
				case AMOUNT_BAGGAGE:
					compartmentCar.setAmountBaggage(Integer
							.parseInt(getXMLText(reader)));
					break;
				case WEAR_RATE:
					compartmentCar.setWearRate(Integer
							.parseInt(getXMLText(reader)));
					break;
				case SLEEPING_BERTH:
					compartmentCar.setSleepingBerthtype(getXMLText(reader));
					break;
				case AMOUNT_COMPARTMENT:
					compartmentCar.setAmountCompartment(Integer
							.parseInt(getXMLText(reader)));
					break;
				case PRESENCE_OF_ROSETTE:
					compartmentCar.setPresenceOfRosette(Boolean
							.parseBoolean(getXMLText(reader)));
					break;
				case CONDITIONER:
					compartmentCar.setConditionerType(getXMLText(reader));
					break;
				case LINENS:
					compartmentCar.setLinensType(getXMLText(reader));
					break;
				default:
					throw new TechnicalException("Such element does not exist");
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (CoachEnum.valueOf(name.replace("-", "_").toUpperCase()) == CoachEnum.COMPARTMENT_CAR) {
					return compartmentCar;
				}
			}
		}
		return compartmentCar;
	}

	/**
	 * Builds the sedentary car.
	 *
	 * @param reader the reader
	 * @return the sedentary car
	 * @throws LogicalException the logical exception
	 * @throws TechnicalException the technical exception
	 * @throws XMLStreamException the XML stream exception
	 */
	private SedentaryCar buildSedentaryCar(XMLStreamReader reader)
			throws LogicalException, TechnicalException, XMLStreamException {
		SedentaryCar sedentaryCar = new SedentaryCar();
		sedentaryCar.setId(reader.getAttributeValue(null, "id"));
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (CoachEnum.valueOf(name.replace("-", "_").toUpperCase())) {
				case COMFORT_LEVEL:
					sedentaryCar.setComfortLevel(Integer
							.parseInt(getXMLText(reader)));
					break;
				case AMOUNT_PASSENGERS:
					sedentaryCar.setAmountPassengers(Integer
							.parseInt(getXMLText(reader)));
					break;
				case AMOUNT_BAGGAGE:
					sedentaryCar.setAmountBaggage(Integer
							.parseInt(getXMLText(reader)));
					break;
				case WEAR_RATE:
					sedentaryCar.setWearRate(Integer
							.parseInt(getXMLText(reader)));
					break;
				case SEAT:
					sedentaryCar.setSeatType(getXMLText(reader));
					break;
				case PRESENCE_OF_CONDITIONER:
					sedentaryCar.setPresenceOfConditioner(Boolean
							.parseBoolean(getXMLText(reader)));
					break;
				case PRESENCE_OF_TV:
					sedentaryCar.setPresenceOfTV(Boolean
							.parseBoolean(getXMLText(reader)));
					break;
				default:
					throw new TechnicalException("Such element does not exist");
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (CoachEnum.valueOf(name.replace("-", "_").toUpperCase()) == CoachEnum.SEDENTARY_CAR) {
					return sedentaryCar;
				}
			}
		}
		return sedentaryCar;
	}

	/**
	 * Gets the XML text.
	 *
	 * @param reader the reader
	 * @return the XML text
	 * @throws XMLStreamException the XML stream exception
	 */
	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}
		return text;
	}

	/**
	 * Gets the coaches.
	 *
	 * @return the coaches
	 */
	public ArrayList<Coach> getCoaches() {
		return coaches;
	}

}
