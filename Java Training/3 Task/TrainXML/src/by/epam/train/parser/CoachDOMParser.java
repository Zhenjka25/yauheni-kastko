package by.epam.train.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.train.entity.Coach;
import by.epam.train.entity.CompartmentCar;
import by.epam.train.entity.SedentaryCar;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;

// TODO: Auto-generated Javadoc
/**
 * The Class CoachDOMParser.
 */
public class CoachDOMParser {
	
	/** The coaches. */
	private ArrayList<Coach> coaches = new ArrayList<>();
	
	/** The document builder. */
	private DocumentBuilder documentBuilder;

	/**
	 * Inits the document builder.
	 *
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void initDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		documentBuilder = factory.newDocumentBuilder();
	}

	/**
	 * Gets the coaches.
	 *
	 * @return the coaches
	 */
	public ArrayList<Coach> getCoaches() {
		return coaches;
	}

	/**
	 * Builds the list coaches.
	 *
	 * @param f the f
	 * @throws LogicalException the logical exception
	 * @throws TechnicalException the technical exception
	 */
	public void buildListCoaches(File f) throws LogicalException,
			TechnicalException {
		try {
			initDocumentBuilder();
			Document document = documentBuilder.parse(f);
			Element root = document.getDocumentElement();
			NodeList compartmentCarList = root
					.getElementsByTagName("compartment-car");
			NodeList sedentaryCarList = root
					.getElementsByTagName("sedentary-car");
			for (int i = 0; i < compartmentCarList.getLength(); i++) {
				Element compartmentCarElement = (Element) compartmentCarList
						.item(i);
				CompartmentCar compartmentCar = buildCompartmentCar(compartmentCarElement);
				coaches.add(compartmentCar);
			}
			for (int i = 0; i < sedentaryCarList.getLength(); i++) {
				Element sedentaryCarElement = (Element) sedentaryCarList
						.item(i);
				SedentaryCar sedentaryCar = buildSedentaryCar(sedentaryCarElement);
				coaches.add(sedentaryCar);
			}
		} catch (IOException | SAXException | ParserConfigurationException e) {
			throw new TechnicalException(e);
		}
	}

	/**
	 * Builds the compartment car.
	 *
	 * @param compartmentCarElement the compartment car element
	 * @return the compartment car
	 * @throws LogicalException the logical exception
	 * @throws TechnicalException the technical exception
	 */
	private CompartmentCar buildCompartmentCar(Element compartmentCarElement)
			throws LogicalException, TechnicalException {
		CompartmentCar compartmentCar = new CompartmentCar();
		compartmentCar.setId(compartmentCarElement.getAttribute("id"));
		int comfortLevel = Integer.parseInt(getElementTextContent(
				compartmentCarElement, "comfort-level"));
		compartmentCar.setComfortLevel(comfortLevel);
		int amountPassengers = Integer.parseInt(getElementTextContent(
				compartmentCarElement, "amount-passengers"));
		compartmentCar.setAmountPassengers(amountPassengers);
		int amountBaggage = Integer.parseInt(getElementTextContent(
				compartmentCarElement, "amount-baggage"));
		compartmentCar.setAmountBaggage(amountBaggage);
		int wearRate = Integer.parseInt(getElementTextContent(
				compartmentCarElement, "wear-rate"));
		compartmentCar.setWearRate(wearRate);
		compartmentCar.setSleepingBerthtype(getElementTextContent(
				compartmentCarElement, "sleeping-berth"));
		int amountCompartment = Integer.parseInt(getElementTextContent(
				compartmentCarElement, "amount-compartment"));
		compartmentCar.setAmountCompartment(amountCompartment);
		boolean presenceOfRosette = Boolean.parseBoolean(getElementTextContent(
				compartmentCarElement, "presence-of-rosette"));
		compartmentCar.setPresenceOfRosette(presenceOfRosette);
		compartmentCar.setConditionerType(getElementTextContent(
				compartmentCarElement, "conditioner"));
		compartmentCar.setLinensType(getElementTextContent(
				compartmentCarElement, "linens"));
		return compartmentCar;
	}

	/**
	 * Builds the sedentary car.
	 *
	 * @param sedentaryCarElement the sedentary car element
	 * @return the sedentary car
	 * @throws LogicalException the logical exception
	 * @throws TechnicalException the technical exception
	 */
	private SedentaryCar buildSedentaryCar(Element sedentaryCarElement)
			throws LogicalException, TechnicalException {
		SedentaryCar sedentaryCar = new SedentaryCar();
		sedentaryCar.setId(sedentaryCarElement.getAttribute("id"));
		int comfortLevel = Integer.parseInt(getElementTextContent(
				sedentaryCarElement, "comfort-level"));
		sedentaryCar.setComfortLevel(comfortLevel);
		int amountPassengers = Integer.parseInt(getElementTextContent(
				sedentaryCarElement, "amount-passengers"));
		sedentaryCar.setAmountPassengers(amountPassengers);
		int amountBaggage = Integer.parseInt(getElementTextContent(
				sedentaryCarElement, "amount-baggage"));
		sedentaryCar.setAmountBaggage(amountBaggage);
		int wearRate = Integer.parseInt(getElementTextContent(
				sedentaryCarElement, "wear-rate"));
		sedentaryCar.setWearRate(wearRate);
		sedentaryCar.setSeatType(getElementTextContent(sedentaryCarElement,
				"seat"));
		boolean presenceOfConditioner = Boolean
				.parseBoolean(getElementTextContent(sedentaryCarElement,
						"presence-of-conditioner"));
		sedentaryCar.setPresenceOfConditioner(presenceOfConditioner);
		boolean presenceOfTV = Boolean.parseBoolean(getElementTextContent(
				sedentaryCarElement, "presence-of-TV"));
		sedentaryCar.setPresenceOfTV(presenceOfTV);
		return sedentaryCar;
	}

	/**
	 * Gets the element text content.
	 *
	 * @param element the element
	 * @param elementName the element name
	 * @return the element text content
	 * @throws TechnicalException the technical exception
	 */
	private String getElementTextContent(Element element, String elementName)
			throws TechnicalException {
		String text;
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		if (node != null) {
			text = node.getTextContent();
		} else {
			throw new TechnicalException("Element does not exist");
		}
		return text;
	}
}
