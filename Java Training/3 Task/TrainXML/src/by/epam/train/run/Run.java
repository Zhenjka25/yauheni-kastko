package by.epam.train.run;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import by.epam.train.builder.AbstractCoachBuilder;
import by.epam.train.exception.LogicalException;
import by.epam.train.exception.TechnicalException;
import by.epam.train.factory.CoachBuilderFactory;
import by.epam.train.reporter.Reporter;
import by.epam.train.reporter.WriterToFile;

// TODO: Auto-generated Javadoc
/**
 * The Class Run.
 */
public class Run {
	
	/** The log. */
	private static Logger log = Logger.getLogger(Run.class);
	static {
		PropertyConfigurator.configure("resources/log4j.properties");
	}
	
	/** The builder. */
	private static AbstractCoachBuilder builder;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		try {
			if (args.length >= 1) {
				String XMLPath = "Coaches.xml";
				String coachesInformationPath = "Coaches.txt";
				String coachesInformation;
				CoachBuilderFactory factory = CoachBuilderFactory.getInstance();
				builder = factory.createCoachBuilder(args[0]);
				builder.buildCoaches(XMLPath);
				coachesInformation = Reporter.buildCoachesInformation(builder
						.getCoaches());
				WriterToFile.writeToFile(coachesInformation,
						coachesInformationPath);
				log.info("information is recorded in the file "
						+ coachesInformationPath);
			} else {
				throw new TechnicalException("must be specified arguments");
			}
		} catch (TechnicalException | LogicalException e) {
			log.error(e);
		}
	}
}
