package by.epam.train.reporter;

import java.util.List;

import by.epam.train.entity.Coach;

// TODO: Auto-generated Javadoc
/**
 * The Class Reporter.
 */
public class Reporter {
	
	/**
	 * Builds the coaches information.
	 *
	 * @param coaches the coaches
	 * @return the string
	 */
	public static String buildCoachesInformation(List<Coach> coaches) {
		StringBuilder information = new StringBuilder();
		for (Coach coach : coaches) {
			information.append(coach.toString() + "\n");
		}
		return information.toString();
	}
}
