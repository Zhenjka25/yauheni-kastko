package by.epam.train.factory;

import by.epam.train.builder.AbstractCoachBuilder;
import by.epam.train.builder.DOMCoachBuilder;
import by.epam.train.builder.SAXCoachBuilder;
import by.epam.train.builder.StAXCoachBuilder;
import by.epam.train.exception.TechnicalException;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating CoachBuilder objects.
 */
public class CoachBuilderFactory {
	
	/** The instance. */
	private static CoachBuilderFactory instance = null;
	
	/** The instance created. */
	private volatile static boolean instanceCreated = false;

	/**
	 * The Enum TypeParser.
	 */
	private enum TypeParser {
		
		/** The sax. */
		SAX, 
 /** The stax. */
 STAX, 
 /** The dom. */
 DOM
	}

	/**
	 * Instantiates a new coach builder factory.
	 */
	private CoachBuilderFactory() {
	}

	/**
	 * Gets the single instance of CoachBuilderFactory.
	 *
	 * @return single instance of CoachBuilderFactory
	 */
	public static CoachBuilderFactory getInstance() {
		if (!instanceCreated) {
			synchronized (CoachBuilderFactory.class) {
				try {
					if (!instanceCreated) {
						instance = new CoachBuilderFactory();
						instanceCreated = true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return instance;
	}

	/**
	 * Creates a new CoachBuilder object.
	 *
	 * @param typeParser the type parser
	 * @return the abstract coach builder
	 * @throws TechnicalException the technical exception
	 */
	public AbstractCoachBuilder createCoachBuilder(String typeParser)
			throws TechnicalException {
		try {
			TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
			switch (type) {
			case DOM:
				return new DOMCoachBuilder();
			case STAX:
				return new StAXCoachBuilder();
			case SAX:
				return new SAXCoachBuilder();
			default:
				throw new TechnicalException(
						"such type of parser does not exist");
			}
		} catch (IllegalArgumentException e) {
			throw new TechnicalException(e);
		}
	}
}
