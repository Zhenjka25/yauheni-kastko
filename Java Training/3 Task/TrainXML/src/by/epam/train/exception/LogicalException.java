package by.epam.train.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class LogicalException.
 */
public class LogicalException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new logical exception.
	 */
	public LogicalException() {
	}

	/**
	 * Instantiates a new logical exception.
	 *
	 * @param message the message
	 * @param cause the cause
	 */
	public LogicalException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Instantiates a new logical exception.
	 *
	 * @param message the message
	 */
	public LogicalException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new logical exception.
	 *
	 * @param cause the cause
	 */
	public LogicalException(Throwable cause) {
		super(cause);
	}
}
