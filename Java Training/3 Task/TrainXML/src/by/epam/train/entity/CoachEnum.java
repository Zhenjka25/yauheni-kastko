package by.epam.train.entity;

// TODO: Auto-generated Javadoc
/**
 * The Enum CoachEnum.
 */
public enum CoachEnum {
	
	/** The coaches. */
	COACHES, 
	
	/** The compartment car. */
	COMPARTMENT_CAR, 
	
	/** The sedentary car. */
	SEDENTARY_CAR, 
	
	/** The comfort level. */
	COMFORT_LEVEL, 
	
	/** The amount passengers. */
	AMOUNT_PASSENGERS, 
	
	/** The amount baggage. */
	AMOUNT_BAGGAGE, 
	
	/** The wear rate. */
	WEAR_RATE,
	
	/** The sleeping berth. */
	SLEEPING_BERTH,
	
	/** The amount compartment. */
	AMOUNT_COMPARTMENT,
	
	/** The presence of rosette. */
	PRESENCE_OF_ROSETTE,
	
	/** The conditioner. */
	CONDITIONER,
	
	/** The linens. */
	LINENS,
	
	/** The seat. */
	SEAT,
	
	/** The presence of conditioner. */
	PRESENCE_OF_CONDITIONER,
	
	/** The presence of tv. */
	PRESENCE_OF_TV;
}
