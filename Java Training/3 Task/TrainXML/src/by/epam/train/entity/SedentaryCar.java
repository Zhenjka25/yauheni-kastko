package by.epam.train.entity;

// TODO: Auto-generated Javadoc
/**
 * The Class SedentaryCar.
 */
public class SedentaryCar extends Coach {
	
	/** The seat type. */
	private String seatType;
	
	/** The presence of conditioner. */
	private boolean presenceOfConditioner;
	
	/** The presence of tv. */
	private boolean presenceOfTV;

	/**
	 * Gets the seat type.
	 *
	 * @return the seat type
	 */
	public String getSeatType() {
		return seatType;
	}

	/**
	 * Sets the seat type.
	 *
	 * @param seatType the new seat type
	 */
	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	/**
	 * Checks if is presence of conditioner.
	 *
	 * @return true, if is presence of conditioner
	 */
	public boolean isPresenceOfConditioner() {
		return presenceOfConditioner;
	}

	/**
	 * Sets the presence of conditioner.
	 *
	 * @param presenceOfConditioner the new presence of conditioner
	 */
	public void setPresenceOfConditioner(boolean presenceOfConditioner) {
		this.presenceOfConditioner = presenceOfConditioner;
	}

	/**
	 * Checks if is presence of tv.
	 *
	 * @return true, if is presence of tv
	 */
	public boolean isPresenceOfTV() {
		return presenceOfTV;
	}

	/**
	 * Sets the presence of tv.
	 *
	 * @param presenceOfTV the new presence of tv
	 */
	public void setPresenceOfTV(boolean presenceOfTV) {
		this.presenceOfTV = presenceOfTV;
	}

	/* (non-Javadoc)
	 * @see by.epam.train.entity.Coach#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (presenceOfConditioner ? 1231 : 1237);
		result = prime * result + (presenceOfTV ? 1231 : 1237);
		result = prime * result
				+ ((seatType == null) ? 0 : seatType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see by.epam.train.entity.Coach#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SedentaryCar other = (SedentaryCar) obj;
		if (presenceOfConditioner != other.presenceOfConditioner)
			return false;
		if (presenceOfTV != other.presenceOfTV)
			return false;
		if (seatType == null) {
			if (other.seatType != null)
				return false;
		} else if (!seatType.equals(other.seatType))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see by.epam.train.entity.Coach#toString()
	 */
	@Override
	public String toString() {
		StringBuilder information = new StringBuilder();
		information.append("Sedentary car:" + "\n");
		information.append(super.toString());
		information.append("seat type: " + seatType + "\n");
		information.append("presence of conditioner: " + presenceOfConditioner
				+ "\n");
		information.append("presence of TV: " + presenceOfTV + "\n");
		return information.toString();
	}

}
