package by.epam.train.entity;

import by.epam.train.exception.LogicalException;

// TODO: Auto-generated Javadoc
/**
 * The Class Coach.
 */
public class Coach {
	
	/** The id. */
	private String id;
	
	/** The comfort level. */
	private int comfortLevel;
	
	/** The amount passengers. */
	private int amountPassengers;
	
	/** The amount baggage. */
	private int amountBaggage;
	
	/** The wear rate. */
	private int wearRate;
	
	/** The Constant COMFORT_LEVEL_MIN. */
	public final static int COMFORT_LEVEL_MIN = 1;
	
	/** The Constant COMFORT_LEVEL_MAX. */
	public final static int COMFORT_LEVEL_MAX = 10;
	
	/** The Constant AMOUNT_PASSENGERS_MAX. */
	public final static int AMOUNT_PASSENGERS_MAX = 81;
	
	/** The Constant AMOUNT_BAGGAGE_MAX. */
	public final static int AMOUNT_BAGGAGE_MAX = 100;
	
	/** The Constant WEAR_RATE_MAX. */
	public final static int WEAR_RATE_MAX = 10;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the comfort level.
	 *
	 * @return the comfort level
	 */
	public int getComfortLevel() {
		return comfortLevel;
	}

	/**
	 * Sets the comfort level.
	 *
	 * @param comfortLevel the new comfort level
	 * @throws LogicalException the logical exception
	 */
	public void setComfortLevel(int comfortLevel) throws LogicalException {
		if ((comfortLevel >= COMFORT_LEVEL_MIN)
				&& (comfortLevel <= COMFORT_LEVEL_MAX)) {
			this.comfortLevel = comfortLevel;
		} else {
			throw new LogicalException(
					"Value of comfortLevel must be between 1 to 10");
		}
	}

	/**
	 * Gets the amount passengers.
	 *
	 * @return the amount passengers
	 */
	public int getAmountPassengers() {
		return amountPassengers;
	}

	/**
	 * Sets the amount passengers.
	 *
	 * @param amountPassengers the new amount passengers
	 * @throws LogicalException the logical exception
	 */
	public void setAmountPassengers(int amountPassengers)
			throws LogicalException {
		if ((amountPassengers >= 0)
				&& (amountPassengers <= AMOUNT_PASSENGERS_MAX)) {
			this.amountPassengers = amountPassengers;
		} else {
			throw new LogicalException(
					"Value of amountPassengers must be between 0 to 81");
		}
	}

	/**
	 * Gets the amount baggage.
	 *
	 * @return the amount baggage
	 */
	public int getAmountBaggage() {
		return amountBaggage;
	}

	/**
	 * Sets the amount baggage.
	 *
	 * @param amountBaggage the new amount baggage
	 * @throws LogicalException the logical exception
	 */
	public void setAmountBaggage(int amountBaggage) throws LogicalException {
		if ((amountBaggage >= 0) && (amountBaggage <= AMOUNT_BAGGAGE_MAX)) {
			this.amountBaggage = amountBaggage;
		} else {
			throw new LogicalException(
					"Value of amountBaggage must be between 0 to 100");
		}
	}

	/**
	 * Gets the wear rate.
	 *
	 * @return the wear rate
	 */
	public int getWearRate() {
		return wearRate;
	}

	/**
	 * Sets the wear rate.
	 *
	 * @param wearRate the new wear rate
	 * @throws LogicalException the logical exception
	 */
	public void setWearRate(int wearRate) throws LogicalException {
		if ((wearRate >= 0) && (wearRate <= WEAR_RATE_MAX)) {
			this.wearRate = wearRate;
		} else {
			throw new LogicalException(
					"Value of wearRate must be between 0 to 10");
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amountBaggage;
		result = prime * result + amountPassengers;
		result = prime * result + comfortLevel;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + wearRate;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coach other = (Coach) obj;
		if (amountBaggage != other.amountBaggage)
			return false;
		if (amountPassengers != other.amountPassengers)
			return false;
		if (comfortLevel != other.comfortLevel)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (wearRate != other.wearRate)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder information = new StringBuilder();
		information.append("id: " + id + "\n");
		information.append("comfort level: " + comfortLevel + "\n");
		information.append("amount of passengers: " + amountPassengers + "\n");
		information.append("amount of baggage: " + amountBaggage + "\n");
		information.append("wear rate: " + wearRate + "\n");
		return information.toString();
	}

}
