package by.epam.train.entity;

import by.epam.train.exception.LogicalException;

// TODO: Auto-generated Javadoc
/**
 * The Class CompartmentCar.
 */
public class CompartmentCar extends Coach {
	
	/** The sleeping berth type. */
	private String sleepingBerthType;
	
	/** The amount compartment. */
	private int amountCompartment;
	
	/** The presence of rosette. */
	private boolean presenceOfRosette;
	
	/** The conditioner type. */
	private String conditionerType;
	
	/** The linens type. */
	private String linensType;
	
	/** The Constant AMOUNT_COMPARTMENT_MIN. */
	public final static int AMOUNT_COMPARTMENT_MIN = 6;
	
	/** The Constant AMOUNT_COMPARTMENT_MAX. */
	public final static int AMOUNT_COMPARTMENT_MAX = 10;

	/**
	 * Gets the sleeping berthtype.
	 *
	 * @return the sleeping berthtype
	 */
	public String getSleepingBerthtype() {
		return sleepingBerthType;
	}

	/**
	 * Sets the sleeping berthtype.
	 *
	 * @param sleepingBerthType the new sleeping berthtype
	 */
	public void setSleepingBerthtype(String sleepingBerthType) {
		this.sleepingBerthType = sleepingBerthType;
	}

	/**
	 * Gets the amount compartment.
	 *
	 * @return the amount compartment
	 */
	public int getAmountCompartment() {
		return amountCompartment;
	}

	/**
	 * Sets the amount compartment.
	 *
	 * @param amountCompartment the new amount compartment
	 * @throws LogicalException the logical exception
	 */
	public void setAmountCompartment(int amountCompartment)
			throws LogicalException {
		if (amountCompartment >= AMOUNT_COMPARTMENT_MIN
				&& amountCompartment <= AMOUNT_COMPARTMENT_MAX) {
			this.amountCompartment = amountCompartment;
		} else {
			throw new LogicalException(
					"Value of comfortLevel must be between 6 to 10");
		}
	}

	/**
	 * Checks if is presence of rosette.
	 *
	 * @return true, if is presence of rosette
	 */
	public boolean isPresenceOfRosette() {
		return presenceOfRosette;
	}

	/**
	 * Sets the presence of rosette.
	 *
	 * @param presenceOfRosette the new presence of rosette
	 */
	public void setPresenceOfRosette(boolean presenceOfRosette) {
		this.presenceOfRosette = presenceOfRosette;
	}

	/**
	 * Gets the conditioner type.
	 *
	 * @return the conditioner type
	 */
	public String getConditionerType() {
		return conditionerType;
	}

	/**
	 * Sets the conditioner type.
	 *
	 * @param conditionerType the new conditioner type
	 */
	public void setConditionerType(String conditionerType) {
		this.conditionerType = conditionerType;
	}

	/**
	 * Gets the linens type.
	 *
	 * @return the linens type
	 */
	public String getLinensType() {
		return linensType;
	}

	/**
	 * Sets the linens type.
	 *
	 * @param linensType the new linens type
	 */
	public void setLinensType(String linensType) {
		this.linensType = linensType;
	}

	/* (non-Javadoc)
	 * @see by.epam.train.entity.Coach#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + amountCompartment;
		result = prime * result
				+ ((conditionerType == null) ? 0 : conditionerType.hashCode());
		result = prime * result
				+ ((linensType == null) ? 0 : linensType.hashCode());
		result = prime * result + (presenceOfRosette ? 1231 : 1237);
		result = prime
				* result
				+ ((sleepingBerthType == null) ? 0 : sleepingBerthType
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see by.epam.train.entity.Coach#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompartmentCar other = (CompartmentCar) obj;
		if (amountCompartment != other.amountCompartment)
			return false;
		if (conditionerType == null) {
			if (other.conditionerType != null)
				return false;
		} else if (!conditionerType.equals(other.conditionerType))
			return false;
		if (linensType == null) {
			if (other.linensType != null)
				return false;
		} else if (!linensType.equals(other.linensType))
			return false;
		if (presenceOfRosette != other.presenceOfRosette)
			return false;
		if (sleepingBerthType == null) {
			if (other.sleepingBerthType != null)
				return false;
		} else if (!sleepingBerthType.equals(other.sleepingBerthType))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see by.epam.train.entity.Coach#toString()
	 */
	@Override
	public String toString() {
		StringBuilder information = new StringBuilder();
		information.append("Compartment car:" + "\n");
		information.append(super.toString());
		information.append("sleeping berth type: " + sleepingBerthType + "\n");
		information
				.append("amount of compartment: " + amountCompartment + "\n");
		information.append("presence of rosette: " + presenceOfRosette + "\n");
		information.append("conditioner type: " + conditionerType + "\n");
		information.append("linens type: " + linensType + "\n");
		return information.toString();
	}

}
